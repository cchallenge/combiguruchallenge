﻿using System;
using EduApp.Core;
using WindowsAzure.Messaging;
using Java.Net;
using System.Linq;
using Android.App;
using System.Threading.Tasks;
using Nostromo;
using EduApp.Droid;
using Akavache;
using System.Reactive.Linq;

namespace EduApp.Droid
{
	[Dependency]
	public class NotificationService_Droid : INotificationService
	{
		
		string StringConnection = ConnectionString.CreateUsingSharedAccessKeyWithListenAccess (new URI (Globals.EndPoint), Globals.SharedAccessKey);

		NotificationHub Hub { get; set; }
		string DeviceToken { get; set; }

		string [] _tags;
		public string [] Tags {
			get { return _tags; }
		}

		public NotificationService_Droid ()
		{
			Hub = new NotificationHub (Globals.NotificationHubPath, StringConnection, Application.Context);
		}

		public void RegisterForRemoteNotificationsWithTags (string [] tags)
		{
			_tags = tags;
			Task.Run(() => {
				try {
					Hub.Unregister ();
				} catch (Exception ex) {
					Console.WriteLine (ex.Message);
				}
				try {
					Hub.Register (DeviceToken, tags);
					SaveCorrId (tags [0]);
				} catch (Exception exc) {
					Console.WriteLine (exc.Message);
				}
			});
		}

		public void SetDeviceToken (object token)
		{
			this.DeviceToken = token.ToString();
		}

		public void UnRegisterForRemote ()
		{
			Task.Run (async() => {
				try {
					Hub.Unregister();
					await SaveCorrId ("NoCorrId");
				} catch (Exception ex) {
					Console.WriteLine (ex.Message);
				}
			});
		}

		async Task SaveCorrId (string corrId)
		{
			await BlobCache.UserAccount.InsertObject (Globals.CurrentCorrId, corrId);
		}
	}
}
