﻿using System;
using System.Threading.Tasks;
using Android.Media;
using Android.Runtime;
using EduApp.Core;
using Java.IO;
using Xamarin.Forms;

namespace EduApp.Droid
{
	[Nostromo.Dependency]
	public class SoundService_Droid : ISoundService
	{
		MediaPlayer _mediaPlayer;

		public SoundService_Droid()
		{

		}

		public void ExecuteSound(SoundEffectKind sound)
		{
			Device.BeginInvokeOnMainThread(() =>
			{
				if (_mediaPlayer == null)
				{
					_mediaPlayer = new MediaPlayer();
					_mediaPlayer.Looping = false;
					_mediaPlayer.Prepared += (sender, e) =>
					{
						_mediaPlayer.Start();
					};

					_mediaPlayer.Completion += (sender, e) =>
					{
						_mediaPlayer?.Stop();
						//_mediaPlayer?.Dispose();
						//_mediaPlayer = null;
					};
				}

				_mediaPlayer.Reset();
				var fd = Xamarin.Forms.Forms.Context.Assets.OpenFd(SoundsEffect.GetFileName(sound));
				_mediaPlayer.SetDataSource(fd.FileDescriptor, fd.StartOffset, fd.Length);
				fd.Close();
				_mediaPlayer.Prepare();
			});

		}
	}
}
