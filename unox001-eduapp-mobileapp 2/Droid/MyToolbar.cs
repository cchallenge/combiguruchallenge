﻿using System;
using Android.Graphics;
using Android.Runtime;
using Android.Text;
using Android.Text.Style;
using Android.Widget;
using HockeyApp;
using AToolbar = Android.Support.V7.Widget.Toolbar;

namespace EduApp.Droid
{
	public class MyToolbar : AToolbar
	{
		public MyToolbar(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { init(); }

		public MyToolbar(Android.Content.Context ctx) : base(ctx) { init(); }

		public MyToolbar(Android.Content.Context ctx, Android.Util.IAttributeSet attr) : base(ctx, attr) { init(); }

		public MyToolbar(Android.Content.Context ctx, Android.Util.IAttributeSet attr, int defStyleAttr) : base(ctx, attr, defStyleAttr) { init(); }

		protected virtual void init()
		{
			ToolbarInstance = this;
		}

		protected override void Dispose(bool disposing)
		{
			if (ToolbarInstance == this)
				ToolbarInstance = null;
			
			base.Dispose(disposing);
		}

		public static MyToolbar ToolbarInstance { get; set; }

		string title;
		public string CustomTitle
		{
			get
			{
				return title;
			}

			set
			{
				if (title != value)
				{
					title = value;
					try
					{
						if (this != null && this.Handle != IntPtr.Zero)
						{
							var titleView = this.FindViewById<TextView>(Resource.Id.toolbar_title);
							if (titleView != null)
							{
								titleView.Text = title;
								titleView.Typeface = Typeface.CreateFromAsset(this.Context.Assets, "SFNSDisplay.ttf");
							}
						}
					} catch (Exception ex)
					{
						HockeyApp.MetricsManager.TrackEvent($"ECCEZZIONE TITLE SU TOOLBAR: {ex}");
					}
				}
			}
		}

		Color fontColor;
		public Color FontColor
		{
			get { return fontColor; }
			set
			{
				if (fontColor != value)
				{
					fontColor = value;
					try
					{
						if (this != null && this.Handle != IntPtr.Zero)
						{
							var titleView = this.FindViewById<TextView>(Resource.Id.toolbar_title);
							if (titleView != null)
							{
								titleView.SetTextColor(fontColor);
							}
						}
					}
					catch (Exception ex){
						HockeyApp.MetricsManager.TrackEvent($"ECCEZZIONE FONT COLOR SU TOOLBAR: {ex}");
					}
				}
			}
		}

		protected override void OnLayout(bool changed, int left, int top, int right, int bottom)
		{
			base.OnLayout(changed, left, top, right, bottom);
			this.CustomTitle = base.Title;
		}
	}
}
