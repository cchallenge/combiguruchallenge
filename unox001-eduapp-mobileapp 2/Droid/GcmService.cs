﻿using System;
using System.Text;
using System.Threading.Tasks;
using Akavache;
using Android.App;
using Android.Content;
using EduApp.Core;
using Gcm.Client;
using System.Reactive.Linq;

[assembly: Permission (Name = "com.psmobile.eduapp.permission.C2D_MESSAGE")]
[assembly: UsesPermission (Name = "com.psmobile.eduapp.permission.C2D_MESSAGE")]
[assembly: UsesPermission (Name = "com.google.android.c2dm.permission.RECEIVE")]

//GET_ACCOUNTS is only needed for android versions 4.0.3 and below
[assembly: UsesPermission (Name = "android.permission.GET_ACCOUNTS")]
[assembly: UsesPermission (Name = "android.permission.INTERNET")]
[assembly: UsesPermission (Name = "android.permission.WAKE_LOCK")]

namespace EduApp.Droid
{

	[BroadcastReceiver (Permission= Gcm.Client.Constants.PERMISSION_GCM_INTENTS)]
	[IntentFilter (new string [] { Gcm.Client.Constants.INTENT_FROM_GCM_MESSAGE }, Categories = new string [] { Globals.PackageNameApp })]
	[IntentFilter (new string [] { Gcm.Client.Constants.INTENT_FROM_GCM_REGISTRATION_CALLBACK }, Categories = new string [] { Globals.PackageNameApp })]
	[IntentFilter (new string [] { Gcm.Client.Constants.INTENT_FROM_GCM_LIBRARY_RETRY }, Categories = new string [] { Globals.PackageNameApp })]

	public class PushHandlerBroadcastReceiver : GcmBroadcastReceiverBase<GcmService>
	{
		public static string [] SENDER_IDS = new string [] { Globals.ID_Mittente };
		public const string TAG = "GoogleCloudMessaging";
	}

	[Service]
	public class GcmService : GcmServiceBase
	{
		string _currentCorrId = "";
		public GcmService () : base(PushHandlerBroadcastReceiver.SENDER_IDS)
		{
		}

		protected override void OnError (Context context, string errorId)
		{
			Console.WriteLine (errorId);
		}

		protected override void OnMessage (Context context, Intent intent)
		{
			Task.Run(() => GetCurrentCorrId ()).Wait();
			string message = Globals.UnoxDefaultNotifMessage;
			string corrId = "";

			if (intent != null && intent.Extras != null) {
				message = intent.Extras.GetString ("message");
				corrId = intent.Extras.GetString ("corrId"); 
			}
			if (_currentCorrId.Equals(corrId))
				createNotification ("", message);
		}

		protected override void OnRegistered (Context context, string registrationId)
		{
			Nostromo.Container.Default.Resolve<INotificationService> ().SetDeviceToken (registrationId);
		}

		protected override void OnUnRegistered (Context context, string registrationId)
		{
			Nostromo.Container.Default.Resolve<INotificationService> ().UnRegisterForRemote ();
		}

		async Task GetCurrentCorrId ()
		{
			_currentCorrId = await BlobCache.UserAccount.GetObject<string> (Globals.CurrentCorrId);
		}

		void createNotification (string title, string desc, int idNotification = 0)
		{
			string alert = desc;
			title = this.Resources.GetText (this.Resources.GetIdentifier ("app_name", "string", this.PackageName));
			//Create notification
			var notificationManager = GetSystemService (Context.NotificationService) as NotificationManager;
			//Create an intent to show ui
			var uiIntent = new Intent (this, typeof (MainActivity));

			Android.Support.V7.App.NotificationCompat.Builder builder = new Android.Support.V7.App.NotificationCompat.Builder (this);
			builder.SetContentIntent (PendingIntent.GetActivity (this, 0, uiIntent, 0))
				   .SetSmallIcon (EduApp.Droid.Resource.Drawable.logo_unox)
			       .SetSound (Android.Media.RingtoneManager.GetDefaultUri (Android.Media.RingtoneType.Notification))
				   .SetAutoCancel (true)
				   .SetTicker (alert)
				   .SetContentTitle (title)
			       .SetContentText (alert);

			var notification = builder.Build ();

			notificationManager.Notify (idNotification, notification);
		}
	}
}
