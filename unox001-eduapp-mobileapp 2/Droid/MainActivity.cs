﻿using System;
using HockeyApp.Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using AppCore;
using NLog.Config;
using NLog.Targets;
using NLog;
using System.Reflection;
using EduApp.Core;
using NControl.Droid;
using Xamarin.Facebook;
using Gcm.Client;
using System.Threading.Tasks;
using Android.Support.V7.App;
using Xamarin.Forms.Platform.Android;

namespace EduApp.Droid
{
    //[Activity(Theme = "@style/MyTheme.Splash",
    //    MainLauncher = true, NoHistory = true,
    //    Label = "CombiGuru", Icon = "@mipmap/ic_launcher",
    //    ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
    //    WindowSoftInputMode = SoftInput.AdjustResize,
    //    ScreenOrientation = ScreenOrientation.Portrait)]
    //public class SplashActivity : FormsAppCompatActivity
    //{
    //    public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState) => base.OnCreate(savedInstanceState, persistentState);
    //    public override void OnBackPressed() { }
    //    Launches the startup task
    //    protected override void OnResume()
    //    {
    //        base.OnResume();
    //        StartActivity(new Intent(Application.Context, typeof(MainActivity))); ;
    //    }
    //}


    [Activity(Label = "CombiGuru",
        Theme = "@style/MyTheme.Splash",
        //Theme = "@style/MyTheme.Base",
        MainLauncher = true, 
        Icon = "@mipmap/ic_launcher",
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        WindowSoftInputMode = SoftInput.AdjustResize,
        ScreenOrientation = ScreenOrientation.Portrait
    )]
	public class MainActivity : FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
            //Prima dell'OnCreate Reimpostiamo il tema di base e forziamo l'eliminazione
            //del background per disattivare lo splash screen
            base.SetTheme(Resource.Style.MyTheme_Base);
            Window.SetBackgroundDrawable(null);
            this.Window.DecorView.Background = null; // Resources.GetDrawable(Resource.Drawable.window_background);
            base.OnCreate(bundle);


            EnableNLogConsole();

            TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

            Rg.Plugins.Popup.Popup.Init(this, bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
			UIHelper.PixelsFor1Dip = Resources.DisplayMetrics.Density;

			//CarouselView does not get “Linked-out” when compiling in release mode
			var cv = typeof(Xamarin.Forms.CarouselView);
			var assembly = Assembly.Load(cv.FullName);

			DefaultAppCoreBootstrapper bootstrapper = new DefaultAppCoreBootstrapper (Globals.PSAppServerUrl);
			bootstrapper
				.EnableAndroidDefaultModules ();

			Acr.UserDialogs.UserDialogs.Init(this);
			NControlViewRenderer.Init();

			CrashManager.Register (this, "f1c8a97dd0f94b20a9ebd120a589ebc2");

			PSMFacebook.Droid.FacebookLoginButtonRenderer.Initialize ();

			//System Font bugfix
			Android.Content.Res.Configuration config = Android.Content.Res.Resources.System.Configuration;
			config.FontScale = (float)1;
			var dispM = new Android.Util.DisplayMetrics ();
			WindowManager.DefaultDisplay.GetMetrics (dispM);
			dispM.ScaledDensity = config.FontScale * dispM.Density;
			BaseContext.Resources.UpdateConfiguration (config, dispM);

			RegisterWithGCM();

			SetMediaVolume();

            LoadApplication(new App(bootstrapper, typeof(NotificationService_Droid).Assembly));
        }

        #region MediaVolume
        void SetMediaVolume()
		{
			this.VolumeControlStream = Android.Media.Stream.Music;
		}
  		#endregion

		void RegisterWithGCM ()
		{
			GcmClient.CheckDevice (this);
			GcmClient.CheckManifest (this);
			Console.WriteLine ("RegisterWithGCM");
			GcmClient.Register (this, Globals.ID_Mittente);
		}

		public static void EnableNLogConsole()
		{
			var config = new LoggingConfiguration();

			var consoleTarget = new ConsoleTarget { Layout = "${date:format=HH\\:MM\\:ss} ${logger} ${message}" };
			config.AddTarget("console", consoleTarget);
			#if DEBUG
			config.LoggingRules.Add(new LoggingRule("*", NLog.LogLevel.Trace, consoleTarget));
			#else
			config.LoggingRules.Add(new LoggingRule("*", NLog.LogLevel.Debug, consoleTarget));
			#endif
			LogManager.Configuration = config;
		}


		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			if (!PSMFacebook.Droid.FacebookLoginButtonRenderer.OnActivityResult(requestCode, resultCode, data))
				base.OnActivityResult (requestCode, resultCode, data);
		}

	}
}
