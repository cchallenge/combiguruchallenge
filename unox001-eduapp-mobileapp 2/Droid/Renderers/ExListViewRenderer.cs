using System;
using Android.App;
using Android.Runtime;
using Android.Views.InputMethods;
using Android.Widget;
using EduApp;
using EduApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Xamarin.Forms.ListView),typeof(ExListViewRenderer))]
namespace EduApp.Droid
{

	public class ExListViewRenderer : ListViewRenderer
	{
		public ExListViewRenderer() : base() { 
		
		}

		protected override Android.Widget.ListView CreateNativeControl()
		{
			var res = base.CreateNativeControl();


			res.SetSelector (Android.Resource.Color.Transparent);
			res.CacheColorHint = Android.Graphics.Color.Transparent;

			res.ScrollStateChanged += (sender, e) => {

				switch (e.ScrollState)
				{
					case ScrollState.Fling: 
						HideKeyboardAndFocus();
						break;
					case ScrollState.Idle:
						break;
					case ScrollState.TouchScroll:
						HideKeyboardAndFocus();
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			};
			return res;
		}

		void HideKeyboardAndFocus() {
			var activity = (this.Context as Activity);
			if (activity != null) {
			var focused = (this.Context as Activity)?.CurrentFocus;
				if (focused != null)
				{
					focused.ClearFocus();
					if (activity.Window?.DecorView != null)
					{
						InputMethodManager imm = (InputMethodManager)this.Context.GetSystemService(Activity.InputMethodService);
						imm.HideSoftInputFromWindow(activity.Window.DecorView.WindowToken, 0);
					}
				}
			}
		}

	}
}
