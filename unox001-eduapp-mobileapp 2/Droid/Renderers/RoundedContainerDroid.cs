﻿using System;
using Android.Graphics;
using Android.Util;
using EduApp;
using EduApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(RoundedContainer), typeof(RoundedContainerDroid))]
namespace EduApp.Droid
{
	public class RoundedContainerDroid : ViewRenderer
	{
		private Path _path;
		private RectF _bounds;

		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				if ((int)Android.OS.Build.VERSION.SdkInt < 18)
					SetLayerType(Android.Views.LayerType.Software, null);
			}
		}

		protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
		{
			base.OnSizeChanged(w, h, oldw, oldh);
			if (w != oldw && h != oldh)
			{
				_bounds = new RectF(0, 0, w, h);
			}

			var radius = Math.Min(w, h) / 2;
			_path = new Path();
			_path.Reset();
			_path.AddRoundRect(_bounds, radius, radius, Path.Direction.Cw);
			_path.Close();
		}

        public override void Draw(Canvas canvas)
        {
            //TODO: va in crash!
            if (_path != null)
            {
                canvas.Save();
                canvas.ClipPath(_path);
                base.Draw(canvas);
                canvas.Restore();
            }
		}
	}
}
