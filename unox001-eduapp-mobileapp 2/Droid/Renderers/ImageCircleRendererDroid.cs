﻿using System;
using Android.Graphics;
using Android.Media;
using EduApp;
using EduApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ImageCircle), typeof(ImageCircleRendererDroid))]
namespace EduApp.Droid
{
	public class ImageCircleRendererDroid : ImageRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.Image> e)
		{
			base.OnElementChanged (e);

			if (e.OldElement == null) {
				if ((int)Android.OS.Build.VERSION.SdkInt < 18)
					SetLayerType (Android.Views.LayerType.Software, null);
			}
		}

		public Bitmap FixRotation (string filePath2)
		{
			ExifInterface ei = new ExifInterface (filePath2);

			int orientation = ei.GetAttributeInt (ExifInterface.TagOrientation, (int)Android.Media.Orientation.Normal);

			Matrix mtx = new Matrix ();

			switch (orientation) {
			case (int)Android.Media.Orientation.Rotate90:
				mtx.PreRotate (90);
				break;
			case (int)Android.Media.Orientation.Rotate180:
				mtx.PreRotate (180);
				break;
			case (int)Android.Media.Orientation.Rotate270:
				mtx.PreRotate (270);
				break;
			default:
				break;
			}
			var bitmap = BitmapFactory.DecodeFile (filePath2);
			return Bitmap.CreateBitmap (bitmap, 0, 0, bitmap.Width, bitmap.Height, mtx, false);
		}

		protected override bool DrawChild (Canvas canvas, global::Android.Views.View child, long drawingTime)
		{
			try {
				var radius = Math.Min (Width, Height) / 2;
				var strokeWidth = 10;
				radius -= strokeWidth / 2;

				//Create path to clip
				var path = new Path ();
				path.AddCircle (Width / 2, Height / 2, radius, Path.Direction.Ccw);
				canvas.Save ();
				canvas.ClipPath (path);

				var result = base.DrawChild (canvas, child, drawingTime);

				canvas.Restore ();

				// Create path for circle border
				path = new Path ();
				path.AddCircle (Width / 2, Height / 2, radius, Path.Direction.Ccw);

				var paint = new Paint ();
				paint.AntiAlias = true;
				paint.StrokeWidth = 5;
				paint.SetStyle (Paint.Style.Stroke);
				paint.Color = global::Android.Graphics.Color.White;

				canvas.DrawPath (path, paint);

				//Properly dispose
				paint.Dispose ();
				path.Dispose ();
				return result;
			} catch (Exception ex) {
				Console.WriteLine ("Unable to create circle image: " + ex);
			}

			return base.DrawChild (canvas, child, drawingTime);
		}
	}
}
