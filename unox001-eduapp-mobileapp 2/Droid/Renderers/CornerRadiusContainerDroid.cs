﻿using System;
using Android.Graphics;
using Android.Graphics.Drawables;
using EduApp;
using EduApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CornerRadiusContainer), typeof(CornerRadiusContainerDroid))]
namespace EduApp.Droid
{
	public class CornerRadiusContainerDroid : ViewRenderer
	{
		float _cornerRadius = 0;
		Android.Graphics.Color _bkgColor;
		Android.Graphics.Color _borderColor;
		float _borderWidth;

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if ((int)Android.OS.Build.VERSION.SdkInt < 18)
				SetLayerType(Android.Views.LayerType.Software, null);
			_cornerRadius = (sender as CornerRadiusContainer).CornerRadius;
			_bkgColor = (sender as CornerRadiusContainer).BackgroundColor.ToAndroid();
			_borderColor = (sender as CornerRadiusContainer).BorderColor.ToAndroid();
			_borderWidth = (sender as CornerRadiusContainer).BorderWidth;
			ChangeGradient();
		}

		//protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
		//{
		//	base.OnSizeChanged(w, h, oldw, oldh);

		//}

		void ChangeGradient() { 
			GradientDrawable gradient = new GradientDrawable();
			gradient.SetStroke((int)_borderWidth, _borderColor);
			gradient.SetCornerRadius(_cornerRadius * 2);
			gradient.SetColor(_bkgColor);
			this.SetBackground(gradient);
		}
	}
}

