﻿using System;
using EduApp;
using EduApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer (typeof (EntryDynamicRenderer), typeof (EntryDynamicRendererDroid))]
namespace EduApp.Droid
{
	public class EntryDynamicRendererDroid : EntryRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);
			if (this.Control == null) return;
			var ele = e.NewElement as EntryDynamicRenderer;
			
			Control.Background = null;
			if (ele != null && !ele.IsPassword)
				Control.InputType = Android.Text.InputTypes.TextFlagNoSuggestions;
			//FIXME In sospeso la traduzione del testo della tastiera
			//if (ele != null)
			//	Control.SetImeActionLabel (ele.KeyboardDoneKey, Android.Views.InputMethods.ImeAction.Done);
		}

	}
}

