﻿using System;
using Android.Graphics.Drawables;
using EduApp;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(MultiLineLabel), typeof(MultiLineRendererDroid))]
public class MultiLineRendererDroid : LabelRenderer
{
	protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
	{
		base.OnElementChanged(e);

		MultiLineLabel multiLineLabel = (MultiLineLabel)Element;

		if (multiLineLabel != null && multiLineLabel.Lines != -1)
		{
			Control.SetSingleLine(false);
			Control.SetMaxLines(multiLineLabel.Lines);
			Control.SetMinLines(1);
		}
	}

	float _cornerRadius = 0;
	Android.Graphics.Color _bkgColor;
	Android.Graphics.Color _borderColor;
	float _borderWidth;
	int _padding = 0;
	Android.Views.TextAlignment _textAlign = Android.Views.TextAlignment.TextStart;

	protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
	{
		base.OnElementPropertyChanged(sender, e);
		if ((int)Android.OS.Build.VERSION.SdkInt < 18)
			SetLayerType(Android.Views.LayerType.Software, null);
		
		_cornerRadius = (sender as MultiLineLabel).CornerRadius;
		_bkgColor = (sender as MultiLineLabel).BackgroundColor.ToAndroid();
		_borderColor = (sender as MultiLineLabel).BorderColor.ToAndroid();
		_borderWidth = (sender as MultiLineLabel).BorderWidth;
		_padding = (int)(sender as MultiLineLabel).Padding;

		float scale = Resources.DisplayMetrics.Density;
		int paddingDpi = (int)(_padding * scale + 0.5f);
		this.Control.SetPadding(paddingDpi, paddingDpi, paddingDpi, paddingDpi);

		DrawCornerRadius();
	}

	protected override void OnSizeChanged(int w, int h, int oldw, int oldh)
	{
		
		base.OnSizeChanged(w, h, oldw, oldh);


	}

	void DrawCornerRadius() { 
		GradientDrawable gradient = new GradientDrawable();
		gradient.SetStroke((int)_borderWidth, _borderColor);
		gradient.SetCornerRadius(_cornerRadius * 2);
		gradient.SetColor(_bkgColor);
		this.SetBackground(gradient);
	}
}
