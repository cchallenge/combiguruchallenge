﻿using System;
using EduApp;
using EduApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Button), typeof(ButtonRendererAndroid))]
namespace EduApp.Droid
{
	public class ButtonRendererAndroid : ButtonRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);

			try
			{
				//FIXME: capire a cosa serviva, se pulsante enabled false crasha su samsung s5
				//if (Control != null && Element != null)
				//{
				//	var button = Element as Button;
				//	//Control.StateListAnimator = null;
				//	if (button.BorderColor == Color.Transparent && button.BorderWidth == 0.0f)
				//	{
				//	//	// remove default background image
				//	//	Control.Background = null;

				//	//	// set background color
				//	//	//FIXME: capire perchè con Enabled false crasha
				//	//	//var c = Element.BackgroundColor.ToAndroid();
				//	//	//Control.SetBackgroundColor(c);
				//	}
				//}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}
	}
}

