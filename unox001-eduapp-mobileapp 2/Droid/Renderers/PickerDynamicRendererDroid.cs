﻿using System;
using EduApp;
using EduApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer (typeof (PickerDynamicRenderer), typeof (PickerDynamicRendererDroid))]
namespace EduApp.Droid
{
	public class PickerDynamicRendererDroid : PickerRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged (e);
			var ele = e.NewElement as PickerDynamicRenderer;
			if (this.Control == null) return;

			Control.Background = null;
			if (ele != null)
				Control.Gravity = (ele.TextAlignRight) ? Android.Views.GravityFlags.End | Android.Views.GravityFlags.CenterVertical : Android.Views.GravityFlags.Start | Android.Views.GravityFlags.CenterVertical;

			Control.TextSize = 12;
			Control.Typeface = Android.Graphics.Typeface.CreateFromAsset (this.Context.Assets, "Lato-Regular.ttf");

		}
	}
}
