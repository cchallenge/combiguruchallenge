﻿using System;
using Android.Graphics.Drawables;
using EduApp;
using EduApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer (typeof (PickerBackgroundRenderer), typeof (PickerRendererDroid))]
namespace EduApp.Droid
{
	public class PickerRendererDroid : PickerRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged (e);
			if (this.Control == null)
				return;

			Control.Background = null;
			Control.Typeface = Android.Graphics.Typeface.CreateFromAsset (this.Context.Assets, "Lato-Regular.ttf");
			GradientDrawable gradient = new GradientDrawable ();
			gradient.SetStroke (2, Android.Graphics.Color.White);
			gradient.SetCornerRadius (10);
			gradient.SetColor (Android.Graphics.Color.White);
			Control.SetBackground (gradient);
		}
	}
}
