﻿using System;
using Android.Graphics.Drawables;
using EduApp;
using EduApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EntryWithTitleRenderer),typeof(EntryRendererDroid))]
namespace EduApp.Droid
{
	public class EntryRendererDroid : EntryRenderer
	{
		bool firstChange = true;
		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);
			if (this.Control == null) return;
			var ele = e.NewElement as EntryWithTitleRenderer;

			/*
			 * renderer per togliere l'underline e arrotondare i bordi dall'entry base di forms
			 */
			Control.Background = null;
			if (ele != null && !ele.IsPassword)
				Control.InputType = Android.Text.InputTypes.TextFlagNoSuggestions;

			//FIXME In sospeso la traduzione del testo della tastiera
			//if (ele != null)
			//	Control.SetImeActionLabel (ele.KeyboardDoneKey, Android.Views.InputMethods.ImeAction.Done)

			if (firstChange)
			{
				GradientDrawable gradient = new GradientDrawable();
				gradient.SetStroke(2, Android.Graphics.Color.White);
				gradient.SetCornerRadius(10);
				gradient.SetColor(Android.Graphics.Color.White);
				Control.SetBackground(gradient);
				firstChange = false;
			}

		}

		protected override FormsEditText CreateNativeControl()
		{
			var native = base.CreateNativeControl();
			return native;
		}

	}
}
