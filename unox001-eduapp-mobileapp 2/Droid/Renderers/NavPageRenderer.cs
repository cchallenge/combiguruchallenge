﻿using System;
using Android.App;
using Android.Text;
using Android.Text.Style;
using EduApp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Platform.Android.AppCompat;

[assembly: ExportRenderer(typeof(NavigationPage), typeof(NavPageRenderer))]
namespace EduApp.Droid
{
	public class NavPageRenderer : NavigationPageRenderer
	{
		public NavPageRenderer()
		{
		}

		Color _currentBarBkgColor;
		Color _currentFontColor;
		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			//var activity = ((Activity)Context);
			var color = this.Element.BarBackgroundColor;
			if (color != _currentBarBkgColor)
			{
				_currentBarBkgColor = color;
				var view = MyToolbar.ToolbarInstance;
				if (view != null)
				{
					view.SetContentInsetsRelative(150, 150);
					view.SetBackgroundColor(_currentBarBkgColor.ToAndroid());
				}
			}

			var fontColor = this.Element.BarTextColor;
			if (_currentFontColor != fontColor) {
				_currentFontColor = fontColor;
				var view = MyToolbar.ToolbarInstance;
				if (view != null)
				{
					view.FontColor = _currentFontColor.ToAndroid();
				}
			}
		}
	}
}
