﻿using EduApp.Core;
using System;
using Android.Widget;
using Nostromo;

namespace EduApp.Droid
{
	[Dependency]
	public class ToastService_Droid : IToastService
	{
		public ToastService_Droid ()
		{
		}

		public void ShowToastMessage (string message)
		{
			Toast.MakeText (Android.App.Application.Context, message, ToastLength.Long).Show();
		}
	}
}
