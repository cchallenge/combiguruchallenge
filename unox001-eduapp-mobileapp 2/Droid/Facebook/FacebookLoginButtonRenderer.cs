﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using Java.Lang;
using PSMFacebook;
using PSMFacebook.Droid;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: Permission (Name = Android.Manifest.Permission.Internet)]
[assembly: Permission (Name = Android.Manifest.Permission.WriteExternalStorage)]
[assembly: MetaData ("com.facebook.sdk.ApplicationId", Value = "@string/app_id")]
[assembly: ExportRenderer (typeof (FacebookLoginButton), typeof (FacebookLoginButtonRenderer))]

//RIFERIMENTI PER CONFIGURAZIONE:
//https://components.xamarin.com/gettingstarted/facebookandroid

namespace PSMFacebook.Droid
{
	class FacebookCallback : Java.Lang.Object, IFacebookCallback
	{
		public Action HandleCancel { get; set; }
		public Action<FacebookException> HandleError { get; set; }
		public Action<LoginResult> HandleSuccess { get; set; }

		public void OnCancel ()
		{
			var c = HandleCancel;
			if (c != null)
				c ();
		}

		public void OnError (FacebookException error)
		{
			var c = HandleError;
			if (c != null)
				c (error);
		}

		public void OnSuccess (Java.Lang.Object result)
		{
			var c = HandleSuccess;
			if (c != null)
				c (result.JavaCast<LoginResult> ());
		}
	}

	public class FacebookLoginButtonRenderer : ViewRenderer<FacebookLoginButton, LoginButton>
	{
		static ICallbackManager CallbackManager { get; set; }

		public static void Initialize () {
			if (!FacebookSdk.IsInitialized)
				FacebookSdk.SdkInitialize (Android.App.Application.Context.ApplicationContext);
			CallbackManager = CallbackManagerFactory.Create ();
		}

		public static bool OnActivityResult (int p1, Result p2, Intent intent) {
			return CallbackManager.OnActivityResult (p1, (int) p2, intent);
		}


		protected override void OnElementChanged (ElementChangedEventArgs<FacebookLoginButton> ec)
		{
			base.OnElementChanged (ec);

			var element = ec.NewElement;
			if (element != null) {
				var control = new LoginButton (this.Context);
				//@@ CM imposto il font corretto
				control.Typeface = Android.Graphics.Typeface.CreateFromAsset(this.Context.Assets, "Lato-Regular.ttf");

				LoginManager.Instance.LogOut ();

				control.RegisterCallback (CallbackManager, new FacebookCallback {
					HandleSuccess = (result) => {
						Device.BeginInvokeOnMainThread(() =>
						{
							var token = result.AccessToken.Token;
							element?.LoggedInCommand?.Execute(token);
							//NOTA BENE:  NON MANTENGO L'SDK LOGGATO!!!
							LoginManager.Instance.LogOut();
						});
					}
				});

				if (element.ReadPermissions != null)
					control.SetReadPermissions (element.ReadPermissions);

				var oldControl = Control; 
				SetNativeControl (control);

				if (oldControl != null)
					oldControl.Dispose ();
			}
		}
	}
}
