﻿using NUnit.Framework;
using System;
using AppCore;
using AppCore.Interfaces;
using EduApp.Core;
using System.Threading.Tasks;
using NLog.Config;
using NLog;
using NLog.Targets;

namespace Test
{
	[TestFixture()]
	public class Test
	{
		DefaultAppCoreBootstrapper _bootstrapper { get; set; }

		[TestFixtureSetUp]
		public void initialize()
		{
			_bootstrapper = new DefaultAppCoreBootstrapper();
			_bootstrapper
				.EnableNet45DefaultModules(useTestingModules: true)
				.EnableApiRest(Globals.ApiKeyRest)
				.Register<IAppCatalog, FakeAppCatalog>()
				.Register<IUnoxAppAuthModule, UnoxAccountModule>()
				.Register<IAppAuthModule>((x) => x.ResolveModule<IUnoxAppAuthModule>());

			EnableNLogConsole();
		}

		public class FakeAppCatalog : IAppCatalog
		{
			public IAppCoreContext Context
			{
				get;
				set;
			}

			public string GetLanguage()
			{
				return "EN";
			}

			public string GetValue(string value)
			{
				return string.Empty;
			}

			public void ResetLanguage()
			{
				
			}

			public void SetLanguage(string language = null)
			{
				
			}
		}

		public static void EnableNLogConsole()
		{
			//InternalLogger.LogToConsole = true;
			var config = new LoggingConfiguration();

			var consoleTarget = new ColoredConsoleTarget
			{
				ErrorStream = true,
				Layout = "${date:format=HH\\:MM\\:ss} ${logger} ${message} ${exception:format=tostring}"
			};

			consoleTarget.DetectConsoleAvailable = false;
			config.AddTarget("console", consoleTarget);

			config.LoggingRules.Add(new LoggingRule("*", NLog.LogLevel.Trace, consoleTarget));

			LogManager.Configuration = config;

		}

		//Chiamato prima di ogni test
		[SetUp()]
		public void Reset() { 
			AppCore.Testing.MachineConnectivityStatus = ConnectivityStatus.WifiOrCable;
		}

		[Test()]
		public async Task TestLogin()
		{
			var AM = DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<IUnoxAppAuthModule>();
			//Test login Unox
			var correctLogin = await AM.UnoxLogin("costanza.minarelli@psmobile.it", "costanzapwd");
			Assert.IsTrue(correctLogin.Success, "Errore test login");

			//Test check login
			var checkLogin = await AM.CheckLogin();
			Assert.IsNotNull(checkLogin, "Errore check login dopo aver effettuato login");

			//Test logout
			AM.Logout();
			var checkLogout = await AM.CheckLogin();
			Assert.IsNull(checkLogout, "Errore logout");

			//Test errore login 
			var errorLogin = await AM.UnoxLogin("costanza.minarelli@psmobile.it", "psmobile");
			Assert.IsFalse(errorLogin.Success, "Errore utente errato");
			Assert.IsNotNull(errorLogin.Error, "Errore utente errato");

			//Test senza connessione
			AppCore.Testing.MachineConnectivityStatus = ConnectivityStatus.None;
			var noConnectLogin = await AM.UnoxLogin("ba@psmobile.it", "psmobile");
			Assert.AreEqual (UnoxAccountModule.ERRORS.ENOCONNECTION, noConnectLogin.Error);
			Assert.IsFalse(noConnectLogin.Success, "Errore test login senza connessione");
		}

		[Test ()]
		public async Task TestUpdate ()
		{
			var AM = DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<IUnoxAppAuthModule> ();
			var serializer = DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<AppCore.Interfaces.IJsonSerializer> ();
			//Test login Unox
			var correctLogin = await AM.UnoxLogin ("costanza.minarelli@psmobile.it", "costanzapwd");
			Assert.IsTrue (correctLogin.Success, "Errore test login");

			var newdata = serializer.Deserialize<UserData> (serializer.Serialize (AM.UserData));

			newdata.Language = "de";
			newdata.FirstName = "Osvaldo";
			newdata.LastName = "De Luca";
			newdata.AddressCity = "Genova";

			var newdataSer = serializer.Serialize (serializer.Deserialize<PostUserData>(serializer.Serialize(newdata)));
			var res = await AM.UnoxUpdateUserData (newdata);

			Assert.IsTrue (res.Success);


			var newdataToCompare = serializer.Deserialize<PostUserData> (serializer.Serialize (AM.UserData));



			Assert.AreEqual (newdataSer, serializer.Serialize(newdataToCompare));


			newdata.Language = "IT";
			newdata.FirstName = "Cosimo";
			newdata.LastName = "Minaretti";
			newdata.AddressCity = "Casalecchio";

			newdataSer = serializer.Serialize (serializer.Deserialize<PostUserData> (serializer.Serialize (newdata)));
			res = await AM.UnoxUpdateUserData (newdata);

			Assert.IsTrue (res.Success);
			newdataToCompare = serializer.Deserialize<PostUserData> (serializer.Serialize (AM.UserData));


			Assert.AreEqual (newdataSer, serializer.Serialize (newdataToCompare));


		}



		[Test()]
		public async Task TestValidate ()
		{
			var AM = DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<IUnoxAppAuthModule>();

			var correctLogin = await AM.UnoxLogin("costanza.minarelli@psmobile.it", "costanzapwd");

			//Test validazione dopo login
			var res = await AM.UnoxValidate();
			Assert.AreEqual(res.Status, ValidateStatus.activate, "Errore validazione dopo il login");

			//Test senza connessione
			AppCore.Testing.MachineConnectivityStatus = ConnectivityStatus.None;
			res = await AM.UnoxValidate();
			Assert.AreEqual(res.Status, ValidateStatus.activate, "Errore validazione dopo il login");

			AppCore.Testing.MachineConnectivityStatus = ConnectivityStatus.WifiOrCable;

			//Test logout
			AM.Logout();

			//Test validazione dopo logout
			res = await AM.UnoxValidate();
			Assert.AreEqual(res.Status, ValidateStatus.none, "Errore validazione dopo il login");
		}

		[Test()]
		public async Task TestSignup ()
		{
			var AM = DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<IUnoxAppAuthModule>();

			//Test signup false
			var res = await AM.UnoxSignup("massimo.calabro@psmobile.it", "massimopwd", "", "");
			Assert.False(res.Success, "Errore verifica utente già registrato");

			//FIXME: Commentato perché già usato, per testare nuovamente cambiare l'email
			////Test signup giusto 
			//var gres = await AM.UnoxSignup("costanza.minarelli@psmobile.it", "costanzapwd", "", "");
			//Assert.True(gres.Success, "Errore Signup");

			//Test senza connessione
			AppCore.Testing.MachineConnectivityStatus = ConnectivityStatus.None;
			var noConnectLogin = await AM.UnoxSignup("ba@psmobile.it", "psmobile", "", "");
			Assert.IsNull(noConnectLogin, "Errore test login senza connessione");
		}
	}
}
