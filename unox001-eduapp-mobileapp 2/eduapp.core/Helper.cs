﻿using System;
using System.Text.RegularExpressions;

namespace EduApp.Core
{
	public static class Helper
	{
		public static bool CorrectMailFormat (string email)
		{
			email = email.Trim();
			Regex regexMail = new Regex (@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
			Match matchMail = regexMail.Match (email);
			return matchMail.Success;
		}

		public static bool CorrectPasswordFormat (string password)
		{
			Regex regexMail = new Regex (@"(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).{8,}");
			Match matchMail = regexMail.Match (password);
			return matchMail.Success;
		}

		public static string LowerTrimmedString (string val)
		{
			if (val.Contains (","))
				val = val.Replace (",", ".");
			val = Regex.Replace (val, @"\s+", "");
			return val.ToLower ().Trim ();
		}
	}
}
