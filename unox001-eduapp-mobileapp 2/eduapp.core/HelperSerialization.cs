﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace EduApp.Core
{
	public static class HelperSerialization
	{
		static Dictionary<string, Dictionary<string, string>> dictionaryRegions;

		public static string GetJson (string path)
		{
			string xDocToText = "";
			try {
				var assembly = typeof (HelperSerialization).GetTypeInfo ().Assembly;
				StreamReader stream = new StreamReader (assembly.GetManifestResourceStream (path));
				using (stream) 
				{
					xDocToText = stream.ReadToEnd ();
				}
			} catch (Exception ex) {
				System.Diagnostics.Debug.WriteLine (ex.Message);
			}
			return xDocToText;
		}

		public static Dictionary<string, string> GetCountriesFromJson (string path)
		{
			Dictionary<string, string> dict = new Dictionary<string, string>();
			string val = GetJson (path);
			CountryArray country = JsonConvert.DeserializeObject<CountryArray> (val);
			List<CountryModel> sortedCountry = country.Country.OrderBy(x => x.Denominazione).ToList();
			foreach (var countryVal in sortedCountry)
				dict.Add (countryVal.Codice, countryVal.Denominazione);
			return dict;
		}

		public static void CreateRegionsFromJson (string path)
		{
			dictionaryRegions = new Dictionary<string, Dictionary<string, string>> ();
			string val = GetJson (path);
			RegionArray regions = JsonConvert.DeserializeObject<RegionArray> (val);
			foreach (var region in regions.Region) 
			{
				try {
					if (dictionaryRegions.ContainsKey (region.CountryCode)) {
						var dictLocal = dictionaryRegions [region.CountryCode];
						dictLocal.Add (region.RegionCode, region.RegionDescr);
						dictionaryRegions [region.CountryCode] = dictLocal;
					} else {
						Dictionary<string, string> dictInt = new Dictionary<string, string> ();
						dictInt.Add (region.RegionCode, region.RegionDescr);
						dictionaryRegions.Add (region.CountryCode, dictInt);
					}
				}
				catch (Exception ex) {
					System.Diagnostics.Debug.WriteLine (ex.Message);
				}
			}
		}

		public static Dictionary<string, string> GetRegionsFromCountryCode (string val)
		{
			if (String.IsNullOrEmpty (val) || !dictionaryRegions.ContainsKey(val))
				return new Dictionary<string, string> { { "-", "-" } };

			var dictReg = dictionaryRegions [val];

			dictReg = (new Dictionary<string, string> { { "-", "-" } }).Concat (dictReg).ToDictionary (kvp => kvp.Key, kvp => kvp.Value);

			return dictReg;
		}

	}

	public class CountryArray
	{
		public List<CountryModel> Country { get; set; }
	}

	public class CountryModel
	{
		public string Codice { get; set; }
		public string Denominazione { get; set; }
	}

	public class RegionArray
	{
		public List<RegionModel> Region { get; set; }
	}

	public class RegionModel
	{
		public string CountryCode { get; set; }
		public string RegionCode { get; set; }
		public string RegionDescr { get; set; }
	}
}
