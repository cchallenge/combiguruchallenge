﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppCore.Logging;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using UnoxEduAppModule;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace EduApp.Core
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupView : PopupPage
    {
        private UnoxAppEduModule UM;
        
        // Popup Title
        #region PopupTitle (Bindable string)
        public static readonly BindableProperty PopupTitleProperty = BindableProperty.Create(
                                                                  "PopupTitle", //Public name to use
                                                                  typeof(string), //this type
                                                                  typeof(PopupView), //parent type (tihs control)
                                                                  "Prendi parte alla sfida"); //default value
                                                                    //Catalog.GetValue("OpenChallengeMessage")); // PROBLEM! non vede "Catalog"
        public string PopupTitle
        {
            get { return (string)GetValue(PopupTitleProperty); }
            set { SetValue(PopupTitleProperty, value); }
        }
        #endregion PopupTitle (Bindable string)

        // Popup Text
        #region PopupText (Bindable string)
        public static readonly BindableProperty PopupTextProperty = BindableProperty.Create(
                                                                  "PopupText", //Public name to use
                                                                  typeof(string), //this type
                                                                  typeof(PopupView), //parent type (tihs control)
                                                                  "Con CombiGuru Challenge hai la possibilità di sfidare i tuoi compagni di scuola e vincere un corso esclusivo di una settimana con Iside de Cesare, Roy Caceres, Sandro e Maurizio Serva, Ciro Scamardella"); //default value
        public string PopupText
        {
            get { return (string)GetValue(PopupTextProperty); }
            set { SetValue(PopupTextProperty, value); }
        }
        #endregion PopupText (Bindable string)

        // Popup Input Title
        #region PopupInputTitle (Bindable string)
        public static readonly BindableProperty PopupInputTitleProperty = BindableProperty.Create(
                                                                  "PopupInputTitle", //Public name to use
                                                                  typeof(string), //this type
                                                                  typeof(PopupView), //parent type (tihs control)
                                                                  "Inserisci il codice assegnato alla tua scuola e scopri la tua posizione in classifica"); //default value
        public string PopupInputTitle
        {
            get { return (string)GetValue(PopupInputTitleProperty); }
            set { SetValue(PopupInputTitleProperty, value); }
        }
        #endregion PopupInputTitle (Bindable string)

        // Popup Error Message
        #region PopupErrorMessage (Bindable string)
        public static readonly BindableProperty PopupErrorMessageProperty = BindableProperty.Create(
                                                                  "PopupErrorMessage", //Public name to use
                                                                  typeof(string), //this type
                                                                  typeof(PopupView), //parent type (tihs control)
                                                                  "Institution does not exist"); //default value
        public string PopupErrorMessage
        {
            get { return (string)GetValue(PopupErrorMessageProperty); }
            set { SetValue(PopupErrorMessageProperty, value); }
        }
        #endregion PopupErrorMessage (Bindable string)

        // Popup Button Text
        #region PopupButtonText (Bindable string)
        public static readonly BindableProperty PopupButtonTextProperty = BindableProperty.Create(
                                                                  "PopupButtonText", //Public name to use
                                                                  typeof(string), //this type
                                                                  typeof(PopupView), //parent type (tihs control)
                                                                  "Verifica"); //default value
        public string PopupButtonText
        {
            get { return (string)GetValue(PopupButtonTextProperty); }
            set { SetValue(PopupButtonTextProperty, value); }
        }
        #endregion PopupButtonText (Bindable string)


        public IAppCatalog Catalog;

        public PopupView(UnoxAppEduModule UM, IAppCatalog Catalog)
        {
            InitializeComponent();
            this.UM = UM;
            this.Catalog = Catalog;

            //redef all text entries hereafter
            PopupTitle = Catalog.GetValue("PopupTitle");
            PopupText = Catalog.GetValue("PopupText");
            PopupInputTitle = Catalog.GetValue("PopupInputTitle");
            PopupErrorMessage = Catalog.GetValue("PopupErrorMessage");
            PopupButtonText = Catalog.GetValue("PopupButtonText");
        }

        public async void ValidateAndSaveInstitution(object sender, System.EventArgs e)
        {
            var validInstitution = await UM._ext_SetUserInstitution(InstitutionEntry.Text);
            
            if ( validInstitution.ToString().ToLower() == "true")
            {
                Warning.Text = Catalog.GetValue("PopupErrorMessage");
                Warning.IsVisible = false;
                MessagingCenter.Send<PopupView, string>(this, "UserInstitutionSet", InstitutionEntry.Text);
                await PopupNavigation.Instance.PopAsync(true);
                await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("PopupUserSettedInstitution"));
            }
            else
            {
                Warning.Text = Catalog.GetValue("PopupErrorMessage");
                Warning.IsVisible = true;
                //make text visible
            }
    }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // ### Methods for supporting animations in your popup page ###

        // Invoked before an animation appearing
        protected override void OnAppearingAnimationBegin()
        {
            base.OnAppearingAnimationBegin();
        }

        // Invoked after an animation appearing
        protected override void OnAppearingAnimationEnd()
        {
            base.OnAppearingAnimationEnd();
        }

        // Invoked before an animation disappearing
        protected override void OnDisappearingAnimationBegin()
        {
            base.OnDisappearingAnimationBegin();
        }

        // Invoked after an animation disappearing
        protected override void OnDisappearingAnimationEnd()
        {
            base.OnDisappearingAnimationEnd();
        }

        protected override Task OnAppearingAnimationBeginAsync()
        {
            return base.OnAppearingAnimationBeginAsync();
        }

        protected override Task OnAppearingAnimationEndAsync()
        {
            return base.OnAppearingAnimationEndAsync();
        }

        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return base.OnDisappearingAnimationBeginAsync();
        }

        protected override Task OnDisappearingAnimationEndAsync()
        {
            return base.OnDisappearingAnimationEndAsync();
        }

        // ### Overrided methods which can prevent closing a popup page ###

        // Invoked when a hardware back button is pressed
        protected async void OnCancelButtonPressed()
        {
            await PopupNavigation.Instance.PopAsync(true);
        }


        // Invoked when a hardware back button is pressed
        protected override bool OnBackButtonPressed()
        {
            // Return true if you don't want to close this popup page when a back button is pressed
            //return base.OnBackButtonPressed();
            return false;
        }

        // Invoked when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return false if you don't want to close this popup page when a background of the popup page is clicked
            return base.OnBackgroundClicked();
        }
    }
}