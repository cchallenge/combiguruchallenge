﻿﻿using System;
namespace EduApp.Core
{
	public static class Globals
	{
 
        public static string PSAppServerUrl = "https://eduappwstest.azurewebsites.net";
        //https://combiguru.azurewebsites.net/
        public static string UnoxAppServerUrl = "https://ulg.unox.com/api";

		public static string PrivacyLink = "https://prod-16.westeurope.logic.azure.com/workflows/99150aca3e6a48b18e173006bbc94038/triggers/manual/paths/invoke/lang/{0}/?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=bnzcWvzp9EjLWtROlAAqE9HDPmpoMhsazQDI78oawms";

		public static string AppLanguage = "en";
		public static string ApiKeyRest = "T97O09X2";

		public static readonly string NotDoneChallenge = "#D8D8D8";
		public static readonly string DoneChallenge = "#FECE5E";
		public static readonly string LineNotChallenge = "#1A2129";

		public static readonly string SectionKey = "CurrentSection";
		public static readonly string LevelKey = "CurrentLevel";
		public static readonly string SectionCellModelKey = "SectionCellModel";
		public static readonly string NextLevelCellModelKey = "NextLevel";

        public static readonly string CurrentChallenge = "CurrentChallenge";
        public static readonly string IndexOfLevel = "IndexOfLevelKey";

		public static readonly string SpotRegex = "/?spot/*(?<nome>.+)";
		public static readonly string NextLevelId = "NextLevelId";

		#region AnswerState Colors
		public static readonly string ColorCorretto = "#3CA959";
		public static readonly string ColorDopoCorretto = "#9BD5AB";
		public static readonly string ColorSbagliato = "#C70C0C";
		public static readonly string ColorInAttesa = "#FECE5E";
		#endregion

		#region PushNotificationAzureInfo

#if !DISTRIBUTION
		public static readonly string EndPoint = "sb://eduapp-push.servicebus.windows.net/";
		public static readonly string SharedAccessKey = "EAynomX6sL55gVmage9HRhoFdybCnN/MQKQfoPPKPFU=";
		public static readonly string NotificationHubPath = "EduAppPush";
#else
        public static readonly string EndPoint = "sb://mobileappsnotificationshub.servicebus.windows.net/";
        public static readonly string SharedAccessKey = "JeudvjEwMpwvLKzuMDpqL8aASk77JBn3REzu2fOJHuU=";
        public static readonly string NotificationHubPath = "combigurupush";
#endif
		#endregion

		#region PushNotificationAndroid

#if !DISTRIBUTION
		public static readonly string ID_Mittente = "718046517057";
		public const string PackageNameApp = "com.psmobile.eduapp";
#else
        public static readonly string ID_Mittente = "953505151655";
        public const string PackageNameApp = "com.unox.combiguru";
#endif
		public static readonly string UnoxDefaultNotifMessage = "Unox Notification";
		public static readonly string CurrentCorrId = "CurrentCorrId";
		#endregion
	}
}