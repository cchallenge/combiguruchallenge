﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using AppCore.DeferredAppContentModule;
using AppCore.Interfaces;
using UnoxEduAppModule;
using Nostromo;
using Nostromo.Interfaces;

namespace EduApp.Core
{
    public class UnoxAppModule : IAppModule, IObservable<UserStats>
    {
        public IAppCoreContext Context { get; set; }
        UnoxAppEduModule edu { get; set; }
        DeferredAppContentModule Contents { get; set; }
        IAppCatalog Catalog { get; set; }
        IToastService toast { get; set; }

		readonly ICacheModule cache;
		readonly IUnoxAppAuthModule auth;
		readonly ILocalMachineAppModule machine;

		BehaviorSubject<UserStats> _currentStats;
		readonly IJsonSerializer json;

        public UnoxAppModule( UnoxAppEduModule edu, IUnoxAppAuthModule auth, IAppCatalog catalog, DeferredAppContentModule contents, ICacheModule cache, ILocalMachineAppModule machine, IJsonSerializer json)
		{
			this.json = json;
            this.auth = auth;
			this.edu = edu;
			this.Catalog = catalog;
			this.Contents = contents;
			this.cache = cache;
			this.machine = machine;

			_currentStats = new BehaviorSubject<UserStats>(null);

            toast = Nostromo.Container.Default.Resolve<IToastService>();
		}

		string UnoxLink { get; set;}
		public async Task<string> GetUnoxLink() {
			if (String.IsNullOrEmpty(UnoxLink))
				UnoxLink = await edu.UnoxUrl();

			return UnoxLink;
		}

		public async Task<SectionModel> GetSectionModel(Section section)
		{
			if (section != null)
			{
				var sectionModel = new SectionModel()
				{
					Image = section.Image != null ? Contents.GetLocalPath(section.Image) : null,
					Title = section.Title,
					CorrID = section.CorrelationId
				};
				return sectionModel;
			}
			return null;
		}

		public async Task<LearningModel> GetLearningPath(Func<string, Task> onError)
		{
			System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Chiamo il WS");
			var list = new List<LearningCellModel>();
			var res = await edu.LearningPath(auth.GetUserMeasureUnit(), Catalog.GetLanguage());
			if (res != null)
			{
				var contentsArray = res.Contents.Where (x => !String.IsNullOrEmpty (x)).ToArray ();

                var backFetch = contentsArray.Where(Contents.IsFetched).ToArray();
                var frontFetch = contentsArray.Except(backFetch).ToArray();

                //Effettuo il fetch in foregroun di tutti i contents che non sono mai stati scaricati
                if (frontFetch.Length>0)
                    await Contents.Fetch(frontFetch, null);


                if (!contentsArray.All(Contents.IsFetched))
                {
                    await onError(Catalog.GetValue("ConnectionNeeded"));
                }


                #pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
				//Avvio l'aggiornamento (Verifica) in background, dei contenuti già scaricati
				if (backFetch.Length > 0)
                    Task.Run(()=> Contents.Fetch(backFetch, null));
                #pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed


				System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Finito di scaricare le immagini");
				foreach (var l in res.Levels)
				{
					list.Add(new LearningCellModel()
					{
						ID = l.CorrelationId,
						TitleLesson = l.Title,
						LessonImage = l.Image != null ? Contents.GetLocalPath(l.Image) : null,
						TitleChallenge = l.ChallengeTitle,
						ChallengeImage = l.ChallengeImage != null ? Contents.GetLocalPath(l.ChallengeImage) : null,
						CurrentLevel = l
					});
				}
				System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Ritorno l'oggetto");
				return new LearningModel()
				{
					LearningCell = list
				};
			}

			await onError(Catalog.GetValue("ConnectionNeeded"));
			return null;
		}

        // _ext_
        public async Task<LeaderboardModel> GetLeaderboardPath(Func<string, Task> onError)
        {
            System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Chiamo il WS");
            var list = new List<LeaderboardCellModel>();
            var res = await edu.LearningPath(auth.GetUserMeasureUnit(), Catalog.GetLanguage());
            if (res != null)
            {
                //System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Ho ricevuto il json");
                //if (res.Contents != null)
                //await Contents.Fetch(res.Contents, null); 
                var contentsArray = res.Contents.Where(x => !String.IsNullOrEmpty(x)).ToArray();

                var backFetch = contentsArray.Where(Contents.IsFetched).ToArray();
                var frontFetch = contentsArray.Except(backFetch).ToArray();

                //Effettuo il fetch in foregroun di tutti i contents che non sono mai stati scaricati
                if (frontFetch.Length > 0)
                    await Contents.Fetch(frontFetch, null);


                if (!contentsArray.All(Contents.IsFetched))
                {
                    await onError(Catalog.GetValue("ConnectionNeeded"));
                }


#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                //Avvio l'aggiornamento (Verifica) in background, dei contenuti già scaricati
                if (backFetch.Length > 0)
                    Task.Run(() => Contents.Fetch(backFetch, null));
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed


                System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Finito di scaricare le immagini");
                foreach (var l in res.Levels)
                {
                    list.Add(new LeaderboardCellModel()
                    {
                        ID = l.CorrelationId,
                        LeaderboardPosition = "fake",
                        LeaderboardNickname = l.Title,
                        LeaderboardScore = "fake"
                    });
                }
                System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Ritorno l'oggetto");
                return new LeaderboardModel()
                {
                    LeaderboardCell = list
                };
            }

            await onError(Catalog.GetValue("ConnectionNeeded"));
            return null;
        }

        // _ext_
        public async Task<LeaderboardModel> GetLeaderboard(Func<string, Task> onError)
		{
			System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Chiamo il WS");
			var list = new List<LeaderboardCellModel>();

            String institutionCode = (Context.UserIdentity as _ext_UnoxUserIdentity).Institution;
            if(institutionCode.Equals("N/A"))
            {
                institutionCode = null;
            }
            var leaderboard = await edu._ext_GetInstitutionRanking(institutionCode);
            

            if (leaderboard != null)
            {
                var rankings = leaderboard.Ranking;

                int id = 0;
				foreach (var l in rankings)
				{
                    //System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Aggiungo elemento: {id}");
                    list.Add(new LeaderboardCellModel()
                    {
                        ID = id.ToString(),
                        LeaderboardUniqueIdentifier = l.UniqueIdentifier.ToString(),
                        LeaderboardPosition = l.RankPosition.ToString(),
                        LeaderboardNickname = l.Nickname,
                        LeaderboardScore = l.Points.ToString()
                    });
                    id++;
				}
				return new LeaderboardModel()
				{
                    LeaderboardCell = list
				};
			}

			await onError(Catalog.GetValue("ConnectionNeeded"));
			return null;
        }

        // _ext_
        public async Task<ChallengesListModel> GetChallengesListPath(Func<string, Task> onError)
        {
            System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Chiamo il WS");
            var list = new List<ChallengesListCellModel>();
            var res = await edu._ext_GetChallenges(auth.GetUserMeasureUnit(), Catalog.GetLanguage());

            if (res != null)
            {
                System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Finito di scaricare le immagini");
                foreach (var l in res)
                {
                    list.Add(new ChallengesListCellModel()
                    {
                        ID = l.CorrelationId,
                        TitleChallenge = l.Title,
                        ChallengeImage = l.Image,
                        DateFrom = l.DateFrom,
                        DateTo = l.DateTo,
                        Completed = l.Completed
                    });
                    //index each challenge in the context - used to retrieve data later. Than before instantiating the LessonManager we set the current challenge id
                    Context.SetItem(l.CorrelationId, l);
                }
                System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Ritorno l'oggetto");
                return new ChallengesListModel()
                {
                    ChallengesListCell = list
                };
            }

            await onError(Catalog.GetValue("ConnectionNeeded"));
            return null;
        }


        // _ext_
        public async Task<int> GetUserPoints(Func<string, Task> onError = null)
        {
            System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Chiamo il WS");
            var userLevel = await edu._ext_UserLevel();


            if (userLevel != null)
            {
                var userPoints = userLevel._ext_UserPoints;
                return userPoints;
            }

            if(onError != null)
                await onError(Catalog.GetValue("ConnectionNeeded"));
            return 0;
        }

        /// <summary>
        /// Valore di default per l'expiration dello user stats (e forzare il refetch)
        /// </summary>
        /// <value>The default page cache expiration.</value>
        public TimeSpan DefaultPageCacheExpiration { get; private set; } = TimeSpan.FromMinutes(1);
		string MD5OldValue = string.Empty;

		bool isWorking = false;
		public async Task RequestUserStatsIfNeeded()
		{
			if (!isWorking)
			{
				isWorking = true;
				try
				{
                    //@@ CM Richiedo lo user stats e lo salvo in cache
                    //var value = await cache.Get("user_stats", async ct =>
                    //{
                    //	ct.ThrowIfCancellationRequested();

                    //	if (await machine.CheckConnectivity() == ConnectivityStatus.None)
                    //		throw new System.Net.WebException("ENOCONNECTION", System.Net.WebExceptionStatus.ConnectFailure);

                    //	var res = await edu.UserLevel();
                    //	ct.ThrowIfCancellationRequested();

                    //	return res;
                    //}).TryGet(DefaultPageCacheExpiration);


                    // EB / RP richiedo lo user stats aggiornato ogni volta, dalla cache lo prendo solo nel caso in cui non abbia connessione
                    var value = new UserStats();

                    if (await machine.CheckConnectivity() == ConnectivityStatus.None)
                    {
                        value = await cache.Get("user_stats", async ct =>
                       {
                           return new UserStats();

                       }).TryGet(DefaultPageCacheExpiration);
                    }

                    else
                    {
                        value = await edu._ext_UserLevel();
                    }

					var md5Value = auth.MD5(json.Serialize(value));
					if (value != null)
					{
						await auth.SetDataForNotificaions (value.UserId, Context.UserIdentity.UserName, Catalog.GetLanguage());
						//Confronto i due MD5 e notifico solo se è cambiato oppure se è la prima volta che ricevo il dato
						if (String.IsNullOrEmpty(MD5OldValue) || MD5OldValue != md5Value)
						{
							MD5OldValue = md5Value;
							_currentStats.OnNext(value);
						}
					}
				}
				catch (Exception ex)
				{
					System.Diagnostics.Debug.WriteLine(ex);
				}
				finally
				{
					isWorking = false;
				}
			}
		}

        public async Task GoToChallengePage(IAppCoreContext context, IPageNavigator navigator, Level currentLevel)
        {
            await navigator.PushModalAsync(Build.ViewModel(() => new ChallengePageViewModel(context, currentLevel)), true);
        }

        public async Task GoToExtChallengePage(IAppCoreContext context, IPageNavigator navigator, String correlationId)
        {
            await navigator.PushModalAsync(Build.ViewModel(() => new ExtChallengePageViewModel(context, correlationId)), true);
        }

        //public UserLevel ToUserLevel(UserStats stats)
        //{
        //	var totalLessons = 0;
        //	var totalLessonsFinished = 0;
        //	foreach (var s in stats.SectionsStats)
        //	{
        //		totalLessons += s.LessonsTotalNumber;
        //		totalLessonsFinished += s.LessonsFinishedNumber;
        //	}
        //	System.Diagnostics.Debug.WriteLine($"Ho terminato {totalLessonsFinished} su {totalLessons}");
        //	var perc = 0f;
        //	if (totalLessonsFinished > 0)
        //	{
        //		perc = ((float)totalLessonsFinished / (float)totalLessons) * 100;
        //	}
        //	return new UserLevel()
        //	{
        //		LevelId = stats.Level,
        //		LevelPercentage = perc
        //	};
        //}

        public async Task<IList<LessonHeader>> GetLessonsForSection(string sectionId)
		{
			var res = await edu._ext_Lessons(sectionId, auth.GetUserMeasureUnit(), Catalog.GetLanguage());
			if (res != null)
				return res.ToList();
			else
				return null;
		}

		public FinishedPageType GetFinishedPageType()
		{
			return finishedPageType;
		}

		FinishedPageType finishedPageType;
        public async Task<bool> SetFinished(string id, bool challenge, bool isExtChallenge = false, int correctAnswersCount = 1, int totalQuestionsCount = 1, bool isFinished = true)
		{
            //Oss. The challenge returns an _ext_ChallengeStat (is a _ext_UserStatsDefaultPath) while classical calls return an _ext_UserStatsDefaultPath (is a UserStats)

            var oldStats = _currentStats.Value;

			UserStats newStats = null;
            object response = null;
            if(isExtChallenge)
            {
                var chCorrId = (string)Context.GetItem(Globals.CurrentChallenge);
                var challengeHeader = (_ext_ChallengeHeader)Context.GetItem(chCorrId);
                if(!challengeHeader.Completed)
                { 
                    response = await edu._ext_SetChallengeFinished(id, auth.GetUserMeasureUnit(), Catalog.GetLanguage(), correctAnswersCount.ToString(), totalQuestionsCount.ToString());
                }
            }
            else if (challenge)
            {
                newStats = await edu._ext_SetLearningPathChallengeFinished(id);                
            }
            else
            {
                newStats = await edu._ext_SetLessonFinished(id);
            }

            if (newStats != null)
            {
                //XXX: update points
                (Context.UserIdentity as _ext_UnoxUserIdentity).Score = (newStats as _ext_UserStatsDefaultPath)._ext_UserPoints.ToString();
                _currentStats.OnNext(await cache.Get("user_stats", _ => Task.FromResult(newStats)).TryGet());
                UpdateFinishedPage(newStats, oldStats, challenge);
                return true;
            }
            //else. this is an extChallenge 
            if(response != null)
                (Context.UserIdentity as _ext_UnoxUserIdentity).Score = (response as _ext_ChallengeStat)._ext_UserPoints.ToString();
            finishedPageType = FinishedPageType.Level;
            return true;
		}

        void UpdateFinishedPage(UserStats newStat, UserStats oldStat, bool challenge) {

            // @@ CM di default il currentObject è la lesson
            finishedPageType = FinishedPageType.Lesson;
			var currentLevel = (Level)Context.GetItem(Globals.LevelKey);
			var oldLevel = oldStat.LevelsStat.FirstOrDefault(l => l.CorrelationId == currentLevel.CorrelationId);
			var newLevel = newStat.LevelsStat.FirstOrDefault(l => l.CorrelationId == currentLevel.CorrelationId);

			//@@ CM se ero il current level e la mia percentuale attuale è 1 e quella precendente era inferiore a 1 ho finito il livello
			if (oldLevel.IsCurrentLevel && newLevel.Percentuale >= 1 && oldLevel.Percentuale < 1)
			{
				finishedPageType = FinishedPageType.Level;
			} else {
				// @@ CM se sono il currentLevel potrei aver concluso una sezione
				if (!challenge && newLevel.IsCurrentLevel) {
					var currentSection = (Section)Context.GetItem(Globals.SectionKey);
					var oldSection = oldLevel.SectionsStat.FirstOrDefault(s => s.CorrelationId == currentSection.CorrelationId);
					var newSection = newLevel.SectionsStat.FirstOrDefault(s => s.CorrelationId == currentSection.CorrelationId);

					// @@ CM se la percentuale della sezione corrente era inferiore a 1 e ora è >= 1 ho finito una sezione
					if (newSection.Percentuale >= 1 && oldSection.Percentuale < 1) {
						finishedPageType = FinishedPageType.Section;
					}
				}
			}
		}

		public IDisposable Subscribe(IObserver<UserStats> observer)
		{
			return _currentStats.ObserveOn(SynchronizationContext.Current).Subscribe(observer);
		}
	}
}

public enum FinishedPageType
{
	Level,
	Section,
	Lesson,
    Challenge
}
