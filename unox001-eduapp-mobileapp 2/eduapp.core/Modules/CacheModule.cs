﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using AppCore.Interfaces;
using AppCore.Logging;
using EduApp.Core;

namespace EduApp.Core
{
	public interface ICacheOperation<T> where T : class
	{
		/// <summary>
		/// Se la cache non è expired, restituisce il dato in cache, altrimenti fa il get
		/// </summary>
		/// <returns>The or get.</returns>
		/// <param name="cacheExpiration">Cache expiration.</param>
		Task<T> CacheOrGet(TimeSpan cacheExpiration = default(TimeSpan), TimeSpan fallBackToCacheIfExpired = default(TimeSpan));
		/// <summary>
		/// Prova a fare il get e se non riesce cerca di utilizzare la cache 
		/// </summary>
		/// <returns>The get.</returns>
		/// <param name="fallBackToCache">Fall back to cache.</param>
		Task<T> TryGet(TimeSpan fallBackToCache = default(TimeSpan));

		/// <summary>
		/// Utilizza solo la cache
		/// </summary>
		Task<T> Cache();

	}

	public interface ICacheModule : IAppModule
	{
		ICacheOperation<T> Get<T>(string cacheKey, Func<CancellationToken, Task<T>> fetchFunction) where T : class;
		Task Set<T>(string cacheKey, T value) where T : class;


		Task InvalidateCacheFor(string cacheKey);
		Task SetLastModified(string cacheKey, DateTime lastModified);
		Task<DateTime> GetLastModified(string cacheKey);
	}

	public class CacheModule : ICacheModule
	{
		class cacheOp<T> : ICacheOperation<T> where T : class
		{
			public CancellationToken ct = CancellationToken.None;
			public string key { get; set; }
			public Func<CancellationToken, Task<T>> fetchFunction { get; set; }
			public ILog logger { get; set; }
			public ILocalStorageAppModule storage { get; set; }
			public IJsonSerializer serializer { get; set; }

			DateTime getLastModified()
			{
				string filePath = Path.Combine("cachemodule", $"{key}.json");
				if (storage.Exists(StorageType.Cache, filePath))
				{
					return storage.GetLastModifiedTimeUtc(StorageType.Cache, filePath);
				}

				return DateTime.MinValue;
			}

			async Task<T> loadFromCache()
			{
				var result = default(T);
				string filePath = Path.Combine("cachemodule", $"{key}.json");
				if (storage.Exists(StorageType.Cache, filePath))
				{
					try
					{
						var cacheContent = await storage.ReadAllText(StorageType.Cache, filePath);
						if (!String.IsNullOrEmpty(cacheContent))
						{
							result = serializer.Deserialize<T>(cacheContent);
						}
					}
					catch (Exception ex)
					{
						logger.Error(() => $"non è stato possibile caricare [{key}] dalla cache: {ex}");
					}
				}
				return result;
			}

			async Task storeToCache(T content)
			{
				string filePath = Path.Combine("cachemodule", $"{key}.json");

				if (storage.Exists(StorageType.Cache, filePath))
				{
					await storage.Delete(StorageType.Cache, filePath);
				}

				if (content != null)
				{
					var cacheContent = serializer.Serialize(content);
					await storage.WriteAllText(StorageType.Cache, filePath, cacheContent);
					storage.SetLastModifiedTimeUtc(StorageType.Cache, filePath, DateTime.UtcNow);
				}
			}

			async Task<T> fetch(CancellationToken ct)
			{
				var result = default(T);
				try
				{
					if (!ct.IsCancellationRequested)
					{
						result = await fetchFunction(ct);
					}
					if (result != null)
					{
						await storeToCache(result);
					}
				}
				catch (Exception ex)
				{
					logger.Error(() => $"non è stato possibile effettuare il fetch di [{key}]: {ex}");
				}

				return result;
			}

			public async Task<T> CacheOrGet(TimeSpan cacheExpiration = default(TimeSpan), TimeSpan fallBackToCacheIfExpired = default(TimeSpan))
			{

				if (cacheExpiration != default(TimeSpan) && (getLastModified() + cacheExpiration < DateTime.UtcNow))
					return await TryGet(fallBackToCacheIfExpired);

				var res = await Cache();
				if (res == null)
					res = await fetch(ct);

				return res;
			}

			public Task<T> Cache()
			{
				return loadFromCache();
			}


			public async Task<T> TryGet(TimeSpan fallBackToCache = default(TimeSpan))
			{
				if (fallBackToCache == default(TimeSpan))
					fallBackToCache = TimeSpan.Zero;

				using (var tcs = new CancellationTokenSource())
				{
					var fetchTask = fetch(tcs.Token);


					if (fallBackToCache > TimeSpan.Zero)
					{
						var timeOut = Task.Delay(fallBackToCache, tcs.Token);
						var taskCompleted = await Task.WhenAny(fetchTask, timeOut);

						if (taskCompleted == timeOut)
						{
							var cached = await Cache();
							if (cached != null)
							{
								return cached;
							}
						}
						else {
							tcs.Cancel();
						}
					}

					var res = await fetchTask;
					if (res == null)
					{
						res = await Cache();
					}

					return res;
				}
			}

		}



		public IAppCoreContext Context
		{
			get;
			set;
		}

		readonly ILog logger = LogProvider.GetLogger(typeof(CacheModule));
		readonly ILocalStorageAppModule storage;
		readonly IJsonSerializer serializer;

		public CacheModule(ILocalStorageAppModule storage, IJsonSerializer serializer)
		{
			this.serializer = serializer;
			this.storage = storage;
		}

		public ICacheOperation<T> Get<T>(string cacheKey, Func<CancellationToken, Task<T>> fetchFunction) where T : class
		{
			return new cacheOp<T>()
			{
				key = cacheKey,
				logger = logger,
				serializer = serializer,
				storage = storage,
				fetchFunction = fetchFunction
			};
		}

		public Task InvalidateCacheFor(string cacheKey)
		{
			return Task.Run(() =>
			{
				string filePath = Path.Combine("cachemodule", $"{cacheKey}.json");
				if (storage.Exists(StorageType.Cache, filePath))
				{
					storage.Delete(StorageType.Cache, filePath);
				}
			});
		}

		public Task SetLastModified(string cacheKey, DateTime lastModified)
		{
			return Task.Run(() =>
			{
				string filePath = Path.Combine("cachemodule", $"{cacheKey}.json");
				if (storage.Exists(StorageType.Cache, filePath))
				{
					storage.SetLastModifiedTimeUtc(StorageType.Cache, filePath, lastModified);
				}
			});
		}

		public async Task Set<T>(string cacheKey, T value) where T : class
		{
			await Get<T>(cacheKey, ct => Task.FromResult(value)).TryGet();
		}



		public Task<DateTime> GetLastModified(string cacheKey)
		{
			return Task.Run(() =>
			{
				string filePath = Path.Combine("cachemodule", $"{cacheKey}.json");
				if (storage.Exists(StorageType.Cache, filePath))
				{
					return storage.GetLastModifiedTimeUtc(StorageType.Cache, filePath);
				}
				return DateTime.MinValue;
			});
		}
	}
}

namespace AppCore
{

	public static class CacheModuleInitializer
	{
		public static IAppCoreBootstrapper EnableCacheModule(this IAppCoreBootstrapper bootstrapper)
		{
			bootstrapper.Register<ICacheModule, CacheModule>();
			return bootstrapper;
		}
	}
}