﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AppCore;
using AppCore.Interfaces;
using AppCore.Logging;
using Newtonsoft.Json.Linq;
using System.Globalization;
using UnoxEduAppModule;

namespace EduApp.Core
{
    public class UnoxAccountModule : IAppAuthModule, IUnoxAppAuthModule
    {
        const string APPLICATION = "EDU";

        ILocalMachineAppModule LM { get; set; }
        IPersistenceAppModule Pers { get; set; }
        IAppCatalog Catalog { get; set; }
        IRestAppModule Rest { get; set; }
        ILog logger = LogProvider.GetLogger("UnoxAccountModule");
        private static string UserKey = "userIdentity";
        private static string UserDataKey = "userData";
        private static string NICKNAME_FIELD_FIX = "PrivacySalesInfo";

        public static class ERRORS
        {
            public static string EUSERALREADYEXISTS = "EUSERALREADYEXISTS";
            public static string EDBERROR = "EDBERROR";
            public static string EPENDING = "EPENDING";
            public static string EINVALIDCREDENTIALS = "EINVALIDCREDENTIALS";
            public static string ENOCONNECTION = "ENOCONNECTION";
        }

        readonly IJsonSerializer serializer;

        public UnoxAccountModule(ILocalMachineAppModule lm, IPersistenceAppModule pers, IAppCoreContext context, IAppCatalog catalog, IRestAppModule rest, IJsonSerializer serializer)
        {
            this.serializer = serializer;
            LM = lm;
            Pers = pers;
            Catalog = catalog;
            Rest = rest;
            Context = context;
            Context.UserIdentity = Pers.GetObject<UnoxUserIdentity>(UserKey);
            _userData = Pers.GetObject<UserData>(UserDataKey);
            _fakeValidateTime = DateTime.MinValue;
        }

        public IAppCoreContext Context { get; set; }

        public void AuthenticateClient(HttpClient client)
        {
            lock (client)
            {
                client.DefaultRequestHeaders.Remove(Const.AuthHeader);
                client.DefaultRequestHeaders.Add(Const.AuthHeader, Globals.ApiKeyRest);
            }
        }

        public async Task<IUserIdentity> CheckLogin(bool performOnlineValidation = true)
        {
            return Context.UserIdentity;
        }

        public async Task<IUserIdentity> Login(string username, string password, bool remember)
        {
            throw new Exception("NON devi usare questa chiamata ma la UnoxLogin");
        }

        public Task<UnoxGenericResponse> _ext_UnoxSignup(string email, string password, string nickname, string country, string occupation)
        {
            return _ext_UnoxSignup(email, password, nickname, country, occupation, null);
        }

        public async Task<UnoxGenericResponse> _ext_UnoxSignup(string email, string password, string nickname, string country, string occupation,
                                                   string facebookId,
                                                   string facebookToken = null,
                                                   string firstName = null,
                                                   string lastName = null)
        {
            var safeRes = await SafeSuccessOrErrorOrResult(async () =>
            {
                string language = Catalog.GetLanguage();

                var postData = new Dictionary<string, object> {
					{ "application", APPLICATION }
                };

                if (String.IsNullOrEmpty(email) && string.IsNullOrEmpty(facebookId))
                {
                    throw new Exception("Either email or facebookId must be valid");
                }

                if (!String.IsNullOrEmpty(email))
                {
                    postData["email"] = email ?? "";
                }

                if (!String.IsNullOrEmpty(language))
                {
                    postData["language"] = language;
                }

                postData["password"] = password ?? "";

                //SE loggo con facebook non devo aggiornare la category
                if (String.IsNullOrEmpty(facebookId) || !String.IsNullOrEmpty(occupation))
                {
                    postData["category"] = occupation ?? "";
                }

                //SE loggo con facebook non devo aggiornare la country
                if (String.IsNullOrEmpty(facebookId) || !String.IsNullOrEmpty(country))
                {
                    postData["addressCountry"] = country ?? "";
                }

                var region = RegionInfo.CurrentRegion.ToString().ToUpper();
                postData["measureUnitProfile"] = getMeasureUnitProfile(region);

                if (!String.IsNullOrEmpty(facebookId))
                    postData["facebookId"] = facebookId;

                if (!String.IsNullOrEmpty(facebookToken))
                    postData["facebookToken"] = facebookToken;

                if (!String.IsNullOrEmpty(firstName))
                    postData["firstName"] = firstName;

                if (!String.IsNullOrEmpty(lastName))
                    postData["lastName"] = lastName;

                postData[NICKNAME_FIELD_FIX] = nickname;

                var res = new UnoxGenericResponse();

                var result = await Rest.Post($"{Globals.UnoxAppServerUrl}/Signup/", postData);

                if (result.Success)
                {
                    var user = result.ContentAs<UserData>();
                    if (user.Success == "true")
                    {
                        _userData = user;
                        if(!String.IsNullOrEmpty(facebookId))//facebook profile BE retrieval.
                        {
                            EnsureNicknameNotNull();
                            StoreUserData();
                            SaveUserPartial();//fix. needed to store UserIdentifier, used to retrieve points.
                            var resScore = await DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<UnoxAppEduModule>()._ext_GetUserProfile();
                            FacebookNicknameFix(resScore, nickname);
                            SaveUserData(resScore, true);
                        }
                        res.Success = true;
                    }
                    else
                    {
                        logger.Warn(() => user.ErrorMessage);
                        res.Success = false;
                        res.Error = user.ErrorMessage;
                    }
                }
                else
                {
                    logger.Error(() => result.Exception.ToString());
                    res.Success = result.Success;
                    res.Error = result.Exception.Message;
                }
                return res;
            });

            if (safeRes.Item3 != null)
                return safeRes.Item3;

            return new UnoxGenericResponse
            {
                Success = safeRes.Item1,
                Error = safeRes.Item2
            };
        }

        private void EnsureNicknameNotNull() {
            if (_userData != null)
                if (String.IsNullOrEmpty(_userData.PrivacySalesInfo))
                    _userData.PrivacySalesInfo = "Annonymous";
        }


        public async Task<UnoxGenericResponse> UnoxLogin(string email, string password)
        {
            var safeRes = await SafeSuccessOrErrorOrResult(async () =>
            {

                var res = new UnoxGenericResponse();
                logger.Trace(() => $"Login con username: {email} e password: {password}");

                var result = await Rest.Post($"{Globals.UnoxAppServerUrl}/login/", new Dictionary<string, object> {
                    { "email", email},
                    { "password", password}
                });

                if (result.Success)
                {
                    var user = result.ContentAs<UserData>();

                    if (user.Success == "true")
                    {
                        res.Success = true;
                        _userData = user;
                        _userData.Language = _userData.Language?.Trim();
                        EnsureNicknameNotNull();
                        StoreUserData();
                        SaveUserPartial();//fix. needed to store UserIdentifier, used to retrieve points.
                        var resScore = await DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<UnoxAppEduModule>()._ext_GetUserProfile();
                        SaveUserData(resScore);
                    }
                    else
                    {
                        logger.Warn(() => user.ErrorMessage);
                        res.Success = false;
                        res.Error = user.ErrorMessage;
                    }
                }
                else
                {
                    logger.Error(() => result.Exception.ToString());
                    res.Error = result.Exception.Message;
                    res.Success = result.Success;
                }

                return res;
            });

            if (safeRes.Item3 != null)
                return safeRes.Item3;

            return new UnoxGenericResponse
            {
                Success = safeRes.Item1,
                Error = safeRes.Item2
            };
        }

        public async Task SetDataForNotificaions(string userID, string email, string language)
        {
            System.Diagnostics.Debug.WriteLine("{0} {1} {2}", userID, email, language);
            await Rest.Post($"/account_login/update", new { UserId = userID, Email = email, Lang = language });
        }

        public UnoxUserIdentity SaveUserPartial(string nickname = null)
        {
            var res = new _ext_UnoxUserIdentity()
            {
                UserName = _userData.Email,
                UserIdentifier = _userData.Id,
                //facebook fix.
                Nickname = (!String.IsNullOrEmpty(nickname)) ? nickname : "",
            };

            Nostromo.Container.Default.Resolve<INotificationService>().RegisterForRemoteNotificationsWithTags(new string[] { res.UserIdentifier });

            Context.UserIdentity = res;
            Pers.SetObject(UserKey, res);

            return res;
        }

        public void FacebookNicknameFix(_ext_UserProfile userProfile, string nickname) {
            if (String.IsNullOrEmpty(userProfile.Nickname)) {
                userProfile.Nickname = nickname;
            }
        }

        public UnoxUserIdentity SaveUserData(_ext_UserProfile userProfile = null, bool facebookLogin = false)
        {
            var res = new _ext_UnoxUserIdentity()
            {
                UserName = _userData.Email,
                UserIdentifier = _userData.Id,
                //XXX: fix nickname/appData escape serialization
                Nickname = (facebookLogin)? userProfile.Nickname:_userData.PrivacySalesInfo,
                Score = (userProfile != null) ? userProfile.Score.ToString() : "0",
                Institution = (!String.IsNullOrEmpty(userProfile.Institution))? userProfile.Institution: "N/A"
            };
            Nostromo.Container.Default.Resolve<INotificationService>().RegisterForRemoteNotificationsWithTags(new string[] { res.UserIdentifier });

            Context.UserIdentity = res;
            Pers.SetObject(UserKey, res);

            return res;
        }

        public void Logout()
        {
            Nostromo.Container.Default.Resolve<INotificationService>().UnRegisterForRemote();
            Context.UserIdentity = null;
            _userData = null;
            StoreUserData();
            Pers.SetObject<UnoxUserIdentity>(UserKey, null);
        }


        public string MD5(string stringToConvert)
        {
            return AppCore.Helpers.Security.MD5GenerateHash(stringToConvert);
        }

        DateTime _fakeValidateTime;
        public async Task<UnoxValidateModel> UnoxValidate()
        {
            var res = new UnoxValidateModel();
            bool onlineValidationCompleted = false;
            System.Diagnostics.Debug.WriteLine("///validating new user accunt!");
            if (await LM.CheckConnectivity() != ConnectivityStatus.None)
            {
                try
                {
                    var result = await Rest.Post($"{Globals.UnoxAppServerUrl}/login/", new Dictionary<string, object> {
                    { "email", _userData?.Email},
                    { "token", _userData?.Token}
                });
                    System.Diagnostics.Debug.WriteLine("///user validation outcome: " + result.Success);
                    if (result.Success)
                    {
                        onlineValidationCompleted = true;
                        var user = result.ContentAs<UserData>();
                        System.Diagnostics.Debug.WriteLine("///user validation outcome with user dtype: " + user.Success);
                        if (user.Success == "true")
                        {
                            res.Status = ValidateStatus.activate;
                            _userData = user;
                            EnsureNicknameNotNull();
                            var resScore = await DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<UnoxAppEduModule>()._ext_GetUserProfile();
                            //facebook fix.
                            if (!String.IsNullOrEmpty(user.FacebookToken))
                            {
                                SaveUserPartial((Context.UserIdentity as _ext_UnoxUserIdentity).Nickname);
                                FacebookNicknameFix(resScore, (Context.UserIdentity as _ext_UnoxUserIdentity).Nickname);
                            }
                            else { 
                                SaveUserPartial();
                            }
                            SaveUserData(resScore, (!String.IsNullOrEmpty(user.FacebookToken)));
                            //account validation: retry or facebook login
                            await DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<UnoxAppEduModule>()._ext_UpdateOrSetNickname((Context.UserIdentity as _ext_UnoxUserIdentity).Nickname);
                        }
                        else
                        {
                            logger.Warn(() => user.ErrorMessage);
                            res.Error = user.ErrorMessage;

                            if (user.Error.Equals(ERRORS.EPENDING))
                            {
                                res.Status = ValidateStatus.pending_activation;
                            }
                            else
                            {

                                res.Status = ValidateStatus.none;
                            }
                        }
                    }
                    else
                    {
                        if (res.Status > 0)
                        {
                            onlineValidationCompleted = true;
                            logger.Error(() => result.Exception.ToString());
                            res.Error = result.Exception.Message;
                            res.Status = ValidateStatus.none;
                        }
                    }
                }
                catch (Exception ex)
                {
                    onlineValidationCompleted = false;
                    this.logger.Warn("Validation Exception: " + ex.ToString());
                }
            }


            if (!onlineValidationCompleted)
            {

                if (_userData != null)
                {
                    SaveUserData();
                    res.Status = ValidateStatus.activate;
                }
                else
                {
                    res.Status = ValidateStatus.none;
                }
            }

            return res;
        }

        public async Task<UnoxGenericResponse> UnoxResetPassword(string email)
        {
            var safeRes = await SafeSuccessOrErrorOrResult(async () =>
            {
                var res = new UnoxGenericResponse();
                logger.Trace(() => $"Reset password con email: {email}");

                var result = await Rest.Post($"{Globals.UnoxAppServerUrl}/Resetp/", new Dictionary<string, object> {
                    { "email", email}
                });

                if (result.Success)
                {
                    var user = result.ContentAs<UserData>();

                    if (user.Success == "true")
                    {
                        res.Success = true;
                        res.Error = !String.IsNullOrEmpty(user.ErrorMessage) ? user.ErrorMessage : Catalog.GetValue("CorrectResetPwd");
                    }
                    else
                    {

                        logger.Warn(() => user.ErrorMessage);

                        res.Success = false;
                        res.Error = !String.IsNullOrEmpty(user.ErrorMessage) ? user.ErrorMessage : Catalog.GetValue("UncorrectResetPwd");
                    }
                }
                else
                {

                    logger.Error(() => result.Exception.ToString());
                    res.Success = result.Success;
                    res.Error = result.Exception.Message;
                }

                return res;
            });

            if (safeRes.Item3 != null)
                return safeRes.Item3;

            return new UnoxGenericResponse
            {
                Success = safeRes.Item1,
                Error = safeRes.Item2
            };
        }

        UserData _userData;
        public UserData UserData
        {
            get { return _userData; }
        }

        private void StoreUserData()
        {
            if (_userData != null)
            {
                _userData.Language = _userData.Language?.Trim();
                Catalog.SetLanguage(_userData.Language);
                if (String.IsNullOrEmpty(_userData.Password))
                    _userData.Password = null;
            }
            Pers.SetObject(UserDataKey, _userData);
        }


        async Task<Tuple<bool, string, TResult>> SafeSuccessOrErrorOrResult<TResult>(Func<Task<TResult>> delegateFunc)
        {
            //Se non c'è connessione restituisco la non connessione
            if (await LM.CheckConnectivity() == ConnectivityStatus.None)
                return new Tuple<bool, string, TResult>(false, ERRORS.ENOCONNECTION, default(TResult));

            try
            {
                return new Tuple<bool, string, TResult>(true, "", await delegateFunc());
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string, TResult>(false, ex.ToString(), default(TResult));
            }
        }

        public Task<UnoxGenericResponse> UnoxSignup(string email, string password, string country, string occupation)
        {
            return UnoxSignup(email, password, country, occupation, null);
        }


        public async Task<UnoxGenericResponse> UnoxSignup(string email, string password, string country, string occupation,
                                                           string facebookId,
                                                           string facebookToken = null,
                                                           string firstName = null,
                                                           string lastName = null
                                                          )
        {
            var safeRes = await SafeSuccessOrErrorOrResult(async () =>
            {
                string language = Catalog.GetLanguage();

                var postData = new Dictionary<string, object> {
				//	{ "email", email},
					//{ "password", password},
					//{ "addressCountry",  country },
					//{ "category", occupation },
					//{ "measureUnitProfile", getMeasureUnitProfile(country)},
					{ "application", APPLICATION }
                };

                if (String.IsNullOrEmpty(email) && string.IsNullOrEmpty(facebookId))
                {
                    throw new Exception("Either email or facebookId must be valid");
                }

                if (!String.IsNullOrEmpty(email))
                {
                    postData["email"] = email ?? "";
                }

                if (!String.IsNullOrEmpty(language))
                {
                    postData["language"] = language;
                }

                postData["password"] = password ?? "";


                //SE loggo con facebook non devo aggiornare la category
                if (String.IsNullOrEmpty(facebookId) || !String.IsNullOrEmpty(occupation))
                {
                    postData["category"] = occupation ?? "";
                }

                //SE loggo con facebook non devo aggiornare la country
                if (String.IsNullOrEmpty(facebookId) || !String.IsNullOrEmpty(country))
                {
                    postData["addressCountry"] = country ?? "";
                }

                var region = RegionInfo.CurrentRegion.ToString().ToUpper();
                postData["measureUnitProfile"] = getMeasureUnitProfile(region);

                if (!String.IsNullOrEmpty(facebookId))
                    postData["facebookId"] = facebookId;

                if (!String.IsNullOrEmpty(facebookToken))
                    postData["facebookToken"] = facebookToken;

                if (!String.IsNullOrEmpty(firstName))
                    postData["firstName"] = firstName;

                if (!String.IsNullOrEmpty(lastName))
                    postData["lastName"] = lastName;

                var res = new UnoxGenericResponse();
                logger.Trace(() => $"Signup con email: {email}, password: {password}, country: {country}, occupation: {occupation}");

                var result = await Rest.Post($"{Globals.UnoxAppServerUrl}/Signup/", postData);

                if (result.Success)
                {
                    var user = result.ContentAs<UserData>();

                    if (user.Success == "true")
                    {
                        _userData = user;
                        //var resScore = await DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<UnoxAppEduModule>()._ext_UserLevel();
                        SaveUserData();
                        res.Success = true;
                    }
                    else
                    {

                        logger.Warn(() => user.ErrorMessage);

                        res.Success = false;
                        res.Error = user.ErrorMessage;
                    }
                }
                else
                {

                    logger.Error(() => result.Exception.ToString());
                    res.Success = result.Success;
                    res.Error = result.Exception.Message;
                }

                return res;
            });

            if (safeRes.Item3 != null)
                return safeRes.Item3;

            return new UnoxGenericResponse
            {
                Success = safeRes.Item1,
                Error = safeRes.Item2
            };
        }

        List<string> imperialMeasureCountries = new List<string>()
        {
            "GB",
            "IN",
            "HK",
            "CA",
            "AU",
            "NZ",
            "IE",
            "MY",
            "PH",
            "LK",
            "ZA"
        };

        string getMeasureUnitProfile(string countryKey)
        {
            if(imperialMeasureCountries.Contains(countryKey.ToUpper()))
                return "2";
            return "1";
        }

        public Dictionary<string, string> GetCountries()
        {
            return HelperSerialization.GetCountriesFromJson("EduApp.Core.countries.json");
        }

        public void CreateRegionsDictionary()
        {
            HelperSerialization.CreateRegionsFromJson("EduApp.Core.regions.json");
        }

        public Dictionary<string, string> GetRegions(string val)
        {
            return HelperSerialization.GetRegionsFromCountryCode(val);
        }

        public Dictionary<string, string> GetLanguages()
        {
            //Currently only the English and the Italian language are fully supported.
            return new Dictionary<string, string>() {
                {"IT", "Italiano"},
                {"EN", "English"}
//                {"FR", "Français"},
//                {"ES", "Español"},
//                {"DE", "Deutsch" }
            };
        }

        public Dictionary<string, string> GetOccupations()
        {
            return new Dictionary<string, string>() {
                {"35", "StudentCategory"},
                {"4", "ChefCategory"},
                {"99", "NoProCategory"}
				/*
				 * TODO fixare traduzioni e chiavi che devono darci Unox
				{"1", "Student"},
				{"2", "Chef"},
				{"3", "Sous Chef"},
				{"4", "Professor"},
				{"5", "Manager"},
				{"6", "Owner"},
				{"7", "Cook"},
				{"8", "Other"}
				*/
			};
        }

        public Dictionary<string, string> GetTypeOfKitchen()
        {
            return new Dictionary<string, string>() {
                {"1", "Resturant"},
                {"2", "Shop"},
                {"3", "Dealer"},
                {"4", "Backery"},
                {"5", "School"},
                {"6", "Other"},
                {"0", "None"},
            };
        }

        public Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>() {
                {"1", "sisMet"}, // "gr/m/°C" cambiato riferimento nel resx perchè dava errore nella traduzione di caratteri speciali
				{"2", "sisImp"} // "lbs/in/°F"
			};
        }


        /// <summary>
        /// Restituisce l'unità di misura impostata correntemente nel profilo utente.
        /// Se non è mai stata impostata, restituisce "1" (sistema metrico)
        /// </summary>
        /// <returns>The user measure unit.</returns>
		public string GetUserMeasureUnit()
        {
            const string defaultMU = "1";

            var mu = UserData?.MeasureUnitProfile;

            if (String.IsNullOrEmpty(mu))
                mu = defaultMU;

            return mu;
        }

        public async Task<UnoxGenericResponse> _ext_UnoxUpdateUserData(UserData userData)
        {
            var safeRes = await SafeSuccessOrErrorOrResult(async () =>
            {

                userData.Id = this.Context.UserIdentity.UserIdentifier;
                userData.Token = this.UserData.Token;

                if (!UserData.Email.Equals(userData.Email))
                    return new UnoxGenericResponse { Success = false, Error = "ECANNOTCHANGEUSEREMAIL" };

                userData.Email = this.UserData.Email;

                //Uso postUserData, per avere solo i campi corretti da inviare
                var postUserData = serializer.Deserialize<PostUserData>(serializer.Serialize(userData));
                var dataToPost = serializer.Deserialize<IDictionary<string, object>>(serializer.Serialize(postUserData));

                //La password, la inviamo solo se modificata
                if (dataToPost.ContainsKey("password") && String.IsNullOrEmpty(dataToPost["password"]?.ToString()))
                {
                    dataToPost.Remove("password");
                }

                dataToPost["application"] = APPLICATION;

                var res = new UnoxGenericResponse();
                logger.Trace(() => $"Serialized UpdateUserData" + serializer.Serialize(dataToPost));
                logger.Trace(() => $"Deserialized UpdateUserData" + serializer.Serialize(dataToPost));

                var result = await Rest.Post($"{Globals.UnoxAppServerUrl}/Update/",
                                             dataToPost);

                if (result.Success)
                {
                    var user = result.ContentAs<UserData>();

                    if (user.Success == "true")
                    {
                        _userData = user;
                        _userData.Language = _userData.Language?.Trim();
                        //XXX: check if nickname has changed, issue update call to the BE
                        if (!(Context.UserIdentity as _ext_UnoxUserIdentity).Nickname.Equals(user.PrivacySalesInfo))
                        {
                            var nick_updated = await DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<UnoxAppEduModule>()._ext_UpdateOrSetNickname(user.PrivacySalesInfo);
                            //XXX: what if this call fails?
                        }
                        var resScore = await DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<UnoxAppEduModule>()._ext_GetUserProfile();
                        StoreUserData();
                        SaveUserData(resScore);
                        res.Success = true;
                    }
                    else
                    {
                        logger.Warn(() => user.ErrorMessage);
                        res.Success = false;
                        res.Error = user.ErrorMessage;
                    }
                }
                else
                {
                    logger.Error(() => result.Exception.ToString());
                    res.Success = result.Success;
                    res.Error = result.Exception.Message;
                }

                return res;
            });

            if (safeRes.Item3 != null)
                return safeRes.Item3;

            return new UnoxGenericResponse
            {
                Success = safeRes.Item1,
                Error = safeRes.Item2
            };
        }

        public async Task<UnoxGenericResponse> UnoxUpdateUserData(UserData userData)
        {
            var safeRes = await SafeSuccessOrErrorOrResult(async () =>
            {

                userData.Id = this.Context.UserIdentity.UserIdentifier;
                userData.Token = this.UserData.Token;

                if (!UserData.Email.Equals(userData.Email))
                    return new UnoxGenericResponse { Success = false, Error = "ECANNOTCHANGEUSEREMAIL" };

                userData.Email = this.UserData.Email;

                //Uso postUserData, per avere solo i campi corretti da inviare
                var postUserData = serializer.Deserialize<PostUserData>(serializer.Serialize(userData));
                var dataToPost = serializer.Deserialize<IDictionary<string, object>>(serializer.Serialize(postUserData));

                //La password, la inviamo solo se modificata
                if (dataToPost.ContainsKey("password") && String.IsNullOrEmpty(dataToPost["password"]?.ToString()))
                {
                    dataToPost.Remove("password");
                }

                dataToPost["application"] = APPLICATION;

                var res = new UnoxGenericResponse();
                logger.Trace(() => $"UpdateUserData con " + serializer.Serialize(dataToPost));

                var result = await Rest.Post($"{Globals.UnoxAppServerUrl}/Update/",
                                             dataToPost);

                if (result.Success)
                {
                    var user = result.ContentAs<UserData>();

                    if (user.Success == "true")
                    {
                        _userData = user;
                        _userData.Language = _userData.Language?.Trim();
                        EnsureNicknameNotNull();
                        StoreUserData();
                        SaveUserData();//resScore._ext_UserPoints.ToString()
                        res.Success = true;
                    }
                    else
                    {
                        logger.Warn(() => user.ErrorMessage);
                        res.Success = false;
                        res.Error = user.ErrorMessage;
                    }
                }
                else
                {
                    logger.Error(() => result.Exception.ToString());
                    res.Success = result.Success;
                    res.Error = result.Exception.Message;
                }

                return res;
            });

            if (safeRes.Item3 != null)
                return safeRes.Item3;

            return new UnoxGenericResponse
            {
                Success = safeRes.Item1,
                Error = safeRes.Item2
            };
        }


        class facebookGraphResp
        {
            public string id { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string email { get; set; }
            public JObject picture { get; set; }
        }

        private string PrepNicknameFromFacebookData(string firstName, string lastName) {
            string nickname = "";
            if (!String.IsNullOrEmpty(firstName)) {
                nickname += firstName;
            }
            if (!String.IsNullOrEmpty(lastName))
            {
                nickname += " " + lastName;
            }
            return String.IsNullOrEmpty(nickname) ? "Announymous" : nickname;
        }

        public async Task<UnoxGenericResponse> SignUpWithFacebook(string facebookToken)
        {
            var safeRes = await SafeSuccessOrErrorOrResult(async () =>
            {
                var res = new UnoxGenericResponse();
                res.Success = false;
                var resp = await Rest.Get($"https://graph.facebook.com/me?fields=id,name,picture,first_name,last_name,email&access_token={facebookToken}");
                //Download dei dati
                //https://graph.facebook.com/me?fields=id,name,picture,first_name,last_name&access_token=EAACeyZARCpNoBALNeszHUrr1b1wStw6CDY73ZByxrEPxKMqXGhFAMKaTokV20OxwSygRUgcrZAUH2yQU6eaCF3iUK6dxQZB1YwN9Mhu8MsLcgdKZAfyyS1v74oThsYl6Q3ZBU7eLLd2AMDACkP0byPiXXQdmgR7GNNiEJDGrznF6zfNmE4mzdFQeYtPHgiCwIoA3DKOsBnZCMQIW403qbAtkZB3SzzZAdluwZD

                if (!resp.Success)
                {
                    res.Error = "Impossibile collegarsi a FB";//resp.Exception;
                }
                else
                {
                    var graphResp = resp.ContentAs<facebookGraphResp>();

                    if (graphResp == null)
                    {
                        res.Error = "Ricevuta risposta non valida dal server facebook: " + resp.Content;
                    }
                    else
                    {
                        var facebookId = graphResp.id;
                        var firstName = graphResp.first_name;
                        var lastName = graphResp.last_name;
                        var email = graphResp.email ?? "";

                        //Anche se la mail è nulla o vuota, la passo comunque alla signup, ci penserà il webservice a restituire l'errore in lingua
                        var signupResult = await _ext_UnoxSignup(email, null, PrepNicknameFromFacebookData(firstName, lastName), null, null, facebookId, facebookToken, firstName, lastName);

                        var avatar = $"https://graph.facebook.com/v2.8/{facebookId}/picture?type=normal";

                        if (signupResult.Success == false)
                        {
                            res.Error = signupResult.Error;
                        }
                        else
                        {

                            if (!String.IsNullOrEmpty(avatar))
                                await TryFetchFacebookAvatar(avatar);

                            res.Success = true;
                        }
                    }
                }
                return res;
            });

            if (safeRes.Item3 != null)
                return safeRes.Item3;

            return new UnoxGenericResponse
            {
                Success = safeRes.Item1,
                Error = safeRes.Item2
            };
        }

        async Task TryFetchFacebookAvatar(string avatar)
        {
            try
            {
                var contents = Context.ResolveModule<IAppContentModule>();
                using (var httpClient = new HttpClient(Context
                                                        .ResolveModule<IHttpClientHandlerFactory>()
                                                        .Create(Context, this), true))
                {

                    using (var stream = await httpClient.GetStreamAsync(avatar))
                    {
                        await contents.Store(GetAvatarContentKey(), stream);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Warn(() => $"TryFetchFacebookAvatar({avatar}) Exception: {ex}");
            }
        }

        public string GetAvatarContentKey()
        {
            var usId = Context.UserIdentity;
            return String.Format("/images/avatar/{0}.png", usId.UserIdentifier);
        }

        public void AuthenticateRequest(HttpRequestMessage obj)
        {
            obj.Headers.Add(Const.AuthHeader, Globals.ApiKeyRest);
        }
    }
}
