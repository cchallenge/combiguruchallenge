﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore;
using AppCore.Interfaces;
using AppCore.Logging;
using Newtonsoft.Json.Linq;
using Nostromo;
using Nostromo.Navigation;
using ReactiveUI;

namespace EduApp.Core
{
	public class WelcomePageViewModel : BaseViewModel
	{
		public override string BkgColorHex
		{
			get { return "#FFFFFF"; }
		}

		string titlePage;
		public string TitlePage
		{
			get { return titlePage; }
			set
			{
				this.RaiseAndSetIfChanged(ref titlePage, value);
			}
		}

		string subtitlePage;
		public string SubtitlePage
		{
			get { return subtitlePage; }
			set
			{
				this.RaiseAndSetIfChanged(ref subtitlePage, value);
			}
		}

		string disclaimerText;
		public string DisclaimerText
		{
			get { return disclaimerText; }
			set
			{
				this.RaiseAndSetIfChanged(ref disclaimerText, value);
			}
		}

		string readMoreText;
		public string ReadMoreText
		{
			get { return readMoreText; }
			set
			{
				this.RaiseAndSetIfChanged(ref readMoreText, value);
			}
		}

		string emailAddressText;
		public string EmailAddressText
		{
			get { return emailAddressText; }
			set
			{
				this.RaiseAndSetIfChanged(ref emailAddressText, value);
			}
		}

		string loginText;
		public string LoginText
		{
			get { return loginText; }
			set
			{
				this.RaiseAndSetIfChanged(ref loginText, value);
			}
		}

		string signupText;
		public string SignupText
		{
			get { return signupText; }
			set
			{
				this.RaiseAndSetIfChanged(ref signupText, value);
			}
		}

		public ICommand LoginCommand { get; set; }
		public ICommand SignupCommand { get; set; }
		public ICommand FacebookLoggedInCommand { get; set; }
		public IRestAppModule rest { get; set; }
		public IUnoxAppAuthModule authModule { get; set; }

		public string UnoxLink { get; set; }

		public WelcomePageViewModel(IAppCoreContext context) : base(context)
		{
			this.TitlePage = Catalog.GetValue("WelcomeTitle");
			this.SubtitlePage = Catalog.GetValue("WelcomeSubtitle");
			this.DisclaimerText = Catalog.GetValue("Disclaimer");
			this.ReadMoreText = Catalog.GetValue("ReadMore");
			this.EmailAddressText = Catalog.GetValue("EmailAddText");
			this.LoginText = Catalog.GetValue("Login");
			this.SignupText = Catalog.GetValue("Signup");
			try
			{
				LoginCommand = ReactiveCommand.CreateFromTask(() => Login());
				SignupCommand = ReactiveCommand.CreateFromTask(() => SignUp());
				FacebookLoggedInCommand = ReactiveCommand.CreateFromTask<string>(FacebookLoggedIn);
			}
			catch (Exception ex)
			{

			}
		}

		public override async Task Appearing()
		{
			base.Appearing();
			UnoxLink = await UAM.GetUnoxLink();
		}

		async Task Login()
		{
			await Navigator.PushAsync(Build.ViewModel(() => new LoginPageViewModel(Context)), true);
		}

		async Task SignUp()
		{
            await Navigator.PushAsync(Build.ViewModel(() => new SignupPageViewModel(Context)), true);
        }


		bool busy = false;
		public bool Busy
		{
			get { return busy; }
			set
			{
				this.RaiseAndSetIfChanged(ref busy, value);
			}
		}

		ILog logger = LogProvider.GetLogger(typeof(WelcomePageViewModel));

        public async Task FacebookLoggedIn(string token)
		{
			using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
			{
				Busy = true;
				logger.Trace(() => "logged in with token: " + token);
				var res = await authModule.SignUpWithFacebook(token);
				Busy = false;

				load.Dispose();
				if (res.Success)
				{
					await Navigator.SetNewRootAsync(Build.ViewModel(() => new ValidatePageViewModel(Context, true)), true);
				}
				else {
					await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(res.Error);
				}
			}
		}

	}
}
