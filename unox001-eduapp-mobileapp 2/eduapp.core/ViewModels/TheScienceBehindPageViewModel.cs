﻿using System;
using System.Windows.Input;
using AppCore.Interfaces;
using Nostromo.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public class TheScienceBehindPageViewModel : BaseViewModel
	{
		public ICommand CloseViewCommand { get; set; }

		public override string TextColorHex
		{
			get { return "#FFFFFF"; }
		}

		public override string BarBkgColorHex
		{
			get
			{
				return "#00000000";
			}
		}

		string htmlCode;
		public string HtmlCode
		{
			get { return htmlCode; }
			set
			{
				this.RaiseAndSetIfChanged(ref htmlCode, value);
			}
		}

		public TheScienceBehindPageViewModel(IAppCoreContext context, string html, string title) : base(context)
		{
			this.Title = title;
			CloseViewCommand = ReactiveCommand.CreateFromTask(async () => await Navigator.PopModalAsync(true));
			FormatHtml(html);
		}

		void FormatHtml(string embeddedHtml)
		{
			HtmlCode = String.Format(@"
			<html>
			<style type='text/css'>
			@font-face {{
				font-family: LatoRegular;
				src: url('Fonts/Lato-Regular.ttf')
			}}
			p, h1 {{
				font-family: LatoRegular;
				text-align: justify;
			}}
			</style>
			<body>
			{0}
			</body>
			</html>
			", embeddedHtml);
		}
	}
}
