﻿using System;
using System.Collections.Generic;
using ReactiveUI;

namespace EduApp.Core
{
	public abstract class ViewFactory
	{
		public ViewFactory()
		{
			if (Instance != null)
				throw new InvalidOperationException("non si può");

			Instance = this;
		}

		readonly Dictionary<Type, Type> ModelViews = new Dictionary<Type, Type>();

		public virtual Type GetViewTypeForModel(ReactiveObject it)
		{
			return GetViewTypeForModel(it.GetType());
		}
		public virtual Type GetViewTypeForModel(Type mType)
		{
			return this.ModelViews[mType];
		}

		public static ViewFactory Instance
		{
			get;
			private set;
		}

		public void Initialize()
		{
			RegisterDefaultView();
		}

		public abstract void RegisterDefaultView();


		public virtual void Add<TViewModel, TView>()
			where TViewModel : IFormItemViewModel
		{

			this.ModelViews[typeof(TViewModel)] = typeof(TView);
		}
	}
}
