﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using MvvmHelpers;
using ReactiveUI;

namespace EduApp.Core
{
	public static class TreeHelper
	{
		public static IEnumerable<IFormItemViewModel> OnRevaluateList(IEnumerable<IFormItemViewModel> gerarchia)
		{
			var list = new List<IFormItemViewModel>();
			//se ci sono dei IViewModelContainer prendo la loro lista
			//se no prendo loro
			foreach (var elem in gerarchia)
			{
				if (elem is IFormItemContainerViewModel)
					list.AddRange((elem as IFormItemContainerViewModel).ListViewModel);
				else
					list.Add(elem);
			}
			return list;
		}

		public static CompositeDisposable ObserveTreeChange(IEnumerable<IFormItemViewModel> models, Action onTreeChange)
		{
			var token = new CompositeDisposable();
			foreach (var elem in models)
			{
				if (elem is IFormItemContainerViewModel)
				{
					//Quando cambia il tree di un figlio scateno il mio tree
					token.Add((elem as IFormItemContainerViewModel).WhenAnyValue(e => e.LastChange).Subscribe((obj) => onTreeChange()));
				}
			}

			return token;
		}

		public static IObservable<DateTime> CreateTreeChangeObservable(IEnumerable<IFormItemViewModel> models)
		{
			return Observable.Merge(models
									.OfType<IFormItemContainerViewModel>()
									.Select(e => e.WhenAnyValue(el => el.LastChange)));
		}

		public static bool CompareCollection(ExtraObservableCollection<IFormItemViewModel> obs, List<IFormItemViewModel> list, bool useReplaceAlways = true)
		{
			var equalsFromDown = 0;
			var equalsFromUp = 0;

			while (equalsFromDown < list.Count 
			       && equalsFromDown < obs.Count
			       && list[equalsFromDown] == obs.ElementAt(equalsFromDown))
				equalsFromDown++;

			if (equalsFromDown >= list.Count && equalsFromDown >= obs.Count)
				return false;


			/// ATTENZIONE QUI USO SEMPRE IL REPLACE ALWAYS. PER LE ANIMAZIONI CON LO SCROLL
			/// SE PERO' VOGLIAMO LE ANIMAZIONI, E NON USIAMO LO SCROLL, ALLORA FUNZIONA MEGLIO 
			/// L'AGGIUNGI/RIMUOVI ANIMATO
			if (useReplaceAlways) {
				obs.ReplaceRange (list);
				return true;
			}



			while (equalsFromUp < list.Count
				   && equalsFromUp < obs.Count
				   && list[list.Count-1 - equalsFromUp] == obs.ElementAt(obs.Count-1 - equalsFromUp))
				equalsFromUp++;


			var rangeSource = list.Skip(equalsFromDown).Take(list.Count - equalsFromDown - equalsFromUp).ToArray();
			var rangeTarget = obs.Skip(equalsFromDown).Take(obs.Count - equalsFromDown - equalsFromUp).ToArray();

			if ((rangeSource.Length == 0) && (rangeTarget.Length>0))
			{
				//Oggetti rimossi
				obs.RemoveRange(rangeTarget);
			}
			else if ((rangeTarget.Length == 0 ) && (rangeSource.Length > 0))
			{
				//Oggetti aggiunti
				if (equalsFromDown == 0)
				{
					obs.AddRange(rangeSource);
				}
				else {
					obs.AddRange(rangeSource, equalsFromDown);
				}
			}
			else 
			{
				//Oggetti aggiunti e rimossi
				obs.ReplaceRange(list);
				return true;
			}

			return false;
		}
	}
}
