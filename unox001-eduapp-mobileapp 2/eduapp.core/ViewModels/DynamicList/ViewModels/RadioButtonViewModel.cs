﻿using System;
using MvvmHelpers;
using ReactiveUI;

namespace EduApp.Core
{
	public class RadioButtonViewModel : BaseCellViewModel
	{
		public RadioButtonViewModel(int nColumns)
		{
			Columns = nColumns;
		}

		int columns;
		public int Columns
		{
			get { return columns; }
			set
			{
				this.RaiseAndSetIfChanged(ref columns, value);
			}
		}

		ObservableRangeCollection<RadioModel> items;
		public ObservableRangeCollection<RadioModel> Items
		{
			get { return items; }
			set
			{
				this.RaiseAndSetIfChanged(ref items, value);
			}
		}

		public string SelectedKey { get; set;}
	}

	public class RadioModel : ReactiveObject{
		bool selected;
		public bool Selected
		{
			get { return selected; }

			set
			{
				this.RaiseAndSetIfChanged(ref selected, value);
			}
		}

		public string Text { get; set;}
		public string Key { get; set;}
	}
}
