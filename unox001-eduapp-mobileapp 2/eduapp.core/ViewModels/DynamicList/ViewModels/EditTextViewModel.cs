﻿using System;
using ReactiveUI;

namespace EduApp.Core
{
	public class EditTextViewModel : BaseCellViewModel
	{
		public EditTextViewModel ():base() { 
			ItemCommand = ReactiveCommand.CreateFromTask (async (a) => this);
		}

		bool isEnabled;
		public bool IsEnabled
		{
			get { return isEnabled; }
			set
			{
				this.RaiseAndSetIfChanged(ref isEnabled, value);
			}
		}

		bool isPwd;
		public bool IsPwd 
		{
			get { return isPwd; }
			set {
				this.RaiseAndSetIfChanged (ref isPwd, value);
			}
		}

		string valueEditText;
		public string Value 
		{
			get { return valueEditText; }
			set {
				this.RaiseAndSetIfChanged (ref valueEditText, value);
			}
		}

		KeyboardType keyboardType;
		public KeyboardType KeyboardType {
			get {
				return keyboardType;
			}

			set {
				this.RaiseAndSetIfChanged (ref keyboardType, value);
			}
		}
	}

	public enum KeyboardType
	{
		Default,
		Text,
		Chat,
		Url,
		Email,
		Telephone,
		Numeric
	}
}
