﻿using System;
using System.Linq;

namespace EduApp.Core
{
	public class ComboSelectedWithImageViewModel : ComboViewModel
	{
		public ComboSelectedWithImageViewModel ()
		{
		}

		public void SetSelectedItem (string key)
		{
			if (Source.ContainsKey (key)) {
				var currentValue = Source [key];
				SelectedItem = Collection.ToList ().IndexOf (currentValue);
			}
		}
	}
}
