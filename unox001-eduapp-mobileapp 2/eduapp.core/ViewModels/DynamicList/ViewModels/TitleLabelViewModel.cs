﻿using System;
using ReactiveUI;

namespace EduApp.Core
{
	public class TitleLabelViewModel : BaseCellViewModel
	{
		string titleFirstRow;
		public string TitleFirstRow {
			get { return titleFirstRow; }
			set {
				this.RaiseAndSetIfChanged (ref titleFirstRow, value);
			}
		}

		string titleSecondRow;
		public string TitleSecondRow {
			get { return titleSecondRow; }
			set {
				this.RaiseAndSetIfChanged (ref titleSecondRow, value);
			}
		}
	}
}
