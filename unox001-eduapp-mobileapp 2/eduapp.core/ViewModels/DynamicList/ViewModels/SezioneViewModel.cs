﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using ReactiveUI;

namespace EduApp.Core
{
	public class SezioneViewModel : BaseCellViewModel, IFormItemContainerViewModel
	{
		CompositeDisposable token;
		public SezioneViewModel()
		{
			//Al tap sul oggetto cambio IsExpanded
			ItemCommand = ReactiveCommand.Create(()=> IsExpanded = !IsExpanded,
			                                     outputScheduler: System.Reactive.Concurrency.ImmediateScheduler.Instance);
		}

		IEnumerable<IFormItemViewModel> child;
		public IEnumerable<IFormItemViewModel> Children
		{
			get { return child; }
			set
			{
				child = value;
				HandleChild();
			}
		}

		void HandleChild()
		{
			if (token != null)
			{
				token.Dispose();
				token = null;
			}

			if (child.Count() > 0)
			{
				token = TreeHelper.ObserveTreeChange(child, () =>
				{
					this.LastChange = DateTime.Now;
				});

				foreach (var c in child) {
					if (c is IFormItemContainerViewModel) { 
						if((c as IFormItemContainerViewModel).Children == null)
							throw new InvalidOperationException("Un oggetto di tipo IFormItemContainerViewModel deve avere dei children");
					}
				}
			}
		}

		bool isExpanded;
		public bool IsExpanded
		{
			get { return isExpanded; }

			set
			{
				if (!value)
				{
					//se mi sto chiudendo notifico ai miei figli di chiudersi
					CollapseAllChildren();
				}
				this.RaiseAndSetIfChanged (ref isExpanded, value);

				this.LastChange = DateTime.Now;

			}
		}

		void CollapseAllChildren()
		{
			foreach (var c in Children.OfType<SezioneViewModel>())
				c.IsExpanded = false;
		}

		public IEnumerable<IFormItemViewModel> ListViewModel
		{
			get
			{
				var list = new List<IFormItemViewModel>();
				//di default torno sempre me stesso
				list.Add(this);

				if (IsExpanded)
				{
					//Se sono espanso torno anche tutti i miei figli
					list.AddRange(TreeHelper.OnRevaluateList(Children));
				}

				return list;
			}
		}

        DateTime tree;
		public DateTime LastChange
		{
			get { return tree; }
			set
			{
				this.RaiseAndSetIfChanged(ref tree, value);
			}
		}
	}
}
