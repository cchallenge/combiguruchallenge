﻿using System;
using ReactiveUI;

namespace EduApp.Core
{
	public class LastLearningCellViewModel : BaseLearningCellViewModel
	{
		public LastLearningCellViewModel()
		{
		}

		string lineImage;
		public string LineImage
		{
			get { return lineImage; }
			set
			{
				this.RaiseAndSetIfChanged(ref lineImage, value);
			}
		}
	}
}
