﻿using System;
using System.Windows.Input;
using ReactiveUI;

namespace EduApp.Core
{
	public class BaseLearningCellViewModel : BaseCellViewModel
	{
		public string ID { get; set;}

		bool isEnabledButton;
		public bool IsEnabledButton
		{
			get { return isEnabledButton; }
			set
			{
				this.RaiseAndSetIfChanged(ref isEnabledButton, value);
			}
		}

		float percentage;
		public float Percentage
		{
			get { return percentage; }
			set
			{
				this.RaiseAndSetIfChanged(ref percentage, value);
			}
		}

		float sbiadimento;
		public float Sbiadimento
		{
			get { return sbiadimento; }
			set
			{
				this.RaiseAndSetIfChanged(ref sbiadimento, value);
			}
		}
		
		public ICommand LessonTapCommand { get; set; }

		public ICommand ChallengeTapCommand { get; set; }

		string lessonTitle;
		public string LessonTitle 
		{
			get { return lessonTitle; }
			set {
				this.RaiseAndSetIfChanged (ref lessonTitle, value);
			}
		}

		string challengeTitle;
		public string ChallengeTitle {
			get { return challengeTitle; }
			set {
				this.RaiseAndSetIfChanged (ref challengeTitle, value);
			}
		}

		string challengeImage;
		public string ChallengeImage {
			get { return challengeImage; }
			set {
				this.RaiseAndSetIfChanged (ref challengeImage, value);
			}
		}

		string lessonImage;
		public string LessonImage {
			get { return lessonImage; }
			set {
				this.RaiseAndSetIfChanged (ref lessonImage, value);
			}
		}

		string backgroundChallengeColor;
		public string BackgroundChallengeColor { 
			get { return backgroundChallengeColor; }
			set {
				this.RaiseAndSetIfChanged (ref backgroundChallengeColor, value);
			}
		}

		string backgroundLineColor;
		public string BackgroundLineColor
		{
			get { return backgroundLineColor; }
			set
			{
				this.RaiseAndSetIfChanged(ref backgroundLineColor, value);
			}
		}
	}
}
