﻿using System;
using System.Windows.Input;
using ReactiveUI;

namespace EduApp.Core
{
	public class PhotoViewModel : BaseCellViewModel
	{
		public PhotoViewModel ()
		{
		}

		public ICommand PhotoTapCommand { get; set; }

		string imageSource;
		public string ImageSource 
		{
			get { return imageSource; }
			set {
				imageSource = value;
				this.RaisePropertyChanged ();
			}
		}


	}
}
