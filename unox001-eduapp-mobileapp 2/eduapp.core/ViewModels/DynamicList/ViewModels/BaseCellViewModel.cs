﻿using System;
using System.Windows.Input;
using ReactiveUI;

namespace EduApp.Core
{
	public class BaseCellViewModel : ReactiveObject, IFormItemViewModel
	{
		public ICommand ItemCommand { get; set; }

		public BaseCellViewModel()
		{
			ItemCommand = ReactiveCommand.Create (() => { });
		}

		string title;
		public string Title
		{
			get { return title; }
			set
			{
				this.RaiseAndSetIfChanged(ref title, value);
			}
		}
	}
}
