﻿using System;
using ReactiveUI;

namespace EduApp.Core
{
	public class TypeOfKitchenViewModel : RadioButtonViewModel
	{
		public TypeOfKitchenViewModel(int columns) : base (columns)
		{
		}

		string kitchenNameTitle;
		public string KitchenNameTitle
		{
			get { return kitchenNameTitle; }
			set
			{
				this.RaiseAndSetIfChanged(ref kitchenNameTitle, value);
			}
		}

		string kitchenNameValue;
		public string KitchenNameValue
		{
			get { return kitchenNameValue; }
			set
			{
				this.RaiseAndSetIfChanged(ref kitchenNameValue, value);
			}
		}
	}
}
