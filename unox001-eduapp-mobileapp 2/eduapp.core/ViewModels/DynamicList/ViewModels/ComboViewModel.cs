﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using ReactiveUI;

namespace EduApp.Core
{
	public class ComboViewModel : BaseCellViewModel
	{
		Source source;
		public Source Source
		{
			get { return source; }
			set
			{
				source = value;
				var fixedCol = source.ToArray();
				lookup = fixedCol.Select(kv => kv.Key).ToArray();
				Collection = fixedCol.Select(kv => kv.Value).ToArray();
			}
		}

		IEnumerable<string> lookup;
		IEnumerable<string> collection;
		public IEnumerable<string> Collection
		{
			get { return collection; }
			set {
				if (collection?.Count () == value?.Count () && value != null) {
					var shouldReload = collection
						.Select ((string arg1, int arg2) => Tuple.Create (arg1, value.ElementAt (arg2)))
						.Any (x => x.Item1 != x.Item2);
					if (!shouldReload)
						collection = value;
						return;
				}

				this.RaiseAndSetIfChanged(ref collection, value); }
		}

		int selectedItem = -1;
		public int SelectedItem
		{
			get { return selectedItem; }
			set
			{
				if (value > -1) 
				{
					this.RaiseAndSetIfChanged(ref selectedItem, value);
					System.Diagnostics.Debug.WriteLine("INDEX PICKER {0}", value);
					if (lookup.Count() > value)
					{
						System.Diagnostics.Debug.WriteLine($"Ho {lookup.Count()} oggetti e voglio accedere a quello in posizione {value}");
						SelectedKey = lookup.ElementAt(value);
					}
				}
			}
		}

		public string SelectedKey { get; set;}

	}

	public class Source : Dictionary<string, string>
	{
		public Source(Dictionary<string, string> items) : base()
		{
			foreach (var i in items)
			{
				this.Add(i.Key, i.Value);
			}
		}
	}
}
