﻿using System;
using System.Windows.Input;
using ReactiveUI;

namespace EduApp.Core
{
    public class BaseChallengesListCellViewModel : BaseCellViewModel
    {
        public string ID { get; set; }

        bool isEnabledButton;
        public bool IsEnabledButton
        {
            get { return isEnabledButton; }
            set
            {
                this.RaiseAndSetIfChanged(ref isEnabledButton, value);
            }
        }

        float percentage;
        public float Percentage
        {
            get { return percentage; }
            set
            {
                this.RaiseAndSetIfChanged(ref percentage, value);
            }
        }

        string cellBgColor;
        public string CellBgColor
        {
            get { return cellBgColor; }
            set
            {
                this.RaiseAndSetIfChanged(ref cellBgColor, value);
            }
        }

        public ICommand GoToChallengeCommand { get; set; }
        
        string challengeTitle;
        public string ChallengeTitle
        {
            get { return challengeTitle; }
            set
            {
                this.RaiseAndSetIfChanged(ref challengeTitle, value);
            }
        }

        string challengeImage;
        public string ChallengeImage
        {
            get { return challengeImage; }
            set
            {
                this.RaiseAndSetIfChanged(ref challengeImage, value);
            }
        }

        string challengeDateTime;
        public string ChallengeDateTime
        {
            get { return challengeDateTime; }
            set
            {
                this.RaiseAndSetIfChanged(ref challengeDateTime, value);
            }
        }
    }
}
