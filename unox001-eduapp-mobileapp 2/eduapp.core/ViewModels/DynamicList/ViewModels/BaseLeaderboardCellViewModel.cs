﻿using System;
using System.Windows.Input;
using ReactiveUI;

namespace EduApp.Core
{
    public class BaseLeaderboardCellViewModel : BaseCellViewModel
    {
        public string ID { get; set; }

        string leaderboardPosition;
        public string LeaderboardPosition
        {
            get { return leaderboardPosition; }
            set
            {
                this.RaiseAndSetIfChanged(ref leaderboardPosition, value);
            }
        }

        string leaderboardPositionTitle;
        public string LeaderboardPositionTitle
        {
            get { return leaderboardPositionTitle; }
            set
            {
                this.RaiseAndSetIfChanged(ref leaderboardPositionTitle, value);
            }
        }

        string leaderboardNickname;
        public string LeaderboardNickname
        {
            get { return leaderboardNickname; }
            set
            {
                this.RaiseAndSetIfChanged(ref leaderboardNickname, value);
            }
        }

        string leaderboardNicknameTitle;
        public string LeaderboardNicknameTitle
        {
            get { return leaderboardNicknameTitle; }
            set
            {
                this.RaiseAndSetIfChanged(ref leaderboardNicknameTitle, value);
            }
        }

        string leaderboardScore;
        public string LeaderboardScore
        {
            get { return leaderboardScore; }
            set
            {
                this.RaiseAndSetIfChanged(ref leaderboardScore, value);
            }
        }

        string leaderboardScoreTitle;
        public string LeaderboardScoreTitle
        {
            get { return leaderboardScoreTitle; }
            set
            {
                this.RaiseAndSetIfChanged(ref leaderboardScoreTitle, value);
            }
        }

        // _ext_
        // Bad & fast way to handle dynamic styling
        // FIXME: Implement DynamicStyle in the xaml
        string textFontSize;
        public string TextFontSize
        {
            get { return textFontSize; }
            set
            {
                this.RaiseAndSetIfChanged(ref textFontSize, value);
            }
        }
        string textColor;
        public string TextColor
        {
            get { return textColor; }
            set
            {
                this.RaiseAndSetIfChanged(ref textColor, value);
            }
        }

    }
}
