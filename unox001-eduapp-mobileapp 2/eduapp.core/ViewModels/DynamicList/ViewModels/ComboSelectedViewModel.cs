﻿using System;
using System.Linq;
using System.Windows.Input;
using ReactiveUI;

namespace EduApp.Core
{
	public class ComboSelectedViewModel : ComboViewModel
	{

		public ICommand CommandOnSelectedItem { get; set; }

		public void SetSelectedItem (string key)
		{
			if (Source.ContainsKey (key)) {
				var currentValue = Source [key];
				SelectedItem = Collection.ToList ().IndexOf (currentValue);
			}
		}
	}
}
