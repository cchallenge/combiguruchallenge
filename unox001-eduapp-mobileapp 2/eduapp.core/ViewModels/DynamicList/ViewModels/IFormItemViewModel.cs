﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace EduApp.Core
{
	public interface IFormItemContainerViewModel : IFormItemViewModel
	{
		DateTime LastChange { get; set; }
		IEnumerable<IFormItemViewModel> ListViewModel { get; } //Lista di me + eventualmente i miei figli
		IEnumerable<IFormItemViewModel> Children { get; set; } //Lista dei figli
	}

	public interface IFormItemViewModel : INotifyPropertyChanged
	{
		string Title { get; set; }
	}
}
