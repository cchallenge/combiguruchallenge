﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using AppCore.Logging;
using MvvmHelpers;

namespace EduApp.Core
{
	public class DynamicListSource
	{

		ILog logger = LogProvider.GetLogger(typeof(DynamicListSource));
		/// <summary>
		/// ViewModel degli oggetti attualmente visualizzati nella listView
		/// </summary>
		ExtraObservableCollection<IFormItemViewModel> listVmVisualizzati;
		public ExtraObservableCollection<IFormItemViewModel> ListVMVisualizzati
		{
			get { return listVmVisualizzati; }
			set { listVmVisualizzati = value; }
		}

		public DynamicListSource(IEnumerable<IFormItemViewModel> items)
		{
			Init(items);
		}

		static int s_refCount = 0;
		int refCount = 0;
		private void Init (IEnumerable<IFormItemViewModel> items)
		{
			refCount = s_refCount++;

			if (ListVMVisualizzati == null)
				ListVMVisualizzati = new ExtraObservableCollection<IFormItemViewModel>();

			Child = items.ToList();
		}

		/// <summary>
		/// Gerarchia dei figli di primo livello
		/// </summary>
		IReadOnlyList<IFormItemViewModel> _child;
		public IReadOnlyList<IFormItemViewModel> Child
		{
			get { return _child; }
			set
			{
				_child = value;
				handleChildChange();
			}
		}

		IDisposable token;
		void handleChildChange()
		{
			if (token != null)
			{
				token.Dispose();
				token = null;
			}

			token = TreeHelper.CreateTreeChangeObservable(_child)
							  .Sample(TimeSpan.FromMilliseconds(250))
			                  //Osserviamo il cambiamento sul thread principale, per fare in modo che la ObservableCollection agisca poi li
			                  .ObserveOn(ReactiveUI.RxApp.MainThreadScheduler)
							  .Subscribe(tree => PopulateCollection());


			//Devo popolare la lista almeno una volta
			PopulateCollection();
		}

		void PopulateCollection()
		{
			lock (ListVMVisualizzati)
			{
				var currentGerarchia = TreeHelper.OnRevaluateList(_child).ToList();
				//TreeHelper.CompareCollection(ListVMVisualizzati, currentGerarchia);
				if (TreeHelper.CompareCollection(ListVMVisualizzati, currentGerarchia))
				{
					logger.Trace(()=> String.Format("DYNAMIC FORM SOURCE: Sto aggiornando la lista di {0}", refCount));
				}
			}
		}
	}
}
