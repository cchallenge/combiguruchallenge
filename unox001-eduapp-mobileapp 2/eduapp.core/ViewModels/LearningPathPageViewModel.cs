﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using AppCore.Interfaces;
using Nostromo;
using Nostromo.Interfaces;
using Nostromo.Navigation;
using ReactiveUI;
using UnoxEduAppModule;

namespace EduApp.Core
{
	public class LearningPathPageViewModel : BaseViewModel
	{
		public override string TextColorHex
		{
			get { return "#FFFFFF"; }
		}

		public override string BkgColorHex
		{
			get { return "#2A2928"; }
		}

		DynamicListSource learningDyn;
		public DynamicListSource LearningDyn
		{
			get { return learningDyn; }
			set
			{
				this.RaiseAndSetIfChanged(ref learningDyn, value);
			}
		}

		public LearningPathPageViewModel(IAppCoreContext context) : base(context)
		{
			this.Title = Catalog.GetValue("LearningTitle");
		}

		bool first = true;
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			token?.Dispose();
			token = null;
		}

		IDisposable token;
		public override async Task Appearing()
		{
			System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Chiamo il Appear");
			base.Appearing();
			using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
			{
				if (first)
				{
					first = false;
					await CreateList(load);
					var vm = this;
					token = UAM.Subscribe<UserStats>(u => UpdateUserStats(u, vm));
				}
					await UAM.RequestUserStatsIfNeeded();
			}
			var launchNextLevelFromId = Context.GetItem (Globals.NextLevelId).ToString();
			if (!String.IsNullOrEmpty (launchNextLevelFromId)) 
			{
				Context.SetItem (Globals.NextLevelId, "");
				var nextLearningCellModel = listCell.LearningCell.Where (x => x.ID == launchNextLevelFromId).FirstOrDefault ();
				await TapOnLesson(nextLearningCellModel, null);
			}
		}

		//Funzione che aggiorna lo userStats
		static void UpdateUserStats(UserStats u, LearningPathPageViewModel vm)
		{
			if (u != null) {
				System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Ho richiesto update dello status in LearningPath");
				vm.SetPercentage(u);
				//var level = vm.UAM.ToUserLevel(u);
				//if (level != null)
				//{
				//	vm.SetPercentage(level.LevelPercentage, level.LevelId);
				//}
			}
			System.Diagnostics.Debug.WriteLine ($"{DateTime.Now}: Finita la richiesta update status");
		}

		public void SetPercentage(UserStats stat)
		{
			//Cerco il livello da riempire con la percentuale
			bool checkElem = false;
			foreach (var elem in levelList)
			{
				//@@ CM mi estraggo la percentuale per il livello
				var cLevel = stat.LevelsStat.FirstOrDefault(l => l.CorrelationId == elem.ID);
				if (cLevel != null)
				{
					var cPercentage = cLevel.Percentuale * 100;
					bool isActive = cPercentage >= 100;
					//@@ CM se la percentuale è > 0 allora il pulsante è attivo e arancione
					elem.Percentage = cPercentage;
					elem.IsEnabledButton = cLevel.IsEnabled || cLevel.IsCurrentLevel;
					elem.Sbiadimento = cLevel.Sbiadimento;
					elem.BackgroundChallengeColor = isActive ? Globals.DoneChallenge : Globals.NotDoneChallenge;
					elem.BackgroundLineColor = isActive ? Globals.DoneChallenge : Globals.NotDoneChallenge;

					if (elem is LastLearningCellViewModel && isActive)
						(elem as LastLearningCellViewModel).LineImage = "line_orange.png";
						
					if (elem is LastLearningCellViewModel && !isActive)
						(elem as LastLearningCellViewModel).LineImage = "line_dark.png";
				}
				else {
					throw new Exception($"Sul learning path ho livelli che non sono presenti nello user stats ({elem.ID})");
				}

				//if (elem.ID == levelID)
				//{
				//	elem.IsEnabledButton = true;
				//	elem.Percentage = perc;

				//	if (perc >= 100)
				//	{
				//		elem.BackgroundChallengeColor = Globals.DoneChallenge;
				//		if (elem is LastLearningCellViewModel)
				//			(elem as LastLearningCellViewModel).LineImage = "line_orange.png";
				//	}
				//	checkElem = true;
				//}
				//else {
				//	if (checkElem)
				//	{
				//		//Tutti i livelli dopo sono disabilitati
				//		elem.IsEnabledButton = false;
				//		elem.BackgroundChallengeColor = Globals.NotDoneChallenge;
				//		elem.Percentage = 0;
				//	}
				//	else
				//	{
				//		//Tutti i livelli prima sono pieni
				//		elem.IsEnabledButton = true;
				//		elem.Percentage = 100;
				//		elem.BackgroundChallengeColor = Globals.DoneChallenge;
				//		elem.BackgroundLineColor = Globals.DoneChallenge;
				//	}
				//}
			}
		}

		async Task OnError (string error, IProgressDialog loader) {
			loader.Dispose();
			await UserDialogs.Instance.AlertAsync(error);
			await Navigator.PopAsync(true);
		}

		List<BaseLearningCellViewModel> levelList;
		LearningModel listCell;
		async Task CreateList(IProgressDialog load)
		{

			levelList = new List<BaseLearningCellViewModel>();
			listCell = await UAM.GetLearningPath(m => OnError(m, load));
			if (listCell != null) {
				string lastCellID = listCell.LearningCell.Last ().ID;
				string firstCellID = listCell.LearningCell.First ().ID;
				var learningArray = listCell.LearningCell.ToArray ();

				for (int i = 0; i < learningArray.Count (); i++) {
					BaseLearningCellViewModel mycell;
					LearningCellModel cell = learningArray [i];
					if (cell.ID == firstCellID) {
						mycell = new FirstLearningCellViewModel ();
					} else if (cell.ID == lastCellID) {
						mycell = new LastLearningCellViewModel () {
							LineImage = "line_dark.png"
						};
					} else {
						mycell = new LearningCellViewModel ();
					}

					mycell.ID = cell.ID;
					mycell.LessonImage = cell.LessonImage;
					mycell.LessonTitle = cell.TitleLesson;
					mycell.ChallengeImage = cell.ChallengeImage;
					mycell.ChallengeTitle = cell.TitleChallenge;
					mycell.IsEnabledButton = false;
					mycell.BackgroundChallengeColor = Globals.NotDoneChallenge;
					mycell.BackgroundLineColor = Globals.LineNotChallenge;
                    
					mycell.ChallengeTapCommand = ReactiveCommand.CreateFromTask<string> ((arg) => TapOnChallenge (cell));

					LearningCellModel nextLearningCell;
					if (mycell is LastLearningCellViewModel)
						nextLearningCell = null;
					else
						nextLearningCell = learningArray [i + 1];

					mycell.LessonTapCommand = ReactiveCommand.CreateFromTask<string> ((arg) => TapOnLesson (cell, mycell));

					levelList.Add (mycell);
				}

				LearningDyn = new DynamicListSource (levelList);
			}
		}

		async Task TapOnChallenge(LearningCellModel cell)
		{
			//System.Diagnostics.Debug.WriteLine("#######Tappato Challenge del livello: " + cell.CurrentLevel.CorrelationId);
			var myCell = levelList.FirstOrDefault (e => e.ID == cell.ID);

            if (myCell.IsEnabledButton)
            {
                var index = levelList.IndexOf(myCell);
                Context.SetItem(Globals.IndexOfLevel, index + 1);

                await UAM.GoToChallengePage(Context, this.Navigator, cell.CurrentLevel);
            }
            else
            {
                Nostromo.Container.Default.Resolve<IToastService>().ShowToastMessage(Catalog.GetValue("OpenChallengeMessage"));
            }
		}

		async Task TapOnLesson (LearningCellModel cell, BaseLearningCellViewModel mycell)
		{
            if (mycell == null || mycell.IsEnabledButton)
            {
                //@@ CM richiedo il livello successivo a quello che ho tappato
                var myCell = levelList.FirstOrDefault(e => e.ID == cell.ID);
                var index = levelList.IndexOf(myCell);
                //@@ CM se non è l'ultima cella
                if (index < (levelList.Count() - 1))
                {
                    Context.SetItem(Globals.NextLevelCellModelKey, levelList.ElementAt(index + 1));
                }
                else
                {
                    //@@ CM se è l'ultima cella tolgo il next level
                    Context.SetItem(Globals.NextLevelCellModelKey, null);
                }

                Context.SetItem(Globals.SectionCellModelKey, cell);
                Context.SetItem(Globals.IndexOfLevel, index + 1);

                await Navigator.PushAsync(Build.ViewModel(() => new LevelPageViewModel(Context, cell.CurrentLevel)), true);
            }
            else
            {
                Nostromo.Container.Default.Resolve<IToastService>().ShowToastMessage(Catalog.GetValue("OpenLevelMessage"));
            }
		}
	}
}
