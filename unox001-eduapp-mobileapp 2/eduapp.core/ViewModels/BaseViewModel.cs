﻿using System;
using System.Threading.Tasks;
using AppCore;
using AppCore.DeferredAppContentModule;
using AppCore.Interfaces;
using Nostromo.Interfaces;
using ReactiveUI;
using UnoxEduAppModule;

namespace EduApp.Core
{
	public abstract class BaseViewModel : ReactiveObject, IUnoxPageViewModel, IDisposable
	{
		private bool disposed = false;

		//Implement IDisposable.
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					// Free other state (managed objects).
				}
				// Free your own state (unmanaged objects).
				// Set large fields to null.
				disposed = true;
			}
		}

		// Use C# destructor syntax for finalization code.
		~BaseViewModel()
		{
			// Simply call Dispose(false).
			Dispose(false);
		}

		public UnoxAppModule UAM { get; set;}
		public DeferredAppContentModule Content { get; set;}
		public IAppCoreContext Context { get; set; }
		public IUnoxAppAuthModule UA { get; set; }
		public IAppCatalog Catalog { get; set;}
		public UnoxAppEduModule UM { get; set;}
		public IAnalyticsService AnaS { get; set;}

		public BaseViewModel(IAppCoreContext context)
		{
			Context = context;
			Context.Populate(this);
		}

		public IPageNavigator Navigator { get; set; }

		string title;
		public string Title
		{
			get
			{
				return title;
			}
			set
			{
				title = value;
				this.RaisePropertyChanged();
			}
		}

		public virtual string TextColorHex
		{
			get
			{
				return "#FFFFFF";
			}
		}

		public virtual string BarBkgColorHex
		{
			get
			{
				return "#2A2928";
			}
		}

		public virtual string BkgColorHex
		{
			get
			{
				return "#FECE5E";
			}
		}



		public virtual async Task Appearing()
		{

		}

		public virtual async Task Disappearing()
		{

		}

		public virtual void Load()
		{

		}
	}

	public interface IUnoxPageViewModel : IPageViewModel
	{ 
		string TextColorHex { get; }
		string BarBkgColorHex { get; }
		string BkgColorHex { get;}
	}
}
