﻿using System;
using System.Threading.Tasks;
using AppCore.Interfaces;
using Nostromo.Interfaces;
using UnoxEduAppModule;

namespace EduApp.Core
{
	public class ChallengePageViewModel : BaseViewModel
	{
		public override string TextColorHex
		{
			get { return "#FFFFFF"; }
		}

        public override string  BarBkgColorHex
        {
            get { return "#FECE5E"; }
        }

        readonly Level level;
		public ChallengePageViewModel(IAppCoreContext context, Level level) : base(context)
		{
			this.level = level;
			if (level != null)
				Context.SetItem(Globals.LevelKey, level);
		}

		bool first = true;
		public override async System.Threading.Tasks.Task Appearing()
		{
			base.Appearing();
			if (first)
			{
				first = false;
				//@@ CM nel caso in cui io termini correttamente il challenge atterro qui che si deve chiudere in modo da tornare alla pagina chiamante la modal
				var lm = new LessonManager(level.CorrelationId, Navigator, Context, Catalog, UM, false, string.Empty, true);
				//Nel caso in cui per un qualche motivo non riesco a procedere chiudo la modal
				if (!await lm.FirstStep())
				{
					await this.Navigator.PopModalAsync(true);
				}
			}
		}
	}
}
