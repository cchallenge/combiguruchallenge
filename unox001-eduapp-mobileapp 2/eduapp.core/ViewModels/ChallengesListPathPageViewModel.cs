﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using AppCore.Interfaces;
using Nostromo;
using Nostromo.Interfaces;
using Nostromo.Navigation;
using ReactiveUI;
using UnoxEduAppModule;

namespace EduApp.Core
{
    public class ChallengesListPathPageViewModel : BaseViewModel
    {
        public override string TextColorHex
        {
            get { return "#FFFFFF"; }
        }

        public override string BkgColorHex
        {
            get { return "#2A2928"; }
        }


        DynamicListSource challengesListDyn;
        public DynamicListSource ChallengesListDyn
        {
            get { return challengesListDyn; }
            set
            {
                this.RaiseAndSetIfChanged(ref challengesListDyn, value);
            }
        }

        string noChallengesText;
        public string NoChallengesText
        {
            get { return noChallengesText; }
            set
            {
                this.RaiseAndSetIfChanged(ref noChallengesText, value);
            }
        }

        bool showNoChallengesText;
        public bool ShowNoChallengesText
        {
            get { return showNoChallengesText; }
            set
            {
                this.RaiseAndSetIfChanged(ref showNoChallengesText, value);
            }
        }

        public ChallengesListPathPageViewModel(IAppCoreContext context) : base(context)
        {
            this.Title = Catalog.GetValue("ChallengesListTitle");

            NoChallengesText = Catalog.GetValue("ChallengesListNoChallengesText");
            ShowNoChallengesText = false;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            //token?.Dispose();
            //token = null;
        }

        //IDisposable token;
        public override async Task Appearing()
        {
            System.Diagnostics.Debug.WriteLine($"////////// {DateTime.Now}: Chiamo il Appear\n");
            await base.Appearing();
            using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
            {
                await CreateList(load);
                var vm = this;
                //token = UAM.Subscribe<UserStats>(u => UpdateUserStats(u, vm));
                await UAM.RequestUserStatsIfNeeded();

            }
        }

        async Task OnError(string error, IProgressDialog loader)
        {
            loader.Dispose();
            await UserDialogs.Instance.AlertAsync(error);
            await Navigator.PopAsync(true);
        }

        List<BaseChallengesListCellViewModel> levelList;
        ChallengesListModel listCell;
        async Task CreateList(IProgressDialog load)
        {
            levelList = new List<BaseChallengesListCellViewModel>();
            listCell = await UAM.GetChallengesListPath(m => OnError(m, load));

            if (listCell != null)
            {
                string lastCellID = listCell.ChallengesListCell.Last().ID;
                string firstCellID = listCell.ChallengesListCell.First().ID;
                var learningArray = listCell.ChallengesListCell.ToArray();

                for (int i = 0; i < learningArray.Count(); i++)
                {
                    BaseChallengesListCellViewModel mycell;
                    ChallengesListCellModel cell = learningArray[i];

                    if( i % 2 == 0 )
                    {
                        mycell = new LeftChallengesListCellViewModel();
                    }
                    else
                    {
                        mycell = new RightChallengesListCellViewModel();
                    }


                    mycell.ID = cell.ID;
                    mycell.ChallengeImage = cell.ChallengeImage;
                    //mycell.ChallengeTitle = cell.TitleChallenge + "\n" + String.Format("{0}\n{1}",
                    //       Catalog.GetValue("ChallengesListDateTime"),
                    //       cell.DateTo.ToString("MM/dd H:mm")
                    //);
                    System.Diagnostics.Debug.WriteLine("///Loading challenge with start: " + cell.DateFrom.ToString() + " and end: " + cell.DateTo.ToString());
                    mycell.ChallengeTitle = cell.TitleChallenge;
                    mycell.ChallengeDateTime = Catalog.GetValue("ChallengesListDateTime") + "\n";
                    mycell.ChallengeDateTime += cell.DateTo.Day.ToString() + " ";
                    mycell.ChallengeDateTime += Catalog.GetValue("MonthsYear").Split(',')[cell.DateTo.Month - 1].Trim();
                    mycell.ChallengeDateTime += " " + cell.DateTo.Year;

                    // TODO: attualmente "Percentage" è inutile.
                    if (cell.Completed == true)
                    {
                        mycell.IsEnabledButton = false;
                        mycell.Percentage = 100;
                        mycell.CellBgColor = Globals.DoneChallenge;
                    }
                    else
                    {
                        mycell.IsEnabledButton = true;
                        mycell.Percentage = 0;
                        mycell.CellBgColor = Globals.NotDoneChallenge;
                    }

                    mycell.GoToChallengeCommand = ReactiveCommand.CreateFromTask<string>((arg) => TapAndGoToChallenge(mycell));

                    ChallengesListCellModel nextChallengesListCell;
                    if (cell.ID == lastCellID)
                    {
                        nextChallengesListCell = null;
                    }
                    else
                    {
                        nextChallengesListCell = learningArray[i + 1];
                    }

                    levelList.Add(mycell);
                }

                ChallengesListDyn = new DynamicListSource(levelList);
            }
            else
            {
                ShowNoChallengesText = true;
            }
        }

        async Task TapAndGoToChallenge(BaseChallengesListCellViewModel mycell)
        {
            await UAM.GoToExtChallengePage(Context, this.Navigator, mycell.ID);

            // In caso voglia visualizzare un messaggio per challenge già fatta:

            //if (mycell == null || mycell.IsEnabledButton)
            //{
            //    await UAM.GoToExtChallengePage(Context, this.Navigator, mycell.ID);
            //}
            //else
            //{
            //    Nostromo.Container.Default.Resolve<IToastService>().ShowToastMessage(Catalog.GetValue("ChallengesListChallengeDone"));
            //}
        }
        
    }
}
