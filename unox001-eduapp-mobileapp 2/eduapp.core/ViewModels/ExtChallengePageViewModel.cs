﻿using System;
using System.Threading.Tasks;
using AppCore.Interfaces;
using Nostromo.Interfaces;
using UnoxEduAppModule;

namespace EduApp.Core
{
    public class ExtChallengePageViewModel : BaseViewModel
    {
        public override string TextColorHex
        {
            get { return "#FFFFFF"; }
        }

        public override string BarBkgColorHex
        {
            get { return "#885EAD"; } //XXX - ext color barra
        }

        readonly String correlationId;
        public ExtChallengePageViewModel(IAppCoreContext context, String correlationId) : base(context)
        {
            this.correlationId = correlationId;
        }

        bool first = true;
        public override async System.Threading.Tasks.Task Appearing()
        {
            base.Appearing();
            if (first)
            {
                first = false;
                //@@ CM nel caso in cui io termini correttamente il challenge atterro qui che si deve chiudere in modo da tornare alla pagina chiamante la modal
                var lm = new LessonManager(correlationId, Navigator, Context, Catalog, UM, false, string.Empty, true, true);

                //Nel caso in cui per un qualche motivo non riesco a procedere chiudo la modal
                if (!await lm.FirstStep())
                {
                    await this.Navigator.PopModalAsync(true);
                }
            }
        }
    }
}
