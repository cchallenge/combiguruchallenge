﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore.Interfaces;
using Nostromo.Interfaces;
using ReactiveUI;
using UnoxEduAppModule;

namespace EduApp.Core
{
	public class SectionPageViewModel : BaseViewModel
	{
		LearningCellModel learningCM;
		Section _currentSection;

		string levelText;
		public string LevelText {
			get {
				return levelText;
			}
			set {
				this.RaiseAndSetIfChanged (ref levelText, value);
			}
		}

		string sectionImage;
		public string SectionImage {
			get {
				return sectionImage;
			}
			set {
				this.RaiseAndSetIfChanged (ref sectionImage, value);
			}
		}

		string buttonText;
		public string ButtonText {
			get {
				return buttonText;
			}
			set {
				this.RaiseAndSetIfChanged (ref buttonText, value);
			}
		}

		int startingPosition;
		public int StartPosition {
			get {
				return startingPosition;
			}
			set {
				System.Diagnostics.Debug.WriteLine ($"########CurrentPosition {value}");
				this.RaiseAndSetIfChanged (ref startingPosition, value);
			}
		}

		public override string TextColorHex {
			get { return "#FFFFFF"; }
		}

        public override string BarBkgColorHex
        {
            get { return "#FECE5E"; }
        }

        public ICommand CloseViewCommand { get; set; }

		public ObservableCollection<LessonsData> Lessons { get; set; }
		int indexOfSection = -1;

		public SectionPageViewModel (IAppCoreContext context, bool fromFineLezione = false) : base (context)
		{
			_currentSection = (Section)Context.GetItem(Globals.SectionKey);

			CloseViewCommand = ReactiveCommand.CreateFromTask(async () => await Navigator.PopModalAsync(true));
			Lessons = new ObservableCollection<Core.LessonsData> ();

			learningCM = (LearningCellModel) Context.GetItem (Globals.SectionCellModelKey);

			indexOfSection = learningCM.CurrentLevel.Sections.ToList ().IndexOf (_currentSection);

			this.Title = learningCM.TitleLesson;

		}

		bool firstCall = true;
		IList<LessonHeader> lessonsFromSect;

		public override async Task Appearing ()
		{
			using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading ("")) {
				var sectionModel = await UAM.GetSectionModel (_currentSection);
				LevelText = sectionModel.Title;
				SectionImage = sectionModel.Image;

				if (firstCall) {
					firstCall = false;
					lessonsFromSect = await UAM.GetLessonsForSection (_currentSection.CorrelationId);
					if (lessonsFromSect != null)
					{
						var lastLesson = lessonsFromSect.Last ();
						int lessonsCount = lessonsFromSect.Count;
						int iterateLesson = 0;

						Lessons.Clear();

						foreach (var less in lessonsFromSect)
						{
							var lessonData = new Core.LessonsData
							{
								CanAccessLesson = less.AccessibleLesson,
								CorrelationId = less.CorrelationId,
								NumbOfLessons = String.Format(Catalog.GetValue("LessonOf"), ++iterateLesson, lessonsCount),
								Description = less.Description,
								ButtonText = Catalog.GetValue("Start")
							};
							var canEx = lessonData.WhenAnyValue(x => x.CanAccessLesson);
							bool lastLessonBool = false;
							if (less == lastLesson)
								lastLessonBool = true;
							lessonData.StartLesson = ReactiveCommand.CreateFromTask(async () => await TapOnLesson(lessonData.CorrelationId, lastLessonBool), canEx);
							Lessons.Add(lessonData);
						}

						int index = lessonsFromSect.IndexOf(lessonsFromSect.Where(x => x.AccessibleLesson == true).Last());

						StartPosition = index;
					}
				}
				load.Hide ();
				load.Dispose ();
			}

			base.Appearing ();
		}

		async Task TapOnLesson (string lessonID, bool lastLesson)
		{
			var lessonTo = lessonsFromSect.Where (x => String.Equals (x.CorrelationId, lessonID)).FirstOrDefault();
			var indexOfLesson = lessonsFromSect.IndexOf (lessonTo);
			string logData = String.Format ("Level: {0}; section: {1}; lesson: {2};", Context.GetItem (Globals.IndexOfLevel), indexOfSection + 1, indexOfLesson + 1);

			var lm = new LessonManager(lessonID, Navigator, Context, Catalog, UM, lastLesson, logData);
			await lm.FirstStep();
		}

		public async Task CloseFromExt ()
		{
			await Navigator.PopModalAsync (true);
		}
	}

	public class LessonsData
	{
		public ICommand StartLesson { get; set; }
		public string CorrelationId { get; set; }
		public string NumbOfLessons { get; set; }
		public string Description { get; set; }
		public bool CanAccessLesson { get; set; }
		public string ButtonText { get; set; }
	}
}
