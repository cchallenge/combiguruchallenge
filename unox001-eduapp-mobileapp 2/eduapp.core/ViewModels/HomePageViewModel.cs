﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using AppCore.Interfaces;
using Nostromo;
using Nostromo.Navigation;
using ReactiveUI;
using AppCore.Logging;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Rg.Plugins.Popup.Services;
using Plugin.Share;
using Plugin.Share.Abstractions;

namespace EduApp.Core
{
    public class HomePageViewModel : BaseViewModel
    {

        public ICommand HomeCommand { get; set; }
        public ICommand DefaultLearningPathCommand { get; set; }
        public ICommand ChallengePathCommand { get; set; }

        public override string BkgColorHex
        {
            get { return "#2A2928"; }
        }

        public override string BarBkgColorHex
        {
            get { return "#2A2928"; }
        }

        public override string TextColorHex
        {
            get { return "#FFFFFF"; }
        }

        string score;
        public string Score
        {
            get { return ((Context.UserIdentity as _ext_UnoxUserIdentity).Score) + Catalog.GetValue("GlobalScoreText"); }
            set
            {
                this.RaiseAndSetIfChanged(ref score, value);
            }
        }

        string nickname;
        public string Nickname
        {
            get { return (Context.UserIdentity as _ext_UnoxUserIdentity).Nickname; }
            set
            {
                this.RaiseAndSetIfChanged(ref nickname, value);
            }
        }

        /// <summary>
        /// https://msdn.microsoft.com/en-us/magazine/dn605875.aspx
        /// </summary>
        //public NotifyTaskCompletion<int> ScoreBE { get; private set; }

        string defaultLearningPath;
        public string DefaultLearningPath
        {
            get { return defaultLearningPath; }
            set
            {
                this.RaiseAndSetIfChanged(ref defaultLearningPath, value);
            }
        }

        string customLearningPath;
        public string CustomLearningPath
        {
            get { return customLearningPath; }
            set
            {
                this.RaiseAndSetIfChanged(ref customLearningPath, value);
            }
        }

        public HomePageViewModel(IAppCoreContext context) : base(context)
        {
            HomeCommand = ReactiveCommand.CreateFromTask(async () => await Navigator.PushAsync(Build.ViewModel(() => new LearningPathPageViewModel(Context)), true));
            DefaultLearningPathCommand = ReactiveCommand.CreateFromTask(async () => await Navigator.PushAsync(Build.ViewModel(() => new LearningPathPageViewModel(Context)), true));
            ChallengePathCommand = ReactiveCommand.CreateFromTask(async () => await Navigator.PushAsync(Build.ViewModel(() => new ChallengesListPathPageViewModel(Context)), true));
            DefaultLearningPath = Catalog.GetValue("HomePagePercorsoDefaultTitle");
            CustomLearningPath = Catalog.GetValue("HomePageSfideTitle");
            Context.SetItem(Globals.NextLevelId, "");
        }

        public void ShowMenu()
        {
            UserDialogs.Instance.ActionSheet(new ActionSheetConfig()
            {
                    Title = Catalog.GetValue("Menu"),
                    Options = new List<ActionSheetOption>() {
                        new ActionSheetOption (Catalog.GetValue("Profilo"), async () => GoToProfiloPage()),
                        new ActionSheetOption (Catalog.GetValue("SocialShare"), async() => ShareOnSocial()),
                        new ActionSheetOption (Catalog.GetValue("Logout"), async() => Logout ())
                    },
                    Cancel = new ActionSheetOption(Catalog.GetValue("Cancella"))
             });
		}

        bool institutionSet = false;
        public void UserInstitutionSet(string institution)
        {
            (Context.UserIdentity as _ext_UnoxUserIdentity).Institution = institution;
            institutionSet  = true;
        }

        private bool IsInstitutionSet() {
            return institutionSet || !(Context.UserIdentity as _ext_UnoxUserIdentity).Institution.Equals("N/A");
        }

        public async void ValidateAndSaveInstitution() {
            if (IsInstitutionSet())
            { 
                GoToLeaderboardPathPage();
            }
            else
            {
                var page = new PopupView(UM, Catalog);
                await PopupNavigation.Instance.PushAsync(page);
            }
        }

        public override async Task Appearing()
		{
			//FIXME: da testare
			base.Appearing();
            DefaultLearningPath = Catalog.GetValue("HomePagePercorsoDefaultTitle");
            CustomLearningPath = Catalog.GetValue("HomePageSfideTitle");
            using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
            {
                Score = ((Context.UserIdentity as _ext_UnoxUserIdentity).Score) + Catalog.GetValue("GlobalScoreText");
                Nickname = (Context.UserIdentity as _ext_UnoxUserIdentity).Nickname;
                System.Diagnostics.Debug.WriteLine("Updating menu nickname: " + Nickname);
            }
            //better positioned in here, otherwise they delay the toolbar info. udpate
            var res = await Content.List(Globals.SpotRegex);
            await Content.Fetch(res.Select(r => r.FileKey).ToArray(), null);
        }

        async Task Logout()
		{
			UA.Logout();
			//FIXME: chiamare reset password
			await Navigator.SetNewRootAsync(Build.ViewModel(() => new SyncPageViewModel(Context)), true);
		}

		async Task GoToProfiloPage()
		{
			await Navigator.PushAsync(Build.ViewModel(() => new ProfiloPageViewModel(Context, false)), true);
		}

		async Task GoToLeaderboardPathPage()
        {
            await Navigator.PushAsync(Build.ViewModel(() => new LeaderboardPathPageViewModel(Context)), true);
        }

		async Task ShareOnSocial()
        {
            if (!CrossShare.IsSupported)
            {
                // Show a message/toast here??
                return;
            }

            // Share message and an optional title.
            await CrossShare.Current.Share(new ShareMessage
            {
                Title = Catalog.GetValue("SocialShareTitle"),
                Text = Catalog.GetValue("SocialShareDescription"),
                Url = Catalog.GetValue("SocialShareUrl")
            });
        }
    }
}
