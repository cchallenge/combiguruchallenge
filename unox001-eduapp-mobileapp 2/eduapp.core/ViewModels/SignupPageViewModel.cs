﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore.Interfaces;
using Nostromo;
using Nostromo.Navigation;
using ReactiveUI;
using AppCore.Logging;
using Xamarin.Forms.Xaml;

namespace EduApp.Core
{
    public class SignupPageViewModel : BaseViewModel
	{
		public override string BkgColorHex
		{
			get { return "#FFFFFF"; }
		}

		public ICommand SignUpCommand { get; set; }
		public ICommand InfoCommand { get; set; }
        public ICommand InfoNickCommand { get; set; }


        string titlePage;
		public string TitlePage {
			get { return titlePage; }
			set {
				this.RaiseAndSetIfChanged (ref titlePage, value);
			}
		}

		string subtitlePage;
		public string SubtitlePage {
			get { return subtitlePage; }
			set {
				this.RaiseAndSetIfChanged (ref subtitlePage, value);
			}
		}

		string usernameTitle;
		public string UsernameTitle {
			get { return usernameTitle; }
			set {
				this.RaiseAndSetIfChanged (ref usernameTitle, value);
			}
		}

		string passwordTitle;
		public string PasswordTitle {
			get { return passwordTitle; }
			set {
				this.RaiseAndSetIfChanged (ref passwordTitle, value);
			}
		}

		string confPasswordTitle;
		public string ConfPasswordTitle {
			get {return confPasswordTitle; }
			set {
                this.RaiseAndSetIfChanged (ref confPasswordTitle, value);
			}
		}
       
        string nicknameTitle;
        public string NicknameTitle {
            get {return nicknameTitle; }
            set
            {
                this.RaiseAndSetIfChanged(ref nicknameTitle, value);
            }
        }

		string username;
		public string Username {
			get { return username; }
			set {
				this.RaiseAndSetIfChanged (ref username, value);
			}
		}

		string password;
		public string Password {
			get { return password; }
			set {
				this.RaiseAndSetIfChanged (ref password, value);
			}
		}

		string confPassword;
		public string ConfPassword {
			get { return confPassword; }
			set {
				this.RaiseAndSetIfChanged (ref confPassword, value);
			}
		}

        string nickname;
        public string Nickname {
            get { return nickname; }
            set
            {
                this.RaiseAndSetIfChanged(ref nickname, value);
            }
        }

        string signupButtonText;
		public string SignupButtonText {
			get { return signupButtonText; }
			set {
				this.RaiseAndSetIfChanged (ref signupButtonText, value);
			}
		}

        ILog logger = LogProvider.GetLogger(typeof(SignupPageViewModel));
        public SignupPageViewModel(IAppCoreContext context) : base(context)
		{
			this.TitlePage = Catalog.GetValue ("SignupTitle");
			this.SubtitlePage = Catalog.GetValue ("SignupSubtitle");
			this.PasswordTitle = Catalog.GetValue ("Pwd");
			this.UsernameTitle = Catalog.GetValue ("Usr");
			this.ConfPasswordTitle = Catalog.GetValue ("ConfPwd");
            this.NicknameTitle = Catalog.GetValue("Nick");
            this.SignupButtonText = Catalog.GetValue("Signup");

			SignUpCommand = ReactiveCommand.CreateFromTask(() => SignUp());
			InfoCommand = ReactiveCommand.CreateFromTask (() => InfoPwd ());
            InfoNickCommand = ReactiveCommand.CreateFromTask(() => InfoNickname());

//            logger.Trace("---Constructor finished its job retrieving NicknameT " + this.NicknameTitle);

        }

        public void Close ()
		{
			this.Navigator.PopToRootAsync (true);
		}

		async Task InfoPwd ()
		{
			await Acr.UserDialogs.UserDialogs.Instance.AlertAsync (Catalog.GetValue ("PasswordDetails"), Catalog.GetValue ("Info"));
		}

        async Task InfoNickname()
        {
            await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("NicknameDetails"), Catalog.GetValue("Info"));
        }

        async Task SignUp()
		{			
			if (!AllFieldFill ()) 
			{
				await Acr.UserDialogs.UserDialogs.Instance.AlertAsync (Catalog.GetValue ("EmptyFields"), Catalog.GetValue ("Errore"));
				return;
			}
			if (!CorrectMailFormat ()) 
			{
				await Acr.UserDialogs.UserDialogs.Instance.AlertAsync (Catalog.GetValue ("MailFormatError"), Catalog.GetValue ("Errore"));
				return;
			}
			if (!CorrectPasswordFormat()) 
			{
				await Acr.UserDialogs.UserDialogs.Instance.AlertAsync (Catalog.GetValue ("PasswordFormatError"), Catalog.GetValue ("Errore"));
				return;
			}

            if (!PasswordsAreEquals())
			{
				await Acr.UserDialogs.UserDialogs.Instance.AlertAsync (Catalog.GetValue ("PasswordNotEqualsError"), Catalog.GetValue ("Errore"));
				return;
			}

            if (!NicknameSizeCriteriaFulfilled())
            {
                await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("NicknameEntryError"), Catalog.GetValue("Errore"));
                return;
            }

            using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading ("")) {
				var res = await UA._ext_UnoxSignup (Username, Password, Nickname, country: "", occupation: "");
				if (res != null) {
					if (res.Success) {

                        await Navigator.SetNewRootAsync (Build.ViewModel (() => new ValidatePageViewModel (Context, true)), true);

					} else {
						load.Dispose ();
						await Acr.UserDialogs.UserDialogs.Instance.AlertAsync (res.Error, Catalog.GetValue ("Errore"));
					}
				} else {
					load.Dispose ();
					await Acr.UserDialogs.UserDialogs.Instance.AlertAsync (Catalog.GetValue ("ErroreConnessione"));
				}
			}
		}

		bool AllFieldFill ()
		{
			if (String.IsNullOrEmpty (Username) || String.IsNullOrEmpty (Password) || String.IsNullOrEmpty (ConfPassword) || String.IsNullOrEmpty(Nickname))
                return false;
			return true;
		}

		bool CorrectMailFormat ()
		{
			return Helper.CorrectMailFormat (Username);
		}

		bool CorrectPasswordFormat ()
		{
			string password = Password.Trim ();
            logger.Trace("---Inputed password is: " + password);
            return Helper.CorrectPasswordFormat (password);
		}

		bool PasswordsAreEquals ()
		{
			bool result = Password.Equals (ConfPassword, StringComparison.Ordinal);
			return result;
		}

        bool NicknameSizeCriteriaFulfilled() {

            return (!String.IsNullOrEmpty(Nickname) && Nickname.Length >= 5 && Nickname.Length < 30);
        }
	}
}