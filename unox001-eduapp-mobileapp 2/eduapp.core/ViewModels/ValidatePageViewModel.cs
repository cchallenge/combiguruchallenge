﻿using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using AppCore.Interfaces;
using Nostromo;
using Nostromo.Navigation;
using AppCore.Logging;


namespace EduApp.Core
{
	public class ValidatePageViewModel : BaseViewModel
	{
		public override string BkgColorHex
		{
			get { return "#FFFFFF"; }
		}

        bool fromLogin;
		public ValidatePageViewModel(IAppCoreContext context, bool fromLogin = false) : base(context)
		{
            this.fromLogin = fromLogin;
		}

		public override async Task Appearing()
		{
			base.Appearing();
			//Lasciare questa attesa se no ci sono problemi con il loading venendo dalla login
			await Task.Delay(500);
			await verifyValidate();
		}

        ILog logger = LogProvider.GetLogger("ValidatePageViewModel");

        async Task verifyValidate()
		{
			using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
			{
				var res = await UA.UnoxValidate();
                switch (res.Status)
                {
                    case ValidateStatus.activate:
                        if (fromLogin)
                        {
                            await this.Navigator.SetNewRootAsync(Build.ViewModel(() => new WalkThroughPageViewModel(Context)), true);
                        }
                        else
                        {
                            await this.Navigator.SetNewRootAsync(Build.ViewModel(() => new HomePageViewModel(Context)), true);
                        }
                        break;
                    case ValidateStatus.pending_activation:
                        load.Dispose();

                        if (await Acr.UserDialogs.UserDialogs.Instance.ConfirmAsync(res.Error, null, Catalog.GetValue("Retry"), Catalog.GetValue("Cancella")))
                            await verifyValidate();
						else
							await Logout();
                        break;
					default:
                        await this.Navigator.SetNewRootAsync(Build.ViewModel(() => new WelcomePageViewModel(Context)), true);
						break;
				}
			}
		}

		async Task Logout()
		{
			UA.Logout();
			//FIXME: chiamare reset password
			await Navigator.SetNewRootAsync(Build.ViewModel(() => new SyncPageViewModel(Context)), true);
		}
	}
}
