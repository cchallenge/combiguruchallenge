﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using AppCore.Interfaces;
using Nostromo;
using Nostromo.Interfaces;
using Nostromo.Navigation;
using ReactiveUI;
using UnoxEduAppModule;

namespace EduApp.Core
{
    public class LeaderboardPathPageViewModel : BaseViewModel
    {
        public override string TextColorHex
        {
            get { return "#FFFFFF"; }
        }

        public override string BkgColorHex
        {
            get { return "#2A2928"; }
        }

        DynamicListSource leaderboardDyn;
        public DynamicListSource LeaderboardDyn
        {
            get { return leaderboardDyn; }
            set
            {
                this.RaiseAndSetIfChanged(ref leaderboardDyn, value);
            }
        }

        public LeaderboardPathPageViewModel(IAppCoreContext context) : base(context)
        {
            this.Title = Catalog.GetValue("Leaderboard");
        }

        bool first = true;

        public override async Task Appearing()
        {
            System.Diagnostics.Debug.WriteLine($"{DateTime.Now}: Chiamo il Appear");
            base.Appearing();
            using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
            {
                if (first)
                {
                    first = false;
                    await CreateList(load);
                    var vm = this;
                }
                await UAM.RequestUserStatsIfNeeded();
            }
        }

        async Task OnError(string error, IProgressDialog loader)
        {
            loader.Dispose();
            await UserDialogs.Instance.AlertAsync(error);
            await Navigator.PopAsync(true);
        }

        List<BaseLeaderboardCellViewModel> levelList;
        LeaderboardModel listCell;
        async Task CreateList(IProgressDialog load)
        {

            levelList = new List<BaseLeaderboardCellViewModel>();
            listCell = await UAM.GetLeaderboard(m => OnError(m, load));

            if (listCell != null)
            {
                string lastCellID = listCell.LeaderboardCell.Last().ID;
                string firstCellID = listCell.LeaderboardCell.First().ID;
                var leaderboardArray = listCell.LeaderboardCell.ToArray();
                UserData _userData = UA.UserData;

                for (int i = 0; i < leaderboardArray.Count(); i++)
                {
                    BaseLeaderboardCellViewModel mycell;
                    LeaderboardCellModel cell = leaderboardArray[i];

                    if (cell.ID == firstCellID)
                    {
                        mycell = new FirstLeaderboardCellViewModel();
                        mycell.LeaderboardPositionTitle = Catalog.GetValue("LeaderboardPositionTitle");
                        mycell.LeaderboardNicknameTitle = Catalog.GetValue("LeaderboardNicknameTitle");
                        mycell.LeaderboardScoreTitle = Catalog.GetValue("LeaderboardScoreTitle");
                    }
                    else if (cell.ID == lastCellID)
                    {
                        mycell = new LastLeaderboardCellViewModel();
                    }
                    else
                    {
                        mycell = new LeaderboardCellViewModel();
                    }

                    mycell.ID = cell.ID;
                    mycell.LeaderboardPosition = cell.LeaderboardPosition;
                    mycell.LeaderboardNickname = cell.LeaderboardNickname;
                    mycell.LeaderboardScore = cell.LeaderboardScore;
                    mycell.TextFontSize = "11";
                    mycell.TextColor = "White";

                    // If the user is the current one, highlight it!
                    if (_userData != null)
                    {
                        if(cell.LeaderboardUniqueIdentifier == _userData.Id)
                        {
                            mycell.TextFontSize = "15";
                            mycell.TextColor = "palevioletred";
                        }
                    }

                    LeaderboardCellModel nextLeaderboardCell;
                    if (cell.ID == lastCellID)
                    {
                        nextLeaderboardCell = null;
                    }
                    else
                    {
                        nextLeaderboardCell = leaderboardArray[i + 1];
                    }

                    levelList.Add(mycell);
                }

                LeaderboardDyn = new DynamicListSource(levelList);
            }
        }
    }
}
