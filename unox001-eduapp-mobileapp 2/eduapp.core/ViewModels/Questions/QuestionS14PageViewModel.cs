﻿using System;
using System.Collections.Generic;
using AppCore.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public class QuestionS14PageViewModel : BaseGraficoViewModel
	{
		IEnumerable<S14Model> topItems;
		public IEnumerable<S14Model> TopItems
		{
			get { return topItems; }
			set
			{
				this.RaiseAndSetIfChanged(ref topItems, value);
			}
		}

		public QuestionS14PageViewModel(IAppCoreContext context, LessonManager lm, BaseQuestionsModel model, bool isChallenge, bool isExtChallenge = false) : base(context, lm, model, isChallenge, isExtChallenge)
		{
		}

		public override void PopulateData()
		{
			var s14Model = (model as S14QuestionModel);
			Items = s14Model.Points;
			CorrectAnswers = s14Model.CorrectAnswers;
			YAxisElements = s14Model.YAxisTitles;
			XAxisElements = s14Model.XAxisTitles;

			var items = s14Model.Answers;
			//@@CM imposto il local path
			foreach (var it in items)
			{
				if (it != null)
				{
					foreach (var i in it.SubAnswers)
						i.ImagePath = !String.IsNullOrEmpty(i.ImagePath) ? Content.GetLocalPath(i.ImagePath) : null;
				}
			}

			TopItems = items;

			BottomImage = !String.IsNullOrEmpty(s14Model.BottomImagePath) ? Contents.GetLocalPath(s14Model.BottomImagePath) : null;
		}
	}
}
