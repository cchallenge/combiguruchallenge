﻿using System;
using System.Windows.Input;
using AppCore;
using AppCore.Interfaces;
using Nostromo.Interfaces;
using ReactiveUI;
using UnoxEduAppModule;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Nostromo;
using Nostromo.Navigation;

namespace EduApp.Core
{
	public class FineLivelloPageViewModel : BaseViewModel
	{
		string subtitle;
		public string Subtitle
		{
			get { return subtitle; }
			set
			{
				this.RaiseAndSetIfChanged(ref subtitle, value);
			}
		}

		string image;
		public string Image
		{
			get { return image; }
			set
			{
				this.RaiseAndSetIfChanged(ref image, value);
			}
		}

		ICommand nextCommand;
		public ICommand NextCommand
		{
			get { return nextCommand; }
			set
			{
				this.RaiseAndSetIfChanged(ref nextCommand, value);
			}
		}

		public string UnoxLink { get; set;}

		public FineLivelloPageViewModel(IAppCoreContext context) : base (context)
		{
			var currentLevel = (Level)context.GetItem(Globals.LevelKey);
			Title = currentLevel.Title;
            Subtitle = String.Format("{0}\n{1} {2}!", Catalog.GetValue("GreatJob"), Catalog.GetValue("YouSuccessfullyCompleted"), Title);

            //UnoxLink = "http://www.unox.com/it/";

            NextCommand = ReactiveCommand.CreateFromTask(async () => await Navigator.PopModalAsync(true));
            //FIXME: Scaricare da /spot/qualcosa e prenderla random
        }

        async Task OnError(string error, IProgressDialog loader)
        {
            loader.Dispose();
            await UserDialogs.Instance.AlertAsync(error);
            await Navigator.PopAsync(true);
        }

        public override async System.Threading.Tasks.Task Appearing()
		{
			base.Appearing();
            var res = (await Content.List(Globals.SpotRegex)).Where(f => Content.IsFetched(f.FileKey)).ToArray();
            Image = Content.GetLocalPath(GetRandomImage(res));

            using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
            {
                UnoxLink = await UAM.GetUnoxLink();

                // _ext_
                int userPoints = await UAM.GetUserPoints(m => OnError(m, load));//Int32.Parse((Context.UserIdentity as _ext_UnoxUserIdentity).Score); 
                Subtitle += String.Format("\n\n{0}: {1}", Catalog.GetValue("Punteggio"), userPoints);

            }
        }

		Random rand;
		string GetRandomImage(Document[] images)
		{
			if (rand == null)
			{
				rand = new Random();
			}
			int toSkip = rand.Next(0, images.Count());

			return images.Skip(toSkip).Take(1).First().FileKey;
		}
	}
}
