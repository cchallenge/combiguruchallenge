﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppCore.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public class QuestionS10S11S12PageViewModel : BaseGraficoViewModel
	{
		string topImage;
		public string TopImage
		{
			get { return topImage; }
			set
			{
				this.RaiseAndSetIfChanged(ref topImage, value);
			}
		}

		public QuestionS10S11S12PageViewModel(IAppCoreContext context, LessonManager lm, BaseQuestionsModel model, bool isChallenge, bool isExtChallenge = false) : base(context, lm, model, isChallenge, isExtChallenge)
		{

		}

		public override void PopulateData()
		{
			var s101112Model = (model as S10S11S12QuestionModel);
			Items = s101112Model.Points;
			CorrectAnswers = s101112Model.CorrectAnswers;
			YAxisElements = s101112Model.YAxisTitles;
			XAxisElements = s101112Model.XAxisTitles;

			BottomImage = !String.IsNullOrEmpty(s101112Model.BottomImagePath) ? Contents.GetLocalPath(s101112Model.BottomImagePath) : null;
			TopImage = !String.IsNullOrEmpty(s101112Model.TopImagePath) ? Contents.GetLocalPath(s101112Model.TopImagePath) : null;
		}
	}
}
