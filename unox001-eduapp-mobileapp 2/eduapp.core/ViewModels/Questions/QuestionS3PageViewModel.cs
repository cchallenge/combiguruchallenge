﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppCore.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public class QuestionS3PageViewModel : BaseQuestionViewModel
	{
		IEnumerable<S3Response> responseList;
		public IEnumerable<S3Response> ResponseList
		{
			get { return responseList; }
			set
			{
				this.RaiseAndSetIfChanged(ref responseList, value);
			}
		}

		string questionImage;
		public string QuestionImage
		{
			get { return questionImage; }
			set
			{
				this.RaiseAndSetIfChanged(ref questionImage, value);
			}
		}

		List<bool> resultsResponse;

		public QuestionS3PageViewModel(IAppCoreContext context, LessonManager lm, BaseQuestionsModel model, bool isChallenge, bool isExtChallenge = false) : base(context, lm, model, isChallenge, isExtChallenge)
		{
			ButtonTitle = Catalog.GetValue("Continue");
			IsButtonEnabled = false;
			resultsResponse = new List<bool> ();
		}

		public override async Task OnCheckTap()
		{
			lm.LogData (!resultsResponse.Contains (false), model.ID);
			await lm.NextStep(model, this.Navigator);
		}

		bool first = true;
		public override async Task Appearing()
		{
			if (first)
			{
				first = false;
				QuestionTitle = model.Title;
				QuestionSubtitle = model.Description;
				var qImage = (model as S3QuestionModel).ImagePath;
				QuestionImage = !String.IsNullOrEmpty( qImage ) ? Contents.GetLocalPath(qImage) : null;

				var response = (model as S3QuestionModel).Answers.Select(r => new S3Response()
				{
					ImageSource = r.ImagePath != null ? Contents.GetLocalPath(r.ImagePath) : null,
					CorrectOrder = r.CorrectOrder,
					StatoRisposta = AnswerState.None,
					IsEnabledButton = true,
					Text = r.Text,
					StarBkgColor = r.BkgColor
				}).ToList();

				ResponseList = response;
			}
		}

		public bool VerifyButtonOrder(int currentPos, int correctPos)
		{
			//@@ CM prendo l'oggetto con quella posizione
			var elem = ResponseList.FirstOrDefault(e => e.CorrectOrder == correctPos);
			elem.IsEnabledButton = false;
			if (currentPos == correctPos)
			{
				elem.StatoRisposta = AnswerState.Corretto;
				resultsResponse.Add (true);
				return true;
			}
			else {
				elem.StatoRisposta = AnswerState.Sbagliato;
				resultsResponse.Add (false);
				return false;
			}
		}

		public void SetAllCorrect()
		{
			foreach (var elem in ResponseList)
			{
				elem.StatoRisposta = AnswerState.DopoCorretto;
				elem.IsEnabledButton = false;
			}

			lm.AddCorrectResponse(model.ID);
			Progress = lm.GetPercentage();
			IsButtonEnabled = true;
		}

		public void ResetAll()
		{
			foreach (var elem in ResponseList)
			{
				elem.StatoRisposta = AnswerState.None;
				elem.IsEnabledButton = true;
			}
		}
	}
}
