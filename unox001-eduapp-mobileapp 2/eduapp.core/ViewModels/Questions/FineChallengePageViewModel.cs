﻿using System;
using System.Windows.Input;
using AppCore;
using AppCore.Interfaces;
using Nostromo.Interfaces;
using ReactiveUI;
using UnoxEduAppModule;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Nostromo.Navigation;

namespace EduApp.Core
{
    public class FineChallengePageViewModel : BaseViewModel
    {
        string subtitle;
        public string Subtitle
        {
            get { return subtitle; }
            set
            {
                this.RaiseAndSetIfChanged(ref subtitle, value);
            }
        }

        string image;
        public string Image
        {
            get { return image; }
            set
            {
                this.RaiseAndSetIfChanged(ref image, value);
            }
        }

        ICommand nextCommand;
        public ICommand NextCommand
        {
            get { return nextCommand; }
            set
            {
                this.RaiseAndSetIfChanged(ref nextCommand, value);
            }
        }

        public string UnoxLink { get; set; }

        public FineChallengePageViewModel(IAppCoreContext context, int correctAnswers = 1, int totalAnswers = 1, bool isFinished = true) : base(context)
        {
            var chCorrId = (string)Context.GetItem(Globals.CurrentChallenge);
            var challengeHeader = (_ext_ChallengeHeader)Context.GetItem(chCorrId);
            Title = challengeHeader.Title;
            float percentage = (float)correctAnswers / (float)totalAnswers;

            if (percentage == 1)
            {
                Subtitle = String.Format("{0}\n{1} {2}!", Catalog.GetValue("GreatJob"), Catalog.GetValue("YouSuccessfullyCompleted"), Title);
            }
            else
            {
                string variableJob = "";

                if( percentage >= 0.6 )
                {
                    variableJob = Catalog.GetValue("MediumJob");
                }
                else if( percentage >= 0.2 && percentage < 0.6 )
                {
                    variableJob = Catalog.GetValue("DecentJob");
                }
                else
                {
                    variableJob = Catalog.GetValue("BadJob");
                }

                string questionOrQuestions = "";
                if( correctAnswers == 1 )
                {
                    questionOrQuestions = Catalog.GetValue("Question");
                }
                else
                {
                    questionOrQuestions = Catalog.GetValue("Questions");
                }

                Subtitle = String.Format("{0}\n{1} {2} {3}", variableJob, Catalog.GetValue("YouSuccessfullyCompleted"), correctAnswers, questionOrQuestions);
            }
            
            //UnoxLink = "http://www.unox.com/it/";

            NextCommand = ReactiveCommand.CreateFromTask(async () => await Navigator.PopModalAsync(true));
            //FIXME: Scaricare da /spot/qualcosa e prenderla random
        }

        async Task OnError(string error, IProgressDialog loader)
        {
            loader.Dispose();
            await UserDialogs.Instance.AlertAsync(error);
            await Navigator.PopAsync(true);
        }

        public override async System.Threading.Tasks.Task Appearing()
        {
            base.Appearing();
            var res = (await Content.List(Globals.SpotRegex)).Where(f => Content.IsFetched(f.FileKey)).ToArray();
            Image = Content.GetLocalPath(GetRandomImage(res));

            using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
            {
                UnoxLink = await UAM.GetUnoxLink();

                // _ext_
                int userPoints = await UAM.GetUserPoints(m => OnError(m, load));//Int32.Parse((Context.UserIdentity as _ext_UnoxUserIdentity).Score); 
                Subtitle += String.Format("\n\n{0}: {1}", Catalog.GetValue("Punteggio"), userPoints);

            }
        }

        Random rand;
        string GetRandomImage(Document[] images)
        {
            if (rand == null)
            {
                rand = new Random();
            }
            int toSkip = rand.Next(0, images.Count());

            return images.Skip(toSkip).Take(1).First().FileKey;
        }
    }
}
