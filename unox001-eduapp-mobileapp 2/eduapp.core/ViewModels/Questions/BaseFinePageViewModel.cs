﻿using System;
using System.Windows.Input;
using AppCore.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public class BaseFinePageViewModel: BaseViewModel
	{
        public ISoundService SoundS { get; set; }

        string topImage;
		public string TopImage
		{
			get { return topImage; }
			set
			{
				this.RaiseAndSetIfChanged(ref topImage, value);
			}
		}

		string topText;
		public string TopText
		{
			get { return topText; }
			set
			{
				this.RaiseAndSetIfChanged(ref topText, value);
			}
		}

		ICommand nextCommand;
		public ICommand NextCommand
		{
			get { return nextCommand; }
			set
			{
				this.RaiseAndSetIfChanged(ref nextCommand, value);
			}
		}

		public BaseFinePageViewModel(IAppCoreContext context) : base (context)
		{
		}
	}
}
