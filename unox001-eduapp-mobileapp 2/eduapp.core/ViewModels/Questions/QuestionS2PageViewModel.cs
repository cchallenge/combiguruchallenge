﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore.Interfaces;
using MvvmHelpers;
using Nostromo.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public class QuestionS2PageViewModel : BaseQuestionViewModel
	{
		IEnumerable<S2Response> leftButtons;
		public IEnumerable<S2Response> LeftButtons
		{
			get { return leftButtons; }
			set
			{
				this.RaiseAndSetIfChanged(ref leftButtons, value);
			}
		}

		IEnumerable<S2Response> rightButtons;
		public IEnumerable<S2Response> RightButtons
		{
			get { return rightButtons; }
			set
			{
				this.RaiseAndSetIfChanged(ref rightButtons, value);
			}
		}

		bool isItemEnabled;
		public bool IsItemEnabled
		{
			get { return isItemEnabled; }
			set
			{
				this.RaiseAndSetIfChanged(ref isItemEnabled, value);
			}
		}

		public ICommand ItemCommand { get; set; }
		List<bool> resultsResponse;

		public QuestionS2PageViewModel(IAppCoreContext context, LessonManager lm, BaseQuestionsModel model, bool isChallenge, bool isExtChallenge = false) : base(context, lm, model, isChallenge, isExtChallenge)
		{
			resultsResponse = new List<bool> ();
			ButtonTitle = Catalog.GetValue("Continue");

			var canExecute = this.WhenAnyValue(x => x.IsItemEnabled);
			ItemCommand = ReactiveCommand.CreateFromTask<string>((b) => TapOnButton(b), canExecute);

			this.IsItemEnabled = true;
		}

		public override async Task OnCheckTap()
		{
			lm.LogData (!resultsResponse.Contains (false), model.ID);
			await lm.NextStep(model, this.Navigator);
		}

		bool first = true;
		public override async Task Appearing()
		{
			base.Appearing();
			if (first)
			{
				first = false;

				QuestionTitle = model.Title;
				QuestionSubtitle = model.Description;

				LeftButtons = (model as S2QuestionModel)
									 .LeftAnswers
									 .Select(x => new S2Response()
									 {
										 ID = x.CorrelationId,
										 LinkedId = x.LinkedCorrelationId,
										 Text = x.Text,
										 StatoRisposta = AnswerState.None,
									 })
									 .ToList();

				RightButtons = (model as S2QuestionModel)
				                      .RightAnswers
									 .Select(x => new S2Response()
									 {
										 ID = x.CorrelationId,
										 LinkedId = x.LinkedCorrelationId,
										 Text = x.Text,
										 StatoRisposta = AnswerState.None
									 })
									 .ToList();
			}
		}

		//Indicano se per quella colonna ho già selezionato un elemento
		bool isLeftTap = false;
		bool isRightTap = false;

		//Elementi selezionati
		S2Response leftItem;
		S2Response rightItem;
		async Task TapOnButton(string correlationId)
		{
			this.IsItemEnabled = false;

			//Se ho tappato un oggetto a sinistra
			var res = LeftButtons.FirstOrDefault(c => c.ID == correlationId);
			if (res != null)
			{
				var check = verifyTap(res, LeftButtons, true);
				if (check)
				{
					isLeftTap = true;
					leftItem = res;
				}
			}
			else {
				res = rightButtons.FirstOrDefault(c => c.ID == correlationId);
				var check = verifyTap(res, RightButtons, false);
				if (check)
				{
					isRightTap = true;
					rightItem = res;
				}
			}

			await checkCompatibility();
			this.IsItemEnabled = true;
		}

		bool verifyTap(S2Response res, IEnumerable<S2Response> list, bool left)
		{
			if (res.StatoRisposta != AnswerState.DopoCorretto)
			{
				if (left ? isLeftTap : isRightTap)
				{
					foreach (var elem in list)
					{
						if (elem.StatoRisposta != AnswerState.DopoCorretto)
							elem.StatoRisposta = AnswerState.None;
					}
				}

				res.StatoRisposta = AnswerState.InAttesa;
				return true;
			}

			return false;
		}

		async Task checkCompatibility()
		{
			if (leftItem != null && rightItem != null)
			{
				//verifico compatibilità
				var check = leftItem.LinkedId == rightItem.ID;
				await Task.Delay(10);
				leftItem.StatoRisposta = check ? AnswerState.Corretto : AnswerState.Sbagliato;
				rightItem.StatoRisposta = check ? AnswerState.Corretto : AnswerState.Sbagliato;

				await Task.Delay(500);
				leftItem.StatoRisposta = check ? AnswerState.DopoCorretto : AnswerState.None;
				rightItem.StatoRisposta = check ? AnswerState.DopoCorretto : AnswerState.None;

				leftItem = null;
				rightItem = null;

				resultsResponse.Add (check);
				verifyButtonEnabled();
			}
		}

		void verifyButtonEnabled()
		{
			foreach (var elem in LeftButtons)
				if (elem.StatoRisposta != AnswerState.DopoCorretto)
					return;


			IsButtonEnabled = true;
			lm.AddCorrectResponse(model.ID);
			Progress = lm.GetPercentage();
		}
	}
}
