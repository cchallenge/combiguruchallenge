﻿using System;
using AppCore.Interfaces;
using ReactiveUI;
using Nostromo.Interfaces;
using UnoxEduAppModule;

namespace EduApp.Core
{ 
	public class FineChallengeLosePageViewModel : BaseFinePageViewModel
	{

		public FineChallengeLosePageViewModel(IAppCoreContext context) : base(context)
		{
            var currentLevel = (Level)context.GetItem(Globals.LevelKey);
			Title = currentLevel.Title;
			TopImage = Content.GetLocalPath(currentLevel.ChallengeImage);
			TopText = currentLevel.ChallengeTitle;

			NextCommand = ReactiveCommand.CreateFromTask(async () => await this.Navigator.PopModalAsync(true));
		}
	}
}
