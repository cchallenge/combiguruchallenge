﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppCore.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public abstract class BaseGraficoViewModel : BaseQuestionViewModel
	{
		string bottomImage;
		public string BottomImage
		{
			get { return bottomImage; }
			set
			{
				this.RaiseAndSetIfChanged(ref bottomImage, value);
			}
		}

		//Matrice degli elementi usata per disegnarli
		bool[,] items;
		public bool[,] Items
		{
			get { return items; }
			set
			{
				this.RaiseAndSetIfChanged(ref items, value);
			}
		}

		//Elementi sull'asse delle x
		string[] xAxisElements;
		public string[] XAxisElements
		{
			get { return xAxisElements; }
			set
			{
				this.RaiseAndSetIfChanged(ref xAxisElements, value);
			}
		}

		//Elementi sull'asse delle y
		string[] yAxisElements;
		public string[] YAxisElements
		{
			get { return yAxisElements; }
			set
			{
				this.RaiseAndSetIfChanged(ref yAxisElements, value);
			}
		}

		//Matrice degli oggetti tappati usata per disegnare le linee
		public bool[,] TappedItems { get; protected set; }

		//Matrice delle risposte corrette
		public bool[,] CorrectAnswers { get; set; }

		//Stato delle risposte date
		AnswerState responseState;
		public AnswerState ResponseState
		{
			get { return responseState; }
			set
			{
				this.RaiseAndSetIfChanged(ref responseState, value);
			}
		}

		//DateTime di aggiornamento della matrice degli oggetti tappati
		DateTime tappedMatrixChange;
		public DateTime TappedMatrixChange
		{
			get { return tappedMatrixChange; }
			set
			{
				this.RaiseAndSetIfChanged(ref tappedMatrixChange, value);
			}
		}

		List<bool> resultsResponse;
		
		public BaseGraficoViewModel(IAppCoreContext context, LessonManager lm, BaseQuestionsModel model, bool isChallenge, bool isExtChallenge) : base(context, lm, model, isChallenge, isExtChallenge)
		{
			ButtonTitle = Catalog.GetValue("Continue");
			IsButtonEnabled = false;
			resultsResponse = new List<bool> ();
		}

		public override async Task OnCheckTap()
		{
			lm.LogData (!resultsResponse.Contains (false), model.ID);
			await lm.NextStep(model, this.Navigator);
		}

		bool first = true;
		public override async Task Appearing()
		{
			base.Appearing();
			if (first)
			{
				first = false;

				ResponseState = AnswerState.InAttesa;
				QuestionTitle = model.Title;
				QuestionSubtitle = model.Description;

				PopulateData();
			}
		}

		public abstract void PopulateData();

		public async void AddPoint(string id)
		{
			if (TappedItems == null)
				TappedItems = new bool[4, 4];

			var pos = Convert.ToInt32(id);
			System.Diagnostics.Debug.WriteLine($"Ricevuto tap da {pos}");

			var y = pos % 4;
			var x = pos / 4;
			//@@ CM devo verificare se per quella colonna ho già tappato un oggetto
			for (int my = 0; my < 4; my++)
			{
				TappedItems[x, my] = false;
			}

			TappedItems[x, y] = true;
			TappedMatrixChange = DateTime.Now;
			//@@ CM se ho tappato un pallino per ogni colonna verifico se il tracciato è corretto
			if (verifyAllColumnTapped())
			{
				var res = TappedItems.Rank == CorrectAnswers.Rank &&
										Enumerable.Range(0, TappedItems.Rank).All(dimension => TappedItems.GetLength(dimension) == CorrectAnswers.GetLength(dimension)) &&
										TappedItems.Cast<bool>().SequenceEqual(CorrectAnswers.Cast<bool>());
				if (res)
				{
					ResponseState = AnswerState.Corretto;
					await Task.Delay(1000);
					resultsResponse.Add (true);
					ResponseState = AnswerState.DopoCorretto;
					lm.AddCorrectResponse(model.ID);
					Progress = lm.GetPercentage();
					IsButtonEnabled = true;
                }
                else {
					ResponseState = AnswerState.Sbagliato;
					await Task.Delay(1000);
					resultsResponse.Add (false);
                    
                    ShowCorrectAnswer();

					//Resetta tutti i dati inseriti
					//resetAllAnswers();

                    ButtonCommand = ReactiveCommand.CreateFromTask(async () => {
                        await lm.NextStep(model, this.Navigator);
                    });
                    await lm.AddWrongResponse(model.ID);
                }
			}
		}

		bool verifyAllColumnTapped()
		{
			bool all = true;
			for (int x = 0; x < TappedItems.GetLength(1); x++)
			{
				//@@ CM verifico se ci sono dei punti tappabili su quella colonna
				var tappableItem = Enumerable.Range(0, Items.GetLength(1)).Select(i => Items[x, i]).Any(y => y);
				if (tappableItem)
					all &= Enumerable.Range(0, TappedItems.GetLength(0)).Select(i => TappedItems[x, i]).Any(y => y);
			}

			System.Diagnostics.Debug.WriteLine($"Ho tappato tutte le colonne? {all}");
			return all;
		}

		void resetAllAnswers()
		{
			TappedItems = null;
			TappedMatrixChange = DateTime.Now;
			ResponseState = AnswerState.InAttesa;
		}

        void ShowCorrectAnswer()
        {

            for(int i = 0; i < CorrectAnswers.GetLength(0); i++)
            {
                for(int j = 0; j< CorrectAnswers.GetLength(1); j++)
                {
                    TappedItems[i, j] = CorrectAnswers[i, j];
                }
            }
            ResponseState = AnswerState.DopoCorretto;
            TappedMatrixChange = DateTime.Now;
        }
    }
}
