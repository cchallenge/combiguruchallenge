﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public class QuestionS5S9PageViewModel : BaseQuestionViewModel
	{
		private List<S5S9ModelId> listModelId;

		private int NumbersOfTrue { get; set; }
		private int Selected { get; set; }

		public ICommand ItemCommand { get; set; }

		IEnumerable<S5S9Response> buttons;
		public IEnumerable<S5S9Response> Buttons 
		{
			get { return buttons; }
			set 
			{
				this.RaiseAndSetIfChanged (ref buttons, value);
			}
		}

		string questionSelector;
		public string QuestionSelector {
			get { return questionSelector; }
			set {
				this.RaiseAndSetIfChanged (ref questionSelector, value);
			}
		}

		bool isItemEnabled;
		protected bool IsItemEnabled {
			get { return isItemEnabled; }
			set {
				this.RaiseAndSetIfChanged (ref isItemEnabled, value);
			}
		}

		public QuestionS5S9PageViewModel(IAppCoreContext context, LessonManager lm, BaseQuestionsModel model, bool isChallenge, bool isExtChallenge = false) : base(context, lm, model, isChallenge, isExtChallenge)
        {
			this.IsItemEnabled = true;
			this.IsButtonEnabled = false;

			var canExecuteItem = this.WhenAnyValue (x => x.IsItemEnabled);
			ItemCommand = ReactiveCommand.CreateFromTask<string> ((b) => TapOnButton (b), canExecuteItem);

			ButtonTitle = Catalog.GetValue ("Check");
		}

		async Task TapOnButton (string Id)
		{
			var countSelected = Buttons.Where (x => x.StatoRisposta == AnswerState.InAttesa).Count ();
			var buttonFromId = Buttons.Where (x => x.ID == Convert.ToInt16 (Id)).FirstOrDefault ();
			if (buttonFromId.StatoRisposta == AnswerState.None && countSelected >= NumbersOfTrue)
				return;
			else {
				if (buttonFromId.StatoRisposta == AnswerState.InAttesa) {
					buttonFromId.StatoRisposta = AnswerState.None;
					Selected--;
				} else {
					buttonFromId.StatoRisposta = AnswerState.InAttesa;
					Selected++;
				}
			}

			ChangeQuestionSelector ();
		}

		public override async Task OnCheckTap ()
		{
			IsButtonEnabled = false;
			IsItemEnabled = false;
			var selected = Buttons.Where (x => x.StatoRisposta == AnswerState.InAttesa);
			bool wrongResp = false;
			foreach (var btn in selected) {
				var listElem = listModelId.Where (x => x.ModelId == btn.ID).FirstOrDefault ();
				if (listElem.Correct)
					btn.StatoRisposta = AnswerState.Corretto;
				else {
					btn.StatoRisposta = AnswerState.Sbagliato;
					wrongResp = true;
				}
			}
			lm.LogData (!wrongResp, model.ID);
			if (wrongResp)
				await lm.AddWrongResponse (model.ID);
			else
				lm.AddCorrectResponse (model.ID);
			
			await Task.Delay (1000);
			foreach (var element in Buttons)
				element.StatoRisposta = (element.IsCorrect) ? AnswerState.DopoCorretto : AnswerState.None;

			ButtonTitle = Catalog.GetValue ("Continue");
			Progress = lm.GetPercentage ();
			ButtonCommand = ReactiveCommand.CreateFromTask (async() => {
				await lm.NextStep (model, this.Navigator);
			});
		}

		bool first = true;
		public override async Task Appearing ()
		{
			base.Appearing();

			if (first) 
			{
				first = false;
				QuestionTitle = model.Title;
				QuestionSubtitle = model.Description;

				int i = 0;
				listModelId = new List<S5S9ModelId> ();
				var modelList = (model as S5S9QuestionModel).Answers;
				foreach (var element in modelList) {
					var s5s9 = new S5S9ModelId () {
						ModelId = i,
						Answer = element.Answer,
						Correct = element.Correct
					};
					i++;
					listModelId.Add (s5s9);
				}

				NumbersOfTrue = listModelId.Where (x => x.Correct == true).Count ();
				Selected = 0;
				ChangeQuestionSelector ();

				var buttons = listModelId.Select (x => new S5S9Response () {
					ID = x.ModelId,
					IsCorrect = x.Correct,
					Text = x.Answer,
					StatoRisposta = AnswerState.None
				}).ToList();

				Buttons = buttons;
			}
		}

		void ChangeQuestionSelector ()
		{
			IsButtonEnabled = Selected == NumbersOfTrue;
			QuestionSelector = String.Format ("{0}/{1}", Selected.ToString (), NumbersOfTrue.ToString ());
		}

	}

	public class S5S9ModelId
	{
		public int ModelId { get; set; }
		public string Answer { get; set; }
		public bool Correct { get; set; }
	}
}
