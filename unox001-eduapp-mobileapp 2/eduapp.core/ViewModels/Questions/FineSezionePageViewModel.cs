﻿using System;
using System.Threading.Tasks;
using AppCore.Interfaces;
using Nostromo.Interfaces;
using ReactiveUI;
using UnoxEduAppModule;
using Acr.UserDialogs;
using Nostromo.Navigation;

namespace EduApp.Core
{
	public class FineSezionePageViewModel: BaseFinePageViewModel
	{
        string subtitle;
        public string Subtitle
        {
            get { return subtitle; }
            set
            {
                this.RaiseAndSetIfChanged(ref subtitle, value);
            }
        }

        public FineSezionePageViewModel(IAppCoreContext context) : base (context)
		{
			var currentSection = (Section)context.GetItem(Globals.SectionKey);
			var currentLevel = (Level)context.GetItem(Globals.LevelKey);

			this.Title = currentLevel.Title;

			TopText = currentSection.Title;
			TopImage = Content.GetLocalPath(currentSection.Image);

            NextCommand = ReactiveCommand.CreateFromTask(async () => await Navigator.PopModalAsync(true));
		}

		public string UnoxLink { get; set; }

		public override async Task Appearing()
		{
			base.Appearing();
            using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
            {
                UnoxLink = await UAM.GetUnoxLink();
                // _ext_
                int userPoints = await UAM.GetUserPoints(m => OnError(m, load));//Int32.Parse((Context.UserIdentity as _ext_UnoxUserIdentity).Score); 
                Subtitle += String.Format("\n\n{0}: {1}", Catalog.GetValue("Punteggio"), userPoints);
            }
        }

        async Task OnError(string error, IProgressDialog loader)
        {
            loader.Dispose();
            await UserDialogs.Instance.AlertAsync(error);
            await Navigator.PopAsync(true);
        }

    }
}
