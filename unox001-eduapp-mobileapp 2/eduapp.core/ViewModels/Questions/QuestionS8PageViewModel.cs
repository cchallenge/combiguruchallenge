﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using AppCore.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public class QuestionS8PageViewModel : BaseQuestionViewModel
	{

		List<S8Response> subElements;
		public List<S8Response> SubElements
		{
			get { return subElements; }
			set { this.RaiseAndSetIfChanged(ref subElements, value); }
		}

		List<S8ResponseList> elements;
		public List<S8ResponseList> Elements
		{
			get { return elements; }
			set { this.RaiseAndSetIfChanged(ref elements, value); }
		}

		public QuestionS8PageViewModel(IAppCoreContext context, LessonManager lm, BaseQuestionsModel model, bool isChallenge, bool isExtChallenge = false) : base(context, lm, model, isChallenge, isExtChallenge)
		{
			this.IsButtonEnabled = false;
			ButtonTitle = Catalog.GetValue("Check");
		}

		bool first = true;
		public override async Task Appearing()
		{
			base.Appearing();
			if (first)
			{

				Elements = new List<S8ResponseList>();

				first = false;
				QuestionTitle = model.Title;
				QuestionSubtitle = model.Description;

				var modelList = (model as S8QuestionModel).Answers;

				foreach (var sub in modelList)
				{
					var subModelList = sub.SubAnswers;
					var elementsList = subModelList.Select(x => new S8Response()
					{
						Text = Helper.LowerTrimmedString(x.Answer),
						MinResponse = x.MinValueAnswer,
						MaxResponse = x.MaxValueAnswer,
						Measure = x.UnitMeasure,
						IconSource = !String.IsNullOrEmpty(x.ImagePath) ? Contents.GetLocalPath(x.ImagePath) : null,
						IsIconVisible = !String.IsNullOrEmpty(x.ImagePath)
					}).ToList();

					S8ResponseList s8 = new S8ResponseList();
					SubElements = elementsList;
					s8.ResponseList = SubElements;
					s8.StatoRisposta = AnswerState.None;
					s8.IsEditable = true;

					Elements.Add(s8);

					this.SubElements.Select(x => x.WhenAny(i => i.Response,
															i => SubElements.All(i2 => !String.IsNullOrEmpty(i2.Response)))
					)
					.Merge()
					.Distinct()
					.Subscribe(allItemFilled => this.IsButtonEnabled = allItemFilled);
				}
			}
		}

		System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^(?:-?\d{1,5})([.,][0-9]{1,2})?$");
		public override async Task OnCheckTap()
		{
			bool wrongResp = false;

			foreach (var allElements in Elements)
			{
				var list = allElements.ResponseList;

				List<bool> listOfBool = new List<bool>();

				foreach (var test in list)
				{
					bool match = regex.IsMatch(test.Text);
					if (!String.IsNullOrEmpty(test.MinResponse) && !String.IsNullOrEmpty(test.MaxResponse) && match)
					{

						bool res = EvaluateResponseWithRange(test.Response, test.MinResponse, test.MaxResponse);
						listOfBool.Add(res);
					}
					else
						listOfBool.Add(test.Text.Equals(Helper.LowerTrimmedString(test.Response)));
					if (listOfBool.Count == 4)
					{

						if (listOfBool.Contains(false))
						{
							allElements.StatoRisposta = AnswerState.Sbagliato;
							wrongResp = true;

						}
						else
							allElements.StatoRisposta = AnswerState.Corretto;

						allElements.IsEditable = false;

						listOfBool.Clear();
					}
				}

			}

			lm.LogData(!wrongResp, model.ID);
			if (wrongResp)
				await lm.AddWrongResponse(model.ID);
			else
				lm.AddCorrectResponse(model.ID);

			await Task.Delay(1000);
			foreach (var els in Elements)
			{
				els.StatoRisposta = AnswerState.DopoCorretto;
				foreach (var el in els.ResponseList)
					el.Response = el.Text;
			}

			ButtonTitle = Catalog.GetValue("Continue");
			Progress = lm.GetPercentage();

			ButtonCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				await lm.NextStep(model, this.Navigator);
			});
		}

		bool EvaluateResponseWithRange(string valore, string minVal, string maxVal)
		{
			valore = valore?.Replace(",", ".");
			minVal = minVal?.Replace(",", ".");
			maxVal = maxVal?.Replace(",", ".");

			double dminValue = 0;
			double.TryParse(minVal, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out dminValue);
			double dmaxValue = 0;
			double.TryParse(maxVal, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out dmaxValue);
			double val = 0;
			double.TryParse(valore, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out val);
			if (val < 0)
			{
				if (val <= dminValue && val >= dmaxValue)
					return true;
				else
				{
					return false;
				}
			}
			else
			{
				if (val >= dminValue && val <= dmaxValue)
					return true;
				else
				{
					return false;
				}
			}
		}
	}
}
