﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore.DeferredAppContentModule;
using AppCore.Interfaces;
using Nostromo;
using Nostromo.Interfaces;
using Nostromo.Navigation;
using ReactiveUI;
using UnoxEduAppModule;
using AppCore;

namespace EduApp.Core
{
	public abstract class BaseQuestionViewModel : BaseViewModel
	{
		public override string TextColorHex
		{
			get { return "#FFFFFF"; }
		}

		public override string BarBkgColorHex
		{
			get
			{
				return "#00000000";
			}
		}

		//public string QuestionId { get; set;}
		public ICommand CloseViewCommand { get; set;}
		public DeferredAppContentModule Contents { get; set;}
		protected readonly LessonManager lm;
		protected readonly BaseQuestionsModel model;

		string backgroundQuestionColor;
		public string BackgroundQuestionColor
        {
			get { return backgroundQuestionColor; }
			set
			{
				this.RaiseAndSetIfChanged(ref backgroundQuestionColor, value);
			}
		}

		string questionTitle;
		public string QuestionTitle
		{
			get { return questionTitle; }
			set
			{
				this.RaiseAndSetIfChanged(ref questionTitle, value);
			}
		}

		string questionSubtitle;
		public string QuestionSubtitle
		{
			get { return questionSubtitle; }
			set
			{
				this.RaiseAndSetIfChanged(ref questionSubtitle, value);
			}
		}

		string buttonTitle;
		public string ButtonTitle
		{
			get { return buttonTitle; }
			set
			{
				this.RaiseAndSetIfChanged(ref buttonTitle, value);
			}
		}

		double progress;
		public double Progress
		{
			get { return progress; }
			set
			{
				this.RaiseAndSetIfChanged(ref progress, value);
			}
		}

		bool isButtonEnabled;
		public bool IsButtonEnabled
		{
			get { return isButtonEnabled; }
			set
			{
				this.RaiseAndSetIfChanged(ref isButtonEnabled, value);
			}
		}

		ICommand buttonCommand;
		public ICommand ButtonCommand
		{
			get { return buttonCommand; }
			set
			{
				this.RaiseAndSetIfChanged(ref buttonCommand, value);
			}
		}

		readonly bool isChallenge;
		readonly bool isExtChallenge;

        public BaseQuestionViewModel(IAppCoreContext context, LessonManager lm, BaseQuestionsModel model, bool isChallenge, bool isExtChallenge = false) : base (context)
		{
            System.Diagnostics.Debug.WriteLine("///Building question model base view !");
			this.isChallenge = isChallenge;
			this.isExtChallenge = isExtChallenge;

            //XXX - cambia il colore di sfondo in base al tipo di Challenge
            BackgroundQuestionColor = "#FECE5E"; // UnoxOrange

            if (isExtChallenge)
            {
                var chCorrId = (string)Context.GetItem(Globals.CurrentChallenge);
                var challengeHeader = (_ext_ChallengeHeader)Context.GetItem(chCorrId);
                System.Diagnostics.Debug.WriteLine("///Peeking into context challenge data which were found " + (challengeHeader!=null));
                this.Title = challengeHeader.Title;

                BackgroundQuestionColor = "#885EAD"; // BackgroundExtChallenge
            }
            else if (!isChallenge)
            {
                var currentSection = (Section)Context.GetItem(Globals.SectionKey);
                this.Title = currentSection.Title;
            }
            else
            {
                var currentLevel = (Level)Context.GetItem(Globals.LevelKey);
                this.Title = currentLevel.Title;
            }
            System.Diagnostics.Debug.WriteLine("///Just set question model Title !");
            this.model = model;
			this.lm = lm;
			Progress = lm.GetPercentage();
            System.Diagnostics.Debug.WriteLine("///Set other challenge parameters !");
            CloseViewCommand = ReactiveCommand.CreateFromTask(() => RequestClose());

			var canExecute = this.WhenAnyValue(x => x.IsButtonEnabled);
			ButtonCommand = ReactiveCommand.CreateFromTask(() => OnCheckTap(), canExecute);
		}

		async Task RequestClose()
		{
            if (isExtChallenge && lm.UserPathStarted())
            {
                if (await Acr.UserDialogs.UserDialogs.Instance.ConfirmAsync(Catalog.GetValue("ExtChallengeExitRequest"), okText: Catalog.GetValue("SiResponse"), cancelText: Catalog.GetValue("NoResponse")))
                {
                    await lm.ForceChallengeFinished();
                    await Navigator.PopModalAsync(true);
                }
            }
            else
            {
                if (await Acr.UserDialogs.UserDialogs.Instance.ConfirmAsync(Catalog.GetValue("ExitRequest"), okText: Catalog.GetValue("SiResponse"), cancelText: Catalog.GetValue("NoResponse")))
                {
                    if (isChallenge || isExtChallenge)
                    {
                        await Navigator.PopModalAsync(true);
                    }
                    else
                    {
                        await Navigator.SetNewRootAsync(Build.ViewModel(() => new SectionPageViewModel(Context)), true);
                    }
                }
            }
		}

		public abstract Task OnCheckTap();
	}
}
