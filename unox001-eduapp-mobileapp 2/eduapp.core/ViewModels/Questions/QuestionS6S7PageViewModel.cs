﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public class QuestionS6S7PageViewModel : BaseQuestionViewModel
	{
		public ICommand ItemCommand { get; set; }

		List<S6S7Response> elements;
		public List<S6S7Response> Elements
		{
			get { return elements; }
			set { this.RaiseAndSetIfChanged(ref elements, value); }
		}

		public QuestionS6S7PageViewModel(IAppCoreContext context, LessonManager lm, BaseQuestionsModel model, bool isChallenge, bool isExtChallenge = false) : base(context, lm, model, isChallenge, isExtChallenge)
		{
			this.IsButtonEnabled = false;

			Elements = new List<S6S7Response>();
			ButtonTitle = Catalog.GetValue("Check");
		}

		System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^(?:-?\d{1,5})([.,][0-9]{1,2})?$");
		public override async Task OnCheckTap()
		{
			bool wrongResp = false;

			foreach (var element in Elements)
			{
				bool match = regex.IsMatch(element.Text);
				if (!String.IsNullOrEmpty(element.MinResponse) && !String.IsNullOrEmpty(element.MaxResponse) && match)
				{
					if (EvaluateResponseWithRange(element.Response, element.MinResponse, element.MaxResponse))
						element.StatoRisposta = AnswerState.Corretto;
					else
					{
						element.StatoRisposta = AnswerState.Sbagliato;
						wrongResp = true;
					}
				}
				else
				{
					if (element.Text.Equals(Helper.LowerTrimmedString(element.Response)))
					{
						element.StatoRisposta = AnswerState.Corretto;
					}
					else
					{
						element.StatoRisposta = AnswerState.Sbagliato;
						wrongResp = true;
					}
				}
				element.IsEditable = false;
			}

			lm.LogData(!wrongResp, model.ID);
			if (wrongResp)
				await lm.AddWrongResponse(model.ID);
			else
				lm.AddCorrectResponse(model.ID);

			await Task.Delay(1000);
			foreach (var element in Elements)
			{
				element.StatoRisposta = AnswerState.DopoCorretto;
				element.Response = element.Text;
			}
			ButtonTitle = Catalog.GetValue("Continue");
			Progress = lm.GetPercentage();
			ButtonCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				await lm.NextStep(model, this.Navigator);
			});
		}

		bool EvaluateResponseWithRange(string valore, string minVal, string maxVal)
		{
			valore = valore?.Replace(",", ".");
			minVal = minVal?.Replace(",", ".");
			maxVal = maxVal?.Replace(",", ".");

			double dminValue = 0;
			double.TryParse(minVal, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out dminValue);
			double dmaxValue = 0;
			double.TryParse(maxVal, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out dmaxValue);
			double val = 0;
			double.TryParse(valore, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out val);
			if (val < 0)
			{
				if (val <= dminValue && val >= dmaxValue)
					return true;
				else
				{
					return false;
				}
			}
			else
			{
				if (val >= dminValue && val <= dmaxValue)
					return true;
				else
				{
					return false;
				}
			}
		}

		bool first = true;
		public override async Task Appearing()
		{
			if (first)
			{
				first = false;
				QuestionTitle = model.Title;
				QuestionSubtitle = model.Description;

				var modelList = (model as S6S7QuestionModel).Answers;

				var elementsList = modelList.Select(x => new S6S7Response()
				{
					Text = Helper.LowerTrimmedString(x.Answer),
					MinResponse = x.MinValueAnswer,
					MaxResponse = x.MaxValueAnswer,
					Measure = x.UnitMeasure,
					IconSource = !String.IsNullOrEmpty(x.ImagePath) ? Contents.GetLocalPath(x.ImagePath) : null,
					IsIconVisible = !String.IsNullOrEmpty(x.ImagePath),
					StatoRisposta = AnswerState.None,
					IsEditable = true
				}).ToList();

				Elements = elementsList;

				this.Elements.Select(x => x.WhenAny(i => i.Response,
										 i => Elements.All(i2 => !String.IsNullOrEmpty(i2.Response)))
					)
				.Merge()
				.Distinct()
				.Subscribe(allItemFilled => this.IsButtonEnabled = allItemFilled);

			}
			base.Appearing();
		}
	}
}
