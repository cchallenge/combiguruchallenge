﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppCore.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public class QuestionS13PageViewModel : BaseQuestionViewModel
	{
		public List<bool> logResultsResponse;

		List<S13ResponseList> elements;
		public List<S13ResponseList> Elements {
			get { return elements; }
			set { this.RaiseAndSetIfChanged (ref elements, value); }
		}

		List<SBaseModel> xAxis;
		public List<SBaseModel> XAxis {
			get { return xAxis; }
			set { this.RaiseAndSetIfChanged (ref xAxis, value); }
		}
        
		public QuestionS13PageViewModel(IAppCoreContext context, LessonManager lm, BaseQuestionsModel model, bool isChallenge, bool isExtChallenge = false) : base(context, lm, model, isChallenge, isExtChallenge)
        {
			this.IsButtonEnabled = false;
			ButtonTitle = Catalog.GetValue ("Continue");
			logResultsResponse = new List<bool> ();
		}

		bool first = true;
		public async override Task Appearing ()
		{
			base.Appearing ();
			if (first) 
			{

				Elements = new List<S13ResponseList> ();

				first = false;

				QuestionTitle = model.Title;
				QuestionSubtitle = model.Description;

				var modelList = (model as S13QuestionModel).Answers;
				var xax = (model as S13QuestionModel).XAxis.ToList();
				xax = xax.Select ( x => new SBaseModel { 
					ImagePath = !String.IsNullOrEmpty (x.ImagePath) ? Contents.GetLocalPath (x.ImagePath) : null,
					Answer = x.Answer,
					UnitMeasure = x.UnitMeasure
				}).ToList();
				XAxis = xax;

				foreach (var sub in modelList) 
				{
					var subModelList = sub.SubAnswers;
					subModelList = subModelList.Select (x => new SBaseModel {
						ImagePath = !String.IsNullOrEmpty (x.ImagePath) ? Contents.GetLocalPath (x.ImagePath) : null,
						Answer = x.Answer,
						UnitMeasure = x.UnitMeasure,
					});

					S13ResponseList s13 = new S13ResponseList();
					s13.ResponseList = subModelList.ToList ();
					s13.StatoRisposta = AnswerState.None;
					s13.Order = sub.Order;

					Elements.Add (s13);
				}
			}
		}

		public void ProceedProgress ()
		{
			lm.AddCorrectResponse (model.ID);
			Progress = lm.GetPercentage ();
		}

		public async override Task OnCheckTap ()
		{
			lm.LogData (!logResultsResponse.Contains (false), model.ID);
			await lm.NextStep (model, this.Navigator);
		}
	}
}
