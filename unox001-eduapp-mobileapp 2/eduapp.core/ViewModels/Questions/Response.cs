﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using ReactiveUI;

namespace EduApp.Core
{

	public class S13ResponseList : ReactiveObject
	{
		List<SBaseModel> responseList;
		public List<SBaseModel> ResponseList {
			get { return responseList; }
			set { this.RaiseAndSetIfChanged (ref responseList, value); }
		}

		AnswerState statoRisposta;
		public AnswerState StatoRisposta {
			get { return statoRisposta; }
			set {
				this.RaiseAndSetIfChanged (ref statoRisposta, value);
			}
		}

		int order;
		public int Order {
			get { return order; }
			set { order = value; }
		}
	}

	public class S8ResponseList : ReactiveObject
	{
		List<S8Response> responseList;
		public List<S8Response> ResponseList 
		{
			get { return responseList; }
			set { this.RaiseAndSetIfChanged (ref responseList, value); }
		}

		AnswerState statoRisposta;
		public AnswerState StatoRisposta {
			get { return statoRisposta; }
			set {
				this.RaiseAndSetIfChanged (ref statoRisposta, value);
			}
		}

		bool isEditable;
		public bool IsEditable {
			get { return isEditable; }
			set {
				this.RaiseAndSetIfChanged (ref isEditable, value);
			}
		}
	}

	public class S8Response : ReactiveObject
	{
		string text;
		public string Text {
			get { return text; }
			set {
				this.RaiseAndSetIfChanged (ref text, value);
			}
		}

		bool isIconVisible;
		public bool IsIconVisible {
			get { return isIconVisible; }
			set {
				this.RaiseAndSetIfChanged (ref isIconVisible, value);
			}
		}

		string measure;
		public string Measure {
			get { return measure; }
			set {
				this.RaiseAndSetIfChanged (ref measure, value);
			}
		}

		string iconSource;
		public string IconSource {
			get { return iconSource; }
			set {
				this.RaiseAndSetIfChanged (ref iconSource, value);
			}
		}

		string response;
		public string Response {
			get { return response; }
			set {
				this.RaiseAndSetIfChanged (ref response, value);
			}
		}

		public string MinResponse { get; set; }

		public string MaxResponse { get; set; }

	}

	public class S6S7Response : ReactiveObject
	{
		public int ID { get; set; }

		string text;
		public string Text {
			get { return text; }
			set {
				this.RaiseAndSetIfChanged (ref text, value);
			}
		}

		bool isEditable;
		public bool IsEditable {
			get { return isEditable; }
			set {
				this.RaiseAndSetIfChanged (ref isEditable, value);
			}
		}

		bool isIconVisible;
		public bool IsIconVisible {
			get { return isIconVisible; }
			set {
				this.RaiseAndSetIfChanged (ref isIconVisible, value);
			}
		}

		string measure;
		public string Measure {
			get { return measure; }
			set {
				this.RaiseAndSetIfChanged (ref measure, value);
			}
		}

		string iconSource;
		public string IconSource {
			get { return iconSource; }
			set {
				this.RaiseAndSetIfChanged (ref iconSource, value);
			}
		}

		AnswerState statoRisposta;
		public AnswerState StatoRisposta {
			get { return statoRisposta; }
			set {
				this.RaiseAndSetIfChanged (ref statoRisposta, value);
			}
		}

		string response;
		public string Response {
			get { return response; }
			set {
				this.RaiseAndSetIfChanged (ref response, value);
			}
		}

		public string MinResponse { get; set; }

		public string MaxResponse { get; set; }

	}


	public class S5S9Response : ReactiveObject
	{
		public int ID { get; set; }
		public bool IsCorrect { get; set; }

		string text;
		public string Text 
		{
			get { return text; }
			set {
				this.RaiseAndSetIfChanged (ref text, value);
			} 
		}

		AnswerState statoRisposta;
		public AnswerState StatoRisposta {
			get { return statoRisposta; }
			set {
				this.RaiseAndSetIfChanged (ref statoRisposta, value);
			}
		}
	}

	public class S4Response : ReactiveObject
	{
		public string ID { get; set;}
		public bool IsCorrect { get; set;}
		
		bool enabled;
		public bool IsEnabledButton
		{
			get { return enabled; }
			set
			{
				this.RaiseAndSetIfChanged(ref enabled, value);
			}
		}

		public ICommand S4Command { get; set; }
		AnswerState statoRisposta;
		public AnswerState StatoRisposta
		{
			get { return statoRisposta; }
			set
			{
				this.RaiseAndSetIfChanged(ref statoRisposta, value);
			}
		}

		string imageSource;
		public string ImageSource
		{
			get { return imageSource; }
			set
			{
				this.RaiseAndSetIfChanged(ref imageSource, value);
			}
		}
	}

	public class S3Response : ReactiveObject
	{
		//@@ CM Posizione per ritenere la risposta corretta
		public int CorrectOrder { get; set;}
		//@@ CM Colore di sfondo che viene dal CMS
		public string StarBkgColor { get; set;}

		bool enabled;
		public bool IsEnabledButton
		{
			get { return enabled; }
			set
			{
				this.RaiseAndSetIfChanged(ref enabled, value);
			}
		}

		AnswerState statoRisposta;
		public AnswerState StatoRisposta
		{
			get { return statoRisposta; }
			set
			{
				this.RaiseAndSetIfChanged(ref statoRisposta, value);
			}
		}

		string imageSource;
		public string ImageSource
		{
			get { return imageSource; }
			set
			{
				this.RaiseAndSetIfChanged(ref imageSource, value);
			}
		}

		string text;
		public string Text
		{
			get { return text; }
			set
			{
				this.RaiseAndSetIfChanged(ref text, value);
			}
		}
	}

	public class S2Response : ReactiveObject
	{
		public string ID { get; set; }
		public string LinkedId { get; set;}

		string text;
		public string Text
		{
			get { return text; }
			set
			{
				this.RaiseAndSetIfChanged(ref text, value);
			}
		}

		public ICommand S4Command { get; set; }
		AnswerState statoRisposta;
		public AnswerState StatoRisposta
		{
			get { return statoRisposta; }
			set
			{
				this.RaiseAndSetIfChanged(ref statoRisposta, value);
			}
		}
	}
}
