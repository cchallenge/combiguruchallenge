﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore.Interfaces;
using Nostromo;
using Nostromo.Navigation;
using ReactiveUI;

namespace EduApp.Core
{
	public class QuestionS4PageViewModel : BaseQuestionViewModel
	{
		IEnumerable<S4Response> responseList;
		public IEnumerable<S4Response> ResponseList
		{
			get { return responseList; }
			set
			{
				this.RaiseAndSetIfChanged(ref responseList, value);
			}
		}

		public QuestionS4PageViewModel(IAppCoreContext context, LessonManager lm, BaseQuestionsModel model, bool isChallenge, bool isExtChallenge = false) : base(context, lm, model, isChallenge, isExtChallenge)
		{
            System.Diagnostics.Debug.WriteLine("///Constructor:: Preparing question template !");
			ButtonTitle = Catalog.GetValue("Check");
			IsButtonEnabled = false;
		}

		bool first = true;
		public override async Task Appearing()
		{
            System.Diagnostics.Debug.WriteLine("///Appearning:: Preparing question template !");
            base.Appearing();

            if (first)
			{
				first = false;

				QuestionTitle = model.Title;
				QuestionSubtitle = model.Description;

				var response = (model as S4QuestionModel).Answers.Select(r => new S4Response()
				{
					ImageSource = r.ImagePath != null ? Contents.GetLocalPath(r.ImagePath) : null,
					IsCorrect = r.Correct,
					StatoRisposta = AnswerState.None,
					IsEnabledButton = true
				}).ToList();

				foreach (var elem in response)
				{
					elem.S4Command = GetButtonCommand(elem);
				}

				ResponseList = response;
			}
		}

		S4Response selectedItem;
		public override async Task OnCheckTap()
		{
			IsButtonEnabled = false;
			foreach (var elem in ResponseList)
				elem.IsEnabledButton = false;

			if (selectedItem.IsCorrect)
			{
				lm.LogData (true, model.ID);
				lm.AddCorrectResponse(model.ID);
				selectedItem.StatoRisposta = AnswerState.Corretto;
			}
			else {
				lm.LogData (false, model.ID);
				await lm.AddWrongResponse (model.ID);
				selectedItem.StatoRisposta = AnswerState.Sbagliato;
			}

			await Task.Delay(1000);
			foreach (var elem in ResponseList)
				elem.StatoRisposta = AnswerState.None;
			
			ResponseList.FirstOrDefault(e => e.IsCorrect).StatoRisposta = AnswerState.DopoCorretto;

			ButtonTitle = Catalog.GetValue("Continue");
			Progress = lm.GetPercentage();

			ButtonCommand = ReactiveCommand.CreateFromTask(async () => {
				await lm.NextStep(model, this.Navigator);
			});
		}

		ICommand GetButtonCommand (S4Response button) { 
			var canExecute = button.WhenAnyValue(x => x.IsEnabledButton);
			return ReactiveCommand.CreateFromTask(() => OnItemTap(button), canExecute);
		}

		async Task OnItemTap(S4Response button)
		{
			IsButtonEnabled = true;

			foreach (var elem in ResponseList)
				elem.StatoRisposta = AnswerState.None;

			selectedItem = button;
			button.StatoRisposta = AnswerState.InAttesa;
		}
	}
}
