﻿using System;
using AppCore.Interfaces;
using Nostromo;
using Nostromo.Navigation;
using Nostromo.Interfaces;
using ReactiveUI;
using UnoxEduAppModule;

namespace EduApp.Core
{
	public class FineLezionePageViewModel : BaseFinePageViewModel
	{
		IAppCoreContext _context;


        string subtitle;
        public string Subtitle
        {
            get { return subtitle; }
            set
            {
                this.RaiseAndSetIfChanged(ref subtitle, value);
            }
        }


        public FineLezionePageViewModel(IAppCoreContext context, bool lastLesson) : base (context)
		{
			_context = context;
			var currentSection = (Section)context.GetItem(Globals.SectionKey);
			var currentLevel = (Level)context.GetItem(Globals.LevelKey);

			this.Title = currentLevel.Title;

			TopText = currentSection.Title;
			TopImage = Content.GetLocalPath(currentSection.Image);

            int userPoints = Int32.Parse((Context.UserIdentity as _ext_UnoxUserIdentity).Score); //await UAM.GetUserPoints(m => OnError(m, load));
            Subtitle = String.Format("{0}\n{1}: {2}", Catalog.GetValue("GreatJob"), Catalog.GetValue("Punteggio"), userPoints);

            if (lastLesson)
				NextCommand = ReactiveCommand.CreateFromTask (async () => await Navigator.PopModalAsync (true));
			else
				NextCommand = ReactiveCommand.CreateFromTask (async () => await Navigator.SetNewRootAsync (Build.ViewModel (() => new SectionPageViewModel(context, true)), true));
			
		}
	}
}
