﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore.Interfaces;
using Nostromo;
using Nostromo.Navigation;
using ReactiveUI;

namespace EduApp.Core
{
	public class LoginPageViewModel : BaseViewModel
	{
		string titlePage;
		public string TitlePage
		{
			get { return titlePage; }
			set
			{
				this.RaiseAndSetIfChanged(ref titlePage, value);
			}
		}

		string subtitlePage;
		public string SubtitlePage
		{
			get { return subtitlePage; }
			set
			{
				this.RaiseAndSetIfChanged(ref subtitlePage, value);
			}
		}

		string loginText;
		public string LoginText
		{
			get { return loginText; }
			set
			{
				this.RaiseAndSetIfChanged(ref loginText, value);
			}
		}

		string forgotText;
		public string ForgotText
		{
			get { return forgotText; }
			set
			{
				this.RaiseAndSetIfChanged(ref forgotText, value);
			}
		}

		string usernameTitle;
		public string UsernameTitle {
			get { return usernameTitle; }
			set {
				this.RaiseAndSetIfChanged (ref usernameTitle, value);
			}
		}

		string passwordTitle;
		public string PasswordTitle {
			get { return passwordTitle; }
			set {
				this.RaiseAndSetIfChanged (ref passwordTitle, value);
			}
		}

		string username;
		public string Username
		{
			get { return username; }
			set
			{
				this.RaiseAndSetIfChanged(ref username, value);
			}
		}

		string password;
		public string Password
		{
			get { return password; }
			set
			{
				this.RaiseAndSetIfChanged(ref password, value);
			}
		}

		public ICommand LoginCommand { get; set; }

		public LoginPageViewModel(IAppCoreContext context) : base(context)
		{
			this.TitlePage = Catalog.GetValue("LoginTitle");
			this.SubtitlePage = Catalog.GetValue("LoginSubtitle");
			this.LoginText = Catalog.GetValue("Login");
			this.ForgotText = Catalog.GetValue("ForgotPwd");
			this.PasswordTitle = Catalog.GetValue("Pwd");
			this.UsernameTitle = Catalog.GetValue("Usr");

			LoginCommand = ReactiveCommand.CreateFromTask(() => Login());
		}

		public override void Load()
		{
			base.Load();

			AnaS.TrackView("Login View");
		}

		public void Close ()
		{
			this.Navigator.PopToRootAsync (true);
		}

        public async void RequestResetPassword()
        {
            using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
            {
                //passiamo comunque l'username vuoto al reset password, ci pensa il webservice a restituire il messaggio di errore in linguq
                var res = await UA.UnoxResetPassword((Username ?? "").Trim());
                if (res != null)
                {
                    load.Dispose();
                    await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue(res.Error));
                }
            }
        }

		async Task Login()
		{
			using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
			{
				if (!String.IsNullOrEmpty(Username) && !String.IsNullOrEmpty(Password))
				{
					var res = await UA.UnoxLogin(Username.Trim(), Password.Trim());
					if (res != null)
					{
						load.Dispose();
						if (res.Success)
						{
							await Navigator.PushAsync(Build.ViewModel(() => new ValidatePageViewModel(Context, true)), true);
						}
						else {
							await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue(res.Error), Catalog.GetValue("Errore"));
						}
					}
				}
				else {
					load.Dispose();
					await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("ErroreVerificaCompilazioneDati"));
				}
			}
		}
	}
}
