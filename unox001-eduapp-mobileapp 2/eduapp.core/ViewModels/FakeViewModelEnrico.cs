﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{
	public class FakeViewModelEnrico : BaseViewModel
	{

		public List<List<SBaseModel>> AllItems { get; set; }

		List<SBaseModel> xAxis;
		public List<SBaseModel> XAxis {
			get { return xAxis; } 
			set { this.RaiseAndSetIfChanged (ref xAxis, value); } 
		}

		ICommand tapOnButton;
		public ICommand TapOnButton {
			get { return tapOnButton; }
			set {
				this.RaiseAndSetIfChanged (ref tapOnButton, value);
			}
		}

		string questionTitle;
		public string QuestionTitle {
			get { return questionTitle; }
			set {
				this.RaiseAndSetIfChanged (ref questionTitle, value);
			}
		}

		string questionSubtitle;
		public string QuestionSubtitle {
			get { return questionSubtitle; }
			set {
				this.RaiseAndSetIfChanged (ref questionSubtitle, value);
			}
		}

		public FakeViewModelEnrico (IAppCoreContext context) : base (context)
		{
			TapOnButton = ReactiveCommand.CreateFromTask (() => Tap() );
			QuestionTitle = "QUESTION TITLE";
			QuestionSubtitle = "sduigdfhufghgfiudhu sadgduaisg dasgdsgsagd sadg sidf gdsifgasdugdsiff v";
		}

		async Task Tap ()
		{
			System.Diagnostics.Debug.WriteLine ("Tappato sul bottone");
		}

		public override async Task Appearing ()
		{
			
			AllItems = new List<List<SBaseModel>> ();

			var list1 = new List<SBaseModel> () {
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "130",
					UnitMeasure = "°C",
				},
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "20",
					UnitMeasure = "%",
				},
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "10",
					UnitMeasure = "%",
				}
			};

			AllItems.Add (list1);

			var list2 = new List<SBaseModel> () {
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "130",
					UnitMeasure = "°C",
				},
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "20",
					UnitMeasure = "%",
				},
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "10",
					UnitMeasure = "%",
				}
			};

			AllItems.Add (list2);

			var list3 = new List<SBaseModel> () {
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "130",
					UnitMeasure = "°C",
				},
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "20",
					UnitMeasure = "%",
				},
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "10",
					UnitMeasure = "%",
				}
			};

			AllItems.Add (list3);

			var list4 = new List<SBaseModel> () {
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "130",
					UnitMeasure = "°C",
				},
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "20",
					UnitMeasure = "%",
				},
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "10",
					UnitMeasure = "%",
				}
			};

			AllItems.Add (list4);

			var listAx = new List<SBaseModel> () {
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "10",
					UnitMeasure = "°C",
				},
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "20",
					UnitMeasure = "%",
				},
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "30",
					UnitMeasure = "%",
				},
				new SBaseModel() {
					ImagePath = "chef_icon.png",
					Answer = "40",
					UnitMeasure = "%",
				}
			};
			XAxis = listAx;

			base.Appearing ();

		}
	
	}
}

