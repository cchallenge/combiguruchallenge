﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore.DeferredAppContentModule;
using AppCore.Interfaces;
using Nostromo;
using Nostromo.Interfaces;
using Nostromo.Navigation;
using ReactiveUI;
using UnoxEduAppModule;

namespace EduApp.Core
{
	public class LevelPageViewModel : BaseViewModel
	{
		public override string TextColorHex {
			get { return "#FFFFFF"; }
		}

		public override string BkgColorHex {
			get { return "#2A2928"; }
		}

        public override string BarBkgColorHex
        {
            get { return "#2A2928"; }
        }

        string titleLesson;
		public string TitleLesson 
		{
			get { return titleLesson; }
			set { this.RaiseAndSetIfChanged (ref titleLesson, value); }
		}

		float progress;
		public float ProgressLevel
		{
			get { return progress; }
			set { this.RaiseAndSetIfChanged(ref progress, value); }
		}

		float sbiadimento;
		public float SbiadimentoLevel
		{
			get { return sbiadimento; }
			set { this.RaiseAndSetIfChanged(ref sbiadimento, value); }
		}

		float progressNext;
		public float ProgressNextLevel
		{
			get { return progressNext; }
			set { this.RaiseAndSetIfChanged(ref progressNext, value); }
		}

		string backgroundImage;
		public string BackgroundImage {
			get { return backgroundImage; }
			set { this.RaiseAndSetIfChanged (ref backgroundImage, value); }
		}

		string backgroundNextImage;
		public string BackgroundNextImage {
			get { return backgroundNextImage; }
			set { this.RaiseAndSetIfChanged (ref backgroundNextImage, value); }
		}

		bool nextLevelVisibility;
		public bool NextLevelVisibility 
		{
			get { return nextLevelVisibility; }
			set { this.RaiseAndSetIfChanged (ref nextLevelVisibility, value); }
		}

		string nextLevelTitle;
		public string NextLevelTitle 
		{
			get { return nextLevelTitle; }
			set { this.RaiseAndSetIfChanged (ref nextLevelTitle, value); }
		}

		string topTitle;
		public string TopTitle {
			get { return topTitle; }
			set { this.RaiseAndSetIfChanged (ref topTitle, value); }
		}

		string bottomTitle;
		public string BottomTitle {
			get { return bottomTitle; }
			set { this.RaiseAndSetIfChanged (ref bottomTitle, value); }
		}

		bool isFinishedLevel;
		public bool IsFinishedLevel {
			get { return isFinishedLevel; }
			set {
				this.RaiseAndSetIfChanged (ref isFinishedLevel, value);
			}
		}

		//@@CM Data aggiornamento grafica
		DateTime updateDate;
		public DateTime UpdateDate
		{
			get { return updateDate; }
			set
			{
				this.RaiseAndSetIfChanged(ref updateDate, value);
			}
		}

		public ICommand NextLevelCommand { get; set;}

		public IEnumerable<SectionModel> CurrentSections { get; set;}

		Level CurrentLevel;
		BaseLearningCellViewModel _nextLevel;

		public ICommand TheScienceCommand { get; set; }
		public ICommand ChallengeCommand { get; set;}

		bool showTheScienceBehind;
		public bool ShowTheScienceBehind
		{
			get { return showTheScienceBehind; }
			set
			{
				this.RaiseAndSetIfChanged(ref showTheScienceBehind, value);
			}
		}

		string scienceTitle;
		public string ScienceTitle 
		{
			get { return scienceTitle; }
			set { this.RaiseAndSetIfChanged (ref scienceTitle, value); }
		}

		string scienceImage;
		public string ScienceImage {
			get { return scienceImage; }
			set { this.RaiseAndSetIfChanged (ref scienceImage, value); }
		}

		string challengeTitle;
		public string ChallengeTitle 
		{
			get { return challengeTitle; }
			set { this.RaiseAndSetIfChanged (ref challengeTitle, value); }
		}

		string challengeImage;
		public string ChallengeImage {
			get { return challengeImage; }
			set { this.RaiseAndSetIfChanged (ref challengeImage, value); }
		}

		public LevelPageViewModel (IAppCoreContext context, Level currentLevel) : base (context)
		{
			this.Title = Catalog.GetValue ("LevelPageTitle");
			//@@ CM salvo il current level per poterlo usare nel Lesson Manager
			Context.SetItem(Globals.LevelKey, currentLevel);
			CurrentLevel = currentLevel;

			ScienceTitle = currentLevel.ScienceTitle;
			ScienceImage = !String.IsNullOrEmpty (currentLevel.ScienceImage) ? Content.GetLocalPath (currentLevel.ScienceImage) : null;
			ChallengeTitle = currentLevel.ChallengeTitle;
			ChallengeImage = !String.IsNullOrEmpty (currentLevel.ChallengeImage) ? Content.GetLocalPath (currentLevel.ChallengeImage) : null;
			
			_nextLevel = (BaseLearningCellViewModel)Context.GetItem (Globals.NextLevelCellModelKey);
			NextLevelCommand = ReactiveCommand.CreateFromTask(() => NextLevelTap());

			TheScienceCommand = ReactiveCommand.CreateFromTask(() => GoToScienceBehind(currentLevel.ScienceHTML));
			ShowTheScienceBehind = !String.IsNullOrEmpty(currentLevel.ScienceHTML);
			ChallengeCommand = ReactiveCommand.CreateFromTask(() => GoToChallenge());
		}

		async Task GoToChallenge()
		{
            await UAM.GoToChallengePage(Context, this.Navigator, CurrentLevel);
		}

		async Task GoToScienceBehind(string htmlText)
		{
			await Navigator.PushModalAsync(Build.ViewModel(() => new TheScienceBehindPageViewModel(Context, htmlText, CurrentLevel.Title)), true);
		}

		async Task NextLevelTap()
		{
			if (IsFinishedLevel) {
				Context.SetItem (Globals.NextLevelId, _nextLevel.ID);
				await Navigator.PopAsync(true);
			}
		}

		List<SectionModel> _currentSections;
		bool first = true;
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			token?.Dispose();
			token = null;
		}

		IDisposable token;
		public override async Task Appearing ()
		{
			base.Appearing ();
			using (Acr.UserDialogs.UserDialogs.Instance.Loading ("")) {
				await CreatePage ();

				if (first) {
					first = false;
					var vm = this;
					token = UAM.Subscribe<UserStats> (u => UpdateUserStats (u, vm));
					await UAM.RequestUserStatsIfNeeded ();
				}
			}
		}

		async Task CreatePage() {
			
			_currentSections = new List<SectionModel>();

			foreach (var section in CurrentLevel.Sections)
			{
				var newSection = await UAM.GetSectionModel(section);
				_currentSections.Add(newSection);
			}

			NextLevelVisibility = _nextLevel != null;
			BackgroundNextImage = _nextLevel != null ? _nextLevel.LessonImage : null;
			NextLevelTitle = (_nextLevel != null) ? _nextLevel.LessonTitle : string.Empty;

			TitleLesson = CurrentLevel.Title;
			BackgroundImage = CurrentLevel.Image != null ? Content.GetLocalPath(CurrentLevel.Image) : null;
	
		}

		static void UpdateUserStats(UserStats currentStats, LevelPageViewModel vm) { 
			if (currentStats != null)
			{
				var currentLevel = currentStats.LevelsStat.FirstOrDefault(l => l.CorrelationId == vm.CurrentLevel.CorrelationId);
				if (currentLevel != null)
				{
					// @@ CM imposto i valori del livello
					vm.ProgressLevel = currentLevel.Percentuale * 100;
					vm.SbiadimentoLevel = currentLevel.Sbiadimento;
					// @@ CM il livello è finito se la percentuale è il 100%
					vm.IsFinishedLevel = vm.ProgressLevel >= 100;
					var totalSections = currentLevel.SectionsStat.Count();
					// @@ CM le sezioni finite sono tutte quelle che hanno la percentuale >=1
					var sectionsFinished = currentLevel.SectionsStat.Where(s => s.Percentuale >= 1).Count();
					vm.BottomTitle = String.Format("{0}/{1}", sectionsFinished, totalSections);

					foreach (var s in vm._currentSections) { 
						var cSection = currentLevel.SectionsStat.FirstOrDefault(se => se.CorrelationId == s.CorrID);
						if (cSection != null) {
							s.Sbiadimento = cSection.Sbiadimento;
							s.SubTitle = String.Format("{0}/{1}", cSection.LessonsFinishedNumber, cSection.LessonsTotalNumber);
							s.Percentage = cSection.Percentuale * 100;
						} else {
                            throw new Exception($"Sulla level page ho una sezione che non è presente nello user stats ({s.CorrID})");
						}
					}
				}
				else
				{
					throw new Exception($"Sulla level page ho un livello che non è presente nello user stats ({vm.CurrentLevel.CorrelationId})");
				}

				//int totalLessons = 0;
				//int totalLessonsFinished = 0;

				//int sectionsFinished = 0;
				//int totalSections = currentStats.SectionsStats.Count();

				//if (currentStats.Level == vm.CurrentLevel.CorrelationId)
				//{
				//	//@@CM Sono nel livello corrente
				//	foreach (var elem in currentStats.SectionsStats)
				//	{
				//		if (elem.LessonsFinishedNumber == elem.LessonsTotalNumber)
				//			sectionsFinished++;
				//	}
				//	//@@CM Sezioni finite su sezioni totali
				//	//@@CM Il livello è finito se sono fatte tutte le sue sezioni
				//	vm.IsFinishedLevel = sectionsFinished == totalSections;
				//	foreach (var s in vm._currentSections)
				//	{
				//		var cSection = currentStats.SectionsStats.FirstOrDefault(se => se.CorrelationId == s.CorrID);
				//		totalLessons += cSection.LessonsTotalNumber;
				//		totalLessonsFinished += cSection.LessonsFinishedNumber;

				//		s.SubTitle = String.Format("{0}/{1}", cSection.LessonsFinishedNumber, cSection.LessonsTotalNumber);
				//		s.Percentage = ((float)cSection.LessonsFinishedNumber / (float)cSection.LessonsTotalNumber) * 100;

				//		System.Diagnostics.Debug.WriteLine($"Per la sezione {cSection.CorrelationId} ho concluso {cSection.LessonsFinishedNumber} su {cSection.LessonsTotalNumber}");
				//	}
				//}
				//else {
				//	//@@CM Sono in un livello precedente a quello corrente quindi do per scontato che tutte le section e il livello sia al 100% completato
				//	vm.IsFinishedLevel = true;
				//	foreach (var s in vm.CurrentLevel.Sections) {
				//		var section = vm._currentSections.FirstOrDefault(se => se.CorrID == s.CorrelationId);
				//		section.SubTitle = String.Format("{0}/{1}", s.NLessons, s.NLessons);
				//		System.Diagnostics.Debug.WriteLine($"Per la sezione NON CORRENTE {s.CorrelationId} ho concluso {s.NLessons} su {s.NLessons}");
				//		section.Percentage = 100f;
				//		totalLessons += s.NLessons;
				//		totalLessonsFinished += s.NLessons;
				//	}
				//	totalSections = vm.CurrentLevel.Sections.Count();
				//	sectionsFinished = totalSections;
				//}

				////@@ CM La percentuale del livello è data dal rapporto tra tutte le lezioni finite e tutte le lezioni per tutte le sezioni
				//System.Diagnostics.Debug.WriteLine($"Per il livello {vm.CurrentLevel.CorrelationId} ho concluso {totalLessonsFinished} su {totalLessons}");
				//vm.ProgressLevel = ((float)totalLessonsFinished / (float)totalLessons) * 100;
				//vm.BottomTitle = String.Format("{0}/{1}", sectionsFinished, totalSections);
			}
			else {
				System.Diagnostics.Debug.WriteLine("NON HO UNO STATO PER L'UTENTE");
			}

			vm.CurrentSections = new List<SectionModel>(vm._currentSections);
			if (vm._nextLevel != null)
				vm.ProgressNextLevel = vm._nextLevel.Percentage;

			vm.UpdateDate = DateTime.Now;
		}

		public async Task OpenSection (string sectionCorrId) {
			using (var load = Acr.UserDialogs.UserDialogs.Instance.Loading(""))
			{
				var section = CurrentLevel.Sections.Where((arg) => arg.CorrelationId == sectionCorrId).FirstOrDefault();
				//var indexSection = CurrentLevel.Sections.ToList ().IndexOf (section);
				if (section != null)
				{
					Context.SetItem(Globals.SectionKey, section);
					await Navigator.PushModalAsync(Build.ViewModel(() => new SectionPageViewModel(Context)), true);
				}
				else {
					load.Hide();
					load.Dispose();
				}
			}
		}

	}
}
