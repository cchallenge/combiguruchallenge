﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using AppCore.Interfaces;
using MvvmHelpers;
using Nostromo;
using Nostromo.Navigation;
using ReactiveUI;

namespace EduApp.Core
{
	public class WalkThroughPageViewModel : BaseViewModel
	{
		public override string BkgColorHex
		{
			get { return "#000000"; }
		}

		int position;
		public int Position
		{
			get { return position; }
			set
			{
				this.RaiseAndSetIfChanged(ref position, value);
			}
		}

		public ObservableCollection<WalkThroughModel> Items { get; set; }

		public WalkThroughPageViewModel(IAppCoreContext context) : base(context)
		{
			Items = new ObservableCollection<WalkThroughModel> {
				new WalkThroughModel
				{
					BkgImageName = "walk_01.png",
					ImageName = "walk_01_image.png",
					Title = Catalog.GetValue("walk1_title"),
					Subtitle = Catalog.GetValue("walk1_subtitle")
				},
				new WalkThroughModel
				{
					BkgImageName = "walk_02.png",
					ImageName = "walk_02_image.png",
					Title = Catalog.GetValue("walk2_title"),
					Subtitle = Catalog.GetValue("walk2_subtitle")
				},
				new WalkThroughModel
				{
					BkgImageName = "walk_03.png",
					ImageName = "walk_03_image.png",
					Title = Catalog.GetValue("walk3_title"),
					Subtitle = Catalog.GetValue("walk3_subtitle"),
					ShowDoneButton = true,
					DoneCommand = ReactiveCommand.CreateFromTask(async () => Navigator.SetNewRootAsync(Build.ViewModel(() => new HomePageViewModel(Context)), true))
				}
			};
		}
	}

	public class WalkThroughModel
	{
		public string BkgImageName { get; set; }
		public string ImageName { get; set; }
		public string Title { get; set; }
		public string Subtitle { get; set; }
		public bool ShowDoneButton { get; set;}
		public ICommand DoneCommand { get; set;}
	}
}
