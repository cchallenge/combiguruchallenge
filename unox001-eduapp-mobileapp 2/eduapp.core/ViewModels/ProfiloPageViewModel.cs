﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using AppCore.DeferredAppContentModule;
using AppCore.Interfaces;
using MvvmHelpers;
using Nostromo;
using Nostromo.Navigation;
using Plugin.Media;
using Plugin.Media.Abstractions;
using ReactiveUI;

namespace EduApp.Core
{
	public class ProfiloPageViewModel : BaseViewModel
	{
		public ILocalStorageAppModule Local { get; set; }
        public IUnoxAppAuthModule Auth { get; set; }

		public override string BkgColorHex
		{
			get { return "#FFFFFF"; }
		}

        public override string BarBkgColorHex
        {
            get { return "#FFFFFF"; }
        }

        public override string TextColorHex {
            get { return "#2A2928"; }
        }

        DynamicListSource dyn;

		public ICommand DoneCommand { get; set; }

		public DynamicListSource Dyn
		{
			get { return dyn; }
			set
			{
				this.RaiseAndSetIfChanged(ref dyn, value);
			}
		}

		UserData _userData;
		bool _fromLoginOrSignup;
		public ProfiloPageViewModel(IAppCoreContext context, bool fromLoginOrSignup) : base(context)
		{
			this.Title = Catalog.GetValue("Profilo").ToUpper();
			_fromLoginOrSignup = fromLoginOrSignup;

			_userData = UA.UserData;
			if (_userData != null)
				System.Diagnostics.Debug.WriteLine($"email: {_userData.Email}, country: {_userData.AddressCountry}, occupation: {_userData.Category}");

			DoneCommand = ReactiveCommand.CreateFromTask(() => OnDonePressed());
		}

		bool isWorking = false;
		async Task OnDonePressed()
		{
			if (!isWorking)
			{
				isWorking = true;
				if (String.IsNullOrEmpty(_email.Value))
				{
					await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("EmptyFields"), Catalog.GetValue("Errore"));
					isWorking = false;
					return;
				}
				if (!Helper.CorrectMailFormat(_email.Value))
				{
					await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("MailFormatError"), Catalog.GetValue("Errore"));
					isWorking = false;
					return;
				}
				if (!String.IsNullOrEmpty(_password.Value))
				{
					if (!Helper.CorrectPasswordFormat(_password.Value))
					{
						await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("PasswordFormatError"), Catalog.GetValue("Errore"));
						isWorking = false;
						return;
					}
					else if (!_password.Value.Equals(_confPassword.Value, StringComparison.Ordinal))
					{
						await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("PasswordNotEqualsError"), Catalog.GetValue("Errore"));
						isWorking = false;
						return;
					}
				}

                if (String.IsNullOrEmpty(_ext_nickname.Value) || _ext_nickname.Value.Length < 5 || _ext_nickname.Value.Length >= 30)
                {
                    await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("NicknameEntryError"), Catalog.GetValue("Errore"));
                    isWorking = false;
                    return;
                }

                var data = new UserData()
				{
					FirstName = _nome.Value,
					LastName = _cognome.Value,
					Email = _email.Value,
					//Company = _azienda.Value,
					//AddressCity = _location.Value,
					//Phone = _phoneNumber.Value,
					//AddressStreet = _addressStreet.Value,
					//AddressPostalCode = _zipCode.Value,
					//AddressCountry = _country.SelectedKey,
					//AddressProvince = _province.SelectedKey,
					Category = _occupation.SelectedKey,
					KitchenType = _typeOfKitchen.SelectedKey,
					BusinessName = _typeOfKitchen.KitchenNameValue,
					Language = _language.SelectedKey,
					MeasureUnitProfile = _settings.SelectedKey,
                    PrivacySalesInfo = _ext_nickname.Value
                };

				if (!string.IsNullOrEmpty(_password.Value))
					data.Password = _password.Value;

				var response = await UA._ext_UnoxUpdateUserData(data);

				if (response.Success)
				{

                    if (_lastPhotoStream != null)
					{
						var success = await StoreContent(UA.GetAvatarContentKey(), _lastPhotoStream);
						if (!success)
						{
							isWorking = false;
							return;
						}
					}

                    await Auth.SetDataForNotificaions((Context.UserIdentity as UnoxUserIdentity).UserIdentifier, Context.UserIdentity.UserName, Catalog.GetLanguage());

                    if (_fromLoginOrSignup)
						await Navigator.SetNewRootAsync(Build.ViewModel(() => new WalkThroughPageViewModel (Context)), true);
					else
						await Navigator.PopToRootAsync(true);
				}
				else
				{
					await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue(response.Error), Catalog.GetValue("Errore"));
					isWorking = false;
				}
			}
		}

		PhotoViewModel photoVM;
		RadioButtonViewModel _occupation;
		RadioButtonViewModel _settings;
		TypeOfKitchenViewModel _typeOfKitchen;
		EditTextViewModel _nome;
		EditTextViewModel _cognome;
		EditTextViewModel _email;
		EditTextViewModel _password;
        EditTextViewModel _confPassword;
        EditTextViewModel _ext_nickname;
        //EditTextViewModel _azienda;
        //EditTextViewModel _location;
        //EditTextViewModel _phoneNumber;
        //EditTextViewModel _addressStreet;
        //EditTextViewModel _zipCode;
        //ComboSelectedViewModel _country;
        //ComboSelectedViewModel _province;
        ComboSelectedWithImageViewModel _language;

        bool first = true;
		public override async Task Appearing()
		{
			base.Appearing();
            
			if (first)
			{
				await Task.Yield();
				first = false;
				using (Acr.UserDialogs.UserDialogs.Instance.Loading(""))
				{
					var occupationList = new ObservableRangeCollection<RadioModel>(
					UA.GetOccupations()
					.Select(o => new RadioModel()
					{
						Text = Catalog.GetValue(o.Value),
						Selected = o.Key == _userData.Category,
						Key = o.Key
					})
					.ToList());

					_occupation = GetRadioButtonVM ("", occupationList, 1);
					//_occupation = GetRadioButtonVM(String.Empty, occupationList, 1);
					_occupation.SelectedKey = _userData.Category;
					_occupation.ItemCommand = ReactiveCommand.CreateFromTask<string>((arg) => ChangeRadioButton(arg, _occupation));

					var kitchenList = new ObservableRangeCollection<RadioModel>(
						UA.GetTypeOfKitchen()
						.Select(o => new RadioModel()
						{
							Text = Catalog.GetValue(o.Value),
							Selected = o.Key == _userData.KitchenType,
							Key = o.Key
						})
						.ToList());

					_typeOfKitchen = new TypeOfKitchenViewModel(3)
					{
						Title = "", //Catalog.GetValue("TipologiaCucina"),
						Items = kitchenList,
						KitchenNameTitle = Catalog.GetValue("NameOfRestaurant"),
						KitchenNameValue = _userData.BusinessName
					};
					_typeOfKitchen.SelectedKey = _userData.KitchenType;
					_typeOfKitchen.ItemCommand = ReactiveCommand.CreateFromTask<string>((arg) => ChangeRadioButton(arg, _typeOfKitchen));

					var settingsList = new ObservableRangeCollection<RadioModel>(
						UA.GetSettings()
						.Select(o => new RadioModel()
						{
							Text = Catalog.GetValue(o.Value),
							Selected = o.Key == _userData.MeasureUnitProfile,
							Key = o.Key
						})
						.ToList());

					_settings = GetRadioButtonVM(Catalog.GetValue("MeasureUnitProfile"), settingsList, 1);
					_settings.SelectedKey = _userData.MeasureUnitProfile;
					_settings.ItemCommand = ReactiveCommand.CreateFromTask<string>((arg) => ChangeRadioButton(arg, _settings));

					photoVM = new PhotoViewModel() { ImageSource = "photoimage.png" };
					photoVM.PhotoTapCommand = ReactiveCommand.CreateFromTask<int>((arg) => TapOnPhoto());

					var countries = UA.GetCountries();
					var languages = UA.GetLanguages();
					UA.CreateRegionsDictionary();
					var regions = UA.GetRegions(_userData.AddressCountry);
                    //unox fb login-nickname-update-fix: should be fixed on Unox side.
                    string nickname_ext = !String.IsNullOrEmpty(_userData.FacebookToken) ? 
                        (Context.UserIdentity as _ext_UnoxUserIdentity).Nickname : _userData.PrivacySalesInfo;

                    var list = new List<BaseCellViewModel>() {
						photoVM,
						GetTitleLabelVM (Catalog.GetValue("HelloChef"), _userData.FirstName),
						GetSezioneVM (Catalog.GetValue("OggBasic"), new List<BaseCellViewModel>() {
							(_nome = GetEditTextVM (Catalog.GetValue("OggNome"), false, _userData.FirstName)),
							(_cognome = GetEditTextVM (Catalog.GetValue("OggCognome"), false, _userData.LastName)),
							(_password = GetEditTextVM (Catalog.GetValue("OggPassword"), true, String.Empty)),
							(_confPassword = GetEditTextVM (Catalog.GetValue("OggConfPassword"), true, String.Empty)),
							(_email = GetEditTextVM (Catalog.GetValue("OggEmail"), false, _userData.Email, KeyboardType.Email)),
                            (_ext_nickname = GetEditTextVM (Catalog.GetValue("OggNickname"), false, nickname_ext, KeyboardType.Text)) //fix. nickname serialization workaround
							//(_azienda = GetEditTextVM (Catalog.GetValue("OggAzienda"), false, _userData.Company)),
							//(_location = GetEditTextVM (Catalog.GetValue("OggCity"), false, _userData.AddressCity)),
							//(_country = GetComboVM(countries, _userData.AddressCountry, Catalog.GetValue("OggCountry"))),
							//(_province = GetComboVM(regions, _userData.AddressProvince, Catalog.GetValue("OggProvince"))),
							//(_phoneNumber = GetEditTextVM (Catalog.GetValue("OggTel"), false, _userData.Phone, KeyboardType.Telephone)),
							//(_addressStreet = GetEditTextVM (Catalog.GetValue("OggIndirizzo"), false, _userData.AddressStreet)),
							//(_zipCode = GetEditTextVM (Catalog.GetValue("OggZip"), false, _userData.AddressPostalCode))
						}),
						GetSezioneVM (Catalog.GetValue("OggAboutYou"), new List<BaseCellViewModel>() {_occupation}),
						GetSezioneVM (Catalog.GetValue("OggAboutPlace"), new List<BaseCellViewModel>() {_typeOfKitchen}),
						(_language = GetComboWithImageVM (languages, Catalog.GetLanguage())),
						GetSezioneVM (Catalog.GetValue("Settings"), new List<BaseCellViewModel>() {_settings})
					};

					_email.IsEnabled = false;
					//_country.CommandOnSelectedItem = ReactiveCommand.CreateFromTask(() => ChangeRegionsPicker());

					Dyn = new DynamicListSource(list);
					LoadImageFromDeferred();
				}
			}
		}

		async Task LoadImageFromDeferred()
		{
			using (UserDialogs.Instance.Loading(""))
			{
				var fileKey = UA.GetAvatarContentKey();
				await Content.Fetch(new string[] { fileKey }, null);
				var res = Content.IsFetched(fileKey);

				if (!res)
				{
					System.Diagnostics.Debug.WriteLine("Errore fetch immagine");
				}
				else {

					photoVM.ImageSource = Content.GetLocalPath(fileKey);
				}
			}
		}

		async Task ChangeRegionsPicker()
		{
			//System.Diagnostics.Debug.WriteLine(_country.SelectedKey);
			//var regions = UA.GetRegions(_country.SelectedKey);
			//_province.Source = new Source(regions);
			//var firstDictRegion = regions.First();
			//_province.SetSelectedItem (firstDictRegion.Key);
		}

		#region FOTO
		Stream _lastPhotoStream;
		async Task TapOnPhoto()
		{
			UserDialogs.Instance.ActionSheet(new ActionSheetConfig()
			{
				Options = new List<ActionSheetOption>() {
					new ActionSheetOption (Catalog.GetValue("AperturaGalleria"), () => TakePhoto (true)),
					new ActionSheetOption (Catalog.GetValue("AperturaFotocamera"), () => TakePhoto()),
					new ActionSheetOption (Catalog.GetValue("Cancella"), null)
				},
				Title = Catalog.GetValue("FotoTitle")
			});
		}

		public async Task TakePhoto(bool fromGallery = false)
		{
			MediaFile fileFoto;
			if (!fromGallery)
			{
				fileFoto = await CrossMedia.Current.TakePhotoAsync(
					new StoreCameraMediaOptions
					{
						SaveToAlbum = false,
						DefaultCamera = CameraDevice.Rear,
						CompressionQuality = 20,
						PhotoSize = PhotoSize.Small
					}
				);
			}
			else {
				fileFoto = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
				{
					CompressionQuality = 20,
					PhotoSize = PhotoSize.Small
				});
			}

			if (fileFoto != null)
			{
				var key = UA.GetAvatarContentKey();
				_lastPhotoStream = fileFoto.GetStream();
				await SaveLocalImage(key, _lastPhotoStream);
				System.Diagnostics.Debug.WriteLine("Stream lenght {0}", fileFoto.GetStream().Length);
				fileFoto.Dispose();
				_lastPhotoStream.Position = 0;
				photoVM.ImageSource = Local.GetAbsoluteFullPath(StorageType.Cache, key);
			}
		}

		async Task SaveLocalImage(string key, Stream fotoStream)
		{
			using (var sout = await Local.OpenWrite(StorageType.Cache, key))
			{
				await fotoStream.CopyToAsync(sout);
			}
		}


		async Task<bool> StoreContent(string key, Stream fotoStream)
		{
			var res = await Content.Store(key, fotoStream);
			if (Content is DeferredAppContentModule)
				await (Content as DeferredAppContentModule).WaitForDeferredSyncTerminated();
			return res.Success;
		}

		#endregion

		async Task ChangeRadioButton(string position, RadioButtonViewModel model)
		{
			if (!String.IsNullOrEmpty(position))
			{
				model.SelectedKey = position;
				foreach (var elem in model.Items)
				{
					if (elem.Key != position)
						elem.Selected = false;
					else
						elem.Selected = true;
				}
			}
			System.Diagnostics.Debug.WriteLine($"Ho tappato {position}");
		}

		#region CellViewModel
		static SezioneViewModel GetSezioneVM(string title, IEnumerable<BaseCellViewModel> children)
		{
			return new SezioneViewModel()
			{
				Title = title,
				Children = children
			};
		}

		static EditTextViewModel GetEditTextVM(string title, bool isPassword, string val, KeyboardType kbtype = KeyboardType.Default)
		{
			return new EditTextViewModel()
			{
				Title = title,
				IsPwd = isPassword,
				Value = val,
				KeyboardType = kbtype,
				IsEnabled = true
			};
		}

		static RadioButtonViewModel GetRadioButtonVM(string title, ObservableRangeCollection<RadioModel> items, int columns)
		{
			return new RadioButtonViewModel(columns) { Title = title, Items = items };
		}

		static TitleLabelViewModel GetTitleLabelVM(string titleFirstRow, string titleSecondRow)
		{
			return new TitleLabelViewModel() { TitleFirstRow = titleFirstRow, TitleSecondRow = titleSecondRow };
		}

		static ComboSelectedViewModel GetComboVM(Dictionary<string, string> dict, string selectedKey, string title)
		{
			var comboVM = new ComboSelectedViewModel()
			{
				Source = new Source(dict),
				Title = title,
			};

			if (!String.IsNullOrEmpty(selectedKey))
				comboVM.SetSelectedItem(selectedKey);
				
			return comboVM;
		}

		static ComboSelectedWithImageViewModel GetComboWithImageVM(Dictionary<string, string> dict, string selectedKey)
		{
			var comboVM = new ComboSelectedWithImageViewModel()
			{
				Source = new Source(dict),
			};
			comboVM.SetSelectedItem(selectedKey.ToUpper()); //la chiave della lingua è in maiuscolo, mentre il GetLanguage me la restituisce minuscola
			return comboVM;
		}
		#endregion
	}
}
