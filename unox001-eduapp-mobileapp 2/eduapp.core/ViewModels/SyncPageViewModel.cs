﻿using System;
using System.Threading.Tasks;
using AppCore.Interfaces;
using AppCore.Logging;
using Nostromo;
using Nostromo.Navigation;

namespace EduApp.Core
{
    public class SyncPageViewModel : BaseViewModel
    {
        public override string BkgColorHex
        {
            get { return "#FFFFFF"; }
        }

        public SyncPageViewModel(IAppCoreContext context) : base(context)
        {
        }

        public override async Task Appearing()
        {
            base.Appearing();
            await Sync();
        }

        ILog logger = LogProvider.GetLogger("SyncPageViewModel");
        async Task Sync()
        {
            await Task.Delay(500);
            string language = string.Empty;
            language = System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToUpper();

            if (!UA.GetLanguages().ContainsKey(language))
            {
                //se la lingua del dispostivo non è tra quelle censite in app imposto inglese come default
                language = "EN";
            }
            Catalog.SetLanguage(language);

            var res = await UA.CheckLogin();
            if (res != null)
            {
                //Se ho l'utente verifico che sia stato abilitato
                await this.Navigator.SetNewRootAsync(Build.ViewModel(() => new ValidatePageViewModel(Context)), true);
            }
            else
            {
                await this.Navigator.SetNewRootAsync(Build.ViewModel(() => new WelcomePageViewModel(Context)), true);
            }
        }
    }
}
