using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AppCore.Interfaces;
using ReactiveUI;

namespace EduApp.Core
{

	public enum AnswerState
	{
		Corretto,
		Sbagliato,
		InAttesa,
		DopoCorretto,
		None
	}
}
