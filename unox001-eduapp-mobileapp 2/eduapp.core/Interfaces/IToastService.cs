﻿using System;
namespace EduApp.Core
{
	public interface IToastService
	{
		void ShowToastMessage (string message);
	}
}
