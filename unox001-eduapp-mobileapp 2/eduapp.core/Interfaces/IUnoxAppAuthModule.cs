﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AppCore.Interfaces;
using Newtonsoft.Json;

namespace EduApp.Core
{
	public interface IUnoxAppAuthModule : IAppAuthModule
	{
		Task<UnoxGenericResponse> UnoxLogin(string email, string password);
		Task<UnoxValidateModel> UnoxValidate();
		Task<UnoxGenericResponse> UnoxSignup (string email, string password, string country, string occupation);
        Task<UnoxGenericResponse> _ext_UnoxSignup(string email, string password, string nickname, string country, string occupation);
        Task<UnoxGenericResponse> UnoxUpdateUserData(UserData data);
        Task<UnoxGenericResponse> _ext_UnoxUpdateUserData(UserData data);
        Task<UnoxGenericResponse> UnoxResetPassword (string email);
		Task SetDataForNotificaions (string userID, string email, string language);

		UserData UserData { get; }
		Dictionary<string, string> GetCountries ();
		Dictionary<string, string> GetLanguages ();
		void CreateRegionsDictionary ();
		Dictionary<string, string> GetRegions (string val);
		Dictionary<string, string> GetOccupations ();
		Dictionary<string, string> GetTypeOfKitchen();
		Dictionary<string, string> GetSettings ();
		string GetUserMeasureUnit();
		Task<UnoxGenericResponse> SignUpWithFacebook (string token);
		string GetAvatarContentKey ();
	}

	//Dati dell'utente
	//public class UserData { 
	//	public string Email { get; set; } //EMAIL
	//	public string Password { get; set; } //PASSWORD
	//	public string FirstName { get; set; } //NAME
	//	public string LastName { get; set; } //LASTNAME
	//	public string BusinessName { get; set; } //NAME OF RESTAURANT
	//	public string Language { get; set; } //LANGUAGE
	//	public string Company { get; set; } //COMPANY NAME
	//	public string AddressCountry { get; set; } //LOCATION????
	//	public string AddressProvince { get; set; }
	//	public string AddressCity { get; set; }
	//	public string AddressStreet { get; set; } //ADDRESS
	//	public string AddressPostalCode { get; set; } //ZIP
	//	public string Phone { get; set; } //PHONE
	//	public string Category { get; set; } //OCCUPATION
	//	public string KitchenType { get; set; } //TYPE OF KITCHEN
	//	public string MeasureUnitProfile { get; set;}
	//}

	public class PostUserData { 
		public string Id { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string Token { get; set; }
		public string AddressCountry { get; set; }
		public string WebGroup { get; set; }
		public string Language { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string BusinessName { get; set; }
		public string Company { get; set; }
		public string AddressCity { get; set; }
		public string AddressStreet { get; set; }
		public string AddressProvince { get; set; }
		[JsonProperty (PropertyName = "addressPostaCode")]
		public string AddressPostalCode { get; set; }
		public string Phone { get; set; }
		public string KitchenType { get; set; }
		public string Category { get; set; }
		public string MeasureUnitProfile { get; set; }
        public string PrivacySalesInfo { get; set; }//nickname fix.
        //public string Application { get; set; }
    }

	public class UserData : PostUserData
	{
		public string FacebookId { get; set; }
		public string FacebookToken { get; set; }
		public string AuthType { get; set; }
		public string Status { get; set; }
		public string ContactID { get; set; }
		public DateTime? CreationDate { get; set; }
		public DateTime? ActivationDate { get; set; }
		public string Success { get; set; }
		public string Error { get; set; }
		public string ErrorMessage { get; set; }
    }

	public class UnoxValidateModel { 
		public ValidateStatus Status { get; set;}
		public string Error { get; set; }
	}

	public enum ValidateStatus { 
		pending_activation,
		activate,
		none
	}

	public class UnoxGenericResponse
	{
		public bool Success { get; set; }
		public string Error { get; set; }
	}

    public class _ext_UnoxUserIdentity : UnoxUserIdentity
    {
        public string Nickname { get; set; }
        public string Institution { get; set; }
        public string Score { get; set; }

    }


    public class UnoxUserIdentity : IUnoxUserIdentity
	{
		public string UserIdentifier { get; set; }
		public string UserName { get; set; }

		public string[] Claims { get; set; }

		public string Error { get; set; }

		public string[] GetClaims() { return Claims; }
	}

	public interface IUnoxUserIdentity : IUserIdentity
	{
		string Error { get; set; }
	}
}
