﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AppCore.Interfaces;
using AppCore.Logging;

namespace EduApp.Core
{
	public static class IStorageExtensions
	{
		/// <summary>
		/// Restituisce un testo, oppure null se non ha connessione o si verifica un'errore;
		/// </summary>
		/// <returns>The all text.</returns>
		/// <param name="module">Module.</param>
		/// <param name="key">Key.</param>
		/// <param name="text">Text.</param>
		public static async Task<string> ReadAllText(this ILocalStorageAppModule storage, StorageType storageType, string filePath, Encoding encoding = null)
		{
			if (encoding == null)
				encoding = Encoding.UTF8;
			try
			{
				using (var stream = await storage.OpenRead(storageType, filePath))
				using (var sr = new StreamReader(stream, encoding))
				{
					return await sr.ReadToEndAsync();
				}

			}
			catch (Exception ex)
			{
				LogProvider.GetLogger(storageType.GetType()).ErrorException(nameof(ReadAllText), ex);
			}

			return null;
		}

		public static async Task<bool> WriteAllText(this ILocalStorageAppModule storage, StorageType storageType, string filePath, string text, Encoding encoding = null)
		{
			if (encoding == null)
				encoding = Encoding.UTF8;
			try
			{
				if (storage.Exists(storageType, filePath))
					await storage.Delete(storageType, filePath);

				using (var stream = await storage.OpenWrite(storageType, filePath))
				using (var sr = new StreamWriter(stream, encoding))
				{
					await sr.WriteAsync(text);
					return true;
				}

			}
			catch (Exception ex)
			{
				LogProvider.GetLogger(storageType.GetType()).ErrorException(nameof(WriteAllText), ex);
			}

			return false;
		}

	}

}
