﻿using System;


namespace EduApp.Core
{
	public interface INotificationService
	{
		void RegisterForRemoteNotificationsWithTags (string [] tags);
		void UnRegisterForRemote ();
		void SetDeviceToken (object token);
	}
}
