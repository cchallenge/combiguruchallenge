﻿using System;
namespace EduApp.Core
{
	public interface IAnalyticsService
	{
		void TrackView (string viewName);
		void TaskEvent (string eventCategory, string eventAction, string label);
	}
}
