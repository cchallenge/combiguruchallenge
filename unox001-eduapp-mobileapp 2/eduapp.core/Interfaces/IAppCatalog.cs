﻿using System;
using AppCore.Interfaces;

namespace EduApp.Core
{
	public interface IAppCatalog : IAppModule
	{
		string GetValue (string value);

		void SetLanguage (string language = null);

		void ResetLanguage ();

		string GetLanguage ();
	}
}
