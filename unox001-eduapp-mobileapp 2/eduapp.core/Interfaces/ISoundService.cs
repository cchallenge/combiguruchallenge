﻿using System;
using System.Collections.Generic;

namespace EduApp.Core
{
	public interface ISoundService
	{
		void ExecuteSound (SoundEffectKind sound);
	}

    public enum SoundEffectKind
    {
        RightAnswer,
        WrongAnswer,
        ChallengeCompleted,
        ChallengeFailed
    }

    public static class SoundsEffect
    {
        public static string GetFileName(SoundEffectKind sound)
        {
            switch (sound)
            {
                case SoundEffectKind.RightAnswer:
                    return "right_answer_short.mp3";
                case SoundEffectKind.WrongAnswer:
                    return "wrong_answer.mp3";
                case SoundEffectKind.ChallengeCompleted:
                    return "challenge_completed.mp3";
                case SoundEffectKind.ChallengeFailed:
				default:
                    return "challenge_failed.mp3";
            }
        }
    }
    
}