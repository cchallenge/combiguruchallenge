﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using AppCore;
using AppCore.DeferredAppContentModule;
using AppCore.Interfaces;
using Newtonsoft.Json;
using Nostromo;
using Nostromo.Interfaces;
using Nostromo.Navigation;
using UnoxEduAppModule;

namespace EduApp.Core
{
	public class LessonManager
	{
		string _Id;
		int _correctResponse;
		int _totalResponse;
		List<BaseQuestionsModel> _questions;
		bool _lastLesson;
		string _logData;
		protected IPageNavigator Navigator { get; set; }
		//public IAnalyticsService AnaS { get; set; }
		readonly IAppCoreContext context;
		readonly UnoxAppEduModule UM;
		readonly IAppCatalog Catalog;
		readonly bool challenge;
		readonly bool isExtChallenge;

        ISoundService SoundS { get; set; }

		IUnoxAppAuthModule uam;
		IUnoxAppAuthModule UAM { get { return uam ?? (uam = DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<IUnoxAppAuthModule>()); } }


		public LessonManager (string id, IPageNavigator navigator, IAppCoreContext context, IAppCatalog catalog, UnoxAppEduModule um, bool lastLesson, string logData, bool challenge = false, bool isExtChallenge = false)
		{
			this.challenge = challenge;
			this.isExtChallenge = isExtChallenge;
			this.Catalog = catalog;
			this.context = context;
			this.Navigator = navigator;
			this.UM = um;
			_lastLesson = lastLesson;
			_Id = id;
			_correctResponse = 0;
			_totalResponse = 0;
			_questions = new List<BaseQuestionsModel>();
			_logData = logData;

            if (SoundS == null) {
                SoundS = Nostromo.Container.Default.Resolve<ISoundService>();
            }
		}

		async Task<bool> RequestQuestions(IProgressDialog load)
		{
			try
			{
				//Richiedo le domande per la lezione corrente
				Lesson lesson = null;
                if (isExtChallenge)
                {
                    lesson = await UM._ext_GetChallenge(_Id, UAM.GetUserMeasureUnit(), Catalog.GetLanguage());
                    //XXX: through this I am able to capture the challengeHeader, stored previously when loading the challenge list
                    context.SetItem(Globals.CurrentChallenge, lesson.CorrelationId);
                }
				else if (challenge)
				{
					lesson = await UM._ext_GetLearningPathChallenge(_Id, UAM.GetUserMeasureUnit(), Catalog.GetLanguage());
				}
				else {
					lesson = await UM.Lesson(_Id, UAM.GetUserMeasureUnit(), Catalog.GetLanguage());
				}

                if (lesson != null)
                {
                    var Contents = DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<DeferredAppContentModule>();

					var lessonsContentsArray = lesson.Contents.Where(x => !String.IsNullOrEmpty(x)).ToArray();

					var backFetch = lessonsContentsArray.Where(Contents.IsFetched).ToArray();
					var frontFetch = lessonsContentsArray.Except(backFetch).ToArray();

                    if (frontFetch.Length > 0)
						await Contents.Fetch(frontFetch, null);

					if (!lessonsContentsArray.All(Contents.IsFetched))
                    {
                        load.Dispose();
						await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("ConnectionNeeded"));
						return false;
					}
					else
                    {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        //Avvio l'aggiornamento (Verifica) in background, dei contenuti già scaricati
                        if (backFetch.Length > 0)
							Task.Run(() => Contents.Fetch(backFetch, null));
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

                        foreach (var q in lesson.Questions)
                        {
                            switch (q.TemplateType)
							{
								case UnoxEduAppModule.TemplateType.S14:
									_questions.Add(new S14QuestionModel()
									{
										ID = q.CorrelationId,
										Title = q.Title,
										Description = q.Description,
										Type = (TemplateType)q.TemplateType,
										BottomImagePath = q.Payload["S14Model"]["BottomImagePath"].ToString(),
										CorrectAnswers = JsonConvert.DeserializeObject<bool[,]>(q.Payload["S14Model"]["CorrectAnswers"].ToString()),
										Points = JsonConvert.DeserializeObject<bool[,]>(q.Payload["S14Model"]["Points"].ToString()),
										YAxisTitles = JsonConvert.DeserializeObject<string[]>(q.Payload["S14Model"]["YAxisTitles"].ToString()),
										XAxisTitles = JsonConvert.DeserializeObject<string[]>(q.Payload["S14Model"]["XAxisTitles"].ToString()),
										Answers = JsonConvert.DeserializeObject<IEnumerable<S14Model>>(q.Payload["S14Model"]["Answers"].ToString())
									});
									break;
								case UnoxEduAppModule.TemplateType.S13:
									_questions.Add(new S13QuestionModel()
									{
										ID = q.CorrelationId,
										Title = q.Title,
										Description = q.Description,
										Type = (TemplateType)q.TemplateType,
										Answers = JsonConvert.DeserializeObject<IEnumerable<S13Model>>(q.Payload["S13Model"]["Answers"].ToString()),
										XAxis = JsonConvert.DeserializeObject<IEnumerable<SBaseModel>>((q.Payload["S13Model"])["XAxis"].ToString())
									});
									break;
								case UnoxEduAppModule.TemplateType.S10_S11_S12:
									_questions.Add(new S10S11S12QuestionModel()
									{
										ID = q.CorrelationId,
										Title = q.Title,
										Description = q.Description,
										Type = (TemplateType)q.TemplateType,
										BottomImagePath = q.Payload["S10_S11_S12Model"]["BottomImagePath"].ToString(),
										TopImagePath = q.Payload["S10_S11_S12Model"]["TopImagePath"].ToString(),
										CorrectAnswers = JsonConvert.DeserializeObject<bool[,]>(q.Payload["S10_S11_S12Model"]["CorrectAnswers"].ToString()),
										Points = JsonConvert.DeserializeObject<bool[,]>(q.Payload["S10_S11_S12Model"]["Points"].ToString()),
										YAxisTitles = JsonConvert.DeserializeObject<string[]>(q.Payload["S10_S11_S12Model"]["YAxisTitles"].ToString()),
										XAxisTitles = JsonConvert.DeserializeObject<string[]>(q.Payload["S10_S11_S12Model"]["XAxisTitles"].ToString())
									});
									break;
								case UnoxEduAppModule.TemplateType.S8:
									_questions.Add(new S8QuestionModel()
									{
										ID = q.CorrelationId,
										Title = q.Title,
										Description = q.Description,
										Type = (TemplateType)q.TemplateType,
										Answers = JsonConvert.DeserializeObject<IEnumerable<S8Model>>(q.Payload["S8Model"].ToString())
									});
									break;
								case UnoxEduAppModule.TemplateType.S6_S7:
									_questions.Add(new S6S7QuestionModel()
									{
										ID = q.CorrelationId,
										Title = q.Title,
										Description = q.Description,
										Type = (TemplateType)q.TemplateType,
										Answers = JsonConvert.DeserializeObject<IEnumerable<SBaseModelRange>>(q.Payload["S6_S7Model"].ToString())
									});
									break;
								case UnoxEduAppModule.TemplateType.S5_S9:
									_questions.Add(new S5S9QuestionModel()
									{
										ID = q.CorrelationId,
										Title = q.Title,
										Description = q.Description,
										Type = (TemplateType)q.TemplateType,
										Answers = JsonConvert.DeserializeObject<IEnumerable<S5S9Model>>(q.Payload["S5_S9Model"].ToString())
									});
									break;
								case UnoxEduAppModule.TemplateType.S4:
									_questions.Add(new S4QuestionModel()
									{
										ID = q.CorrelationId,
										Title = q.Title,
										Description = q.Description,
										Type = (TemplateType)q.TemplateType,
										Answers = JsonConvert.DeserializeObject<IEnumerable<S4Model>>(q.Payload["S4Model"].ToString())
									});
									break;
								case UnoxEduAppModule.TemplateType.S3:
									_questions.Add(new S3QuestionModel()
									{
										ID = q.CorrelationId,
										Title = q.Title,
										Description = q.Description,
										Type = (TemplateType)q.TemplateType,
										ImagePath = q.Payload["S3Model"]["ImagePath"].ToString(),
										Answers = JsonConvert.DeserializeObject<IEnumerable<S3Model>>((q.Payload["S3Model"])["Answers"].ToString())
									});
									break;
								case UnoxEduAppModule.TemplateType.S2:
									_questions.Add(new S2QuestionModel()
									{
										ID = q.CorrelationId,
										Title = q.Title,
										Description = q.Description,
										Type = (TemplateType)q.TemplateType,
										RightAnswers = JsonConvert.DeserializeObject<IEnumerable<S2Model>>(q.Payload["S2Model"]["RightAnswers"].ToString()),
										LeftAnswers = JsonConvert.DeserializeObject<IEnumerable<S2Model>>(q.Payload["S2Model"]["LeftAnswers"].ToString())
									});
									break;
								default:

									break;
							}
						}
                        _totalResponse = _questions.Count();
                        return true;
					}
				}
				else {
                    load.Dispose();
					await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("ConnectionNeeded"));
					return false;
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
				load.Dispose();
				await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("ConnectionNeeded"));
				return false;
			}
		}

		int countingWrong = -1;
		public float GetPercentage()
		{
			//Ritorna la percentuale di risposte corrette
			if (challenge) 
			{
				if (countingWrong < wrongResponseId.Count && wrongResponseId.Count <= 2) 
				{
					countingWrong = wrongResponseId.Count;
					Nostromo.Container.Default.Resolve<IToastService> ().ShowToastMessage (String.Format (Catalog.GetValue("ChallengeResiduoVite"), 3 - countingWrong));
				}
				if (wrongResponseId.Count > 2) 
				{
					countingWrong = 0;
					CheckChallenge ();
				}
			}
			return (float)_correctResponse / (float)_totalResponse;
		}

		public void AddCorrectResponse(string id)
		{
			SoundS.ExecuteSound (SoundEffectKind.RightAnswer);

			_correctResponse++;
			if (wrongResponseId.Contains(id))
			{
				wrongResponseId.Remove(id);
			}
		}

		async Task CheckChallenge ()
		{
			var indexLevel = context.GetItem (Globals.IndexOfLevel);
			Nostromo.Container.Default.Resolve<IAnalyticsService> ().TaskEvent ("LogChallenge", "NotCorrect", string.Format ("Challenge: {0}", indexLevel));
			
			await Task.Delay (1000);
            SoundS.ExecuteSound(SoundEffectKind.ChallengeFailed);
            await Navigator.SetNewRootAsync (Build.ViewModel (() => new FineChallengeLosePageViewModel (context)), true);
		}

		public void LogData (bool correctAnswer, string modelId)
		{
			var question = _questions.Where (x => string.Equals (x.ID, modelId)).FirstOrDefault();
			var indexQuestion = _questions.IndexOf (question);
			if (!challenge && !isFinished) {
				string eventAction = correctAnswer ? "Correct" : "NotCorrect";
				string _logDataAndQuestion = string.Format ("{0} question: {1}", _logData, (indexQuestion + 1).ToString ());
				Nostromo.Container.Default.Resolve<IAnalyticsService> ().TaskEvent ("LogAnswer", eventAction, _logDataAndQuestion);
			}
		}

		HashSet<string> wrongResponseId = new HashSet<string>();
		public async Task AddWrongResponse(string id)
		{
            SoundS.ExecuteSound (SoundEffectKind.WrongAnswer); 
			wrongResponseId.Add(id);

			//if (wrongResponseId.Count > 2 && challenge)
			//{
			//	await Navigator.SetNewRootAsync(Build.ViewModel(() => new FineChallengeLosePageViewModel(context)), true);
			//}
		}

		public async Task<bool> FirstStep()
		{
			using (var load = UserDialogs.Instance.Loading(""))
			{
				var res = await RequestQuestions(load);
                if (res)
                {
                    await NextStep(null);
                }

				return res;
			}
		}

		bool isFinished = false;
		int pos = -1;
		public async Task NextStep(BaseQuestionsModel questionModel, IPageNavigator navigator = null)
        {
            if (navigator != null)
				this.Navigator = navigator;

			if (questionModel != null)
            {
                //Verifico in _questions a che posizione si trova la domanda
                pos = _questions.IndexOf(questionModel);

                // Se sono una ExtChallenge voglio terminare la challenge corrente e salvare il risultato, così differenzio tra utenti bravi e quelli meno

                if (isExtChallenge && wrongResponseId.Count == 3)
                {
                    var res = await DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<UnoxAppModule>().SetFinished(_Id, challenge, isExtChallenge, _correctResponse, _totalResponse, isFinished);
                    if (!res)
                    {
                        await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("ErroreConnessione"));
                    }
                    else
                    {
                        await ShowFinishedPage(isFinished);
                    }
                }
                else if (pos < (_questions.Count() - 1) && !isFinished)
                {
                    //altrimenti dico di navigare al viewmodel corretto
                    var nextModel = _questions.ElementAt(pos + 1);
                    await navigateTo(nextModel);
                }
                else
                {
                    //Ho terminato le domande quindi devo riproporre quelle sbagliate
                    isFinished = true;
                    //se ho risposto correttamente a tutte le domande oppure sono challenge e ho meno di 3 errori dichiaro chiusa la lezione
                    if (_correctResponse == _questions.Count() || (challenge && wrongResponseId.Count < 3))
                    {
                        var res = await DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<UnoxAppModule>().SetFinished(_Id, challenge, isExtChallenge, _correctResponse, _totalResponse, isFinished);
                        if (!res)
                        {
                            await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("ErroreConnessione"));
                        }
                        else
                        {
                            await ShowFinishedPage(isFinished);
                        }
                    }
                    else
                    {
                        //Se sono challenge qui NON devo mai atterrare
                        if (challenge)
                        {
                            throw new Exception("QUALCOSA NON E' STATO IMPLEMENTATO BENE!!! SONO CHALLENGE E QUI NON DEVO MAI ATTERRARE!!!");
                        }
                        else
                        {
                            //Devo prendere random una risposta sbagliata
                            var id = GetRandomWrongQuestion();
                            var nextModel = _questions.FirstOrDefault(q => q.ID == id);
                            await navigateTo(nextModel);
                        }
                    }
                }
			}
			else
            {
				await navigateTo(_questions.FirstOrDefault());
			}
		}

		async Task ShowFinishedPage (bool isFinished = true) {
			var res = DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<UnoxAppModule>().GetFinishedPageType();

            if (isExtChallenge)
            {
                var remLives = 3 - countingWrong; // WARNING: 3 is hardcoded!!
                Nostromo.Container.Default.Resolve<IAnalyticsService>().TaskEvent("LogExtChallenge", "Correct", string.Format("ExtChallenge ID: {0}; remaining lives: {1}", Globals.CurrentChallenge, remLives));
                
                //System.Diagnostics.Debug.WriteLine($"isExtChallenge - RES VALE: {res}");
                SoundS.ExecuteSound(SoundEffectKind.ChallengeCompleted);
                await Navigator.SetNewRootAsync(Build.ViewModel(() => new FineChallengePageViewModel(context, _correctResponse, _totalResponse, isFinished)), true);
            }
            else if (challenge)
            {
                var indexLevel = context.GetItem(Globals.IndexOfLevel);
                var remLives = 3 - countingWrong;
                Nostromo.Container.Default.Resolve<IAnalyticsService>().TaskEvent("LogChallenge", "Correct", string.Format("Challenge: {0}; remaining lives: {1}", indexLevel, remLives));
                //Se ho già mostrato fine livello non lo rimostro ma faccio popModal
                if (res == FinishedPageType.Level)
                {
                    SoundS.ExecuteSound(SoundEffectKind.ChallengeCompleted);
                    await Navigator.SetNewRootAsync(Build.ViewModel(() => new FineLivelloPageViewModel(context)), true);
                }
                else
                    await Navigator.PopModalAsync(true);
            }else
            {
                //TODO: verificare se funziona
                System.Diagnostics.Debug.WriteLine("ChallengeCompleted Sound");
                SoundS.ExecuteSound(SoundEffectKind.ChallengeCompleted);
                switch (res)
                {
                    case FinishedPageType.Lesson:
                        await Navigator.SetNewRootAsync(Build.ViewModel(() => new FineLezionePageViewModel(context, _lastLesson)), true);
                        break;
                    case FinishedPageType.Level:
                        await Navigator.SetNewRootAsync(Build.ViewModel(() => new FineLivelloPageViewModel(context)), true);
                        break;
                    case FinishedPageType.Section:
                        await Navigator.SetNewRootAsync(Build.ViewModel(() => new FineSezionePageViewModel(context)), true);
                        break;
                }
            }
		}

        public bool UserPathStarted() {
            return ((_correctResponse + countingWrong) >= 2);//user has given 2 responses, either correct of wrong ones. This considers the path started
        }

        public async Task ForceChallengeFinished()
        {
            var res = await DefaultAppCoreBootstrapper.Instance.MainContext.ResolveModule<UnoxAppModule>().SetFinished(_Id, challenge, isExtChallenge, _correctResponse, _totalResponse, isFinished);
            if (!res)
            {
                    await Acr.UserDialogs.UserDialogs.Instance.AlertAsync(Catalog.GetValue("ErroreConnessione"));
            }
        }

		Random rand;
		string GetRandomWrongQuestion()
		{
			if (rand == null)
			{
				rand = new Random();
			}
			int toSkip = rand.Next(0, wrongResponseId.Count);

			return wrongResponseId.Skip(toSkip).Take(1).First();
		}

		async Task navigateTo(BaseQuestionsModel model)
		{
            IPageViewModel vm;
            switch (model.Type)
			{
				case TemplateType.S2:
					vm = Build.ViewModel(() => new QuestionS2PageViewModel(context, this, model, challenge, isExtChallenge));
					break;
				case TemplateType.S3:
					vm = Build.ViewModel(() => new QuestionS3PageViewModel(context, this, model, challenge, isExtChallenge));
					break;
				case TemplateType.S4:
					vm = Build.ViewModel(() => new QuestionS4PageViewModel(context, this, model, challenge, isExtChallenge));
					break;
				case TemplateType.S5_S9:
					vm = Build.ViewModel(() => new QuestionS5S9PageViewModel(context, this, model, challenge, isExtChallenge));
					break;
				case TemplateType.S6_S7:
					vm = Build.ViewModel(() => new QuestionS6S7PageViewModel(context, this, model, challenge, isExtChallenge));
					break;
				case TemplateType.S8:
					vm = Build.ViewModel(() => new QuestionS8PageViewModel(context, this, model, challenge, isExtChallenge));
					break;
				case TemplateType.S10_S11_S12:
					vm = Build.ViewModel(() => new QuestionS10S11S12PageViewModel(context, this, model, challenge, isExtChallenge));
					break;
				case TemplateType.S13:
					vm = Build.ViewModel(() => new QuestionS13PageViewModel(context, this, model, challenge, isExtChallenge));
					break;
				case TemplateType.S14:
					vm = Build.ViewModel(() => new QuestionS14PageViewModel(context, this, model, challenge, isExtChallenge));
					break;
				default:
					System.Diagnostics.Debug.WriteLine("Richiesto template non ancora implementato");
					return;
			}

			await Navigator.SetNewRootAsync(vm, true);
		}
	}
}
