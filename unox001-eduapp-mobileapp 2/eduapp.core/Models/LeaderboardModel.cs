﻿using System;
using System.Collections.Generic;
using UnoxEduAppModule;

namespace EduApp.Core
{
    public class LeaderboardModel
    {
        public IEnumerable<LeaderboardCellModel> LeaderboardCell { get; set; }
    }

    public class LeaderboardCellModel
    {
        public string ID { get; set; }
        public string LeaderboardUniqueIdentifier { get; set; }
        public string LeaderboardPosition { get; set; }
        public string LeaderboardNickname { get; set; }
        public string LeaderboardScore { get; set; }
    }
}
