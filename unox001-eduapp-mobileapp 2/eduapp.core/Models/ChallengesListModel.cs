﻿using System;
using System.Collections.Generic;
using UnoxEduAppModule;

namespace EduApp.Core
{
    public class ChallengesListModel
    {
        public IEnumerable<ChallengesListCellModel> ChallengesListCell { get; set; }
    }

    public class ChallengesListCellModel
    {
        public string ID { get; set; }
        public string TitleChallenge { get; set; }
        public string ChallengeImage { get; set; }
        //public Level CurrentLevel { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public bool Completed { get; set; }
    }

//    public class ChallengesListSectionModel
//{
//        public string CorrID { get; set; }
//        public string Image { get; set; }
//        public string Title { get; set; }
//        public string SubTitle { get; set; }
//        public float Percentage { get; set; }
//        public float Sbiadimento { get; set; }
//    }
}
