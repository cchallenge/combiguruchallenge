﻿using System;
using System.Collections.Generic;

namespace EduApp.Core
{
	public class BaseQuestionsModel
	{
		public string ID { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public TemplateType Type { get; set;}
	}

	public enum TemplateType
	{
		Unknown = -1,
		S1,
		S2,
		S3,
		S4,
		S5_S9,
		S6_S7,
		S8,
		S10_S11_S12,
		S13,
		S14,
	}

	#region Domanda S2
	public class S2QuestionModel : BaseQuestionsModel
	{
		public IEnumerable<S2Model> LeftAnswers { get; set; }
		public IEnumerable<S2Model> RightAnswers { get; set; }
	}

	public class S2Model
	{
		public string Text { get; set; }
		public string CorrelationId { get; set; }
		public string LinkedCorrelationId { get; set; }
	}
	#endregion

	#region Domanda S3
	public class S3QuestionModel : BaseQuestionsModel
	{
		public string ImagePath { get; set;}
		public IEnumerable<S3Model> Answers { get; set; }
	}

	public class S3Model
	{
		public string Text { get; set; }
		public string ImagePath { get; set; }
		public string BkgColor { get; set; }
		public int CorrectOrder { get; set; }
	}
	#endregion

	#region Domanda S4
	public class S4QuestionModel : BaseQuestionsModel
	{
		public IEnumerable<S4Model> Answers { get; set;}
	}

	public class S4Model {
		public string ImagePath { get; set;}
		public bool Correct { get; set;}
	}
	#endregion



	#region Domanda S5_S9
	public class S5S9QuestionModel : BaseQuestionsModel
	{
		public IEnumerable<S5S9Model> Answers { get; set; }
	}

	public class S5S9Model
	{
		public string Answer { get; set; }
		public bool Correct { get; set; }
	}
	#endregion

	#region Domanda S6
	public class S6S7QuestionModel : BaseQuestionsModel
	{
		public IEnumerable<SBaseModelRange> Answers { get; set; }
	}
	#endregion

	#region Domanda S8
	public class S8QuestionModel : BaseQuestionsModel
	{
		public IEnumerable<S8Model> Answers { get; set; }
	}

	public class S8Model 
	{
		public IEnumerable<SBaseModelRange> SubAnswers { get; set; }
	}
	#endregion

	public class SBaseModelRange : SBaseModel
	{
		public string MinValueAnswer { get; set; }
		public string MaxValueAnswer { get; set; }
	}

	public class SBaseModel
	{
		public string ImagePath { get; set; }
		public string Answer { get; set; }
		public string UnitMeasure { get; set; }
	}

	#region Domanda S10_S11_S12
	public class S10S11S12QuestionModel : BaseQuestionsModel
	{
		public string TopImagePath { get; set; }
		public string BottomImagePath { get; set; }
		public bool[,] CorrectAnswers { get; set; }
		public bool[,] Points { get; set; }
		public string[] XAxisTitles { get; set; }
		public string[] YAxisTitles { get; set; }
	}
	#endregion

	#region Domanda S13
	public class S13QuestionModel : BaseQuestionsModel
	{
		public IEnumerable<S13Model> Answers { get; set; }
		public IEnumerable<SBaseModel> XAxis { get; set; }
	}

	public class S13Model
	{
		public IEnumerable<SBaseModel> SubAnswers { get; set; }
		public int Order { get; set; }
	}
	#endregion

	#region Domanda S14
	public class S14QuestionModel : BaseQuestionsModel
	{
		public string BottomImagePath { get; set; }
		public bool[,] CorrectAnswers { get; set; }
		public bool[,] Points { get; set; }
		public string[] XAxisTitles { get; set; }
		public string[] YAxisTitles { get; set; }
		public IEnumerable<S14Model> Answers { get; set; }
	}

	public class S14Model
	{
		public IEnumerable<SBaseModel> SubAnswers { get; set; }
	}
	#endregion
}
