﻿using System;
using System.Collections.Generic;
using UnoxEduAppModule;

namespace EduApp.Core
{
	public class UserLevel { 
		public string LevelId { get; set;}
		public float LevelPercentage { get; set;}
	}

	public class LearningModel
	{
		public IEnumerable<LearningCellModel> LearningCell { get; set; }
	}

	public class LearningCellModel
	{
		public string ID { get; set; }
		public string TitleLesson { get; set; }
		public string LessonImage { get; set; }
		public string TitleChallenge { get; set; }
		public string ChallengeImage { get; set; }
		public Level CurrentLevel { get; set;}
	}

	public class SectionModel
	{
		public string CorrID { get; set; }
		public string Image { get; set; }
		public string Title { get; set; }
		public string SubTitle { get; set; }
		public float Percentage { get; set; }
		public float Sbiadimento { get; set;}
	}
}
