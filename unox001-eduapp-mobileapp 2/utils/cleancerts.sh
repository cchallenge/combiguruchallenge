#!/bin/sh
if [ $# -eq 0 ]; then
	echo "Usage: $0 <file>.sln"
	exit
fi

cat $1 | grep -oh "\"[^\"]*\.csproj\"" | tr \\ // | tr \" \ | while read line
do
	line="./$line"
	echo "processing $line"
 	sed "s/<CodesignKey>[^<]*<\/CodesignKey>/<CodesignKey>iPhone Distribution<\/CodesignKey>/;s/<CodesignProvision>[^<]*<\/CodesignProvision>/<CodesignProvision><\/CodesignProvision>/" $line > $line.new; 
 	cp $line.new $line; 
 	rm $line.new;
done
