#/bin/sh!

if [ "$1" == "-r" ]; then

	git branch -r --merged | egrep -v "[^/]*/(hockeyapp|master|itunes|develop.*|production.*)$" | sed 's/origin\///' | xargs -n 1 git push --delete origin

elif [ "$1" == "-l" ]; then 
	 git branch --merged | egrep -v "^\s*(\*.*|master|itunes|hockeyapp|develop.*|production.*)$" | xargs git branch -d
else
	echo "usage: "
	echo "    $0 -r  --> elimina i branches remoti (merged)"
	echo "    $0 -l  --> elimina i branches locali (merged)"
fi
