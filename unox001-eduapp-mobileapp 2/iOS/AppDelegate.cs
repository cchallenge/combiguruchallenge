﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AppCore;
using AVFoundation;
using EduApp.Core;
using Foundation;
using HockeyApp.iOS;
using MediaPlayer;
using NControl.iOS;
using Newtonsoft.Json.Linq;
using NLog;
using NLog.Config;
using NLog.Targets;
using UIKit;

namespace EduApp.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			EnableNLogConsole();

			global::Xamarin.Forms.Forms.Init();
			NControlViewRenderer.Init();

			//CarouselView does not get “Linked-out” when compiling in release mode
			var cv = typeof(Xamarin.Forms.CarouselView);
			var assembly = Assembly.Load(cv.FullName);

			DefaultAppCoreBootstrapper bootstrapper = new DefaultAppCoreBootstrapper (Globals.PSAppServerUrl);
			bootstrapper
				.EnableiOSDefaultModules ();

			var notifSettings = UIUserNotificationSettings.GetSettingsForTypes (UIUserNotificationType.Sound | UIUserNotificationType.Alert | UIUserNotificationType.Badge, null);
			UIApplication.SharedApplication.RegisterUserNotificationSettings (notifSettings);
			UIApplication.SharedApplication.RegisterForRemoteNotifications ();

			LoadApplication(new App((bootstrapper), typeof(NotificationService_iOS).Assembly));

			//var fontList = new System.Text.StringBuilder ();
			//var familyNames = UIFont.FamilyNames;
			//foreach (var familyName in familyNames) {
			//	fontList.Append (String.Format ("Family: {0}\n", familyName));
			//	Console.WriteLine ("Family: {0}\n", familyName);
			//	var fontNames = UIFont.FontNamesForFamilyName (familyName);
			//	foreach (var fontName in fontNames) {
			//		Console.WriteLine ("\tFont: {0}\n", fontName);
			//		fontList.Append (String.Format ("\tFont: {0}\n", fontName));
			//	}
			//};

			var manager = BITHockeyManager.SharedHockeyManager;
			manager.Configure ("72a73ba8b8ad42968b99383413676d4e");
			manager.StartManager ();
			manager.Authenticator.AuthenticateInstallation ();

			AVAudioSession.SharedInstance ().Init ();
			NSError error;
			AVAudioSession.SharedInstance().SetCategory(AVAudioSessionCategory.Playback);
			AVAudioSession.SharedInstance().SetMode(AVAudioSession.ModeSpokenAudio, out error);
		    
		    if (!AVAudioSession.SharedInstance ().SetActive(true, out error)) {
		    }
    

			return base.FinishedLaunching(app, options);
		}

		//Azure push notif Xamarin.Forms https://docs.microsoft.com/it-it/azure/app-service-mobile/app-service-mobile-xamarin-forms-get-started-push
		//Generare distribution profile https://developer.apple.com/library/content/documentation/IDEs/Conceptual/AppDistributionGuide/AddingCapabilities/AddingCapabilities.html#//apple_ref/doc/uid/TP40012582-CH26-SW6
		//Xamarin.iOS local notifications https://developer.xamarin.com/guides/ios/application_fundamentals/notifications/remote_notifications_in_ios/

		public override void RegisteredForRemoteNotifications (UIApplication application, NSData deviceToken)
		{
			var DeviceToken = deviceToken.ToString ().Replace ("<", "").Replace (">", "").Replace (" ", "");

			Nostromo.Container.Default.Resolve<INotificationService> ().SetDeviceToken (deviceToken); 
		}

		public override void FailedToRegisterForRemoteNotifications (UIApplication application, NSError error)
		{
			new UIAlertView ("Error registering push notification", error.LocalizedDescription, null, "OK", null).Show ();
		}

		public static void EnableNLogConsole()
		{
			var config = new LoggingConfiguration();

			var consoleTarget = new ConsoleTarget { Layout = "${date:format=HH\\:MM\\:ss} ${logger} ${message}" };
			config.AddTarget("console", consoleTarget);

			#if !DISTRIBUTION
			config.LoggingRules.Add (new LoggingRule ("*", NLog.LogLevel.Trace, consoleTarget));
			#else
			config.LoggingRules.Add(new LoggingRule("*", NLog.LogLevel.Debug, consoleTarget));
			#endif
			LogManager.Configuration = config;
		}

		public override bool OpenUrl (UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
		{
			return PSMFacebook.iOS.FacebookLoginButtonRenderer.OpenUrl(application,url,sourceApplication,annotation);
		}
	}
}
