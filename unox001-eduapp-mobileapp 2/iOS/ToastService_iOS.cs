﻿using System;
using Nostromo;
using EduApp.Core;
using ToastIOS;

namespace EduApp.iOS
{
	[Dependency]
	public class ToastService_iOS : IToastService
	{
		public ToastService_iOS ()
		{
		}

		public void ShowToastMessage (string message)
		{
			Toast.MakeText (message, Toast.LENGTH_SHORT).Show ();
		}
	}
}
