﻿using System;
using EduApp;
using EduApp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(EntryWithTitleRenderer), typeof(EntryRendereriOS))]
namespace EduApp.iOS
{
	public class EntryRendereriOS : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);

			try
			{
				if (Control != null && Element != null)
				{
					Control.SpellCheckingType = UITextSpellCheckingType.No;             // No Spellchecking
					Control.AutocorrectionType = UITextAutocorrectionType.No;           // No Autocorrection
					Control.AutocapitalizationType = UITextAutocapitalizationType.None; // No Autocapitalization
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}
	}
}
