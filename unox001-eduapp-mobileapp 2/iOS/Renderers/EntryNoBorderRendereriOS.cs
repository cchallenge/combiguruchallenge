﻿using System;
using System.Drawing;
using EduApp;
using EduApp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (EntryDynamicRenderer), typeof (EntryNoBorderRendereriOS))]
namespace EduApp.iOS
{
	public class EntryNoBorderRendereriOS : EntryRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);

			if (this.Control == null) return;

			var ele = e.NewElement as EntryDynamicRenderer;
			if (ele != null && !ele.IsPassword)
				this.Control.AutocorrectionType = UIKit.UITextAutocorrectionType.No;
			
			this.Control.BorderStyle = UIKit.UITextBorderStyle.None;

		}
	}
}
