﻿using System;
using CoreGraphics;
using EduApp;
using EduApp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(RoundedContainer), typeof(RoundedContaineriOS))]
namespace EduApp.iOS
{
	public class RoundedContaineriOS : ViewRenderer
	{
		public override void LayoutSubviews()
		{
			base.LayoutSubviews();
			if (Element != null)
			{
				double min = Math.Min(Element.Width, Element.Height);
				var newLayer = (float)(min / 2.0);
				if (newLayer > 0 && Layer.CornerRadius < newLayer)
				{
					Layer.CornerRadius = newLayer;
				}
				ClipsToBounds = true;
				Layer.MasksToBounds = true;
				BackgroundColor = _bkgColor;
			}
		}

		UIColor _bkgColor;
		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			_bkgColor = (sender as RoundedContainer).BackgroundColor.ToUIColor();
			this.LayoutSubviews();
		}
	}
}
