﻿using System;
using EduApp;
using EduApp.iOS;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (ViewPage), typeof (ViewPageRenderer))]
namespace EduApp.iOS
{
	public class ViewPageRenderer : PageRenderer
	{

		NSObject observerHideKeyboard;
		NSObject observerShowKeyboard;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			var cp = Element as ViewPage;

			if (cp == null) return;

			foreach (var g in View.GestureRecognizers)
				g.CancelsTouchesInView = false;
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			observerShowKeyboard = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, OnKeyboardNotification);
			observerHideKeyboard = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, OnKeyboardNotification);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			NSNotificationCenter.DefaultCenter.RemoveObserver (observerShowKeyboard);
			NSNotificationCenter.DefaultCenter.RemoveObserver (observerHideKeyboard);
		}

		void OnKeyboardNotification (NSNotification notification)
		{
			if (!IsViewLoaded) return;

			var frameBegin = UIKeyboard.FrameBeginFromNotification (notification);
			var frameEnd = UIKeyboard.FrameEndFromNotification (notification);

			var page = Element as ContentPage;

			if ((page == null) && (page.Content is ScrollView)) return;

			var padding = page.Padding;
			var newBottom = padding.Bottom + frameBegin.Top - frameEnd.Top;
			page.Padding = new Thickness (padding.Left, padding.Top, padding.Right, newBottom < 0 ? 0 : newBottom);
		}
	}
}
