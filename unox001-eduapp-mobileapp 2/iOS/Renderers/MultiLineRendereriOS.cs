﻿using System;
using CoreGraphics;
using EduApp;
using EduApp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(MultiLineLabel), typeof(MultiLineRendereriOS))]
namespace EduApp.iOS
{
	public class MultiLineRendereriOS : LabelRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged(e);

			MultiLineLabel multiLineLabel = (MultiLineLabel)Element;

			if (multiLineLabel != null && multiLineLabel.Lines != -1)
			{
				Control.Lines = multiLineLabel.Lines;
			}
		}

		nfloat _cornerRadius = 0;
		UIColor _bkgColor;
		CGColor _borderColor;
		nfloat _borderWidth;
		nfloat _padding = 0;
		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			_cornerRadius = (sender as MultiLineLabel).CornerRadius;
			_bkgColor = (sender as MultiLineLabel).BackgroundColor.ToUIColor();
			_borderColor = (sender as MultiLineLabel).BorderColor.ToCGColor();
			_borderWidth = (sender as MultiLineLabel).BorderWidth;
			_padding = (sender as MultiLineLabel).Padding;
		}

		public override CGSize SizeThatFits(CGSize size)
		{
			//@@ CM tolgo il padding prima del SizeThatFits per simulare il fatto che il mio contenuto abbia un padding
			var pad = _padding * 2;
			if (!float.IsInfinity((float)size.Width))
				size = new CGSize(size.Width - pad, size.Height);

			if (!float.IsInfinity((float)size.Height))
				size = new CGSize(size.Width, size.Height - pad);
			   
			var res = base.SizeThatFits(size);

			//@@ CM rimetto il padding al mio oggetto per ritornare le dimensioni corrette del mio oggetto
			res = new CGSize(res.Width + pad, res.Height + pad);
			return res;
		}

		public override void LayoutSubviews()
		{
			this.Control.Frame = new CGRect(_padding, _padding, this.Frame.Width - (_padding * 2), this.Frame.Height - (_padding * 2));

			if (_cornerRadius > 0)
			{
				Layer.CornerRadius = _cornerRadius;
				Layer.BorderColor = _borderColor;
				Layer.BorderWidth = _borderWidth;
			}
			BackgroundColor = _bkgColor;
		}
	}
}
