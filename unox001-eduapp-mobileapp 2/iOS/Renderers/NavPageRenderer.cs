﻿using System;
using EduApp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(NavigationPage), typeof(NavPageRenderer))]
namespace EduApp.iOS
{
	public class NavPageRenderer : NavigationRenderer
	{
		public NavPageRenderer()
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
            // remove lower border and shadow of the navigation bar
            NavigationBar.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
            NavigationBar.ShadowImage = new UIImage();
            NavigationBar.Translucent = false;
            NavigationBar.BarTintColor = this.Element.BackgroundColor.ToUIColor();
            this.AutomaticallyAdjustsScrollViewInsets = true;

        }
    }
}
