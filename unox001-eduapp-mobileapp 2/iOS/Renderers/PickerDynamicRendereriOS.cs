﻿using System;
using EduApp;
using EduApp.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(PickerDynamicRenderer), typeof(PickerDynamicRendereriOS))]
namespace EduApp.iOS
{
	public class PickerDynamicRendereriOS : PickerRenderer
	{

		protected override void OnElementChanged (ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged (e);
			var ele = e.NewElement as PickerDynamicRenderer;
			if (Control == null) return;
			/*
			 * Nel picker dynamic tolgo il bordo, e allineo il testo a destra
			 */

			Control.BorderStyle = UIKit.UITextBorderStyle.None;
			if (ele != null) 
				Control.TextAlignment = (ele.TextAlignRight) ? UIKit.UITextAlignment.Right : UIKit.UITextAlignment.Left;

			var font = UIKit.UIFont.FromName  ("SFNSDisplay", 12f);

			Control.Font = font;

		}
	}
}
