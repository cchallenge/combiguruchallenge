﻿using System;
using CoreGraphics;
using EduApp;
using EduApp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CornerRadiusContainer), typeof(CornerRadiusContaineriOS))]
namespace EduApp.iOS
{
	public class CornerRadiusContaineriOS : ViewRenderer
	{
		nfloat _cornerRadius = 0;
		UIColor _bkgColor;
		CGColor _borderColor;
		nfloat _borderWidth;
		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			_cornerRadius = (sender as CornerRadiusContainer).CornerRadius;
			_bkgColor = (sender as CornerRadiusContainer).BackgroundColor.ToUIColor();
			_borderColor = (sender as CornerRadiusContainer).BorderColor.ToCGColor();
			_borderWidth = (sender as CornerRadiusContainer).BorderWidth;
			this.LayoutSubviews();
		}

		public override void LayoutSubviews()
		{
			if (_cornerRadius > 0)
			{
				Layer.CornerRadius = _cornerRadius;
				Layer.BorderColor = _borderColor;
				Layer.BorderWidth = _borderWidth;
			}
			BackgroundColor = _bkgColor;
		}
	}
}
