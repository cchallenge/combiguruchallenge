﻿using System;
using EduApp.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ViewCell), typeof(StandardViewCellRenderer))]
namespace EduApp.iOS
{
	public class StandardViewCellRenderer : ViewCellRenderer
	{
		public override UIKit.UITableViewCell GetCell(Cell item, UIKit.UITableViewCell reusableCell, UIKit.UITableView tv)
		{
			var cell = base.GetCell(item, reusableCell, tv);

			cell.SelectionStyle = UIKit.UITableViewCellSelectionStyle.None;
			//tv.SeparatorStyle = UIKit.UITableViewCellSeparatorStyle.None;

			return cell;
		}
	}
}
