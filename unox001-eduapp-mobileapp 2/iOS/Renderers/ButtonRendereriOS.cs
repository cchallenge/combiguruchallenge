﻿using System;
using EduApp;
using EduApp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ButtonBorderRenderer), typeof(ButtonRendereriOS))]
namespace EduApp.iOS
{
	public class ButtonRendereriOS : ButtonRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);

			try
			{
				if (Control != null && Element != null)
				{
					var button = Element as Button;
					//Occorre perchè di default il UIButton nel border ha un alpha
					var color = button.BorderColor;
					if (color != Color.Transparent && button.BorderWidth > 0.0f)
					{
						Control.Layer.BorderColor = (new UIColor((nfloat)color.R, (nfloat)color.G, (nfloat)color.B, 1)).CGColor;
						Control.Layer.MasksToBounds = true;
						Control.Layer.Opacity = 1.0f;
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}
	}
}
