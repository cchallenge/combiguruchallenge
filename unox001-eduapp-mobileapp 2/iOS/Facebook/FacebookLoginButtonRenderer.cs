﻿using System;
using PSMFacebook;
using Facebook.LoginKit;
using Xamarin.Forms.Platform.iOS;
using Facebook.LoginKit;
using PSMFacebook.iOS;
using Xamarin.Forms;
using UIKit;
using Foundation;

[assembly: ExportRenderer (typeof (FacebookLoginButton), typeof (FacebookLoginButtonRenderer))]
namespace PSMFacebook.iOS
{
	class FacebookLoginButtonRenderer : ViewRenderer<FacebookLoginButton, UIView>
	{

		public static bool OpenUrl (UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
		{
			bool handled = Facebook.CoreKit.ApplicationDelegate.SharedInstance.OpenUrl (application, url, sourceApplication, annotation);

			return handled;
		}


		public FacebookLoginButtonRenderer ()
		{

		}

		bool WillLogin (LoginButton bt)
		{
			return true;
		}


		protected override void OnElementChanged (ElementChangedEventArgs<FacebookLoginButton> e)
		{
			base.OnElementChanged (e);

			var element = Element;


			if (element != null) {
				
				var control = new LoginButton (this.Bounds);
				//@@ CM capire se vogliono un altro testo
				control.SetAttributedTitle(new NSAttributedString(""), UIControlState.Normal);

				control.ReadPermissions = element.ReadPermissions;
			//	if (Facebook.CoreKit.AccessToken.CurrentAccessToken != null) {
					var lm = new LoginManager ();
					lm.LogOut ();
			//	}

				this.SetNativeControl (control);
				control.WillLogin = new LoginButtonWillLogin (WillLogin);
				control.Completed += (object sender, LoginButtonCompletedEventArgs le) => {
					if (le.Error == null && le.Result.Token != null) {
						element?.LoggedInCommand?.Execute (le.Result.Token.TokenString);
						//NOTA BENE:  NON MANTENGO L'SDK LOGGATO!!!
						lm.LogOut ();
					}
				};
			}
		}

	}
}
