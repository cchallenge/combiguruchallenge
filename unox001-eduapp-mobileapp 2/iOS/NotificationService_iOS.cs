﻿using System;
using EduApp.Core;
using WindowsAzure.Messaging;
using Foundation;
using System.Linq;
using Nostromo;
using EduApp.iOS;

namespace EduApp.iOS
{
	[Dependency]
	public class NotificationService_iOS : INotificationService
	{
		
		string StringConnection = SBConnectionString.CreateListenAccess (new NSUrl (Globals.EndPoint), Globals.SharedAccessKey);
		
		SBNotificationHub Hub { get; set; }
		NSData DeviceToken { get; set; }

		string [] _tags;
		public string[] Tags {
			get { return _tags; }
		}

		public NotificationService_iOS ()
		{
			Hub = new SBNotificationHub (StringConnection, Globals.NotificationHubPath);
		}

		public async void RegisterForRemoteNotificationsWithTags (string [] tags)
		{
			_tags = tags;
			if (DeviceToken != null) 
			{
				Hub.UnregisterAllAsync (DeviceToken, (error) => {
					if (error != null) 
					{
						Console.WriteLine ("Error calling Unregister: {0}", error.ToString ());
						return;
					}

					NSSet nativeTags = null;
					if (tags?.Any () ?? false) 
					{
						nativeTags = new NSSet (tags);
					}

					Hub.RegisterNativeAsync (DeviceToken, nativeTags, (errorC) => {
						if (errorC != null)
							Console.WriteLine ("RegisterNativeAsync error: " + errorC.ToString ());
					});

					Console.WriteLine ("Device registered with deviceToken: " + DeviceToken);
				});
			}
		}

		public void SetDeviceToken (object token)
		{
			this.DeviceToken = token as NSData;
		}

		public void UnRegisterForRemote ()
		{
			NSError error = new NSError ();
			if (DeviceToken != null) 
			{
				Hub.UnregisterAll (DeviceToken, out error);
				if (error != null)
					Console.WriteLine (error.ToString ());
			}
		}
	}
}
