﻿using System;
using System.IO;
using AVFoundation;
using EduApp.Core;
using Foundation;
using Xamarin.Forms;

namespace EduApp.iOS
{
	[Nostromo.Dependency]
	public class SoundService_iOS : ISoundService
	{

		AVAudioPlayer rightPlayer;
		AVAudioPlayer wrongPlayer;
		AVAudioPlayer chCompletedPlayer;
		AVAudioPlayer chFailedPlayer;

		public void ExecuteSound(SoundEffectKind sound)
		{
			Device.BeginInvokeOnMainThread(() =>
			{
				switch (sound)
				{
					case SoundEffectKind.RightAnswer:
						if (rightPlayer == null)
						{
							rightPlayer = createAudioPlayer(sound);
						}
						else
						{
							rightPlayer.Stop();
							rightPlayer.Play();
						}
						break;
					case SoundEffectKind.WrongAnswer:
						if (wrongPlayer == null)
						{
							wrongPlayer = createAudioPlayer(sound);
						}
						else
						{
							wrongPlayer.Stop();
							wrongPlayer.Play();
						}
						break;
					case SoundEffectKind.ChallengeCompleted:
						if (chCompletedPlayer == null)
						{
							chCompletedPlayer = createAudioPlayer(sound);
						}
						else
						{
							chCompletedPlayer.Stop();
							chCompletedPlayer.Play();
						}
						break;
					default:
						if (chFailedPlayer == null)
						{
							chFailedPlayer = createAudioPlayer(sound);
						}
						else
						{
							chFailedPlayer.Stop();
							chFailedPlayer.Play();
						}
						break;
				}
			});
		}

		AVAudioPlayer createAudioPlayer(SoundEffectKind sound)
		{
			var filePath = NSBundle.MainBundle.PathForResource(Path.GetFileNameWithoutExtension(SoundsEffect.GetFileName(sound)), Path.GetExtension(SoundsEffect.GetFileName(sound)));

			var url = NSUrl.FromString(filePath);

			var player = AVAudioPlayer.FromUrl(url);

			var audioSession = AVAudioSession.SharedInstance();

			var err = audioSession.SetCategory(AVAudioSessionCategory.PlayAndRecord);
			if (err != null)
			{
				Console.WriteLine("audioSession: {0}", err);
			}

			var b = audioSession.OverrideOutputAudioPort(AVAudioSessionPortOverride.Speaker, out err);
			if (b == false || err != null)
			{
				Console.WriteLine("audioSession: {0}", err);
				throw new Exception("Audio Session Error");
			}

			player.FinishedPlaying += (object sender, AVStatusEventArgs e) =>
			{
				player.Stop();
			};

			player.PrepareToPlay();
			player.Play();

			return player;
		}
	}
}
