﻿using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using AppCore.Interfaces;
using EduApp.Core;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EduApp
{
	[ContentProperty ("Text")]
	public class TranslateExtension : IMarkupExtension, IAppCatalog
	{
		const string ResourceId = "EduApp.AppResources";
		CultureInfo ci = null;
		public string Text { get; set; }

		public IAppCoreContext Context {
			get;
			set;
		}

		static string Language { get; set; }
		#if DEBUG
		= "IT";
		#endif

		public static string TranslateOne (string key)
		{
			System.Diagnostics.Debug.WriteLine (Language);
			var val = AppResources.ResourceManager.GetString (key, new CultureInfo (Language));
			System.Diagnostics.Debug.WriteLine (val);
			return AppResources.ResourceManager.GetString (key, new CultureInfo (Language));
		}

		public TranslateExtension ()
		{
		}

		public string GetValue (string value)
		{
			Text = value;
			return ReadResource ();
		}

		string ReadResource ()
		{
			ResourceManager temp = new ResourceManager (ResourceId, typeof(TranslateExtension).GetTypeInfo().Assembly);
			ci = new CultureInfo (Language);
			var transaltion = temp.GetString (Text, ci);
			if (transaltion == null)
				transaltion = Text;
			return transaltion;
		}

		public object ProvideValue (IServiceProvider serviceProvider)
		{
			if (Text == null)
				return "";
			return ReadResource ();
		}

		public void SetLanguage (string language = null)
		{
			if (language == null)
				Language = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
			else
				Language = language;
		}

		public void ResetLanguage ()
		{
			Language = string.Empty;
		}

		public string GetLanguage ()
		{
			return Language;
		}
	}
}
