﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Xamarin.Forms;
using System.Linq;
using ReactiveUI;
using MvvmHelpers;
using System.Threading.Tasks;
using System.Reactive.Linq;
using System.Collections;

namespace EduApp
{
	public partial class DynamicList : ContentView
	{
		public static readonly BindableProperty DynamicSourceProperty = BindableProperty.Create (
			propertyName: "DynamicSource",
			returnType: typeof (DynamicListSource),
			declaringType: typeof (DynamicList),
			defaultValue: null,
			defaultBindingMode: BindingMode.OneWay,
			propertyChanged:  HandleDynamicListSourceChange
		);

		public DynamicListSource DynamicSource
		{
			get { return (DynamicListSource)GetValue(DynamicSourceProperty); }
			set { SetValue(DynamicSourceProperty, value); }
		}

		#region Ascolto cambio della lista
		static void HandleDynamicListSourceChange (BindableObject bindable, object oldValue, object newValue)
		{
			(bindable as DynamicList).ChangingSource (newValue as DynamicListSource);
		}

		IDisposable dynamicListSourceListener;
		void ChangingSource (DynamicListSource dynamicListSource)
		{
			dynamicListSourceListener?.Dispose ();
			dynamicListSourceListener = null;
			if (dynamicListSource != null) {
				dynamicListSourceListener = dynamicListSource.WhenAnyValue (dls => dls.ListVMVisualizzati)
				                                             .ObserveOn ( ReactiveUI.RxApp.MainThreadScheduler )
				                                             .Subscribe (ListenListChanges);
			}
		}

		ExtraObservableCollection<IFormItemViewModel> _formsViewModelsCollection;
		public void ListenListChanges (ExtraObservableCollection<IFormItemViewModel> list) {
			if (_formsViewModelsCollection != null)
				_formsViewModelsCollection.CollectionChanged -= HandleNotifyCollectionChangedEventHandler;

			_formsViewModelsCollection = list;
			//@@CM aggiorno l'ItemSource della lista
			this.list.ItemsSource = list;

			if (_formsViewModelsCollection != null)
				_formsViewModelsCollection.CollectionChanged += HandleNotifyCollectionChangedEventHandler;
		}

		async void HandleNotifyCollectionChangedEventHandler (object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if (elemToFocus != null) {
				await Task.Yield ();
				this.list.ScrollTo (elemToFocus, ScrollToPosition.Start, false);
				elemToFocus = null;
			}
		}
		#endregion

		static void HandleItemSourceChange(BindableObject bindable, object oldValue, object newValue)
		{
			(bindable as DynamicList).list.ItemsSource = (IEnumerable)newValue;
		}

		ListView list;
		public DynamicList ()
		{
			//@@CM in iOS il RecycleElement genera rallentamenti e ricrea le celle 4/5 volte
			Device.OnPlatform(
				iOS: () => list = new ListView(ListViewCachingStrategy.RetainElement), 
				Android: () => list = new ListView(ListViewCachingStrategy.RecycleElement)
			);
			list.HasUnevenRows = true;
			list.BackgroundColor = Color.Transparent;

			//@@MC -> Fix per iOS che toglie 1 in altezza
			this.list.SeparatorVisibility = SeparatorVisibility.Default;
			this.list.SeparatorColor = Color.Transparent;
			this.list.Header = null;
			this.list.Footer = null;
			//Imposto un DataTemplateSelector custom per gestire celle diverse
			this.list.ItemTemplate = new DynamicListDataTemplate ();
			this.list.ItemSelected += OnSelection;
			this.list.ItemTapped += OnItemTapped;

			this.Content = list;
		}

		void OnSelection (object sender, SelectedItemChangedEventArgs e)
		{
			this.list.SelectedItem = null;
		}

		BaseCellViewModel elemToFocus;
		async void OnItemTapped (object sender, ItemTappedEventArgs e)
		{
			this.list.SelectedItem = null;
			elemToFocus = null;
			if (e.Item != null) {
				var elem = e.Item as BaseCellViewModel;
				elem.ItemCommand.Execute (null);

				if (elem is SezioneViewModel) {
					var sezione = elem as SezioneViewModel;
					if (sezione.IsExpanded)
						elemToFocus = sezione;
				}
			}
		}
	}
}
