﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace EduApp
{
	public partial class BaseQuestionContainer : Frame
	{
		public static readonly BindableProperty QuestionTitleProperty = BindableProperty.Create(nameof(QuestionTitle), typeof(string), typeof(BaseQuestionContainer), String.Empty, BindingMode.OneWay);

		public string QuestionTitle
		{
			get { return (string)this.GetValue(QuestionTitleProperty); }
			set { this.SetValue(QuestionTitleProperty, value); }
		}

		public static readonly BindableProperty ProgressProperty = BindableProperty.Create(nameof(Progress), typeof(double), typeof(BaseQuestionContainer), 0.0, BindingMode.OneWay, propertyChanged:OnProgressChange);

		public double Progress
		{
			get { return (double)this.GetValue(ProgressProperty); }
			set { this.SetValue(ProgressProperty, value); }
		}

		public static readonly BindableProperty QuestionSubtitleProperty = BindableProperty.Create(nameof(QuestionSubtitle), typeof(string), typeof(BaseQuestionContainer), String.Empty, BindingMode.OneWay);

		public string QuestionSubtitle
		{
			get { return (string)this.GetValue(QuestionSubtitleProperty); }
			set { this.SetValue(QuestionSubtitleProperty, value); }
		}

		public static readonly BindableProperty QuestionSelectorProperty = BindableProperty.Create (nameof (QuestionSelector), typeof (string), typeof (BaseQuestionContainer), String.Empty, BindingMode.OneWay);

		public string QuestionSelector {
			get { return (string)this.GetValue (QuestionSelectorProperty); }
			set { this.SetValue (QuestionSelectorProperty, value); }
		}

		public static readonly BindableProperty ButtonTitleProperty = BindableProperty.Create(nameof(ButtonTitle), typeof(string), typeof(BaseQuestionContainer), String.Empty, BindingMode.OneWay);

		public string ButtonTitle
		{
			get { return (string)this.GetValue(ButtonTitleProperty); }
			set { this.SetValue(ButtonTitleProperty, value); }
		}

		public static readonly BindableProperty ButtonCommandProperty = BindableProperty.Create(nameof(ButtonCommand), typeof(ICommand), typeof(BaseQuestionContainer), null, BindingMode.OneWay);

		public ICommand ButtonCommand
		{
			get { return (ICommand)this.GetValue(ButtonCommandProperty); }
			set { this.SetValue(ButtonCommandProperty, value); }
		}

		public View Question
		{
			get { return this.questionContainer; }
			set
			{
				this.questionContainer.Content = null;
				this.questionContainer.Content = value;
			}
		}

		static void OnProgressChange(BindableObject bindable, object oldValue, object newValue)
		{
			var parent = (BaseQuestionContainer)bindable;

			var pWidth = (double)newValue;
			if (pWidth <= 0.0)
				pWidth = 0.0001;
			parent.PrimaColonna.Width = new GridLength(pWidth, GridUnitType.Star);

			var tWidth = (1.0 - (double)newValue);
			if (tWidth <= 0.0)
				tWidth = 0.0001;
			parent.TerzaColonna.Width = new GridLength(tWidth, GridUnitType.Star);
		}

		public BaseQuestionContainer()
		{
			InitializeComponent();
		}
	}
}
