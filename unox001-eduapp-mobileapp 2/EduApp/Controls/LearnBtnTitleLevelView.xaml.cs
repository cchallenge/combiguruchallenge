﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace EduApp
{
	public partial class LearnBtnTitleLevelView : StackLayout
	{
		public static readonly BindableProperty SbiadimentoProperty = BindableProperty.Create(nameof(Sbiadimento), typeof(float), typeof(LearnBtnTitleLevelView), 0.0f, BindingMode.OneWay);

		public float Sbiadimento
		{
			get { return (float)this.GetValue(SbiadimentoProperty); }
			set { this.SetValue(SbiadimentoProperty, value); }
		}

		public static readonly BindableProperty ProgressProperty = BindableProperty.Create (nameof (Progress), typeof (float), typeof (LearnBtnTitleLevelView), 0.0f, BindingMode.OneWay);

		public float Progress {
			get { return (float)this.GetValue (ProgressProperty); }
			set { this.SetValue (ProgressProperty, value); }
		}

		public static readonly BindableProperty BkgImageSourceProperty = BindableProperty.Create (nameof (BkgImageSource), typeof (string), typeof (LearnBtnTitleLevelView), String.Empty, BindingMode.OneWay);

		public string BkgImageSource {
			get { return (string)this.GetValue (BkgImageSourceProperty); }
			set { this.SetValue (BkgImageSourceProperty, value); }
		}

		public static readonly BindableProperty TopTitleProperty = BindableProperty.Create (nameof (TopTitle), typeof (string), typeof (LearnBtnTitleLevelView), String.Empty, BindingMode.OneWay);

		public string TopTitle {
			get { return (string)this.GetValue (TopTitleProperty); }
			set { this.SetValue (TopTitleProperty, value); }
		}

		public static readonly BindableProperty BottomTitleProperty = BindableProperty.Create (nameof (BottomTitle), typeof (string), typeof (LearnBtnTitleLevelView), String.Empty, BindingMode.OneWay);

		public string BottomTitle {
			get { return (string)this.GetValue (BottomTitleProperty); }
			set { this.SetValue (BottomTitleProperty, value); }
		}

		public LearnBtnTitleLevelView ()
		{
			InitializeComponent ();
		}

		protected override void OnChildAdded (Element child)
		{
			base.OnChildAdded (child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved (Element child)
		{
			base.OnChildRemoved (child);
			child.BindingContext = null;
		}
	}
}
