﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using EduApp.Core;
using ReactiveUI;
using System.Reactive.Linq;
using Xamarin.Forms;

namespace EduApp
{
	public partial class S6ButtonView : Grid
	{
		public S6ButtonView ()
		{
			InitializeComponent ();
		}

		public static readonly BindableProperty StatoRispostaProperty = BindableProperty.Create (nameof (StatoRisposta), typeof (AnswerState), typeof (S6ButtonView), AnswerState.None, BindingMode.OneWay);

		public AnswerState StatoRisposta {
			get { return (AnswerState)this.GetValue (StatoRispostaProperty); }
			set { this.SetValue (StatoRispostaProperty, value); }
		}

		public static readonly BindableProperty MeasureTitleProperty = BindableProperty.Create (nameof (MeasureTitle), typeof (string), typeof (S6ButtonView), string.Empty, BindingMode.OneWay);

		public string MeasureTitle {
			get { return (string)this.GetValue (MeasureTitleProperty); }
			set { this.SetValue (MeasureTitleProperty, value); }
		}

		public static readonly BindableProperty ButtonIconProperty = BindableProperty.Create (nameof (ButtonIcon), typeof (string), typeof (S6ButtonView), string.Empty, BindingMode.OneWay);

		public string ButtonIcon {
			get { return (string)this.GetValue (ButtonIconProperty); }
			set { this.SetValue (ButtonIconProperty, value); }
		}

		public static readonly BindableProperty ResponseTextProperty = BindableProperty.Create (nameof (ResponseText), typeof (string), typeof (S6ButtonView), string.Empty, BindingMode.TwoWay );

		public string ResponseText {
			get { return (string)this.GetValue (ResponseTextProperty); }
			set { this.SetValue (ResponseTextProperty, value); }
		}

		public static readonly BindableProperty IsEditableProperty = BindableProperty.Create (nameof (IsEditable), typeof (bool), typeof (S6ButtonView), true, BindingMode.OneWay);

		public bool IsEditable {
			get { return (bool)this.GetValue (IsEditableProperty); }
			set { this.SetValue (IsEditableProperty, value); }
		}

		public static readonly BindableProperty IsIconVisibleProperty = BindableProperty.Create (nameof (IsIconVisible), typeof (bool), typeof (S6ButtonView), true, BindingMode.OneWay);

		public bool IsIconVisible {
			get { return (bool)this.GetValue (IsIconVisibleProperty); }
			set { this.SetValue (IsIconVisibleProperty, value); }
		}

		protected override void OnChildAdded (Element child)
		{
			base.OnChildAdded (child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved (Element child)
		{
			base.OnChildRemoved (child);
			child.BindingContext = null;
		}
	}
}
