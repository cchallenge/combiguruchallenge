﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

/*
 * TODO in attesa di capire dal cliente cosa vuol fare con il bottone info sul campo password!
 * 
 */

namespace EduApp
{
	public partial class EntryWithTitleAndInfo : StackLayout
	{

		public Action OnEntryCompleted { get; set; }

		public EntryWithTitleAndInfo ()
		{
			InitializeComponent ();
			this.entry.Completed += OnCompleted;
		}

		void OnCompleted (object sender, EventArgs e)
		{
			OnEntryCompleted?.Invoke ();
		}

		public void FocusEntry ()
		{
			this.entry.Focus ();
		}

		public static readonly BindableProperty TitleEntryTextProperty = BindableProperty.Create (
																						 propertyName: "TitleEntryText",
																						 returnType: typeof (string),
																						 declaringType: typeof (EntryWithTitleAndInfo),
																						 defaultValue: String.Empty,
																						 defaultBindingMode: BindingMode.OneWay
																						);

		public string TitleEntryText {
			get { return (string)GetValue (TitleEntryTextProperty); }
			set { SetValue (TitleEntryTextProperty, value); }
		}

		public static readonly BindableProperty InfoPasswordCommandProperty = BindableProperty.Create (
																							propertyName: "InfoPasswordCommand",
																							returnType: typeof(ICommand),
																							declaringType: typeof(EntryWithTitleAndInfo),
																							defaultValue: null,
																							defaultBindingMode: BindingMode.OneWay
																						);

		public ICommand InfoPasswordCommand {
			get { return (ICommand)GetValue (InfoPasswordCommandProperty); }
			set { SetValue (InfoPasswordCommandProperty, value); }
		}

		public static readonly BindableProperty ValueEntryTextProperty = BindableProperty.Create (
																						 propertyName: "ValueEntryText",
																						 returnType: typeof (string),
																						 declaringType: typeof (EntryWithTitleAndInfo),
																						 defaultValue: String.Empty,
																						 defaultBindingMode: BindingMode.TwoWay
																						);

		public string ValueEntryText {
			get { return (string)GetValue (ValueEntryTextProperty); }
			set { SetValue (ValueEntryTextProperty, value); }
		}

		public static readonly BindableProperty ReturnTypeProperty = BindableProperty.Create (
																	propertyName: "ReturnType",
																 	returnType: typeof (ReturnKeyTypes),
																 	declaringType: typeof (EntryWithTitle),
																	defaultValue: ReturnKeyTypes.Done
		);

		public ReturnKeyTypes ReturnType {
			get { return (ReturnKeyTypes)GetValue (ReturnTypeProperty); }
			set { SetValue (ReturnTypeProperty, value); }
		}

		public static readonly BindableProperty IsPwdProperty = BindableProperty.Create (
																						 propertyName: "IsPwd",
																						 returnType: typeof (bool),
																						 declaringType: typeof (EntryWithTitleAndInfo),
																						 defaultValue: false,
																						 defaultBindingMode: BindingMode.OneWay
																						);

		public bool IsPwd {
			get { return (bool)GetValue (IsPwdProperty); }
			set { SetValue (IsPwdProperty, value); }
		}

		protected override void OnChildAdded (Element child)
		{
			base.OnChildAdded (child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved (Element child)
		{
			base.OnChildRemoved (child);
			child.BindingContext = null;
		}
	}
}
