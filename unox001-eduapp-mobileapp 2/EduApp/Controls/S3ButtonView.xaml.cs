﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	public partial class S3ButtonView : CornerRadiusContainer
	{
		void Handle_Tapped(object sender, System.EventArgs e)
		{
			if(TapCommand.CanExecute(null))
				TapCommand.Execute(null);
		}

		public ICommand TapCommand { get; set; }

		public static readonly BindableProperty CurrentPositionProperty = BindableProperty.Create(nameof(CurrentPosition), typeof(int), typeof(S3ButtonView), 0, BindingMode.OneWay);

		public int CurrentPosition
		{
			get { return (int)this.GetValue(CurrentPositionProperty); }
			set { this.SetValue(CurrentPositionProperty, value); }
		}

		public static readonly BindableProperty ImageSourceProperty = BindableProperty.Create(nameof(ImageSource), typeof(string), typeof(S3ButtonView), String.Empty, BindingMode.OneWay);

		public string ImageSource
		{
			get { return (string)this.GetValue(ImageSourceProperty); }
			set { this.SetValue(ImageSourceProperty, value); }
		}

		public static readonly BindableProperty CorrectPositionProperty = BindableProperty.Create(nameof(CorrectPosition), typeof(int), typeof(S3ButtonView), 0, BindingMode.OneWay);

		public int CorrectPosition
		{
			get { return (int)this.GetValue(CorrectPositionProperty); }
			set { this.SetValue(CorrectPositionProperty, value); }
		}

		public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(S3ButtonView), String.Empty, BindingMode.OneWay);

		public string Title
		{
			get { return (string)this.GetValue(TitleProperty); }
			set { this.SetValue(TitleProperty, value); }
		}

		public S3ButtonView()
		{
			InitializeComponent();
		}

		protected override void OnChildAdded(Element child)
		{
			base.OnChildAdded(child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved(Element child)
		{
			base.OnChildRemoved(child);
			child.BindingContext = null;
		}
	}
}
