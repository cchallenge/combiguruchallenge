﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace EduApp
{
	public partial class WalkThroughImage : Grid
	{
		public static readonly BindableProperty BkgImageProperty = BindableProperty.Create(nameof(BkgImage), typeof(string), typeof(WalkThroughImage), "", BindingMode.OneWay);

		public string BkgImage
		{
			get { return (string)this.GetValue(BkgImageProperty); }
			set { this.SetValue(BkgImageProperty, value); }
		}

		public static readonly BindableProperty TitleImageProperty = BindableProperty.Create(nameof(TitleImage), typeof(string), typeof(WalkThroughImage), "", BindingMode.OneWay);

		public string TitleImage
		{
			get { return (string)this.GetValue(TitleImageProperty); }
			set { this.SetValue(TitleImageProperty, value); }
		}

		public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(WalkThroughImage), string.Empty, BindingMode.OneWay);

		public string Title
		{
			get { return (string)this.GetValue(TitleProperty); }
			set { this.SetValue(TitleProperty, value); }
		}

		public static readonly BindableProperty SubtitleProperty = BindableProperty.Create(nameof(Subtitle), typeof(string), typeof(WalkThroughImage), string.Empty, BindingMode.OneWay);

		public string Subtitle
		{
			get { return (string)this.GetValue(SubtitleProperty); }
			set { this.SetValue(SubtitleProperty, value); }
		}

		public static readonly BindableProperty ShowDoneProperty = BindableProperty.Create(nameof(ShowDone), typeof(bool), typeof(WalkThroughImage), false, BindingMode.OneWay);

		public bool ShowDone
		{
			get { return (bool)this.GetValue(ShowDoneProperty); }
			set { this.SetValue(ShowDoneProperty, value); }
		}

		public static readonly BindableProperty DoneCommandProperty = BindableProperty.Create(nameof(DoneCommand), typeof(ICommand), typeof(WalkThroughImage), null, BindingMode.OneWay);

		public ICommand DoneCommand
		{
			get { return (ICommand)this.GetValue(DoneCommandProperty); }
			set { this.SetValue(DoneCommandProperty, value); }
		}

		public WalkThroughImage()
		{
			InitializeComponent();
		}

		protected override void OnChildAdded(Element child)
		{
			base.OnChildAdded(child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved(Element child)
		{
			base.OnChildRemoved(child);
			child.BindingContext = null;
		}
	}
}
