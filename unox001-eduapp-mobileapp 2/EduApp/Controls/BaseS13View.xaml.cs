﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace EduApp
{
	public partial class BaseS13View : StackLayout
	{
		public BaseS13View ()
		{
			InitializeComponent ();
		}

		public static readonly BindableProperty BaseImageProperty = BindableProperty.Create (nameof (BaseImage), typeof (string), typeof (BaseS13View), string.Empty, BindingMode.OneWay);

		public string BaseImage {
			get { return (string)this.GetValue (BaseImageProperty); }
			set { this.SetValue (BaseImageProperty, value); }
		}

		public static readonly BindableProperty BaseMeasureProperty = BindableProperty.Create (nameof (BaseMeasure), typeof (string), typeof (BaseS13View), string.Empty, BindingMode.OneWay);

		public string BaseMeasure {
			get { return (string)this.GetValue (BaseMeasureProperty); }
			set { this.SetValue (BaseMeasureProperty, value); }
		}

		public static readonly BindableProperty BaseTextProperty = BindableProperty.Create (nameof (BaseText), typeof (string), typeof (BaseS13View), string.Empty, BindingMode.OneWay);

		public string BaseText {
			get { return (string)this.GetValue (BaseTextProperty); }
			set { this.SetValue (BaseTextProperty, value); }
		}

		protected override void OnChildAdded (Element child)
		{
			base.OnChildAdded (child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved (Element child)
		{
			base.OnChildRemoved (child);
			child.BindingContext = null;
		}
	}
}
