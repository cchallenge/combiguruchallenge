﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	public partial class FirstLearningCellView : ViewUserControl
	{
		public FirstLearningCellViewModel FirstLearningCellVM { get { return (ViewModel as FirstLearningCellViewModel); } }
		
		public FirstLearningCellView ()
		{
			InitializeComponent ();
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();
			this.OnPropertyChanged (nameof (FirstLearningCellVM));
		}
	}
}
