﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EduApp.Core;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	public abstract class ComboBaseView : ContentView
	{
		public ComboViewModel ComboVM { get { return (ViewModel as ComboViewModel); } }

		protected abstract Picker Picker { get; }
		public ComboBaseView()
		{
			this.WhenAnyValue(v => v.ComboVM.Collection).Subscribe((obj) => PopulatePicker(obj));
			this.WhenAnyValue(v => v.ComboVM.SelectedItem).Subscribe((obj) => SelectedIndex(obj));
		}

		void SelectedIndex(int index)
		{
			if (index > -1 && index < Picker.Items.Count)
			{
				Picker.SelectedIndex = index;
			}
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			this.OnPropertyChanged(nameof(ComboVM));
		}

		public virtual ReactiveObject ViewModel
		{
			get { return this.BindingContext as ReactiveObject; }
			set { this.BindingContext = value; }
		}

		void PopulatePicker(IEnumerable<string> items)
		{
			Picker.Items.Clear();
			if (items != null)
			{
				foreach (var item in items)
				{
					Picker.Items.Add(item);
				}
			}
		}
	}
}

