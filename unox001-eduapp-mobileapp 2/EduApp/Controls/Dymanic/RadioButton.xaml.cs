﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace EduApp
{
	public partial class RadioButton : Grid
	{
		public RadioButton()
		{
			InitializeComponent();
		}

		public static readonly BindableProperty TitleProperty = BindableProperty.Create(
																								 propertyName: "Title",
																								 returnType: typeof(string),
																								 declaringType: typeof(RadioButton),
																								 defaultValue: String.Empty,
																								 defaultBindingMode: BindingMode.OneWay
																								);
		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set { SetValue(TitleProperty, value); }
		}

		public static readonly BindableProperty ComProperty = BindableProperty.Create(
																									propertyName: "Com",
																									returnType: typeof(ICommand),
																									declaringType: typeof(RadioButton),
																									defaultValue: null,
																									defaultBindingMode: BindingMode.OneWay
																									);
		public ICommand Com
		{
			get { return (ICommand)GetValue(ComProperty); }
			set { SetValue(ComProperty, value); }
		}

		public static readonly BindableProperty ComParamProperty = BindableProperty.Create(
																									propertyName: "ComParam",
																									returnType: typeof(object),
																									declaringType: typeof(RadioButton),
																									defaultValue: null,
																									defaultBindingMode: BindingMode.OneWay
																									);

		public object ComParam
		{
			get { return (object)GetValue(ComParamProperty); }
			set { SetValue(ComParamProperty, value); }
		}


		public static readonly BindableProperty SelectedProperty = BindableProperty.Create(
																									propertyName: "Selected",
																									returnType: typeof(bool),
																									declaringType: typeof(RadioButton),
																									defaultValue: false,
																									defaultBindingMode: BindingMode.OneWay
																									);

		public bool Selected
		{
			get { return (bool)GetValue(SelectedProperty); }
			set { SetValue(SelectedProperty, value); }
		}

		protected override void OnChildAdded(Element child)
		{
			base.OnChildAdded(child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved(Element child)
		{
			base.OnChildRemoved(child);
			child.BindingContext = null;
		}
	}
}
