﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using AppCore.Logging;
using EduApp.Core;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	public partial class SezioneView : ViewUserControl
	{
		public static ImageSource SezioneExpandedImage { get; set; } = ImageSource.FromResource ("EduApp.DynImages.up_arrow.png");
		public static ImageSource SezioneNotExpandedImage { get; set; } = ImageSource.FromResource ("EduApp.DynImages.down_arrow.png");
		//ILog logger = LogProvider.GetLogger (typeof (SezioneView));
		public SezioneView()
		{
			InitializeComponent();
			sezioneImage.Source = SezioneExpandedImage;


			this.WhenAnyValue (v => v.SezioneVM.IsExpanded)
				.Subscribe ((obj) => {
						sezioneImage.Source = obj ? SezioneExpandedImage : SezioneNotExpandedImage;
					}
				);

			//this.AppendDecorator (backgroundColor: "#FFFFFF", margin:0);
		}

		public SezioneViewModel SezioneVM { get; private set; }

		protected async override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			if (SezioneVM != this.ViewModel as SezioneViewModel) {
				if (this.ViewModel != null) {
					SezioneVM = this.ViewModel as SezioneViewModel;
					this.OnPropertyChanged (nameof (SezioneVM));
				}
			}
		}
	}
}
