﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	public partial class ComboSelectedWithImageView : ViewUserControl
	{
		public static ImageSource Image { get; set; } = ImageSource.FromResource ("EduApp.DynImages.down_arrow.png");
		public ComboSelectedWithImageViewModel ComboVM { get { return (ViewModel as ComboSelectedWithImageViewModel); } }

		public ComboSelectedWithImageView ()
		{
			InitializeComponent ();
			this.WhenAnyValue (v => v.ComboVM.Collection).Subscribe ((obj) => PopulatePicker (obj));
			this.WhenAnyValue (v => v.ComboVM.SelectedItem).Subscribe ((obj) => SelectedIndex (obj));
			image.Source = Image;
			picker.TextAlignRight = false;
			//this.AppendDecorator (backgroundColor: "#FFFFFF", margin: 0);
		}

		void SelectedIndex (int index)
		{
			if (index > -1 && index < this.picker.Items.Count) {
				this.picker.SelectedIndex = index;
			}
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();
			this.OnPropertyChanged (nameof (ComboVM));
		}

		void PopulatePicker (IEnumerable<string> items)
		{
			this.picker.Items.Clear ();
			if (items != null) {
				foreach (var item in items) {
					this.picker.Items.Add (item);
				}
			}
		}
	}
}
