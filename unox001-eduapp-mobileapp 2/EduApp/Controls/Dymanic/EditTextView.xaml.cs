﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	public partial class EditTextView : ViewUserControl
	{
		public EditTextViewModel EditVM { get { return (ViewModel as EditTextViewModel); } }

		public EditTextView()
		{
			InitializeComponent();
			//AppendDecorator ();
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			if (EditVM != null) {
				KeyboardType keyboardtype = EditVM.KeyboardType;
				entry.Keyboard = _Keyboard (keyboardtype);
			}
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();
			this.OnPropertyChanged (nameof (EditVM));
		}

		Keyboard _Keyboard (KeyboardType kbt)
		{
			switch (kbt) {
			case KeyboardType.Text:
				return Keyboard.Text;
			case KeyboardType.Chat:
				return Keyboard.Chat;
			case KeyboardType.Url:
				return Keyboard.Url;
			case KeyboardType.Email:
				return Keyboard.Email;
			case KeyboardType.Telephone:
				return Keyboard.Telephone;
			case KeyboardType.Numeric:
				return Keyboard.Numeric;
			default:
				return Keyboard.Default;
			}
		}
	}
}
