﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	public partial class PhotoView : ViewUserControl
	{
		public PhotoViewModel PhotoVM { get { return (ViewModel as PhotoViewModel); } }

		public PhotoView ()
		{
			InitializeComponent ();
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();
			this.OnPropertyChanged (nameof (PhotoVM));
		}
	}
}
