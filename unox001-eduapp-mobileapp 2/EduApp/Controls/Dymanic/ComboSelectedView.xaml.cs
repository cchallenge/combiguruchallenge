﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	public partial class ComboSelectedView : ViewUserControl
	{
		public ComboSelectedViewModel ComboVM { get { return (ViewModel as ComboSelectedViewModel); } }

		public ComboSelectedView()
		{
			System.Diagnostics.Debug.WriteLine("Creo ComboSelectedView");
			InitializeComponent();
			this.WhenAnyValue(v => v.ComboVM.Collection).Subscribe((obj) => PopulatePicker(obj));
			this.WhenAnyValue(v => v.ComboVM.SelectedItem).Subscribe((obj) => SelectedIndex(obj));
			picker.TextAlignRight = true;

			//picker.SelectedIndexChanged += Picker_SelectedIndexChanged;

			//this.AppendDecorator ();
		}

		void Picker_SelectedIndexChanged(object sender, EventArgs e)
		{
            if (!populating && ComboVM != null && ComboVM.CommandOnSelectedItem != null)
				ComboVM.CommandOnSelectedItem.Execute(null);
		}

		int lastIndex = 0;
		void SelectedIndex(int index)
		{
			if (lastIndex != index)
			{
				if (index > -1 && index < this.picker.Items.Count)
				{
					lastIndex = index;
					this.picker.SelectedIndex = index;
				}
			}
		}

		protected override void OnBindingContextChanged()
		{
			picker.SelectedIndexChanged -= Picker_SelectedIndexChanged;
			base.OnBindingContextChanged();
			this.OnPropertyChanged(nameof(ComboVM));
			picker.SelectedIndexChanged += Picker_SelectedIndexChanged;
		}

        bool populating = false;
		void PopulatePicker(IEnumerable<string> items)
		{
            populating = true;
			this.picker.Items.Clear();
			if (items != null)
			{
				foreach (var item in items)
				{
					this.picker.Items.Add(item);
				}

                var selected = (this.ComboVM?.SelectedItem ?? -1);
                if (selected >= 0 && selected < this.picker.Items.Count) {
                    this.picker.SelectedIndex = selected;
                }

                lastIndex = selected;
			}

            populating = false;
		}
	}
}
