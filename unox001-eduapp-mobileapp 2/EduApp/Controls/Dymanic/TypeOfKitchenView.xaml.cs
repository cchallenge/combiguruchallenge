﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace EduApp
{
	public partial class TypeOfKitchenView : BaseRadioButtonView
	{
		protected override Grid Container { get { return this.container;} }
		public TypeOfKitchenView()
		{
			InitializeComponent();
		}
	}
}
