﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	public partial class TitleLabelView : ViewUserControl
	{
		public TitleLabelViewModel TitleLabelVM { get { return (ViewModel as TitleLabelViewModel); } }

		public TitleLabelView ()
		{
			InitializeComponent ();
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();
			this.OnPropertyChanged (nameof (TitleLabelVM));
		}
	}
}
