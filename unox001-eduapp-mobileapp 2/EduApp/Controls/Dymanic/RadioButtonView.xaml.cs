﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduApp.Core;
using MvvmHelpers;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	public partial class RadioButtonView : BaseRadioButtonView
	{
		protected override Grid Container
		{
			get { return this.container; }
		}

		public RadioButtonView()
		{
			InitializeComponent();
			//this.AppendDecorator (margin: 0, heightRequest:-1);
		}
	}

	public abstract class BaseRadioButtonView : ViewUserControl
	{
		protected abstract Grid Container { get; }

		public BaseRadioButtonView()
		{
			this.WhenAnyValue(v => v.RadioVM.Columns).Subscribe(i => columns = i);
			this.WhenAnyValue(v => v.RadioVM.Items).Subscribe(i => HandleItem(i));
		}

		int columns = 0;
		void HandleItem(ObservableRangeCollection<RadioModel> items)
		{
			int i = 0;
			int y = 0;

			foreach (var v in this.Container.Children.OfType<RadioButton> ().ToArray ()) {
				this.Container.Children.Remove (v);
			}
			foreach (var elem in items)
			{
				var btn = new RadioButton()
				{
					Title = elem.Text,
					Com = RadioVM.ItemCommand,
					ComParam = elem.Key
				};
				btn.BindingContext = elem;
				btn.SetBinding(RadioButton.SelectedProperty, new Binding("Selected"));

				this.Container.Children.Add(btn, i, y);

				if (i < (columns - 1))
				{
					i++;
				}
				else {
					i = 0;
					y++;
				}
			}
		}

		public RadioButtonViewModel RadioVM { get { return (ViewModel as RadioButtonViewModel); } }

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			this.OnPropertyChanged(nameof(RadioVM));
		}
	}
}
