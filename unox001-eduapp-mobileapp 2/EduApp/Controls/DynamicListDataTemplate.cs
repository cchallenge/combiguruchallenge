﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	
	public class DynamicListDataTemplate : DataTemplateSelector
	{


		static DynamicListDataTemplate()
		{
			if (_templates == null)
				_templates = new SortedDictionary<string, DataTemplate>();
		}

		static IDictionary<string, DataTemplate> _templates;

		public static void RegisterTemplateFor<T>() where T : BaseCellViewModel { 
			lock (_templates)
			{
				var view = ViewFactory.Instance.GetViewTypeForModel(typeof(T));
				if (!_templates.ContainsKey(view.FullName))
				{
					var template = new DataTemplate(view);
					_templates[view.FullName] = template;
				}
			}
		}

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			var itemVm = item as BaseCellViewModel;
			if (itemVm == null)
				return null;
				
			lock (_templates)
			{
				var view = ViewFactory.Instance.GetViewTypeForModel(itemVm);

				if (!_templates.ContainsKey(view.FullName))
				{
					var template = new DataTemplate(view);
					_templates[view.FullName] = template;
				}

				return _templates[view.FullName];
			}
		}

	}
}
