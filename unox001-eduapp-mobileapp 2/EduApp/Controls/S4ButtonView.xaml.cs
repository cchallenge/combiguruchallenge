﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	public partial class S4ButtonView : Grid
	{
		public static readonly BindableProperty ViewCommandProperty = BindableProperty.Create(nameof(ViewCommand), typeof(ICommand), typeof(S4ButtonView), null, BindingMode.OneWay);

		public ICommand ViewCommand
		{
			get { return (ICommand)this.GetValue(ViewCommandProperty); }
			set { this.SetValue(ViewCommandProperty, value); }
		}

		public static readonly BindableProperty StatoRispostaProperty = BindableProperty.Create(nameof(StatoRisposta), typeof(AnswerState), typeof(S4ButtonView), AnswerState.None, BindingMode.OneWay);

		public AnswerState StatoRisposta
		{
			get { return (AnswerState)this.GetValue(StatoRispostaProperty); }
			set { this.SetValue(StatoRispostaProperty, value); }
		}

		public static readonly BindableProperty ImageSourceProperty = BindableProperty.Create(nameof(ImageSource), typeof(string), typeof(S4ButtonView), String.Empty, BindingMode.OneWay);

		public string ImageSource
		{
			get { return (string)this.GetValue(ImageSourceProperty); }
			set { this.SetValue(ImageSourceProperty, value); }
		}

		public S4ButtonView()
		{
			InitializeComponent();
		}

		protected override void OnChildAdded(Element child)
		{
			base.OnChildAdded(child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved(Element child)
		{
			base.OnChildRemoved(child);
			child.BindingContext = null;
		}
	}
}
