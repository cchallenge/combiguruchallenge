﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	public partial class S13ButtonView : ContentView
	{

		void Handle_Tapped (object sender, System.EventArgs e)
		{
			if (TapCommand != null && TapCommand.CanExecute (null))
				TapCommand.Execute (null);
		}

		public ICommand TapCommand { get; set; }

		public S13ButtonView ()
		{
			InitializeComponent ();
		}

		public static readonly BindableProperty ItemsProperty = BindableProperty.Create (nameof (Items), typeof (List<SBaseModel>), typeof (S13ButtonView), null, BindingMode.OneWay);

		public List<SBaseModel> Items {
			get { return (List<SBaseModel>)this.GetValue (ItemsProperty); }
			set { this.SetValue (ItemsProperty, value); }
		}

		public static readonly BindableProperty StatoRispostaProperty = BindableProperty.Create (nameof (StatoRisposta), typeof (AnswerState), typeof (S13ButtonView), AnswerState.None, BindingMode.OneWay);

		public AnswerState StatoRisposta {
			get { return (AnswerState)this.GetValue (StatoRispostaProperty); }
			set { this.SetValue (StatoRispostaProperty, value); }
		}

		protected override void OnChildAdded (Element child)
		{
			base.OnChildAdded (child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved (Element child)
		{
			base.OnChildRemoved (child);
			child.BindingContext = null;
		}
	}
}
