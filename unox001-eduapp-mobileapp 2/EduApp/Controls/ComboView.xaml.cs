﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace EduApp
{
	public partial class ComboView : ComboBaseView
	{
		public ComboView()
		{
			InitializeComponent();
		}

		protected override Picker Picker
		{
			get { return this.picker; }
		}
	}
}
