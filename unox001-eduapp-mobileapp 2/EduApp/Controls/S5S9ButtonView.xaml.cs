﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	public partial class S5S9ButtonView : ContentView
	{
		public S5S9ButtonView()
		{
			InitializeComponent();
		}

		public static readonly BindableProperty AnswerIdProperty = BindableProperty.Create(nameof(AnswerId), typeof(string), typeof(S5S9ButtonView), string.Empty, BindingMode.OneWay);

		public string AnswerId
		{
			get { return (string)this.GetValue(AnswerIdProperty); }
			set { this.SetValue(AnswerIdProperty, value); }
		}


		public static readonly BindableProperty ViewCommandProperty = BindableProperty.Create(nameof(ViewCommand), typeof(ICommand), typeof(S5S9ButtonView), null, BindingMode.OneWay);

		public ICommand ViewCommand
		{
			get { return (ICommand)this.GetValue(ViewCommandProperty); }
			set { this.SetValue(ViewCommandProperty, value); }
		}

		public static readonly BindableProperty StatoRispostaProperty = BindableProperty.Create(nameof(StatoRisposta), typeof(AnswerState), typeof(S5S9ButtonView), AnswerState.None, BindingMode.OneWay);

		public AnswerState StatoRisposta
		{
			get { return (AnswerState)this.GetValue(StatoRispostaProperty); }
			set { this.SetValue(StatoRispostaProperty, value); }
		}

		public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(S5S9ButtonView), string.Empty, BindingMode.OneWay);

		public string Title
		{
			get { return (string)this.GetValue(TitleProperty); }
			set { this.SetValue(TitleProperty, value); }
		}

		protected override void OnChildAdded(Element child)
		{
			base.OnChildAdded(child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved(Element child)
		{
			base.OnChildRemoved(child);
			child.BindingContext = null;
		}


	}
}