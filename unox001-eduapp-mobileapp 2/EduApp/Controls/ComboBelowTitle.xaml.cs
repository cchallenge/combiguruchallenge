﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace EduApp
{
	public partial class ComboBelowTitle : ComboBaseView
	{
		public ComboBelowTitle ()
		{
			InitializeComponent ();
		}

		protected override Picker Picker {
			get { return this.picker; }
		}
	}
}
