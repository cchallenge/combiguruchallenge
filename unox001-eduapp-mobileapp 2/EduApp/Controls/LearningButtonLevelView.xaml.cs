﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace EduApp
{
	public partial class LearningButtonLevelView : Grid
	{
		public static readonly BindableProperty SbiadimentoProperty = BindableProperty.Create(nameof(Sbiadimento), typeof(float), typeof(CarouselIndicator), 1.0f, BindingMode.OneWay);

		public float Sbiadimento
		{
			get { return (float)this.GetValue(SbiadimentoProperty); }
			set { this.SetValue(SbiadimentoProperty, value); }
		}

		public static readonly BindableProperty ProgressProperty = BindableProperty.Create(nameof(Progress), typeof(float), typeof(CarouselIndicator), 0.0f, BindingMode.OneWay,
		                                                                                   propertyChanged:OnProgressChange);

		public float Progress
		{
			get { return (float)this.GetValue(ProgressProperty); }
			set { this.SetValue(ProgressProperty, value); }
		}

		public static readonly BindableProperty ImageSourceProperty = BindableProperty.Create(nameof(ImageSource), typeof(string), typeof(CarouselIndicator), String.Empty, BindingMode.OneWay);

		public string ImageSource
		{
			get { return (string)this.GetValue(ImageSourceProperty); }
			set { this.SetValue(ImageSourceProperty, value); }
		}

		public LearningButtonLevelView()
		{
			InitializeComponent();
		}

		static void OnProgressChange (BindableObject bindable, object oldValue, object newValue)
		{
            var obj = (LearningButtonLevelView)bindable;
            if ((float)newValue > 0)
            {
                var cHeight = obj.HeightRequest;
                if (cHeight > 0)
                {
                    var nHeight = ((float)newValue * cHeight) / 100;
                    obj.imageContainer.Height = new GridLength(nHeight, GridUnitType.Absolute);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine($"Altezza inferiore a 0 per percentuale {(float)newValue }");
                }
            }
            else
            {
                obj.imageContainer.Height = 0;
            }
        }

		//protected override void LayoutChildren(double x, double y, double width, double height)
		//{
		//	OnProgressChange(this, 0f, Progress);
		//	base.LayoutChildren(x, y, width, height);
		//}

		protected override void OnChildAdded(Element child)
		{
			base.OnChildAdded(child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved(Element child)
		{
			base.OnChildRemoved(child);
			child.BindingContext = null;
		}
	}
}
