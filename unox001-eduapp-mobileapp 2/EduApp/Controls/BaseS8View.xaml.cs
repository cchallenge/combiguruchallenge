﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace EduApp
{
	public partial class BaseS8View : StackLayout
	{
		public BaseS8View ()
		{
			InitializeComponent ();
		}

		public static readonly BindableProperty BaseImageProperty = BindableProperty.Create (nameof (BaseImage), typeof (string), typeof (BaseS8View), string.Empty, BindingMode.OneWay);

		public string BaseImage {
			get { return (string)this.GetValue (BaseImageProperty); }
			set { this.SetValue (BaseImageProperty, value); }
		}

		public static readonly BindableProperty BaseEntryProperty = BindableProperty.Create (nameof (BaseEntry), typeof (string), typeof (BaseS8View), string.Empty, BindingMode.TwoWay);

		public string BaseEntry {
			get { return (string)this.GetValue (BaseEntryProperty); }
			set { this.SetValue (BaseEntryProperty, value); }
		}

		public static readonly BindableProperty BaseMeasureProperty = BindableProperty.Create (nameof (BaseMeasure), typeof (string), typeof (BaseS8View), string.Empty, BindingMode.OneWay);

		public string BaseMeasure {
			get { return (string)this.GetValue (BaseMeasureProperty); }
			set { this.SetValue (BaseMeasureProperty, value); }
		}

		public static readonly BindableProperty IsEnabledProperty = BindableProperty.Create (nameof (IsEnabled), typeof (bool), typeof (BaseS8View), true, BindingMode.OneWay);

		public bool IsEnabled {
			get { return (bool)this.GetValue (IsEnabledProperty); }
			set { this.SetValue (IsEnabledProperty, value); }
		}

		protected override void OnChildAdded (Element child)
		{
			base.OnChildAdded (child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved (Element child)
		{
			base.OnChildRemoved (child);
			child.BindingContext = null;
		}
	}
}
