﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	public partial class S8ButtonView : StackLayout
	{
		public S8ButtonView ()
		{
			InitializeComponent ();
		}

		public static readonly BindableProperty ItemsProperty = BindableProperty.Create (nameof (Items), typeof (List<S8Response>), typeof (S8ButtonView), null, BindingMode.OneWay);

		public List<S8Response> Items {
			get { return (List<S8Response>)this.GetValue (ItemsProperty); }
			set { this.SetValue (ItemsProperty, value); }
		}

		public static readonly BindableProperty StatoRispostaProperty = BindableProperty.Create (nameof (StatoRisposta), typeof (AnswerState), typeof (S8ButtonView), AnswerState.None, BindingMode.OneWay);

		public AnswerState StatoRisposta {
			get { return (AnswerState)this.GetValue (StatoRispostaProperty); }
			set { this.SetValue (StatoRispostaProperty, value); }
		}

		public static readonly BindableProperty IsEditableProperty = BindableProperty.Create (nameof (IsEditable), typeof (bool), typeof (S8ButtonView), true, BindingMode.OneWay);

		public bool IsEditable {
			get { return (bool)this.GetValue (IsEditableProperty); }
			set { this.SetValue (IsEditableProperty, value); }
		}

		protected override void OnChildAdded (Element child)
		{
			base.OnChildAdded (child);
			child.BindingContext = this;
		}

		protected override void OnChildRemoved (Element child)
		{
			base.OnChildRemoved (child);
			child.BindingContext = null;
		}
	}
}
