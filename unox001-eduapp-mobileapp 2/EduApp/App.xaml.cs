using System;
using System.Reflection;
using System.Threading.Tasks;
using AppCore;
using AppCore.Interfaces;
using EduApp.Core;
using Nostromo;
using Nostromo.Interfaces;
using Nostromo.Navigation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.GoogleAnalytics;

#if !DEBUG
//[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
#endif
namespace EduApp
{
	public class UnoxDynViewFactory : ViewFactory
	{
		public override void RegisterDefaultView()
		{
			this.Add<SezioneViewModel, SezioneView>();
			this.Add<EditTextViewModel, EditTextView>();
			this.Add<RadioButtonViewModel, RadioButtonView>();
			this.Add<TypeOfKitchenViewModel, TypeOfKitchenView>();
			this.Add<PhotoViewModel, PhotoView>();
			this.Add<TitleLabelViewModel, TitleLabelView>();
			this.Add<ComboSelectedViewModel, ComboSelectedView>();
			this.Add<ComboSelectedWithImageViewModel, ComboSelectedWithImageView>();
			this.Add<FirstLearningCellViewModel, FirstLearningCellView>();
			this.Add<LearningCellViewModel, LearningCellView>();
			this.Add<LastLearningCellViewModel, LastLeariningCellView>();
            // _ext_  vvv
            this.Add<FirstLeaderboardCellViewModel, FirstLeaderboardCellView>();
            this.Add<LeaderboardCellViewModel, LeaderboardCellView>();
			this.Add<LastLeaderboardCellViewModel, LastLeaderboardCellView>();
            this.Add<LeftChallengesListCellViewModel, LeftChallengesListCellView>();
            this.Add<RightChallengesListCellViewModel, RightChallengesListCellView>();
            // _ext_  ^^^

            //Pre registrazione dei templates
            DynamicListDataTemplate.RegisterTemplateFor<SezioneViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<EditTextViewModel>();
            DynamicListDataTemplate.RegisterTemplateFor<RadioButtonViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<TypeOfKitchenViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<PhotoViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<TitleLabelViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<ComboSelectedViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<ComboSelectedWithImageViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<FirstLearningCellViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<LearningCellViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<LastLearningCellViewModel>();
            // _ext_  vvv
            DynamicListDataTemplate.RegisterTemplateFor<FirstLeaderboardCellViewModel>();
            DynamicListDataTemplate.RegisterTemplateFor<LeaderboardCellViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<LastLeaderboardCellViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<LeftChallengesListCellViewModel>();
			DynamicListDataTemplate.RegisterTemplateFor<RightChallengesListCellViewModel>();
            // _ext_  ^^^
        }
    }

	public partial class App : Application, IPageNavigator
	{
		NavigationPage mainPage;
		public App(IAppCoreBootstrapper bootstrapper, params Assembly[] dependencyAssemblies)
		{
			InitializeComponent();

            //https://github.com/KSemenenko/GoogleAnalyticsForXamarinForms
            GoogleAnalytics.Current.Config.TrackingId = "UA-92781429-1";
			//GoogleAnalytics.Current.Config.AppId = "AppID";
			GoogleAnalytics.Current.Config.AppName = "CombiGuru";
			GoogleAnalytics.Current.Config.AppVersion = "2.0.0.0";
			GoogleAnalytics.Current.InitTracker();

			if (Device.OS == TargetPlatform.iOS || Device.OS == TargetPlatform.Android)
			{
				Globals.AppLanguage = System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
			}

			bootstrapper
				.EnableDeferredAppContentModule ()
				.EnableAppContentModule ()
				.EnableApiRest (Globals.ApiKeyRest)
				.Register<IUnoxAppAuthModule, UnoxAccountModule> ()
				.Register<IAppAuthModule> ((x) => x.ResolveModule<IUnoxAppAuthModule> ())
				.Register<IAppCatalog, TranslateExtension> ()
				.Register<ICacheModule, CacheModule>()
				.Register<UnoxEduAppModule.UnoxAppEduModule>()
				.Register<UnoxAppModule>();

			var initializer = NostromoApp.Setup()
										 .UseAutofacContainer()
										 .UseGenericPlatformViewActivator()
										 .RegisterDependenciesInAssemblyOf<App>()
										 .RegisterViewsInAssemblyOf<App>();

			//Dovrebbe risolvere il problema del System.InvalidOperationException: non si può
			if(UnoxDynViewFactory.Instance == null)
				(new UnoxDynViewFactory()).Initialize();

			foreach (var assembly in dependencyAssemblies)
			{
				initializer.RegisterAssemblyDependencies (assembly);
				initializer.RegisterAssemblyViews (assembly);
			}

			initializer.Init();


			var vm = Build.ViewModel(() => new SyncPageViewModel(bootstrapper.MainContext));
			//var vm = Build.ViewModel (() => new FineLivelloPageViewModel (bootstrapper.MainContext));
			
			var firstView = Build.View<Page>(vm);
			firstView.BindingContext = vm;
			vm.Navigator = this;

			ReplaceMainPage(firstView);
		}

		void ReplaceMainPage(Page page)
		{
			if (mainPage != null)
				mainPage = null;

			var vm = ((page as ViewPage).ViewModel as IUnoxPageViewModel);
			mainPage = new NavigationPage(page)
			{
				BackgroundColor = Xamarin.Forms.Color.FromHex(vm.BkgColorHex),
				BarTextColor = Xamarin.Forms.Color.FromHex(vm.TextColorHex),
				BarBackgroundColor = Xamarin.Forms.Color.FromHex(vm.BarBkgColorHex)
			};

			MainPage = mainPage;
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}

		public async Task NavigateToAsync(IPageViewModel viewmodel, NavigatorSettings settings)
		{
			viewmodel.Navigator = this;
			var view = Build.View<Page>(viewmodel);

			view.BindingContext = viewmodel;

			if (settings.IsModal)
			{
				var modalPage = new ModalNavigator(view)
				{
					BarTextColor = Xamarin.Forms.Color.FromHex((viewmodel as IUnoxPageViewModel).TextColorHex),
					BarBackgroundColor = Xamarin.Forms.Color.FromHex((viewmodel as IUnoxPageViewModel).BarBkgColorHex),
					BackgroundColor = Xamarin.Forms.Color.FromHex((viewmodel as IUnoxPageViewModel).BkgColorHex)
				};
				viewmodel.Navigator = modalPage;

				await mainPage.Navigation.PushModalAsync(modalPage, true);
			}
			else {
				(MainPage as NavigationPage).BarTextColor = Xamarin.Forms.Color.FromHex((viewmodel as IUnoxPageViewModel).TextColorHex);
				(MainPage as NavigationPage).BarBackgroundColor = Xamarin.Forms.Color.FromHex((viewmodel as IUnoxPageViewModel).BarBkgColorHex);
				(MainPage as NavigationPage).BackgroundColor = Xamarin.Forms.Color.FromHex((viewmodel as IUnoxPageViewModel).BkgColorHex);

				await mainPage.PushAsync(view, settings.UsePlatformAnimation);
			}
		}

		public async Task BackAsync(NavigatorSettings settings)
		{
			var aps = settings as NavigationSettings;

			if (settings.IsModal)
				await mainPage.Navigation.PopModalAsync(true);
			else if ((bool)aps?.PopToRoot)
			{
				if (aps.NewRoot != null)
				{
					var firstView = Build.View<Page>(aps.NewRoot);
					firstView.BindingContext = aps.NewRoot;
					aps.NewRoot.Navigator = this;

					ReplaceMainPage(firstView);
				}
				else {
					await mainPage.PopToRootAsync(settings.UsePlatformAnimation);
				}
			}
			else {
				await mainPage.PopAsync(settings.UsePlatformAnimation);
			}

		}
	}

	public class ModalNavigator : NavigationPage, IPageNavigator
	{
		public ModalNavigator(Page root) : base(root)
		{

		}

		void ReplaceMainPage(Page page)
		{
			var vm = ((page as ViewPage).ViewModel as IUnoxPageViewModel);
			var root = this.Navigation.NavigationStack[0];
			this.Navigation.InsertPageBefore(page, root);
			this.Navigation.PopToRootAsync(false);
		}

		public async Task BackAsync(NavigatorSettings settings)
		{
			var aps = settings as NavigationSettings;

			if (settings.IsModal)
				await Navigation.PopModalAsync(true);
			else if ((bool)aps?.PopToRoot)
			{
				if (aps.NewRoot != null)
				{
					var firstView = Build.View<Page>(aps.NewRoot);
					firstView.BindingContext = aps.NewRoot;
					aps.NewRoot.Navigator = this;

					ReplaceMainPage(firstView);
				}
				else {
					await PopToRootAsync(settings.UsePlatformAnimation);
				}
			}
			else {
				await PopAsync(settings.UsePlatformAnimation);
			}
		}

		public async Task NavigateToAsync(IPageViewModel viewmodel, NavigatorSettings settings)
		{
			viewmodel.Navigator = this;
			var view = Build.View<Page>(viewmodel);

			BarTextColor = Xamarin.Forms.Color.FromHex((viewmodel as IUnoxPageViewModel).TextColorHex);
			BarBackgroundColor = Xamarin.Forms.Color.FromHex((viewmodel as IUnoxPageViewModel).BarBkgColorHex);
			BackgroundColor = Xamarin.Forms.Color.FromHex((viewmodel as IUnoxPageViewModel).BkgColorHex);

			view.BindingContext = viewmodel;

			if (settings.IsModal)
				await Navigation.PushModalAsync(view, settings.UsePlatformAnimation);
			else
				await PushAsync(view, settings.UsePlatformAnimation);
		}
	}

	/*
	 * per visualizzare nello XamlPreviewer i dati bindati
	*/
	public static class ViewModelLocator
	{
		public static IAppCoreContext Context { get; set; }
		static WelcomePageViewModel welcomeVM;
		static LoginPageViewModel loginVM;

		public static WelcomePageViewModel WelcomeViewModel => welcomeVM ?? (welcomeVM = new WelcomePageViewModel(Context));
		public static LoginPageViewModel LoginViewModel => loginVM ?? (loginVM = new LoginPageViewModel(Context));
	}
}
