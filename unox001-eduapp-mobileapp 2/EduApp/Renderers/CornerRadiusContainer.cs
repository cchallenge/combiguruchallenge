﻿using System;
using Xamarin.Forms;

namespace EduApp
{
	public class CornerRadiusContainer : Grid
	{
		public static readonly BindableProperty CornerRadiusProperty = BindableProperty.Create(nameof(CornerRadius), typeof(float), typeof(CornerRadiusContainer), 0.0f, BindingMode.OneWay);

		public float CornerRadius
		{
			get { return (float)GetValue(CornerRadiusProperty); }
			set { SetValue(CornerRadiusProperty, value); }
		}

		public static readonly BindableProperty BorderColorProperty = BindableProperty.Create(nameof(BorderColor), typeof(Color), typeof(CornerRadiusContainer), Color.Transparent, BindingMode.OneWay);

		public Color BorderColor
		{
			get { return (Color)GetValue(BorderColorProperty); }
			set { SetValue(BorderColorProperty, value); }
		}

		public static readonly BindableProperty BorderWidthProperty = BindableProperty.Create(nameof(BorderWidth), typeof(float), typeof(CornerRadiusContainer), 0.0f, BindingMode.OneWay);

		public float BorderWidth
		{
			get { return (float)GetValue(BorderWidthProperty); }
			set { SetValue(BorderWidthProperty, value); }
		}


		public CornerRadiusContainer()
		{
		}
	}
}
