﻿using System;
using Xamarin.Forms;

namespace EduApp
{
	public class MultiLineLabel : Label
	{
		private static int _defaultLineSetting = -1;

		public static readonly BindableProperty LinesProperty = BindableProperty.Create(nameof(Lines), typeof(int), typeof(MultiLineLabel), _defaultLineSetting);
		public int Lines
		{
			get { return (int)GetValue(LinesProperty); }
			set { SetValue(LinesProperty, value); }
		}

		public static readonly BindableProperty BorderColorProperty = BindableProperty.Create(nameof(BorderColor), typeof(Color), typeof(MultiLineLabel), Color.Transparent);
		public Color BorderColor
		{
			get { return (Color)GetValue(BorderColorProperty); }
			set { SetValue(BorderColorProperty, value); }
		}

		public static readonly BindableProperty BorderWidthProperty = BindableProperty.Create(nameof(BorderWidth), typeof(float), typeof(MultiLineLabel), 0.0f);
		public float BorderWidth
		{
			get { return (float)GetValue(BorderWidthProperty); }
			set { SetValue(BorderWidthProperty, value); }
		}

		public static readonly BindableProperty CornerRadiusProperty = BindableProperty.Create(nameof(CornerRadius), typeof(float), typeof(MultiLineLabel), 0.0f, BindingMode.OneWay);
		public float CornerRadius
		{
			get { return (float)GetValue(CornerRadiusProperty); }
			set { SetValue(CornerRadiusProperty, value); }
		}

		public static readonly BindableProperty PaddingProperty = BindableProperty.Create(nameof(Padding), typeof(float), typeof(MultiLineLabel), 0.0f, BindingMode.OneWay);
		public float Padding
		{
			get { return (float)GetValue(PaddingProperty); }
			set { SetValue(PaddingProperty, value); }
		}

	}
}
