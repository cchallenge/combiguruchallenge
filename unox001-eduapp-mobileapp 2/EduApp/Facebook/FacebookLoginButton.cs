﻿using System;
using System.Windows.Input;
using PSM.Facebook;
using Xamarin.Forms;

namespace PSMFacebook
{
	
	public class FacebookLoginButton : View
	{
		public static readonly BindableProperty ReadPermissionsProperty = BindableProperty.Create<FacebookLoginButton, string []> (p => p.ReadPermissions, null);
		public string [] ReadPermissions {
			get { return (string [])GetValue (ReadPermissionsProperty); }
			set { SetValue (ReadPermissionsProperty, value); }
		} 

		public FacebookLoginButton () {
			ReadPermissions = new string [] {
				"public_profile", "email"
			};

		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();
		}

		public static readonly BindableProperty LoggedInCommandProperty = BindableProperty.Create (nameof (LoggedInCommand), typeof (ICommand), typeof (FacebookLoginButton), null, BindingMode.OneWay);

		public ICommand LoggedInCommand {
			get { return (ICommand)this.GetValue (LoggedInCommandProperty); }
			set { this.SetValue (LoggedInCommandProperty, value); }
		}
	}
}
