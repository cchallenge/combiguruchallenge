using System;
using Xamarin.Forms;

namespace PSM.Facebook
{
	public class FacebookLoginEventArgs : EventArgs
	{
		public string AccessToken { get; set; }
	}
	
}
