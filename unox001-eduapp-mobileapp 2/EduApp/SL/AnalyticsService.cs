﻿using System;
using EduApp.Core;
using Plugin.GoogleAnalytics;

namespace EduApp
{
	[Nostromo.Dependency]
	public class AnalyticsService : IAnalyticsService
	{
		public void TaskEvent(string category, string action, string label)
		{
			GoogleAnalytics.Current.Tracker.SendEvent(category, action, label);
		}

		public void TrackView(string viewName)
		{
			GoogleAnalytics.Current.Tracker.SendView(viewName);
		}
	}
}
