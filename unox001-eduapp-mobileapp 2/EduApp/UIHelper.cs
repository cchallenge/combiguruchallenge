﻿using System;
namespace EduApp
{
	public static class UIHelper
	{
		public static double PixelsFor1Dip { get; set; } = 1;

		public static double ToPixels (double dip) {
			return dip * PixelsFor1Dip;
		}
	}
}
