﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EduApp.Core;
using Nostromo;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(QuestionS4PageViewModel))]
	public partial class QuestionS4Page : BaseQuestionViewPage<QuestionS4PageViewModel>
	{
		public QuestionS4Page()
		{
			InitializeComponent();

			this.ToolbarItems.Add(new ToolbarItem("", "x_close_white.png", () => ViewModel.CloseViewCommand.Execute(null)));
			this.WhenAnyValue(x => x.ViewModel.ResponseList).Subscribe(x => ListChange(x));
		}

		int i = 0;
		void ListChange(IEnumerable<S4Response> items)
		{
			foreach (var elem in items)
			{
				var btn = new S4ButtonView()
				{
					HeightRequest = 68,
					VerticalOptions = LayoutOptions.Center,
					StatoRisposta = elem.StatoRisposta,
					ImageSource = elem.ImageSource,
					ViewCommand = elem.S4Command
				};
				btn.BindingContext = elem;
				btn.SetBinding (S4ButtonView.StatoRispostaProperty, new Binding("StatoRisposta"));

				this.QuestionContainer.Children.Add(
					btn,
					0,
					i
				);
				i++;
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.Appearing();
		}

        protected override bool OnBackButtonPressed()
        {
            ViewModel.CloseViewCommand.Execute(null);
            return true;
        }
    }
}
