﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(HomePageViewModel))]
	public partial class HomePage : ViewPage<HomePageViewModel>
    {
        ToolbarItem tbDone;

        public HomePage()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

		void OMenuTapped (object sender, EventArgs args)
		{
			ViewModel.ShowMenu();
		}

        private void UserInstitutionSet(string institution) {
            ViewModel.UserInstitutionSet(institution); 
        }

        private void ValidateAndSaveInstitution(object o, EventArgs e)
        {
            ViewModel.ValidateAndSaveInstitution();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await Task.Yield();
            MessagingCenter.Subscribe<PopupView, string>(this, "UserInstitutionSet", (sender, institution) => {
                UserInstitutionSet(institution);
            });

            Device.BeginInvokeOnMainThread(() => ViewModel.Appearing());
        }

        protected override async void OnDisappearing() {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<PopupView>(this, "UserInstitutionSet");
        }
    }
}
