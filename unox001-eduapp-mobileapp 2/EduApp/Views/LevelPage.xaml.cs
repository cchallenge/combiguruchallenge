﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EduApp.Core;
using Nostromo;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(LevelPageViewModel))]
	public partial class LevelPage : ViewPage<LevelPageViewModel>
	{
		protected override void OnParentSet()
		{
			base.OnParentSet();
			if (Parent == null)
			{

				if (this.ViewModel != null)
					this.ViewModel.Dispose();
				this.BindingContext = null;

				GC.Collect(0);
			}
		}

		int itemsForRow = 3;

		public LevelPage()
		{
			InitializeComponent();

			this.WhenAnyValue(x => x.ViewModel.UpdateDate).Subscribe((obj) => changeGridFormatting());
		}

		protected override async void OnAppearing()
		{
			base.OnAppearing();
			await Task.Yield();
			Device.BeginInvokeOnMainThread(() => ViewModel.Appearing());
		}

		async Task changeGridFormatting()
		{
			if (ViewModel.CurrentSections != null)
			{
				int x = 0;
				int y = 0;
				int span = 1;
				levelContainer.Children.Clear();

				foreach (var i in ViewModel.CurrentSections)
				{
					if (y > 0)
					{
						var res = ViewModel.CurrentSections.Skip(3 * y).Take(3);
						switch (res.Count())
						{
							case 1:
								x = 1;
								break;
							case 2:
								span = 2;
								break;
							default:
								span = 1;
								break;
						}
					}

					var learnBtn = new LearnBtnTitleLevelView()
					{
						Sbiadimento = i.Sbiadimento,
						TopTitle = i.Title,
						BottomTitle = i.SubTitle,
						BkgImageSource = i.Image,
						HorizontalOptions = LayoutOptions.Fill,
						VerticalOptions = LayoutOptions.Fill,
						Margin = new Thickness(5, 0),
						Progress = i.Percentage
					};

					var tapGestureRecognizer = new TapGestureRecognizer();
					tapGestureRecognizer.Tapped += async (sender, e) =>
					{
						await (ViewModel as LevelPageViewModel).OpenSection(i.CorrID);
					};
					learnBtn.GestureRecognizers.Add(tapGestureRecognizer);

					levelContainer.Children.Add(learnBtn, x, y);
					x += span;
					if (x > (itemsForRow - 1))
					{
						y++;
						x = 0;
					}
				}
			}
		}
	}
}
