﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EduApp.Core;
using Nostromo;
using ReactiveUI;
using Xamarin.Forms;
using System.Linq;

namespace EduApp
{
	[ViewFor(typeof(QuestionS3PageViewModel))]
	public partial class QuestionS3Page : BaseQuestionViewPage<QuestionS3PageViewModel>
	{
		public QuestionS3Page()
		{
			InitializeComponent();
			this.WhenAnyValue(v => v.ViewModel.ResponseList).Subscribe(rl => createButtons(rl));
			this.ToolbarItems.Add(new ToolbarItem("", "x_close_white.png", () => ViewModel.CloseViewCommand.Execute(null)));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.Appearing();
		}

        protected override bool OnBackButtonPressed()
        {
            ViewModel.CloseViewCommand.Execute(null);
            return true;
        }

        async Task OnButtonTap(S3ButtonView button)
		{
			var parent = button.Parent;
			//@@ CM sposto gli oggetti da un container all'altro
			Device.BeginInvokeOnMainThread(async () =>
			{
				(parent as StackLayout).Children.Remove(button);
				if (parent == this.FindByName<StackLayout>("startContainer"))
				{
					button.CurrentPosition = this.endContainer.Children.Count();
					if(Device.OS == TargetPlatform.Android)
						await Task.Delay(80);
					this.endContainer.Children.Add(button);
					if (this.endContainer.Children.Count() == ViewModel.ResponseList.Count())
					{
						//@@ CM se ho spostato tutti gli elementi dal container iniziale a quello finale verifico la loro correttezza

						if (!await verifyOrder())
						{
							await Task.Delay(3000);
							resetButtonOrder();
						}
					}
				}
				else {
					resetGraphicButton(button);
					if (Device.OS == TargetPlatform.Android)
						await Task.Delay(80);
					this.startContainer.Children.Add(button);
				}
			});
		}

		async Task<bool> verifyOrder()
		{
			bool correct = true;
			foreach (var elem in this.endContainer.Children)
			{
				var s3Button = (elem as S3ButtonView);
				correct &= ViewModel.VerifyButtonOrder(s3Button.CurrentPosition, s3Button.CorrectPosition);
			}

			//@@ CM se sono tutti corretti ritorno true
			if (correct)
			{
				await Task.Delay(1000);
				ViewModel.SetAllCorrect();
			}
			return correct;
		}

		async Task resetButtonOrder()
		{
			var child = this.endContainer.Children.ToList();
			foreach (var elem in child)
			{
				this.endContainer.Children.Remove(elem);
				resetGraphicButton((elem as S3ButtonView));
				this.startContainer.Children.Add(elem);
			}
		}

		void resetGraphicButton(S3ButtonView button)
		{
			button.CurrentPosition = -1;
			ViewModel.ResetAll();
		}

		void createButtons(IEnumerable<S3Response> items)
		{
			foreach (var it in items)
			{
				var btn = new S3ButtonView()
				{
					ImageSource = it.ImageSource,
					Title = it.Text,
					HorizontalOptions = LayoutOptions.Fill,
					HeightRequest = 35,
					CorrectPosition = it.CorrectOrder
				};

				btn.BindingContext = it;
				btn.SetBinding(View.BackgroundColorProperty, new Binding("StatoRisposta", BindingMode.Default, new AnswerColorConverter(), it.StarBkgColor));

				btn.TapCommand = ReactiveCommand.CreateFromTask(() => OnButtonTap(btn), it.WhenAnyValue(b => b.IsEnabledButton));

				this.startContainer.Children.Add(btn);
			}
		}
	}
}
