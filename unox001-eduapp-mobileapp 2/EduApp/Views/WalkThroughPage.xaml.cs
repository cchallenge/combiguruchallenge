﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{
	//https://blog.xamarin.com/flip-through-items-with-xamarin-forms-carouselview/
	[ViewFor(typeof(WalkThroughPageViewModel))]
	public partial class WalkThroughPage : ViewPage
	{
		public WalkThroughPage()
		{
			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);

			Carousel.ItemSelected += (sender, args) =>
			{
				var zoo = args.SelectedItem as WalkThroughModel;
				if (zoo == null)
					return;

				Title = zoo.Title;
			};
		}
	}
}
