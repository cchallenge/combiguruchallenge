﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(LearningPathPageViewModel))]
	public partial class LearningPathPage : ViewPage<LearningPathPageViewModel>
	{
		protected override void OnParentSet()
		{
			base.OnParentSet();
			if (Parent == null) {

				if (this.ViewModel != null)
					this.ViewModel.Dispose();
				this.BindingContext = null;

				GC.Collect(0);
			}
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
		}

		public LearningPathPage ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, true);
        }

        protected override async void OnAppearing ()
		{
			//NavigationPage.SetHasNavigationBar(this, true);
			//GC.Collect();
			base.OnAppearing ();
			await Task.Yield();
			Device.BeginInvokeOnMainThread (() => ViewModel.Appearing());
		}
	}
}
