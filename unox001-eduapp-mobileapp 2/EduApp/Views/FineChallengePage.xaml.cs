﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{
    [ViewFor(typeof(FineChallengePageViewModel))]
    public partial class FineChallengePage : ViewPage
    {
        public FineChallengePage()
        {
            InitializeComponent();
        }

        void OpenLink(object sender, System.EventArgs e)
        {
            var uri = (ViewModel as FineChallengePageViewModel).UnoxLink;
            if (!String.IsNullOrEmpty(uri))
                Device.OpenUri(new Uri(uri));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.Appearing();
        }

    }
}