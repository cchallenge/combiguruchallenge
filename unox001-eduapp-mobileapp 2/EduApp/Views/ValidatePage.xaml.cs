﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(ValidatePageViewModel))]
	public partial class ValidatePage : ViewPage
	{
		public ValidatePage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			(this.BindingContext as ValidatePageViewModel).Appearing();
		}
	}
}
