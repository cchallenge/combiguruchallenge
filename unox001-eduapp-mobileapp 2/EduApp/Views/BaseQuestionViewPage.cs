﻿using Nostromo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EduApp
{
    public partial class BaseQuestionViewPage<T> : ViewPage<T>
        where T : class, IPageViewModel
    {
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}
