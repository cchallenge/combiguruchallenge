﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(FineChallengeLosePageViewModel))]
	public partial class FineChallengeLosePage : BaseFinePage
	{
		public FineChallengeLosePage()
		{
			InitializeComponent();
		}
	}
}
