﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(LoginPageViewModel))]
	public partial class LoginPage : ViewPage<LoginPageViewModel>
	{
		public LoginPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			if (Xamarin.Forms.Device.OS == TargetPlatform.Android)
				entryUsr.OnEntryCompleted = () => entryPwd.FocusEntry();
		}

		void OnForgotGestureRecognizer(object sender, EventArgs args)
		{
			ViewModel.RequestResetPassword();
		}

		void CloseTapped (object sender, EventArgs args)
		{
			ViewModel.Close ();
		}
	}
}
