﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EduApp.Core;
using Nostromo;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor (typeof (QuestionS8PageViewModel))]
	public partial class QuestionS8Page : BaseQuestionViewPage<QuestionS8PageViewModel>
	{
		public List<S8ResponseList> Item { get; set; }
		bool first = true;

		public QuestionS8Page ()
		{
			InitializeComponent ();

			this.ToolbarItems.Add (new ToolbarItem ("", "x_close_white.png", () => ViewModel.CloseViewCommand.Execute (null)));			
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			Task.Run(() => ViewModel.Appearing ()).Wait();
			if (first) 
			{
				first = false;
				Item = ViewModel.Elements;
				AddElements (Item);
			}
		}

        protected override bool OnBackButtonPressed()
        {
            ViewModel.CloseViewCommand.Execute(null);
            return true;
        }

        void AddElements (List<S8ResponseList> items)
		{
			foreach (var item in items) {
				var obj = new S8ButtonView () {
					HeightRequest = ( Device.OS == TargetPlatform.Android ) ? 100 : 80,
					Items = item.ResponseList,
					StatoRisposta = AnswerState.None
				};
				obj.BindingContext = item;
				obj.SetBinding (S8ButtonView.ItemsProperty, new Binding ("ResponseList"));
				obj.SetBinding (S8ButtonView.StatoRispostaProperty, new Binding ("StatoRisposta"));
				obj.SetBinding (S8ButtonView.IsEditableProperty, new Binding ("IsEditable"));

				this.container.Children.Add (obj);
			}
		}
	}
}
