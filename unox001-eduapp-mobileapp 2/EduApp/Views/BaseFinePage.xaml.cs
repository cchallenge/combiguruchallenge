﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace EduApp
{
	public partial class BaseFinePage : ViewPage
	{
		public BaseFinePage()
		{
			InitializeComponent();
		}

		public View TopContainer
		{
			get { return this.topContainer; }
			set
			{
				this.topContainer.Content = null;
				this.topContainer.Content = value;
			}
		}

	}
}
