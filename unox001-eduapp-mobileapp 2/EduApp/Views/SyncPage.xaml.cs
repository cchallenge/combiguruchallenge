﻿using System;
using System.Collections.Generic;
using AppCore;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{

    [ViewFor (typeof (SyncPageViewModel))]
    public partial class SyncPage : ViewPage
	{
		public SyncPage ()
		{
			InitializeComponent ();
			NavigationPage.SetHasNavigationBar(this, false);
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			(this.BindingContext as SyncPageViewModel).Appearing();
		}
	}
}
