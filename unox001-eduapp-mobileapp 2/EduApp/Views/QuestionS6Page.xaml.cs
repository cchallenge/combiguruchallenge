﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduApp.Core;
using Nostromo;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor (typeof (QuestionS6S7PageViewModel))]
	public partial class QuestionS6Page : BaseQuestionViewPage<QuestionS6S7PageViewModel>
	{
		public QuestionS6Page ()
		{
			InitializeComponent ();

			this.ToolbarItems.Add (new ToolbarItem ("", "x_close_white.png", () => ViewModel.CloseViewCommand.Execute (null)));
			this.WhenAnyValue (x => x.ViewModel.Elements).Subscribe (x => AddElements(x));
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			ViewModel.Appearing ();
		}

        protected override bool OnBackButtonPressed()
        {
            ViewModel.CloseViewCommand.Execute(null);
            return true;
        }

        void AddElements (IEnumerable<S6S7Response> elements)
		{
			if (elements != null) {
				foreach (var i in elements) {
					var elem = new S6ButtonView () {
						HeightRequest = 40,
						HorizontalOptions = LayoutOptions.Fill,
						VerticalOptions = LayoutOptions.Center,
						MeasureTitle = i.Measure,
						StatoRisposta = i.StatoRisposta,
						ButtonIcon = i.IconSource,
						ResponseText = i.Response,
						IsEditable = i.IsEditable,
						IsIconVisible = i.IsIconVisible
					};
					elem.BindingContext = i;
					elem.SetBinding (S6ButtonView.StatoRispostaProperty, new Binding ("StatoRisposta"));
					elem.SetBinding (S6ButtonView.ResponseTextProperty, new Binding ("Response"));
					elem.SetBinding (S6ButtonView.IsEditableProperty, new Binding ("IsEditable"));
					elem.SetBinding (S6ButtonView.IsIconVisibleProperty, new Binding ("IsIconVisible"));
					
					this.container.Children.Add (elem);
				}
			}
		}
	}
}
