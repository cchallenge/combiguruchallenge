﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduApp.Core;
using NControl.Abstractions;
using NGraphics;
using Nostromo;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	public abstract partial class BaseGraficoPage : BaseQuestionViewPage<BaseGraficoViewModel>
	{
		public BaseGraficoPage()
		{
			InitializeComponent();

			this.ToolbarItems.Add(new ToolbarItem("", "x_close_white.png", () => ViewModel.CloseViewCommand.Execute(null)));

			this.WhenAnyValue(x => x.ViewModel.Items).Subscribe(x => DrawPoints(x));
			this.WhenAnyValue(x => x.ViewModel.TappedMatrixChange).Subscribe(x => DrawLines());
			this.WhenAnyValue(x => x.ViewModel.ResponseState).Subscribe(x => HandleAnswersState(x));

			this.WhenAnyValue(x => x.ViewModel.YAxisElements).Subscribe(x => PopulateYAxis(x));
			this.WhenAnyValue(x => x.ViewModel.XAxisElements).Subscribe(x => PopulateXAxis(x));
		}

        //public abstract BaseGraficoViewModel ViewModel { get; }

        protected override bool OnBackButtonPressed()
        {
            ViewModel.CloseViewCommand.Execute(null);
            return true;
        }

        public View TopContainer
		{
			get { return this.topContainer; }
			set
			{
				this.topContainer.Content = null;
				this.topContainer.Content = value;
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			Device.BeginInvokeOnMainThread(() => base.ViewModel.Appearing());
		}

		void PopulateYAxis(string[] items)
		{
			if (items != null)
			{
				int i = 0;

				foreach (var it in items)
				{
					this.container.Children.Add(new AxisLabel()
					{
						Text = it,
						VerticalOptions = LayoutOptions.Center,
						HorizontalOptions = LayoutOptions.End,
						Margin = new Thickness(5, 0)
					}, 0, i);
					i++;
				}
			}
		}

		void PopulateXAxis(string[] items)
		{
			if (items != null)
			{
				int i = 1;

				foreach (var it in items)
				{
					this.container.Children.Add(new AxisLabel()
					{
						Text = it,
						VerticalOptions = LayoutOptions.Start,
						HorizontalOptions = LayoutOptions.Center,
						Margin = 5
					}, i, this.container.RowDefinitions.Count);
					i++;
				}
			}
		}

		void HandleAnswersState(AnswerState isCorrectAnswers)
		{
			switch (isCorrectAnswers)
			{
				case AnswerState.Corretto:
					manageAllButtonsEnabled(false);
					this.linesDrawer.ResponseHexColor = Globals.ColorCorretto;
					break;
				case AnswerState.DopoCorretto:
					manageAllButtonsEnabled(false);
					this.linesDrawer.ResponseHexColor = Globals.ColorDopoCorretto;
					break;
				case AnswerState.Sbagliato:
					manageAllButtonsEnabled(false);
					this.linesDrawer.ResponseHexColor = Globals.ColorSbagliato;
					break;
				case AnswerState.InAttesa:
					manageAllButtonsEnabled(true);
					this.linesDrawer.ResponseHexColor = Globals.ColorInAttesa;
					break;
			}
		}

		void manageAllButtonsEnabled(bool isEnabled)
		{
			foreach (var e in this.container.Children)
			{
				if (e is Button)
					e.IsEnabled = isEnabled;
			}
		}

		void DrawPoints(bool[,] items)
		{
			if (items != null)
			{
				deleteAllGridButton();
				for (int y = 0; y < ViewModel.Items.GetLength(0); y++)
				{
					for (int x = 0; x < ViewModel.Items.GetLength(1); x++)
					{
						int index = (4 * x) + y;
						//Verifico se devo disegnare il pallino
						if (items[x, y])
						{
							var btn = new Button()
							{
								AutomationId = index.ToString(),
								BackgroundColor = Xamarin.Forms.Color.Transparent,
								Opacity = 0.5,
								HorizontalOptions = LayoutOptions.Fill,
								VerticalOptions = LayoutOptions.Fill
							};
							btn.Clicked += Handle_Clicked;
							//@@ CM lascio libera la prima colonna quindi sposto tutte le x di 1
							this.container.Children.Add(btn, x + 1, y);

							this.linesDrawer.Points.Add(index, new PointsToDraw() { Column = x, Row = y, Selected = false });

							System.Diagnostics.Debug.WriteLine($"Sto disegnando a {x},{y} e sono indice {index}");
							//Indico al linesDrawer quali assi deve disegnare
							this.linesDrawer.Axis[x] = true;
						}
					}
				}
			}
		}

		void deleteAllGridButton()
		{
			foreach (var b in container.Children)
				if (b is Button)
				{
					container.Children.Remove(b);
				}
		}

		void DrawLines()
		{
			this.linesDrawer.Coordinates.Clear();
			resetButtonSelected();

			if (ViewModel.TappedItems != null)
			{
				//Verifico se ho colonne adiacenti valorizzate
				bool itemFound = false;
				int rowFound = -1;
				int columnFound = -1;

				for (int x = 0; x < ViewModel.Items.GetLength(1); x++)
				{
					for (int y = 0; y < ViewModel.Items.GetLength(0); y++)
					{
						if (ViewModel.TappedItems[x, y])
						{
							int index = (4 * x) + y;
							System.Diagnostics.Debug.WriteLine($"Imposto a selezionato l'elemento di indice {index}");
							//Imposto a selezionato il punto tappato
							if (this.linesDrawer.Points.ContainsKey(index))
							{
								this.linesDrawer.Points[index].Selected = true;
							}

							//Se nella colonna precedente avevo una corrispondeza disegno la linea
							if (itemFound == true)
							{
								System.Diagnostics.Debug.WriteLine($"Accoppio {(rowFound)}, {columnFound} con {y},{x}");
								this.linesDrawer.Coordinates.Add(new ColumnsToDraw()
								{
									StartColumn = columnFound,
									EndColumn = x,
									StartRow = rowFound,
									EndRow = y
								});
							}
							//In ogni caso indico che ho trovato un tap
							itemFound = true;
							rowFound = y;
							columnFound = x;
						}
					}
					//@@ CM verifico se ci sono dei punti tappabili su quella colonna
					var tappableItem = Enumerable.Range(0, ViewModel.Items.GetLength(1)).Select(i => ViewModel.Items[x, i]).Any(y => y);
					if (tappableItem)
						itemFound = Enumerable.Range(0, ViewModel.Items.GetLength(1)).Select(i => ViewModel.TappedItems[x, i]).Any(y => y);
				}
			}

			this.linesDrawer.Invalidate();

		}

		void resetButtonSelected()
		{
			foreach (var p in this.linesDrawer.Points.Values)
				p.Selected = false;
		}

		void Handle_Clicked(object sender, System.EventArgs e)
		{
			var btn = (sender as Button);
			System.Diagnostics.Debug.WriteLine($"Ho tappato un bottone con id {btn.AutomationId}");

			ViewModel.AddPoint(btn.AutomationId);
		}
	}

	public class LinesDrawer : NControlView
	{
		public int NColumns { get; set; } = 4;
		public int NRows { get; set; } = 4;

		public IList<ColumnsToDraw> Coordinates = new List<ColumnsToDraw>();
		public Dictionary<int, PointsToDraw> Points = new Dictionary<int, PointsToDraw>();
		public bool[] Axis = new bool[4];

		string responseColor;
		public string ResponseHexColor
		{
			get { return responseColor; }
			set
			{
				responseColor = value;
				if (!String.IsNullOrEmpty(responseColor))
				{
					_responseColorSolid = new SolidBrush(Xamarin.Forms.Color.FromHex(responseColor).ToNColor());
					Colors.TryParse(responseColor, out _responseColor);
				}
				this.Invalidate();
			}
		}

		NGraphics.Color _axisColor;
		SolidBrush _blackCircleColor;
		SolidBrush _responseColorSolid;
		NGraphics.Color _responseColor;
		public LinesDrawer()
		{
			Colors.TryParse("#C1C3C6", out _axisColor);
			_blackCircleColor = new SolidBrush(Xamarin.Forms.Color.FromHex("000000").ToNColor());
		}

		public override void Draw(ICanvas canvas, Rect rect)
		{
			base.Draw(canvas, rect);
			//@@ CM disegna sempre asse x e asse y
			canvas.DrawLine(0, rect.Height - UIHelper.PixelsFor1Dip, rect.Width, rect.Height - UIHelper.PixelsFor1Dip, _axisColor, UIHelper.ToPixels(2));
			canvas.DrawLine(0, 0, 0, rect.Height, _axisColor, UIHelper.PixelsFor1Dip);

			double columnW = (rect.Width / NColumns);
			double rowH = (rect.Height / NRows);
			//@@ CM disegno gli assi
			for (int i = 0; i < Axis.GetLength(0); i++)
			{
				if (Axis[i])
				{
					double x = (columnW * i) + (columnW / 2);
					canvas.DrawLine(x, 0, x, rect.Height, _axisColor, UIHelper.PixelsFor1Dip);
				}
			}

			if (Coordinates != null)
			{
				double halfRowHight = rowH / 2;
				double halfColumnWidth = columnW / 2;

				foreach (var elem in Coordinates)
				{
					double startX = (elem.StartColumn * columnW) + halfColumnWidth;
					double endX = (elem.EndColumn * columnW) + halfColumnWidth;

					double startY = (elem.StartRow * rowH) + halfRowHight;
					double endY = (elem.EndRow * rowH) + halfRowHight;
					canvas.DrawLine(startX, startY, endX, endY, _responseColor, UIHelper.ToPixels(4));
				}
			}

			if (Points != null)
			{
				foreach (var p in Points.Values)
				{
					var dimens = UIHelper.ToPixels(14);
					double firstX = (columnW * p.Column) + (columnW / 2) - (dimens / 2);
					double firstY = (rowH * p.Row) + (rowH / 2) - (dimens / 2);
					var circleColor = p.Selected ? _responseColorSolid : _blackCircleColor;
					canvas.DrawEllipse(firstX, firstY, dimens, dimens, null, circleColor);
				}
			}
		}
	}

	/// <summary>
	/// NGraphics extensions.
	/// </summary>
	public static class NGraphicsExtensions
	{
		/// <summary>
		/// Converts the Xamarin.Forms Color to an NGraphics Color
		/// </summary>
		/// <returns>The N color.</returns>
		/// <param name="color">Color.</param>
		public static NGraphics.Color ToNColor(this Xamarin.Forms.Color color)
		{
			return new NGraphics.Color(color.R, color.G, color.B, color.A);
		}

		/// <summary>
		/// Converts a Xamarin Rectangle to an NGraphics rectangle
		/// </summary>
		/// <returns>The N rect.</returns>
		/// <param name="rect">Rect.</param>
		public static NGraphics.Rect ToNRect(this Xamarin.Forms.Rectangle rect)
		{
			return new NGraphics.Rect(rect.X, rect.Y, rect.Width, rect.Height);
		}
	}

	public class ColumnsToDraw
	{
		public int StartColumn { get; set; }
		public int EndColumn { get; set; }
		public int StartRow { get; set; }
		public int EndRow { get; set; }
	}

	public class PointsToDraw
	{
		public int Column { get; set; }
		public int Row { get; set; }
		public bool Selected { get; set; }
	}
}
