﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(ChallengePageViewModel))]
	public partial class ChallengePage : ViewPage
	{
		public ChallengePage()
		{
			InitializeComponent();
			this.ToolbarItems.Add(new ToolbarItem("", "x_close_white.png", () => (ViewModel as SectionPageViewModel).CloseViewCommand.Execute(null)));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.Appearing();
		}

        protected override bool OnBackButtonPressed()
        {
            (ViewModel as SectionPageViewModel).CloseViewCommand.Execute(null);
            return true;
        }
    }
}
