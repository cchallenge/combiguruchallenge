﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(FineLivelloPageViewModel))]
	public partial class FineLivelloPage : ViewPage
	{
		public FineLivelloPage()
		{
			InitializeComponent();
		}

		void OpenLink(object sender, System.EventArgs e)
		{
			var uri = (ViewModel as FineLivelloPageViewModel).UnoxLink;
			if (!String.IsNullOrEmpty(uri))
				Device.OpenUri(new Uri(uri));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.Appearing();
		}
	}
}
