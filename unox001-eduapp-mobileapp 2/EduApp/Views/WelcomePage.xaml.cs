﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EduApp.Core;
using Nostromo;
using Nostromo.Interfaces;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(WelcomePageViewModel))]
	public partial class WelcomePage : ViewPage
	{
		public WelcomePage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}

		void OnReadMoreGestureRecognizer(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine("Ho tappato ReadMore");
			var lang = (ViewModel as WelcomePageViewModel).Catalog?.GetLanguage().ToUpper();
			var allowedLangs = new List<string> { "IT", "FR", "ES", "DE", "EN" };
			if (!allowedLangs.Contains(lang)) {
				lang = "EN";
			}

			var uri = string.Format(Globals.PrivacyLink, lang);
			if(!String.IsNullOrEmpty(uri))
				Device.OpenUri(new Uri(uri));
		}

		void OpenLink(object sender, System.EventArgs e)
		{
			var uri = (ViewModel as WelcomePageViewModel).UnoxLink;
			if(!String.IsNullOrEmpty(uri))
				Device.OpenUri(new Uri(uri));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.Appearing();
		}
	}
}
