﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(FineLezionePageViewModel))]
	public partial class FineLezionePage : BaseFinePage
	{
		public FineLezionePage()
		{
			InitializeComponent();
		}
	}
}
