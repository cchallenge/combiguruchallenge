﻿using System;
using System.Collections.Generic;
using System.Linq;
using EduApp.Core;
using Nostromo;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(QuestionS14PageViewModel))]
	public partial class QuestionS14Page : BaseGraficoPage
	{
		QuestionS14PageViewModel vM;
		public QuestionS14PageViewModel VM
		{
			get
			{
				return vM;
			}

			set
			{
				vM = value;
				this.OnPropertyChanged();
			}
		}

		public QuestionS14Page()
		{
			InitializeComponent();
			this.WhenAnyValue(x => x.VM.TopItems).Subscribe(x => AddButtons(x));
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			this.VM = this.ViewModel as QuestionS14PageViewModel;
		}

		void AddButtons(IEnumerable<S14Model> items)
		{
			//@@ CM elimino tutti i bottoni dalla grid
			this.topContainer.Children.Clear();

			int i = 0;
			foreach (var item in items)
			{
				if (item != null)
				{
					var view = new S13ButtonView()
					{
						Items = item.SubAnswers.ToList(),
						StatoRisposta = AnswerState.None
					};

					this.topContainer.Children.Add(view, i, 0);
				}
				else {
					var view = new AbsoluteLayout();
					this.topContainer.Children.Add(view, i, 0);
				}

				i++;
			}
		}
	}
}
