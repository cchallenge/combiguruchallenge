﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EduApp.Core;
using Nostromo;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor (typeof (QuestionS13PageViewModel))]
	public partial class QuestionS13Page : BaseQuestionViewPage<QuestionS13PageViewModel>
	{
		List<ExtS13Data> listElementsBottom;
		List<ExtS13Data> listElementsTop;
		public List<S13ResponseList> Item { get; set; }

		bool first = true;
		bool isFinished = false;

		public QuestionS13Page ()
		{
			InitializeComponent ();

			this.ToolbarItems.Add (new ToolbarItem ("", "x_close_white.png", () => ViewModel.CloseViewCommand.Execute (null)));
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			Task.Run (() => ViewModel.Appearing ()).Wait ();
			if (first) 
			{
				first = false;

				Item = ViewModel.Elements;
				AddElements (Item);
			}
		}

        protected override bool OnBackButtonPressed()
        {
            ViewModel.CloseViewCommand.Execute(null);
            return true;
        }

        void AddElements ( List<S13ResponseList> items )
		{
			listElementsTop = new List<ExtS13Data> ();
			listElementsBottom = new List<ExtS13Data> ();

			int i = 0;
			foreach (var item in items) {
				var view = new S13ButtonView () {
					Items = item.ResponseList,
					StatoRisposta = AnswerState.None
				};

				var extBtn = new ExtS13Data () {
					S13View = view,
					StartingOrder = i,
					Order = item.Order,
					S13ResponseList = item
				};
				view.BindingContext = item;
				view.SetBinding (S13ButtonView.StatoRispostaProperty, new Binding ("StatoRisposta"));

				view.TapCommand = ReactiveCommand.CreateFromTask (() => TapOnButton (extBtn));

				this.BottomGrid.Children.Add (extBtn.S13View, i, 0);

				listElementsBottom.Add (extBtn);

				i++;
				
			}
		}

		async Task TapOnButton (ExtS13Data extButton)
		{
			if (listElementsBottom.Contains (extButton)) {
				listElementsBottom.Remove (extButton);
				this.BottomGrid.Children.Remove (extButton.S13View);

				var i = this.TopGrid.Children.Count ();
				extButton.Position = i;
				listElementsTop.Add (extButton);
				this.TopGrid.Children.Add (extButton.S13View, i, 0);

				if (TopGrid.Children.Count == 4) 
				{
					isFinished = true;
					VerifyData (listElementsTop);
				}

			} else if (listElementsTop.Last () == extButton && !isFinished) {
				listElementsTop.Remove (extButton);
				this.TopGrid.Children.Remove (extButton.S13View);
				listElementsBottom.Add (extButton);
				this.BottomGrid.Children.Add(extButton.S13View, extButton.StartingOrder, 0);
			}
		}

		async void VerifyData (List<ExtS13Data>  list)
		{
			bool errorOrd = false;
			foreach (var element in list) {
				if (element.Order == element.Position)
					element.S13ResponseList.StatoRisposta = AnswerState.Corretto;
				else 
				{
					element.S13ResponseList.StatoRisposta = AnswerState.Sbagliato;
					errorOrd = true;
				}
			}

			await Task.Delay (1000); 

			if (!errorOrd) {
				ViewModel.logResultsResponse.Add (true);
				ViewModel.ProceedProgress ();
				ViewModel.IsButtonEnabled = true;
				foreach (var element in list) {
					element.S13ResponseList.StatoRisposta = AnswerState.DopoCorretto;
				}
			} else {
				isFinished = false;
				ViewModel.logResultsResponse.Add (false);
				Clean ();
			}

		}

		void Clean ()
		{
			var list = listElementsTop.OrderBy (x => x.Order).ToList();
			int i = 0;
			foreach (var element in list) 
			{
				element.S13ResponseList.StatoRisposta = AnswerState.None;
				this.TopGrid.Children.Remove (element.S13View);
				this.BottomGrid.Children.Add (element.S13View, i, 0);
				listElementsBottom.Add (element);
				i++;
			}
			listElementsTop.Clear ();
		}
	}

	class ExtS13Data
	{
		public S13ResponseList S13ResponseList { get; set; }
		public S13ButtonView S13View { get; set; }
		public int StartingOrder { get; set; }
		public int Order { get; set; }
		public int Position { get; set; }
	}
}
