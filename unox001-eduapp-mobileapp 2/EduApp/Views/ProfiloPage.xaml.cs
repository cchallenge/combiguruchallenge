﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(ProfiloPageViewModel))]
	public partial class ProfiloPage : ViewPage
	{
		ToolbarItem tbDone;

		public ProfiloPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();
			IAppCatalog Catalog = (this.BindingContext as ProfiloPageViewModel).Catalog;
			tbDone = new ToolbarItem (Catalog.GetValue ("DoneNavigation"), "", () => (ViewModel as ProfiloPageViewModel).DoneCommand.Execute (null));
			this.ToolbarItems.Clear ();
			this.ToolbarItems.Add (tbDone);
		}

		protected override void OnAppearing()
		{
			NavigationPage.SetHasNavigationBar(this, true);
			base.OnAppearing();
			ViewModel.Appearing();
		}

        protected override bool OnBackButtonPressed()
        {
            //AM-104: viene bloccata la navigazione indietro per evitare il bypass dei controlli su login con facebook.
            return true;
        }
    }
}
