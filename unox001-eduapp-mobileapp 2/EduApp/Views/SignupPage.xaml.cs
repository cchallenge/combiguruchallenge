﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Nostromo;
using Xamarin.Forms;
using AppCore.Logging;

namespace EduApp
{
	[ViewFor(typeof(SignupPageViewModel))]
	public partial class SignupPage : ViewPage<SignupPageViewModel>
	{
        public SignupPage()
		{
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            if (Xamarin.Forms.Device.OS == TargetPlatform.Android)
                entryUsr.OnEntryCompleted = () => entryPwd.FocusEntry();

        }

		void CloseTapped (object sender, EventArgs args)
		{
			ViewModel.Close ();
		}

	}
}
