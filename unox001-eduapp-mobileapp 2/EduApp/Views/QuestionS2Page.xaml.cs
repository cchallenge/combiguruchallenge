﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Nostromo;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(QuestionS2PageViewModel))]
	public partial class QuestionS2Page : BaseQuestionViewPage<QuestionS2PageViewModel>
	{
		public QuestionS2Page()
		{
			InitializeComponent();
			this.ToolbarItems.Add(new ToolbarItem("", "x_close_white.png", () => ViewModel.CloseViewCommand.Execute(null)));

			this.WhenAnyValue(t => t.ViewModel.LeftButtons).Subscribe(b => AddButtons(b, true));
			this.WhenAnyValue(t => t.ViewModel.RightButtons).Subscribe(b => AddButtons(b));
		}

		void AddButtons (IEnumerable<S2Response> items, bool left = false)
		{
			var y = 0;
			var x = 0;

			if (!left)
				x++;
			
			foreach (var i in items) {
				var btn = new S2ButtonView()
				{
					HeightRequest = 35,
					HorizontalOptions = LayoutOptions.Fill,
					VerticalOptions = LayoutOptions.Center,
					Title = i.Text,
					StatoRisposta = i.StatoRisposta,
					AnswerId = i.ID,
					Margin = new Thickness(3f, 0f),
					ViewCommand = ViewModel.ItemCommand
				};

				btn.BindingContext = i;
				btn.SetBinding(S2ButtonView.StatoRispostaProperty, new Binding("StatoRisposta"));

				this.container.Children.Add(btn, x, y);
				y++;
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			this.ViewModel.Appearing();
		}

        protected override bool OnBackButtonPressed()
        {
            ViewModel.CloseViewCommand.Execute(null);
            return true;
        }

    }
}
