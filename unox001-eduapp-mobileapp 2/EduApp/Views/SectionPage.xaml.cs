﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EduApp.Core;
using Nostromo;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor(typeof(SectionPageViewModel))]
	public partial class SectionPage : ViewPage
	{
		public SectionPage ()
		{
			InitializeComponent ();
			this.ToolbarItems.Add(new ToolbarItem("", "x_close_white.png", () => (ViewModel as SectionPageViewModel).CloseViewCommand.Execute(null)));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.Appearing();
		}

        protected override bool OnBackButtonPressed()
        {
            (ViewModel as SectionPageViewModel).CloseViewCommand.Execute(null);
            return true;
        }
    }
}
