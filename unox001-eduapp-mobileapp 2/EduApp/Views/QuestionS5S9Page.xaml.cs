﻿using System;
using System.Collections.Generic;
using EduApp.Core;
using Nostromo;
using ReactiveUI;
using Xamarin.Forms;

namespace EduApp
{
	[ViewFor (typeof (QuestionS5S9PageViewModel))]
	public partial class QuestionS5S9Page : BaseQuestionViewPage<QuestionS5S9PageViewModel>
	{
		public QuestionS5S9Page ()
		{
			InitializeComponent ();

			this.ToolbarItems.Add (new ToolbarItem ("", "x_close_white.png", () => ViewModel.CloseViewCommand.Execute (null)));
			this.WhenAnyValue (x => x.ViewModel.Buttons).Subscribe (x => AddButtons (x));
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			ViewModel.Appearing ();

		}

        protected override bool OnBackButtonPressed()
        {
            ViewModel.CloseViewCommand.Execute(null);
            return true;
        }

        void AddButtons (IEnumerable<S5S9Response> items)
		{
			var y = 0;
			foreach (var i in items) {
				var btn = new S5S9ButtonView () {
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.Center,
					Title = i.Text,
					StatoRisposta = i.StatoRisposta,
					AnswerId = i.ID.ToString(),
					ViewCommand = ViewModel.ItemCommand
				};


				//if (y < 4) {
				//	btn.Title = String.Format("{0} AAAAAAAASSDDSDF D S D F G GFDSDF GFDSDFG kojihugyt pouigyfuy puioliukujyht puooluikufjyh upoiylukyujyh oliukujyh", i.Text);
				//}
				btn.BindingContext = i;

				btn.SetBinding (S5S9ButtonView.StatoRispostaProperty, new Binding ("StatoRisposta"));
				            
				this.container.Children.Add (btn);
				container.HorizontalOptions = LayoutOptions.FillAndExpand;
				y++;
			}
		}

	}
}
