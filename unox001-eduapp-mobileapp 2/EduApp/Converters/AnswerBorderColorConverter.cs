using System;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	public class AnswerBorderColorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var state = (AnswerState)value;
			switch (state)
			{
				case AnswerState.Corretto:
				case AnswerState.InAttesa:
				case AnswerState.Sbagliato:
				case AnswerState.DopoCorretto:
					return Color.FromHex("6A6F76");
				default:
					return Color.Transparent;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			// You probably don't need this, this is used to convert the other way around
			// so from color to yes no or maybe
			throw new NotImplementedException();
		}
	}

	
}
