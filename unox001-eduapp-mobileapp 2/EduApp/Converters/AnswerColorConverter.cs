﻿using System;
using EduApp.Core;
using Xamarin.Forms;

namespace EduApp
{
	public class AnswerColorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var state = (AnswerState)value;

			Color clearColor = Color.Transparent;
			if (parameter != null) {
				clearColor = Color.FromHex((string)parameter);
			}

			switch (state)
			{
				case AnswerState.Corretto:
					return Color.FromHex(Globals.ColorCorretto);
				case AnswerState.InAttesa:
					return Color.FromHex(Globals.ColorInAttesa);
				case AnswerState.Sbagliato:
					return Color.FromHex(Globals.ColorSbagliato);
				case AnswerState.DopoCorretto:
					return Color.FromHex(Globals.ColorDopoCorretto);
				default:
					return clearColor;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			// You probably don't need this, this is used to convert the other way around
			// so from color to yes no or maybe
			throw new NotImplementedException();
		}
	}

	public class AnswerColorTextConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var state = (AnswerState)value;
			switch (state)
			{
				case AnswerState.Corretto:
				case AnswerState.InAttesa:
				case AnswerState.Sbagliato:
				case AnswerState.DopoCorretto:
					return Color.White;
				default:
					return Color.FromHex("83878D");
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			// You probably don't need this, this is used to convert the other way around
			// so from color to yes no or maybe
			throw new NotImplementedException();
		}
	}
}
