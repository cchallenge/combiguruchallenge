﻿using NUnit.Framework;
using System;
using AppCore;
using ServerCore;
using System.Threading.Tasks;
using System.Linq;
using ws.Modules;
using System.IO;
using System.Reflection;
using ServerCore.Interfaces;
using Unox.Models.ImporterModels;
using Unox;
using System.Collections.Generic;

namespace EduAppWS.Tests
{
    [TestFixture]
    public class UnoxEduAppTest
    {
        ServerCore.Testing.AppCoreIntegrationTest testHelper = new ServerCore.Testing.AppCoreIntegrationTest();

        Nancy.TinyIoc.TinyIoCContainer _container;
        [OneTimeSetUp]
        public void TestSetup()
        {
            UnoxEduAppModule.UnoxAppEduModule.TESTING = true;
            testHelper.Prepare(bootstrapper => bootstrapper
                                       .EnableNet45DefaultModules(useTestingModules: true)
                               .EnableTokenAuth()
                                       .Register<UnoxEduAppModule.UnoxAppEduModule>(),
                               (r, container, pipelines) =>
                               {
                                   r.EnableInMemoryBEEntityModule()
                                    .EnableFakeContentTestingModule()
                                    .EnableBETestingAuthModule(true);
                                   _container = container;
                               });
            EnsureImportExcel();

        }

        bool alreadyImportedInThisSection = false;
        public void EnsureImportExcel()
        {
            // #################################### modificare questi valori per caricare altri livelli (serve per far girare i json) SOLO WINDOWS puà fare il json
            string level = "1";
            string file = $"Level{level}.xlsx";
            // #############################################################################################

            if (!alreadyImportedInThisSection)
            {
                var importmodule = _container.Resolve<ImportCRUDModule>();
                alreadyImportedInThisSection = true;
                // STEP 1 - init module

                var fi = new FileInfo(Assembly.GetExecutingAssembly().Location);
                var filename = Path.Combine(fi.Directory.FullName, "files", file);
                var jsonfilename = Path.Combine(fi.Directory.FullName, "json", $"{level}.json");

                if (File.Exists(jsonfilename))
                {
                    string sjson = File.ReadAllText(jsonfilename);
                    ImpLevel imported = Newtonsoft.Json.JsonConvert.DeserializeObject<ImpLevel>(sjson);
                    Unox.Models.RawModels.CheckResult cr = new Unox.Models.RawModels.CheckResult();
                    cr.Success = true;
                    var bok = Task.Run(() => importmodule.NormalizeLevel(imported, cr)).Result.Success;

                   

                }
                else
                {
                    using (var stream = File.OpenRead(filename))
                    {
                        var contents = _container.Resolve<IContentBEAppModule>();
                        //contents.AddContent(null, "Level1_new.xlsx", stream);
                        contents.AddContent(null, file, stream);
                    }

                    // STEP 2 - full import Excel 
                    CMSImport entity = new CMSImport();
                    entity.Entity = new Import();
                    entity.Entity.Level = level;
                    entity.Entity.UserName = "test";
                    entity.Entity.ExcelFile = file;

                    var imported = Task.Run(() => importmodule.TryImport(entity,true)).Result;
                }

                var engine = importmodule.EE.Engine;
                Unox.Models.RawModels.Template s4 = engine.New<Unox.Models.RawModels.Template>();

                s4.Name = "S4";
                s4.Titles = new Dictionary<string, string>(){
                {  "IT", "DESCRIZIONE TEMPLATE" },
                {  "EN", "EN DESCRIZIONE TEMPLATE"  },
                {  "DE", "DE DESCRIZIONE TEMPLATE"  },
                {  "FR", "FR DESCRIZIONE TEMPLATE"  },
                {  "ES", "ES DESCRIZIONE TEMPLATE" } };

                Task.Run(() =>  engine.Update<Unox.Models.RawModels.Template>(s4).Result);

                Unox.Models.RawModels.Template s2 = engine.New<Unox.Models.RawModels.Template>();
                s2.Name = "S2";
                s2.Titles = new Dictionary<string, string>(){
                {  "IT", "s2 DESCRIZIONE TEMPLATE" },
                {  "EN", "s2 EN DESCRIZIONE TEMPLATE"  },
                {  "DE", "s2 DE DESCRIZIONE TEMPLATE"  },
                {  "FR", "s2 FR DESCRIZIONE TEMPLATE"  },
                {  "ES", "s2 ES DESCRIZIONE TEMPLATE" } };

                Task.Run(() => engine.Update<Unox.Models.RawModels.Template>(s2).Result);

                Unox.Models.RawModels.Template s5_s9 = engine.New<Unox.Models.RawModels.Template>();
                s5_s9.Name = "S5_S9";
                s5_s9.Titles = new Dictionary<string, string>(){
                {  "IT", "S5_S9 DESCRIZIONE TEMPLATE" },
                {  "EN", "S5_S9 EN DESCRIZIONE TEMPLATE"  },
                {  "DE", "S5_S9 DE DESCRIZIONE TEMPLATE"  },
                {  "FR", "S5_S9 FR DESCRIZIONE TEMPLATE"  },
                {  "ES", "S5_S9 ES DESCRIZIONE TEMPLATE" } };

                Task.Run(() => engine.Update<Unox.Models.RawModels.Template>(s5_s9).Result);


                Unox.Models.RawModels.Template s3 = engine.New<Unox.Models.RawModels.Template>();
                s3.Name = "S3";
                s3.Titles = new Dictionary<string, string>(){
                {  "IT", "s3 DESCRIZIONE TEMPLATE" },
                {  "EN", "s3 EN DESCRIZIONE TEMPLATE"  },
                {  "DE", "s3 DE DESCRIZIONE TEMPLATE"  },
                {  "FR", "s3 FR DESCRIZIONE TEMPLATE"  },
                {  "ES", "s3 ES DESCRIZIONE TEMPLATE" } };

                Task.Run(() => engine.Update<Unox.Models.RawModels.Template>(s3).Result);


                List<string> icons = new List<string>()
                {
                    "Uva", "Umidita", "Chef","Vapore", "Ricetta"

                };

                foreach (string icon in icons)
                {
                    Unox.Models.RawModels.AppIcon icon1 = engine.New<Unox.Models.RawModels.AppIcon>();
                    icon1.Name = icon;
                    icon1.FileKey = $"/images/{icon}.png";
                    icon1.ImageUrl = $"contents/images/{icon}.png";
                    Task.Run(() => engine.Update<Unox.Models.RawModels.AppIcon>(icon1).Result);
                }


                Unox.Models.RawModels.AppSettings unoxurl = engine.New<Unox.Models.RawModels.AppSettings>();
                unoxurl.Name = "UnoxUrl";
                unoxurl.Value = @"http://www.unox.com" ;

                engine.Update <Unox.Models.RawModels.AppSettings>(unoxurl);

            }
        }

        [SetUp]
        public void ResetTest()
        {
            AppCore.Testing.MachineConnectivityStatus = AppCore.Interfaces.ConnectivityStatus.WifiOrCable;
        }

        [Test]
        public async Task TestCase()
        {
            var appModule = testHelper.AppCoreBootstrapper.MainContext.ResolveModule<UnoxEduAppModule.UnoxAppEduModule>();

            var res = await appModule.Test();

            Assert.IsTrue(res.Result);

            AppCore.Testing.MachineConnectivityStatus = AppCore.Interfaces.ConnectivityStatus.None;

            //Assert.Throws<Exception>(() => Task.Run(() => appModule.Test()).Wait());

            res = await appModule.Test();

            Assert.IsNull(res);
        }
        [Test]
        public async Task TestShouldGetChallenge()
        {
            string reqlevel = "2";
            var appModule = testHelper.AppCoreBootstrapper.MainContext.ResolveModule<UnoxEduAppModule.UnoxAppEduModule>();
            var importmodule = _container.Resolve<ImportCRUDModule>();
            // STEP 3 - verifica full import
            var engine = importmodule.EE.Engine;
            var levels = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Level>()).Result;
            var level = (from l in levels
                         where l.Name == reqlevel
                         select l).FirstOrDefault();
            var res = await appModule.Challenge(level.CorrelationId, "2", "it");

            Assert.IsNotNull(res);
        }

        [Test]
        public async Task TestShouldGetALesson()
        {
            /// selezioniamo quale lezione vogliamo
            /// 
            string reqlevel = "1";
            string reqsection = "1_6";
            string reqlesson = "LESSON_1";
            var appModule = testHelper.AppCoreBootstrapper.MainContext.ResolveModule<UnoxEduAppModule.UnoxAppEduModule>();
            var importmodule = _container.Resolve<ImportCRUDModule>();
            // STEP 3 - verifica full import
            var engine = importmodule.EE.Engine;
            var levels = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Level>()).Result;
            var level = (from l in levels
                         where l.Name == reqlevel
                         select l).FirstOrDefault();

            var sections = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Section>("childOf", level)).Result;
            var section = (from s in sections
                           where s.Name == reqsection
                           select s).FirstOrDefault();

            var lessons = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Lesson>("childOf", section)).Result;
            var lesson = (from l in lessons
                          where l.Name == reqlesson
                          select l).FirstOrDefault();

            string corrid = lesson.CorrelationId;
            var res = await appModule.Lesson(corrid, "2", "it");

            Assert.IsNotNull(res);
        }

        [Test]
        public async Task TestShouldGetLearningPath()
        {
            var appModule = testHelper.AppCoreBootstrapper.MainContext.ResolveModule<UnoxEduAppModule.UnoxAppEduModule>();
            var importmodule = _container.Resolve<ImportCRUDModule>();
            // STEP 3 - verifica full import
            var engine = importmodule.EE.Engine;
            var level = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Level>()).Result.FirstOrDefault();
            var section = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Section>("childOf", level)).Result.FirstOrDefault();

            var res = await appModule.LearningPath("2", "it");

            Assert.IsNotNull(res);
        }

        [Test]
        public async Task TestShouldGetUserLevel()
        {
            var appModule = testHelper.AppCoreBootstrapper.MainContext.ResolveModule<UnoxEduAppModule.UnoxAppEduModule>();

            var res = await appModule.UserLevel();

            Assert.IsNotNull(res);
        }

        [Test]
        public async Task TestGetUnoxUrl()
        {
            var appModule = testHelper.AppCoreBootstrapper.MainContext.ResolveModule<UnoxEduAppModule.UnoxAppEduModule>();

            var res = await appModule.UnoxUrl();

            Assert.IsNotNull(res);
        }

        [Test]
        public async Task TestSetLessonFinished()
        {
            var appModule = testHelper.AppCoreBootstrapper.MainContext.ResolveModule<UnoxEduAppModule.UnoxAppEduModule>();
            var importmodule = _container.Resolve<ImportCRUDModule>();
            // STEP 3 - verifica full import
            var engine = importmodule.EE.Engine;
            var level = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Level>()).Result.FirstOrDefault();
            var section = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Section>("childOf", level)).Result.FirstOrDefault();
            var lesson = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Lesson>("childOf", section)).Result.FirstOrDefault();
            string corrid = lesson.CorrelationId;
           
            //await TestShouldGetLearningPath();
            //var appModule = testHelper.AppCoreBootstrapper.MainContext.ResolveModule<UnoxEduAppModule.UnoxAppEduModule>();
             
            var res = await appModule.SetLessonFinished(corrid);

            var achievements = await engine.GetAll<Unox.Models.RawModels.UserAchievement>();

            Assert.IsNotNull(res);
        }

        [Test]
        public async Task TestGetAllLessonsForSection()
        {
            string reqlevel = "1";
            string reqsection = "1_6";
           
            var appModule = testHelper.AppCoreBootstrapper.MainContext.ResolveModule<UnoxEduAppModule.UnoxAppEduModule>();
            var importmodule = _container.Resolve<ImportCRUDModule>();
            // STEP 3 - verifica full import
            var engine = importmodule.EE.Engine;
            var levels = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Level>()).Result;
            var level = (from l in levels
                         where l.Name == reqlevel
                         select l).FirstOrDefault();

            var sections = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Section>("childOf", level)).Result;
            var section = (from s in sections
                           where s.Name == reqsection
                           select s).FirstOrDefault();

            var res = await appModule.Lessons(section.CorrelationId, "2", "it");

            Assert.True(res.Count() > 0);
        }

        [Test()]
        public void TestInit()
        {
            var importmodule = _container.Resolve<ImportCRUDModule>();
            var e = new ExcelParser(importmodule.EE.Engine);
            Assert.IsNotNull(e);

        }

        [Test()]
        public void TestRebuildTree()
        {
            var importmodule = _container.Resolve<ImportCRUDModule>();
            var ok = Task.Run(() => importmodule.RebuildTree()).Result;

            var engine = importmodule.EE.Engine;
            var tree = Task.Run(() => engine.GetAll<Unox.Models.RawModels.Tree>()).Result;
            Assert.IsNotNull(tree);

        }

        [Test()]
        public void TestParseLevel()
        {
            var importmodule = _container.Resolve<ImportCRUDModule>();
            var e = new ExcelParser(importmodule.EE.Engine);
            ImpLevel imported = null;
            Unox.Models.RawModels.CheckResult cr = new Unox.Models.RawModels.CheckResult();
            cr.Success = true;
            Task.Run(() => imported = e.Import("1", @"C:\temp\Level1.xlsx", ref cr));

            //Assert.AreEqual (1, Task.Run (() => e.AsReadOnlyEntitiesToDelete ().Count).Result);
            //Assert.AreEqual (0, Task.Run (() => e.AsReadOnlyEntities () ["User"].Count ()).Result);
            //}

            //disposed deve dare eccezione
            //Assert.Throws<InvalidOperationException> (() => e.Count<User> ());
        }


        [Test()]
        public void TestImportLevel()
        {
            EnsureImportExcel();
            var importmodule = _container.Resolve<ImportCRUDModule>();
            // STEP 3 - verifica full import
            var engine = importmodule.EE.Engine;
            var level = engine.GetAll<Unox.Models.RawModels.Level>();
            Assert.IsNotNull(level);

        }
    }
}
