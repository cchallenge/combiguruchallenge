﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharedCore.Entities;

namespace UnoxEduAppModule
{
    public enum TemplateType
    {
        Unknown = -1,
        S1,
        S2,
        S3,
        S4,
        S5_S9,
        S6_S7,
        S8,
        S10_S11_S12,
        S13,
        S14
    }


    public class LearningPath : Entity
    {
        public IEnumerable<Level> Levels { get; set; }
        public string[] Contents { get; set; }
        //AR+RP CHiave formata da Lang+_+MeasureUnit
        public string Key { get; set; }
    }


    public class Level {
        public string CorrelationId { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string ChallengeImage { get; set; }
        public string ChallengeTitle { get; set; }
        public string ScienceImage { get; set; }
        public string ScienceTitle { get; set; }
        public string ScienceHTML { get; set; }
        public IEnumerable<Section> Sections { get; set; }
    }

    public class Section {
        public string CorrelationId { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public int NLessons { get; set; }
    }

    public class LessonHeader
    {
        public string CorrelationId { get; set; }
        public string Description { get; set; }
        public bool AccessibleLesson { get; set; }
    }

    public class _ext_QualifiedUser {
        public string UniqueIdentifier { get; set; }
        public string Nickname { get; set; }
        public int RankPosition { get; set; }
        public int Points { get; set; }

    }

    public class _ext_Ranking
    {
        public Boolean Compressed { get; set; }
        public IEnumerable<_ext_QualifiedUser> Ranking { get; set; }
    }


    public class _ext_ChallengeHeader
    {
        public string CorrelationId { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public bool Completed { get; set; }
    }

    public class _ext_Challenge : Lesson {

        public override string ToString() {
            string contents = "";
            if (CorrelationId != null)
                contents += " CorrelationId: " + CorrelationId;
            if (Description != null)
                contents += " Description: " + Description;
            if (Questions != null)
                contents += " Domande: notNull";
            if (Contents != null)
                contents += " # Contents: " + Contents.Length;

            return contents;
        }

    }

    public class Lesson {
		public string CorrelationId { get; set; }
		public string Description { get; set;}
		public IEnumerable<Question> Questions { get; set; }
		public string[] Contents { get; set; }
	}

	public class Question { 
		public string CorrelationId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public TemplateType TemplateType { get; set; }
		public JObject Payload { get; set; }
	}

    public class _ext_ChallengeStat
    {
        public int _ext_UserPoints { get; set; }
        public IEnumerable<_ext_ChallengeHeader> ChallengeList { get; set; }
    }


    public class _ext_UserStatsDefaultPath : UserStats {
        public int _ext_UserPoints { get; set; }
    }

    public class _ext_UserProfile {

        public string UserId { get; set; }
        public string Nickname { get; set; }
        public string Institution { get; set; }
        public int Score { get; set; }
    }

	public class UserStats {
		public string UserId { get; set; }
        public IEnumerable<LevelStat> LevelsStat { get; set;}

    }

	public class LevelStat { 
		public string CorrelationId { get; set;}
		/// <summary>
		/// Gets or sets the sbiadimento.
		/// </summary>
		/// <value>Valore da 0 a 1 dove 1 significa totalmente visibile mentre 0 significa trasparente</value>
		public float Sbiadimento { get; set;}
		/// <summary>
		/// Gets or sets the percentuale.
		/// </summary>
		/// <value>Percentuale livello calcolata dividento il numero totale di lezioni per tutte le sezioni diviso il numero di lezioni passate per tutte le sezioni</value>
		public float Percentuale { get; set;}
		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="T:UnoxEduAppModule.LevelStat"/> is current level.
		/// </summary>
		/// <value>Serve per sapere quale è il livello corrente dell'utente che l'app deve abilitare</value>
		public bool IsCurrentLevel { get; set;}
        /// <summary>
        /// True se il livello è stato fatto dall'utente
        /// </summary>
        public bool IsEnabled { get; set; }
        public IEnumerable<SectionStat> SectionsStat { get; set;}
	}

	public class SectionStat { 
		public string CorrelationId { get; set; }
		/// <summary>
		/// Gets or sets the sbiadimento.
		/// </summary>
		/// <value>Valore da 0 a 1 dove 1 significa totalmente visibile mentre 0 significa trasparente</value>
		public float Sbiadimento { get; set; }
		/// <summary>
		/// Gets or sets the percentuale.
		/// </summary>
		/// <value>Percentuale sezione calcolata dividendo il numero totale di lezioni diviso il numero di lezioni passate</value>
		public float Percentuale { get; set; }
		public int LessonsTotalNumber { get; set; }
		public int LessonsFinishedNumber { get; set; }
	}
}
