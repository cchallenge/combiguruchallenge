﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AppCore;
using AppCore.Interfaces;
using Newtonsoft.Json.Linq;
using SharedCore.Interfaces;

namespace UnoxEduAppModule
{
    public class UnoxAppEduModule : IAppModule
    {
        public static bool TESTING { get; set; } = false;
        readonly IRestAppModule rest;

        string UserIdentifier
        {
            get
            {
                if (TESTING)
                    return Context.UserIdentity?.UserIdentifier ?? "222";

                return Context.UserIdentity?.UserIdentifier;

            }
        }

        public UnoxAppEduModule(IRestAppModule rest)
        {
            this.rest = rest;
        }

        public IAppCoreContext Context
        {
            get;
            set;
        }


        public class TestResult
        {
            public bool Result { get; set; }
        }

        public async Task<TestResult> Test()
        {
            var response = await rest.Get("/edu/test");

            return response.ContentAs<TestResult>();
        }

        public async Task<string> UnoxUrl()
        {
            var response = await rest.Get($"/edu/unoxurl");

            return response.Content.ToString();
        }

        //Richiede le lezioni per una section con livello di utente
        public async Task<IEnumerable<LessonHeader>> Lessons(string sectionId, string measureUnit, string lang)
        {
            var response = await rest.Get($"/edu/lessons/{sectionId}?measureUnit={measureUnit}&lang={lang}&userId={UserIdentifier}");

            return response.ContentAs<IEnumerable<LessonHeader>>();
        }

        //Richiede una lezione
        public async Task<Lesson> Lesson(string lessonId, string measureUnit, string lang)
        {
            var response = await rest.Get($"/edu/lesson/{lessonId}?measureUnit={measureUnit}&lang={lang}&userId={UserIdentifier}");

            return response.ContentAs<Lesson>();
        }


        //Richiede un challenge
        public async Task<Lesson> Challenge(string levelId, string measureUnit, string lang)
        {
            var response = await rest.Get($"/edu/challenge/{levelId}?measureUnit={measureUnit}&lang={lang}&userId={UserIdentifier}");

            return response.ContentAs<Lesson>();
        }

        //Richiede il learning path
        public async Task<LearningPath> LearningPath(string measureUnit, string lang)
        {
            var response = await rest.Get($"/edu/learningPath?measureUnit={measureUnit}&lang={lang}&userId={UserIdentifier}");

            return response.ContentAs<LearningPath>();
        }

        public async Task<UserStats> UserLevel()
        {
            var response = await rest.Get($"/edu/learningPath/level?userId={UserIdentifier}");

            return response.ContentAs<UserStats>();
        }

        //Imposta a terminata una lezione
        public async Task<UserStats> SetLessonFinished(string lessonId)
        {
            var response = await rest.Get($"/edu/lesson/{lessonId}/finished?userId={UserIdentifier}");
            return response.ContentAs<UserStats>();
        }

        //Imposta a terminato un challenge
        public async Task<UserStats> SetChallengeFinished(string levelId)
        {
            var response = await rest.Get($"/edu/challenge/{levelId}/finished?userId={UserIdentifier}");
            return response.ContentAs<UserStats>();
        }

        /// //////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Nuove chiamate, alcune ridefinite per sfruttare nuove funzionalita' backend e alcune differenziate 
        /// perche ritornano oggetti aumentati e/o nuovi non interpretabili dal frontend 'vecchio'.
        /// //////////////////////////////////////////////////////////////////////////////////////////////////////

        //Terminata una challenge (finale o shortcut) nel percorso base
        public async Task<_ext_UserStatsDefaultPath> _ext_SetLearningPathChallengeFinished(string levelId)
        {
            var response = await rest.Get($"/edu/lpathchallenge/{levelId}/finished?userId={UserIdentifier}");
            return response.ContentAs<_ext_UserStatsDefaultPath>();
        }

        //Richiede un challenge per il percorso base (i.e., finale o shortcut)
        public async Task<Lesson> _ext_GetLearningPathChallenge(string levelId, string measureUnit, string lang)
        {
            var response = await rest.Get($"/edu/lpathchallenge/{levelId}?measureUnit={measureUnit}&lang={lang}&userId={UserIdentifier}");

            return response.ContentAs<Lesson>();
        }

        //Richiede le lezioni per una section con livello di utente
        public async Task<IEnumerable<LessonHeader>> _ext_Lessons(string sectionId, string measureUnit, string lang)
        {
            var response = await rest.Get($"/edu/_ext_lessons/{sectionId}?measureUnit={measureUnit}&lang={lang}&userId={UserIdentifier}");

            return response.ContentAs<IEnumerable<LessonHeader>>();
        }

        //richiede il livello utente + punti utente
        public async Task<_ext_UserStatsDefaultPath> _ext_UserLevel()
        {
            var response = await rest.Get($"/edu/_ext_learningPath/level?userId={UserIdentifier}");
            return response.ContentAs<_ext_UserStatsDefaultPath>();
        }

        //dichiara una lezione conclusa. Ritorna un obj aumentato
        public async Task<_ext_UserStatsDefaultPath> _ext_SetLessonFinished(string lessonId)
        {
            var response = await rest.Get($"/edu/_ext_lesson/{lessonId}/finished?userId={UserIdentifier}");
            return response.ContentAs<_ext_UserStatsDefaultPath>();
        }

        public async Task<object> _ext_UpdateOrSetNickname(string nickname)
        {
            var response = await rest.Get($"/edu/_ext_update_set_nickname/{nickname}?userId={UserIdentifier}");

            return response.Content;
        }

        public async Task<object> _ext_SetUserInstitution(string institution)
        {
            var response = await rest.Get($"/edu/_ext_set_user_institution/{institution}?userId={UserIdentifier}");

            return response.Content;
        }

        public async Task<_ext_UserProfile> _ext_GetUserProfile() {
            var response = await rest.Get($"/edu/_ext_get_user_profile/user?userId={UserIdentifier}");
            return response.ContentAs<_ext_UserProfile>();
        }


        //ritorna il ranking dei utenti.
        public async Task<_ext_Ranking> _ext_GetInstitutionRanking(string institutionId = null)
        {
            RestResponse response;
            if (institutionId == null)
            {
                response = await rest.Get($"/edu/_ext_ranking/nullInstitute");
            }
            else {
                response = await rest.Get($"/edu/_ext_ranking/{institutionId}/filterInstitute?userId={UserIdentifier}");
            }
            return response.ContentAs<_ext_Ranking>();
        }

        public async Task<IEnumerable<_ext_ChallengeHeader>> _ext_GetChallenges(string measureUnit, string lang)
        {
            var response = await rest.Get($"/edu/_ext_challenges/all?userId={UserIdentifier}&lang={lang}&measureUnit={measureUnit}");

            return response.ContentAs<IEnumerable<_ext_ChallengeHeader>>();
        }

        public async Task<_ext_Challenge> _ext_GetChallenge(string challengeId, string measureUnit, string lang)
        {
            var response = await rest.Get($"/edu/_ext_challenge/{challengeId}?measureUnit={measureUnit}&lang={lang}&userId={UserIdentifier}");

            return response.ContentAs<_ext_Challenge>();
        }

        public async Task<_ext_ChallengeStat> _ext_SetChallengeFinished(string challengeId, string measureUnit, string lang, string totalCorrect, string totalQuestions)
        {
            var response = await rest.Get($"/edu/_ext_challenge/{challengeId}/finished?userId={UserIdentifier}&lang={lang}&measureUnit={measureUnit}&totalCorrect={totalCorrect}&totalQuestions={totalQuestions}");
            return response.ContentAs<_ext_ChallengeStat>();
        }

    }
}
