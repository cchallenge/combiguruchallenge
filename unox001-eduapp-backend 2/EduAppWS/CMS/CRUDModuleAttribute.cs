using System;

namespace EduAppWS.CMS
{

	public class CRUDModuleAttribute: Attribute {
		//public string TableName { get; set; }
		public string PrimaryKeyName { get; set; }

		public string EntityName {
			get;
			set;
		}
	}

}
