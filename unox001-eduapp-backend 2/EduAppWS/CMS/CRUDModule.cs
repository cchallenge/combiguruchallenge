using System;
using System.Linq;
using ServerCore.Interfaces;
using SharedCore.Entities;

namespace EduAppWS.CMS
{

	public abstract class CRUDModule<TCMSEntity,TEntity>: CRUDBaseModule<TCMSEntity,TEntity>
		where TCMSEntity : CMSEntity<TEntity>, new()
		where TEntity : Entity
	{

		protected CRUDModule(IBEEntityModule ee):base("", ee) {
			var attribute = this.GetType ().GetCustomAttributes (true).OfType<CRUDModuleAttribute> ().First ();
			_primarykeyname = attribute.PrimaryKeyName ?? "id";
			//_tablename = attribute.TableName ?? typeof(TEntity).Name;
		}

		string _entityName;
		public override string EntityName {
			get {
				if (String.IsNullOrEmpty (_entityName)) {
					_entityName = this.GetType ().GetCustomAttributes (true).OfType<CRUDModuleAttribute> ().First ().EntityName ?? typeof(TEntity).Name.ToLower() ;
				}
					
				return _entityName;
			}
		}

		//readonly string _tablename;
		readonly string _primarykeyname;
		#region implemented abstract members of CRUDBaseModule
		//public override string TableName {
		//	get {
		//		return _tablename;
		//	}
		//}
		public override string PrimaryKeyName {
			get {
				return _primarykeyname;
			}
		}
		#endregion
	}

}
