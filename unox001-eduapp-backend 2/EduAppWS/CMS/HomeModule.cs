using System;
using Nancy;
using System.Data;
using System.Configuration;
using System.Linq;
using Nancy.ModelBinding;
using Nancy.Security;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using EduAppWS.Modules;
using ServerCore.Interfaces;

namespace Montaggi.CMS
{

	public class HomeModule : DbModule
	{
		public HomeModule (IBEEntityModule ee) : base ("/", ee)
		{
            Get [""] = x => ReplyRoot() ;

			Get ["app.js"] = x => View ["app.js.sshtml", App].WithContentType("text/javascript");
		}

		object ReplyRoot() {
			
			if (Request.Query.action == "logout") {
				return Response
					.AsRedirect ("?")
					.WithCookie("token", "");
			}

			try {
				//				this.RequiresClaims("staff");
				this.RequiresAuthentication();
				return View["home", App.AsJson];
			}
			catch 
			{}

			return View ["login", App];
		}

	}
}

