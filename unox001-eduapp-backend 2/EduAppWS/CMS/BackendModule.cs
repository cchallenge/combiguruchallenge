using System;

namespace EduAppWS.CMS
{
	public class BackendModule: Nancy.NancyModule
	{
		protected App App { 
			get { 
				object app = null;
				Context?.Items.TryGetValue ("app", out app);
				return (app as Lazy<App>).Value;
			}
		}


		public BackendModule (string root) : base (root)
		{
			init ();
		}

		public BackendModule () : base ()
		{
			init ();
		}


		void init() {
			this.Before.AddItemToEndOfPipeline ( c => {
				c.Items.Add("app", new Lazy<App>(()=> new App(c), true));
				return null;
			});
		}
	}
}

