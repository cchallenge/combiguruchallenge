using System;
using System.Collections.Generic;
using System.Linq;

namespace EduAppWS.CMS
{
	public static class Claims
	{
		static Dictionary<Claim, string> AllClaims = new Dictionary<Claim, string>()
		{
			{ Claim.SuperAdmin, "SuperAdmin" },
		};

		public static string[] GetAllClaims()
		{
			return AllClaims.Values.Select(c => c).ToArray();
		}

		public static string GetClaim(Claim c)
		{
			return AllClaims.ContainsKey(c) ? AllClaims[c] : null;
		}
	}

	public enum Claim
	{
		SuperAdmin
	}
}
