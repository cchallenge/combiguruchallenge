using System;
namespace EduAppWS.CMS
{
	public class InvalidPropertyException: Exception
	{
		public class Violation
		{
			public string PropertyPath { get; set; }
			public string Message { get; set; }
		}

		public Violation [] Violations { get; protected set;}
		public string PropertyPath { get; private set; }

		public InvalidPropertyException (string propertyPath, string message) : base (message)
		{
			this.Violations = new [] { new Violation { PropertyPath = propertyPath, Message = message } };
		}
	}
}

