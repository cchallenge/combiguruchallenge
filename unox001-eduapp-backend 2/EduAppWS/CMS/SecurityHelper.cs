using System;
using System.Text;
using System.Security.Cryptography;
using System.Linq;

namespace EduAppWS.CMS
{

	public static class SecurityHelper
	{
		static readonly object checksumlock = new object();
		static readonly MD5CryptoServiceProvider checksum = new MD5CryptoServiceProvider ();

		static public string MD5String(string stringtoconvert)
		{
			return md5(stringtoconvert);
		}

		static int hex (int v)
		{
			if (v < 10)
				return '0' + v;
			return 'a' + v-10;
		}

		static string md5 (string input)
		{
			lock (checksumlock) {
				var bytes = checksum.ComputeHash (Encoding.UTF8.GetBytes (input));
				var ret = new char [32];
				for (int i = 0; i < 16; i++){
					ret [i*2] = (char)hex (bytes [i] >> 4);
					ret [i*2+1] = (char)hex (bytes [i] & 0xf);
				}
				return new string (ret);
			}
		}

		const string pw_seed = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz123456789";

		public static string GenerateNewPassword(int pwd_length = 8, string specialCharPw_seed = null)
		{
			Random rnd = new Random();

			var seed = string.IsNullOrEmpty(specialCharPw_seed) ? pw_seed : string.Format("{0}{1}", pw_seed, specialCharPw_seed);
			
			var newPassword = String.Join("", Enumerable
			                              .Range(0, pwd_length)
			                              .Select(i => seed[rnd.Next(seed.Length)])
			                             );
			
			return newPassword;
		}
	}
}

