using System;
using Nancy;

namespace EduAppWS.CMS
{
	public class HttpResponseException:Exception
	{
		public HttpStatusCode StatusCode { get; private set; }
		public HttpResponseException (HttpStatusCode statusCode, string message) : base (message)
		{
			StatusCode = statusCode;
		}

		public HttpResponseException (HttpStatusCode statusCode) : this (statusCode, "")
		{
		}
	}
}

