using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using SharedCore.Entities;

namespace EduAppWS.CMS
{
	
	public class RelatedAttribute : Attribute { 
		public string TargetEntityType { get; set; }
		public string Relation { get; set; }

		public string GetFullName ()
		{
			return $"{TargetEntityType}:{Relation}";
		}
	}


}
