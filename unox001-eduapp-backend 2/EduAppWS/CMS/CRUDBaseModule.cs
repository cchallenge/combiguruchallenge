using System;
using Nancy;
using System.Data;
using System.Configuration;
using System.Linq;
using Nancy.ModelBinding;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using Nancy.Security;
using EduAppWS.Modules;
using Newtonsoft.Json;
using ServerCore.Interfaces;
using SharedCore.Entities;
using SharedCore.Interfaces;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace EduAppWS.CMS
{

	public abstract class CRUDBaseModule : BackendModule
	{
		protected CRUDBaseModule(IBEEntityModule ee) : base(CMSBootstrapper.RESTRootPath)
		{
			EE = ee;
		}

		public IBEEntityModule EE { get; protected set; }

		public abstract Type EntityType { get; }

		public abstract string PrimaryKeyName { get; }

		public abstract string EntityName { get; }

	}

	public abstract class CRUDBaseModule<TCMSEntity, TEntity> : CRUDBaseModule
		where TCMSEntity : CMSEntity<TEntity>, new()
		where TEntity : Entity
	{


		public override Type EntityType { get { return typeof(TEntity); } }
		readonly string _entityName;
		public override string EntityName
		{
			get { return _entityName; }
		}

		public Nancy.Response ResponseJson(object resp)
		{
			//string json = JsonConvert.SerializeObject (resp);

			//return Response.AsText (json)
			//		.WithContentType ("application/json");
			return Response.AsJson(resp);
		}

		//http://0.0.0.0:8080/crud/people?_page=1&_perPage=30&_sortDir=DESC&_sortField=id;
		public CRUDBaseModule(string entityName, IBEEntityModule ee) : base(ee)
		{
			_entityName = entityName;
			this.Before.AddItemToEndOfPipeline(c =>
			{
				readPaginationContext();
				return null;
			});

			this.Get["/" + EntityName] = x => IfCanRead(() =>
			{
				long count = 0;
				return ResponseJson(GetAll(out count))
					.WithHeader("X-Total-Count", count.ToString());
			});
			this.Get["/" + EntityName + "/{id}"] = x => IfCanRead(() => ResponseJson(WrapResponse(() => GetOne((string)x.id))));
			this.Post["/" + EntityName] = x => IfCanWrite(() => ResponseJson(WrapResponse(() => CreateOne())));
			this.Put["/" + EntityName + "/{id}"] = x => IfCanWrite(() => ResponseJson(WrapResponse(() => UpdateOne((string)x.id))));
			this.Delete["/" + EntityName + "/{id}"] = x => IfCanWrite(() => ResponseJson(WrapResponse(() => DeleteOne((string)x.id))));
		}

		protected virtual TCMSEntity BeforeWrite(TCMSEntity entity) { return entity; }
		protected virtual TCMSEntity AfterWrite(TCMSEntity entity) { return entity; }
		protected virtual TCMSEntity AfterRead(TCMSEntity entity) { return entity; }

		protected object WrapResponse(Func<object> doSomething)
		{
			bool wrapNeeded = this.Context.Request.Query.wrap == "1";

			try
			{
				var resp = doSomething();
				if (wrapNeeded)
				{
					return new Response { Success = true, Payload = resp, Errors = new string[] { } };
				}

				return resp;
			}
			catch (Nancy.ErrorHandling.RouteExecutionEarlyExitException ex)
			{
				throw;
			}
			catch (Exception ex)
			{
				if (!wrapNeeded)

                    throw;

				return new Response
				{
					Success = false,
					Payload = null,
					Errors = new string[] {
						ex.Message,
						  ex.GetType().ToString(),
						  ex.ToString()
					}
				};
			}
		}

		protected virtual Nancy.Response IfCanRead(Func<Nancy.Response> resp)
		{
			return resp();
		}

		protected virtual Nancy.Response IfCanWrite(Func<Nancy.Response> resp)
		{
			return resp();
		}

		public int Page { get; private set; }

		public int PageSize { get; private set; }

		public string SortField { get; protected set; }

		public bool SortDescending { get; private set; }

		void readPaginationContext()
		{
			Page = Math.Max(0, (Request.Query._page - 1));
			PageSize = ((Request.Query._perPage > 0) ? Request.Query._perPage : 1000);
			SortField = Request.Query._sortField ?? "";
			SortDescending = Request.Query._sortDir == "DESC";
		}

		object GetAll(out long count)
		{
			return DoGetAll(out count).Select(e => AfterRead(e)).ToList();
		}

		object GetOne(string id)
		{
			return AfterRead(DoGet(id));
		}

		object CreateOne()
		{
			var entity = this.Bind<TCMSEntity>();
			entity = BeforeWrite(entity);
			DoCreate(entity);
			return AfterWrite(entity);
		}

		object UpdateOne(string id)
		{
			var entity = this.Bind<TCMSEntity>();
			entity = BeforeWrite(entity);
			DoUpdate(entity, id);
			return AfterWrite(entity);
		}

		object DeleteOne(string id)
		{
			return DoDelete(id);
		}

		protected virtual IEnumerable<TCMSEntity> DoGetAll(out long count)
		{
			var dict = getDictionaryForFilterQuery();

			var allResult = createQueryEntity(dict);

			allResult = getFilterQuery(allResult, dict);

			allResult = customQuery(allResult);

			count = allResult.Count();

			if (Page > 0)
			{
				allResult = allResult.Skip(PageSize * Page);
			}

			if (!String.IsNullOrEmpty(this.SortField))
			{
				if (this.SortField != "id")
				{
					//FIXME: Questo è un workaround per gestire l'eccezione camelcase
					var sortField = $"{this.SortField.First().ToString().ToUpper()}{this.SortField.Substring(1)}";
					allResult = allResult.OrderBy(sortField, !this.SortDescending);
				}
			}

			var resource = Task.Run(() => allResult.Take(PageSize).GetAsync()).Result;

			return resource.Select((e) => new TCMSEntity { Entity = e });
		}

		protected virtual IEntityQuery<TEntity> createQueryEntity(Dictionary<string, object> filter)
		{
			if (filter != null)
			{
				var relation = filter
					.Where(kv => kv.Key.StartsWith("childOf", StringComparison.InvariantCulture) || kv.Key.StartsWith("parentOf", StringComparison.InvariantCulture))
					.ToDictionary(kv => kv.Key, kv => kv.Value);

				if (relation?.Any() ?? false)
				{
					var expr = getQueryExpressions(relation);

					return EE.Engine.Query<TEntity>(QueryMode.All, expr.ToArray());
				}
			}

			return EE.Engine.Query<TEntity>();
		}

		IEnumerable<Expression<Func<RelationQuery.Opening, RelationQuery.Closing>>> getQueryExpressions(Dictionary<string, object> relation)
		{
			if (relation == null)
			{
				throw new Exception("Dictionary relation cannot be null");
			}

			var expressions = new List<Expression<Func<RelationQuery.Opening, RelationQuery.Closing>>>();

			foreach (var rel in relation)
			{
				var relationId = string.Empty;

				var keyRelation = rel.Key;

				if (!string.IsNullOrEmpty(keyRelation))
				{
					if (keyRelation.Contains("-"))
					{
						var sp = keyRelation.Split(new char[] { '-' });

						relationId = rel.Value.ToString();

						if (!string.IsNullOrEmpty(relationId))
						{
							expressions.Add(q => getQueryClosedFromOpening(q, sp, relationId));
						}
					}
				}
			}

			return expressions;
		}

		RelationQuery.Closing getQueryClosedFromOpening(RelationQuery.Opening open, string[] relation, string correlationId)
		{
			RelationQuery query = open.When(relation[0]);

			for (int i = 2; i < relation.Count() - 1; i += 2)
			{
				query = query.Type(relation[i - 1], relation[i]);
			}

			return query.Entity(relation.Last(), correlationId);
		}

		protected virtual IEntityQuery<TEntity> customQuery(IEntityQuery<TEntity> query)
		{
			return query;
		}

		protected virtual Dictionary<string, object> getDictionaryForFilterQuery()
		{
			var stringFilter = (string)Context.Request.Query._filters;

			if (!String.IsNullOrEmpty(stringFilter))
			{
				var dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(stringFilter);
				return dict;
			}

			return null;
		}

		protected virtual IEntityQuery<TEntity> getFilterQuery(IEntityQuery<TEntity> query, Dictionary<string, object> dict)
		{
			if (dict != null)
			{
				var oper = "";
				foreach (var kv in dict)
				{
					oper = "=";
					ExpressionType operation = ExpressionType.Equal;

					string field = kv.Key;

					if (field.StartsWith("childOf") || field.StartsWith("parentOf"))
					{
						continue;
					}

					if (field.Contains("-"))
					{
						var sp = field.Split(new char[] { '-' });
						field = sp[0];
						oper = sp[1];
					}

					//FIXME: Questo è un workaround per gestire l'eccezione camelcase
					field = $"{field.First().ToString().ToUpper()}{field.Substring(1)}";

					if (kv.Value is string)
					{
						query = query.WhereContains(field, kv.Value.ToString(), StringComparison.InvariantCultureIgnoreCase);
					}
					else {

						if (kv.Value == null)
						{
							operation = ExpressionType.Equal;
						}
						else {

							switch (oper)
							{
								case "gt":
									operation = ExpressionType.GreaterThan;
									break;
								case "gte":
									operation = ExpressionType.GreaterThanOrEqual;
									break;
								case "lt":
									operation = ExpressionType.LessThan;
									break;
								case "lte":
									operation = ExpressionType.LessThanOrEqual;
									break;
							}

						}

						query = query.WhereProperty(field, operation, kv.Value);
					}
				}
			}

			return query;
		}

		protected virtual TCMSEntity DoGet(string id)
		{
			try
			{

				return new TCMSEntity
				{
					Entity = Task.Run(() => EE.Engine.Get<TEntity>(id)).Result
				};
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.TraceError(ex.ToString());
				throw;
			}
		}

		protected virtual void CheckUpdate(TCMSEntity oldEntity, CMSEntity<TEntity> newEntity) { }

		protected virtual int DoUpdate(TCMSEntity entity, string id)
		{
			var oldEntity = Task.Run(() => EE.Engine.Get<TEntity>(id)).Result;
			entity.Entity.CorrelationId = oldEntity.CorrelationId;
			entity.Entity.SetEngine(EE.Engine);
			CheckUpdate(new TCMSEntity { Entity = oldEntity }, entity);
			var result = Task.Run(async () =>
			{
				var res = await EE.Engine.Update(entity.Entity);
				if (res.Success)
				{
					entity.StoreRelations();
				}
				return res;
			}).Result;

			return result.Success ? 1 : 0;
		}
        protected virtual void OnBeforeCreate(TCMSEntity entity) { }
		protected virtual TCMSEntity DoCreate(TCMSEntity entity)
		{
			//db.Insert(entity);
			entity.Entity.CorrelationId = EE.Engine.NewCorrelationId(typeof(TEntity).Name);
			entity.Entity.SetEngine(EE.Engine);
            OnBeforeCreate(entity);

            Task.Run(async () =>
			{
				await EE.Engine.Update(entity.Entity);
				entity.StoreRelations();
			}).Wait();

			return entity;
		}

		protected virtual int DoDelete(string id)
		{
			var result = Task.Run(() => EE.Engine.Delete<TEntity>(id)).Result;

			return result.Success ? 1 : 0;
		}
	}



}
