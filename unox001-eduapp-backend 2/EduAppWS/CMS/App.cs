using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using Nancy.Security;
using System.Linq;

namespace EduAppWS
{
	public class App
	{
		Nancy.NancyContext context;
		public App(Nancy.NancyContext context) {
			this.context = context;
		}
		public string Name { get; set; } = "Unox-CombiGuru";
		public string RESTPath { get { return CMS.CMSBootstrapper.RESTRootPath; } }
		public string UserLevel { 
			get {
				string userlevel = "other";

				if (CurrentUser.HasClaim(CMS.Claims.GetClaim(CMS.Claim.SuperAdmin)))
				{
					userlevel = "superadmin";
				}

				return userlevel;
			}  
		}

		public string Lang
		{
			get
			{
				string lang = string.Empty;

				return lang;
			}
		}

		//public int UserCompany {
		//	get {
		//		return  ((UserIdentity)CurrentUser).UserCompany;
		//	}
		//}
		public IUserIdentity CurrentUser { get { return context.CurrentUser; } }

		[JsonIgnore]
		public string AsJson {
			get {
				return JsonConvert.SerializeObject (this, new JsonSerializerSettings {
					ContractResolver = new CamelCasePropertyNamesContractResolver () 
				});
			}
		}
	}
}

