using System;
using Nancy.TinyIoc;
using Nancy.ViewEngines.SuperSimpleViewEngine;
using System.Linq;
using System.Collections.Generic;
using Nancy.Conventions;
using Nancy;
using Nancy.Responses.Negotiation;
using Nancy.ModelBinding;

namespace EduAppWS.CMS
{
    public static class CMSBootstrapper
    {
        public static string RESTPath (string storage)
        {
            return string.Format ("{0}/{1}", RESTRootPath.TrimEnd ('/'), storage.TrimStart ('/'));
        }

        public static string RESTRootPath { get; private set; }
        public static void Enable (TinyIoCContainer container, Nancy.Bootstrapper.IPipelines pipelines, string restRootPath = "")
        {

            RESTRootPath = restRootPath;
            var negotiator = container.Resolve<IResponseNegotiator> ();
            //inizializzatore dei mapping automatici di dapper
            //CustomMaps.Initialize (container);

            //Imposta il defaultmapper per datpper
            //DapperExtensions.DapperExtensions.DefaultMapper = typeof (ModuleTablesMapper<>);

            //var assembly = typeof (CMSBootstrapper).Assembly;
            //var interfaceForMatcher = typeof (ISuperSimpleViewEngineMatcher);
            //container.Register<IEnumerable<ISuperSimpleViewEngineMatcher>> (
            //    (c, p) => assembly.GetTypes ()
            //    .Where (t => t.GetInterfaces ().Contains (interfaceForMatcher))
            //    .Select (t => container.Resolve (t))
            //    .OfType<ISuperSimpleViewEngineMatcher> ()
            //    .ToList ()
            //);

            pipelines.OnError.AddItemToEndOfPipeline ((ctx, ex) => {
				System.Diagnostics.Trace.TraceError ("Exception: {0}", ex);
                string exType = ex.GetType ().Name.ToString ();
                if (ex is ModelBindingException) {
                    ex = (ex as ModelBindingException).InnerException;
                }


                if (ex is HttpResponseException) {

                    var he = ex as HttpResponseException;
                    return negotiator.NegotiateResponse (new Negotiator (ctx)
                                                         .WithStatusCode (he.StatusCode)
                                                         .WithModel (new {
                                                             Errors = new string [] { he.StatusCode.ToString (), he.Message },
                                                             Success = false
                                                         }), ctx);
                } else if (ex is InvalidPropertyException) {
                    var pe = ex as InvalidPropertyException;
                    return negotiator.NegotiateResponse (new Negotiator (ctx)
                                                         .WithStatusCode (HttpStatusCode.BadRequest)
                                                         .WithModel (pe.Violations),
                                                         ctx);
                } else {
                    return negotiator.NegotiateResponse (new Negotiator (ctx)
                                                         .WithStatusCode (HttpStatusCode.BadRequest)
                                                         .WithModel (new [] { new InvalidPropertyException.Violation {
                            PropertyPath ="",
                            Message = string.Format("{0} - {1}", exType, ex.Message)
                        } }),
                                                         ctx);
                }
                //return null;
            });
        }

        public static void ConfigureConventions (NancyConventions nancyConventions)
        {
            nancyConventions.ViewLocationConventions.Add ((viewName, model, viewLocationContext) => String.Concat ("CMS/views/", viewName));
        }
    }
}