using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharedCore.Entities;

namespace EduAppWS.CMS
{
	[JsonConverter (typeof (CMSEntitySerializer))]
	public class CMSEntity<TEntity> where TEntity : Entity
	{
		public TEntity Entity { get; set; }

		public void setRelated (string correlationId, [CallerMemberName] string propertyName = "")
		{
			setRelated (new [] { correlationId }, propertyName);
		}

		public void setRelated (string [] correlationIds, [CallerMemberName] string propertyName = "")
		{
			if (correlationIds != null) {
				var attr = getRelatedAttribute (propertyName);
				if (attr != null) {
					relationsDict [attr.GetFullName ()] = correlationIds.Where( v => !String.IsNullOrEmpty(v)).ToArray();
					relationsLookup [attr.GetFullName ()] = attr;
				} else {
					throw new Exception ("Non è stato definito RelatedAttribute per la property: " + propertyName);
				}
			}
		}

		protected Dictionary<string, string []> relationsDict = new Dictionary<string, string []> ();
		protected Dictionary<string, RelatedAttribute> relationsLookup = new Dictionary<string, RelatedAttribute> ();

		public string getSingleRelated ([CallerMemberName] string propertyName = "") {
			return getRelated (propertyName)?.FirstOrDefault ();
		}
		public IEnumerable<string> getRelated ([CallerMemberName] string propertyName = "")
		{
			
			var attr = getRelatedAttribute (propertyName);

			if (attr != null) {
				if (!relationsDict.ContainsKey (attr.GetFullName ())) {
					if (Entity?.Engine == null)
						return null;
					
					var e = Task.Run(()=> Entity.Engine.GetCorrelationIdsByRelation (attr.Relation, typeof(TEntity).Name, this.Entity.CorrelationId, attr.TargetEntityType)).Result;
					relationsDict [attr.GetFullName ()] = e.ToArray ();
					relationsLookup [attr.GetFullName ()] = attr;
				}
			} else {
				throw new Exception ("Non è stato definito RelatedAttribute per la property: " + propertyName);
			}

			return relationsDict [attr.GetFullName ()];
		}

		RelatedAttribute getRelatedAttribute (string propertyName)
		{
			return this.GetType ().GetProperty (propertyName)?
									   .GetCustomAttributes (typeof (RelatedAttribute), true)
									   .FirstOrDefault () as RelatedAttribute;
		}

		/// <summary>
		/// Salva le relazioni modificate direttamente sull'entity.Engine
		/// </summary>
		public void StoreRelations ()
		{
			foreach (var relation in relationsDict) { 
				var attr = relationsLookup [relation.Key];
				Entity?.Engine?.SetEntityRelationExactly (typeof (TEntity).Name, this.Entity.CorrelationId, attr.Relation, attr.TargetEntityType, relation.Value);
			}
		}
	}


	public class CMSEntitySerializer : Newtonsoft.Json.JsonConverter
	{

		public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer)
		{
			var valueType = value.GetType ();
			var entity = valueType.GetProperty ("Entity").GetValue(value);

			var jentity = JObject.FromObject (entity, serializer);

			foreach (var attrProp in valueType.GetProperties ().Where (p => p.GetCustomAttributes (true).OfType<RelatedAttribute> ().Any ())) {
                var propName = Char.ToLower(attrProp.Name[0]) + attrProp.Name.Substring(1);

                var val = attrProp.GetValue(value);
                //if (val != null) {
                jentity.Add(propName, val != null ? JToken.FromObject(val) : null);
                //}
            }

            foreach (var attrProp in valueType.GetProperties()
              .Where(p => !p.GetCustomAttributes(true).OfType<RelatedAttribute>().Any() && !p.Name.Equals("Entity")))
            {
                var propName = Char.ToLower(attrProp.Name[0]) + attrProp.Name.Substring(1);

                var val = attrProp.GetValue(value);
                //if (val != null) {
                jentity.Add(propName, val != null ? JToken.FromObject(val) : null);
                //}
            }



            jentity.WriteTo (writer);
		}

		public override object ReadJson (JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var entityTypeProperty = objectType.GetProperty ("Entity");
			var instance = Activator.CreateInstance (objectType);
			var jobj = JToken.ReadFrom (reader);

			var entity = (Entity)jobj.ToObject (entityTypeProperty.PropertyType);
			entityTypeProperty.SetValue (instance, entity);

			foreach (var attrProp in objectType.GetProperties ()
                .Where (p => p.GetCustomAttributes (true).OfType<RelatedAttribute> ().Any ())) {
                SetObjectProp(instance, jobj, attrProp);
            }

            foreach (var attrProp in objectType.GetProperties()
                .Where(p => !p.GetCustomAttributes(true).OfType<RelatedAttribute>().Any() && !p.Name.Equals("Entity")))
            {
                SetObjectProp(instance, jobj, attrProp);
            }

            return instance;
		}

        private static void SetObjectProp(object instance, JToken jobj, System.Reflection.PropertyInfo attrProp)
        {
            var propName = attrProp.Name;
            if (jobj[propName] != null)
            {
                attrProp.SetValue(instance, jobj[propName].ToObject(attrProp.PropertyType));
            }
            else
            {
                //camelCase
                propName = Char.ToLower(propName[0]) + propName.Substring(1);
                if (jobj[propName] != null)
                {
                    attrProp.SetValue(instance, jobj[propName].ToObject(attrProp.PropertyType));
                }
            }
        }

        Type baseType = typeof (CMSEntity<>);
		public override bool CanConvert (Type objectType)
		{
			var canconvert = objectType.BaseType.IsGenericType && objectType.BaseType.GetGenericTypeDefinition() == baseType;
			canconvert |= objectType.IsGenericType && objectType.GetGenericTypeDefinition () == baseType;
			return canconvert;
		}


		public override bool CanWrite {
			get {
				return true;
			}
		}

		public override bool CanRead {
			get {
				return true;
			}
		}
	}
}
