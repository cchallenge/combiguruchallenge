using System;
using Nancy;
using Nancy.TinyIoc;
using Nancy.Conventions;
using Nancy.Bootstrapper;
using System.Collections.Generic;
using System.IO;
using Nancy.ViewEngines.SuperSimpleViewEngine;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SharedCore.Entities;
using EduAppWS.CMS;
using ServerCore;
using System.Configuration;
using ServerCore.Interfaces;
using System.Threading.Tasks;

namespace EduAppWS
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        public class CustomJsonSerializer : JsonSerializer
        {
            public CustomJsonSerializer ()
            {
                this.ContractResolver = new CamelCasePropertyNamesContractResolver () {

                };
                this.Formatting = Formatting.Indented;
                this.DateTimeZoneHandling = DateTimeZoneHandling.Unspecified;
            }
        }

		public static TinyIoCContainer AppContainer { get; private set; }

		protected override void ApplicationStartup(TinyIoCContainer container, Nancy.Bootstrapper.IPipelines pipelines)
        {
            AppContainer = container;

			StaticConfiguration.DisableErrorTraces = false;

			ServerCoreNancyModule.Enable(container, pipelines)
								 .EnableAzureContentModule(
									 ConfigurationManager.ConnectionStrings["azureStorageCS"].ConnectionString,
									 ConfigurationManager.AppSettings["azureStorageContainer"])
			                     .EnableMongoDBEntityModule(
				                     ConfigurationManager.ConnectionStrings["mongoDBCS"].ConnectionString,
				                     ConfigurationManager.AppSettings["mongoDBName"])
			                     .EnableEntitiesEngineAuthModule()
								 .AllowPublicAccess(@"^/index\.html$")
								 .AllowPublicAccess(@"^/$")
								 .AllowPublicAccess(@"^/login\.html$");


			LogRequest(pipelines);

			ConfigSuperAdmin(container);

			CMS.CMSBootstrapper.Enable(container, pipelines);

			BackBase.BackBase
				//Abilita il modulo
				.Enable(container, pipelines)
				//Rende disponibili tutti i files js sotto la cartella "app/"
				.Register("app/*.js")
				//Controlla la cartella "app" per verificare i cambiamenti 
				// al filesystem, e quindi ricalcolare i file in bundle
				.Watch("app");
		}

		void LogRequest(IPipelines pipelines)
		{
            var bonusPoints = ConfigurationManager.AppSettings["levelCompletedBonus"];
            Console.WriteLine("AppSettings BonusPoints: " + bonusPoints);
            pipelines.BeforeRequest.AddItemToStartOfPipeline((arg) =>
			{
//				Console.WriteLine("TYPE REQUEST: " + arg.Request.Method);
//				Console.WriteLine("URL REQUEST: " + arg.Request.Url);

                var body = arg.Request.Body;
				int length = (int)body.Length; // this is a dynamic variable
				byte[] data = new byte[length];
				body.Read(data, 0, length);

//				Console.WriteLine("BODY REQUEST: " + System.Text.Encoding.Default.GetString(data));

				arg.Request.Body.Position = 0;
				return null;
			});

			pipelines.OnError.AddItemToEndOfPipeline((x, ex) =>
			{
				Console.WriteLine(ex);
				return null;
			});
		}

		protected override void ConfigureConventions(Nancy.Conventions.NancyConventions nancyConventions)
		{
            base.ConfigureConventions(nancyConventions);

			CMS.CMSBootstrapper.ConfigureConventions(nancyConventions);
			nancyConventions.CultureConventions.Clear();
			nancyConventions.CultureConventions.Add(x => System.Globalization.CultureInfo.InvariantCulture);

			nancyConventions.StaticContentsConventions.Add(
				StaticContentConventionBuilder.AddDirectory("", @"Static")
			);
		}

		protected override void ConfigureRequestContainer(Nancy.TinyIoc.TinyIoCContainer container, NancyContext context)
		{
			base.ConfigureRequestContainer(container, context);
		}

		protected void ConfigSuperAdmin(TinyIoCContainer container)
		{
			var authModule = container.Resolve<ServerCore.Interfaces.IAuthBEAppModule>();
            try
			{
				var appContext = container.Resolve<ServerCore.Interfaces.IBEAppCoreContext>();

				authModule.Configure(appContext);

				var existedAdminCMS = authModule.Auth(appContext, "superadmin", SecurityHelper.MD5String(System.Configuration.ConfigurationManager.AppSettings["supermadminpass"]));

				if (existedAdminCMS == null)
				{
					string[] claims = new string[] { Claims.GetClaim(Claim.SuperAdmin) };
					authModule
						.Create(
							appContext,
							"superadmin",
							SecurityHelper.MD5String(System.Configuration.ConfigurationManager.AppSettings["supermadminpass"]),
							claims,
							true
						);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}
	}

	public class NullCodeHandler : Nancy.ErrorHandling.IStatusCodeHandler
	{
		#region IStatusCodeHandler implementation

		public bool HandlesStatusCode(HttpStatusCode statusCode, NancyContext context)
		{
			return true;
		}

		public void Handle(HttpStatusCode statusCode, NancyContext context)
		{

		}

        #endregion

    }
}

