﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.NotificationHubs;
using NLog;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using Unox.Models.RawModels;

namespace EduAppWS
{
	public class SendNotification
	{
		readonly IEntitiesEngine ee;

		NotificationHubClient hub;
		Logger logger;

		public SendNotification(IBEEntityModule ee)
		{
			this.ee = ee.Engine;
			Init();
		}

		void Init()
		{
			logger = LogManager.GetLogger("main");

			logger.Info("Inizializzazione processo di invio notifiche");

			var conn = System.Configuration.ConfigurationManager.AppSettings["pushcs"];
			var name = System.Configuration.ConfigurationManager.AppSettings["pushhp"];

			logger.Info("Creazione client hub di notifica");
			hub = NotificationHubClient
				.CreateClientFromConnectionString(
					conn,
					name
				);
		}

		public async Task<bool> SendPushMessage(LoginStatUser user)
		{
			var txtMessage =
				ee.Query<NotificationMessage>()
				  .WhereEquals("Lang", user.Lang, StringComparison.InvariantCultureIgnoreCase)
				  .FirstOrDefault();

			if (txtMessage == null)
			{
				txtMessage =
				ee.Query<NotificationMessage>()
				  .WhereEquals("Lang", "EN", StringComparison.InvariantCultureIgnoreCase)
				  .FirstOrDefault();
			}

			if (txtMessage != null)
			{
				var message = new PushMessage()
				{
					Message = txtMessage.TextMessage,
					Tags = new List<string>() {

						user.CorrelationId
					}
				};

				var appleState = await SendToAppleDevice(message);
				logger.Info("sent notification (id: {1}) to apple with result: {0}", appleState.State, message.Id);
				var androidState = await SendToGcm(message);
				logger.Info("sent notification (id: {1}) to gcm with result: {0}", androidState.State, message.Id);

				bool success = appleState.State == androidState.State && appleState.State == NotificationOutcomeState.Enqueued;

				if (success)
				{
					user.LastLogin = DateTime.UtcNow;
					await ee.Update(user);
				}

				return success;
			}

			throw new ApplicationException($"The notification message for language {user.Lang} is null");
		}

		async Task<NotificationOutcome> SendToAppleDevice(PushMessage message)
		{
			var appleNot = new ApplePushModel(message.Message, message.AppleBadge);

			var jsonPayload = Newtonsoft.Json.JsonConvert.SerializeObject(appleNot);

			return await hub.SendAppleNativeNotificationAsync(jsonPayload, message.Tags);
		}

		async Task<NotificationOutcome> SendToGcm(PushMessage message)
		{
			var gcmNot = new GCMPushModel(message);

			var jsonPayload = Newtonsoft.Json.JsonConvert.SerializeObject(gcmNot);

			return await hub.SendGcmNativeNotificationAsync(jsonPayload, message.Tags);
		}
	}
}
