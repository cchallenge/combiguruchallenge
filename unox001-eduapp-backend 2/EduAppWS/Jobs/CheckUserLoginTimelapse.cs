﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using Unox.Models.RawModels;
using Hangfire;

namespace EduAppWS.Jobs
{
	public class CheckUserLoginTimelapse
	{
		readonly IEntitiesEngine ee;

		public CheckUserLoginTimelapse(IBEEntityModule ee)
		{
			this.ee = ee.Engine;
		}

		public void StartChecking()
		{
			var currentTime = DateTime.UtcNow;

			var timeLapseDelta = System.Configuration.ConfigurationManager.AppSettings["deltaUserNotificationTimeDay"];

			double timeLapse = 30;
			double.TryParse(timeLapseDelta, out timeLapse);

			#if DEBUG
			timeLapse = 2;
			#endif

			var userLoginTimelapsed = 
				ee.Query<LoginStatUser>()
				  .Where(l => (currentTime - l.LastLogin) >=
						 #if DEBUG 
				         TimeSpan.FromMinutes(timeLapse) 
				         #else 
				         TimeSpan.FromDays(timeLapse) 
				         #endif
				        )
				  .ToList();

			foreach (var user in userLoginTimelapsed)
			{
				BackgroundJob.Enqueue<SendNotification>(not => not.SendPushMessage(user));
			}
		}
	}
}
