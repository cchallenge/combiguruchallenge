﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;

namespace Fluent
{


	static class FuncTest<TSource>
	{
		public static bool FuncEqual<TValue> (
			Expression<Func<TSource, TValue>> x,
			Expression<Func<TSource, TValue>> y)
		{
			return FuncTest.FuncEqual<TSource, TValue> (x, y);
		}
	}
	static class FuncTest
	{
		public static bool FuncEqual<TSource, TValue> (
			Expression<Func<TSource, TValue>> x,
			Expression<Func<TSource, TValue>> y)
		{
			return ExpressionEqual (x, y);
		}
		private static bool ExpressionEqual (Expression x, Expression y)
		{
			// deal with the simple cases first...
			if (ReferenceEquals (x, y)) return true;
			if (x == null || y == null) return false;
			if (x.NodeType != y.NodeType
				|| x.Type != y.Type) return false;

			switch (x.NodeType) {
			case ExpressionType.Lambda:
				return ExpressionEqual (((LambdaExpression)x).Body, ((LambdaExpression)y).Body);
			case ExpressionType.MemberAccess:
				MemberExpression mex = (MemberExpression)x, mey = (MemberExpression)y;
				return mex.Member == mey.Member; // should really test down-stream expression
			default:
				throw new NotImplementedException (x.NodeType.ToString ());
			}
		}
	}

	public static class ObjectExtensions
	{
		class exEqComp : IEqualityComparer
		{
			public new bool Equals (object x, object y)
			{
				throw new NotImplementedException ();
			}

			public int GetHashCode (object obj)
			{
				throw new NotImplementedException ();
			}
		}


		static Hashtable compiled = new Hashtable (new exEqComp ());

		static Expression lastPredicate;
		static Delegate lastObjGetter;
		static PropertyInfo propInfo;

		public static T With<T, TProp>(this T obj, Expression<Func<T, TProp>> predicate, TProp val)
		{
			if (compiled.ContainsKey(predicate)) {
				var values = (Tuple<PropertyInfo, Delegate>)compiled[predicate];
				propInfo.SetValue (lastObjGetter.DynamicInvoke (obj), val);
				return obj;
			}

			lastPredicate = predicate;
			MemberExpression propertyGetExpression = (predicate.Body as MemberExpression);

			var prop = propInfo = (PropertyInfo)propertyGetExpression.Member;

			//Getter per l'oggetto al quale impostare la property
			var theObjGetter = Expression.Lambda (typeof (Func<T, object>),
			                                propertyGetExpression.Expression, predicate.Parameters [0])
								  			//as Expression<Func<T, object>>
			                             ;

			lastObjGetter = theObjGetter.Compile ();
			//Invocazione di parte dell'expression passando l'oggetto come parametro
			prop.SetValue (lastObjGetter.DynamicInvoke(obj), val);
			return obj;
		}

		public static T With<T, TProp> (this T obj, Expression<Func<T, TProp>> predicate, Expression<Func<T, TProp>> valuator)
		{
			MemberExpression propertyGetExpression = (predicate.Body as MemberExpression);

			var prop = (PropertyInfo)propertyGetExpression.Member;

			//Getter per l'oggetto al quale impostare la property
			var theObjGetter = Expression.Lambda (typeof (Func<T, object>),
											propertyGetExpression.Expression, predicate.Parameters [0])
								  			as Expression<Func<T, object>>;


			//Invocazione di parte dell'expression passando l'oggetto come parametro
			prop.SetValue (theObjGetter.Compile () (obj), valuator.Compile().Invoke(obj));
			return obj;
		}

		public static T With<T>(this T obj, Action<T> predicate)
		{
			predicate (obj);
			return obj;
		}


		public static IEnumerable<TResult> Enumerate<TObject, TResult> (this TObject t, Func<TObject, int, TResult> getNext, int count) {
			for (int i = 0; i < count; i++) {
				yield return getNext (t, i);
			}
		}

		public static IEnumerable<TResult> Enumerate<TObject, TResult> (this TObject t, Func<TObject, TResult> getNext, int count)
		{
			return t.Enumerate ((o, idx) => getNext (o), count);
		}

		public static TResult To<TObject, TResult> (this TObject t, Func<TObject, TResult> selectRes)
		{
			return selectRes(t);
		}

		public static TResult Select<TObject, TResult> (this TObject t, Func<TObject, TResult> selectRes)
		{
			return selectRes (t);
		}
	}

	public class If<T> {
		Func<T, bool> evaluate;
		internal T who;
		internal If (T who, Func<T, bool> what) { this.who = who; this.evaluate = what; }
		internal bool Success => this.evaluate (who);

		public Then<T> Then (Func<T, T> ifTrue) {
			return new Then<T> (this, ifTrue);
		}

		public static implicit operator bool (If<T> p)
		{
			return p.Success;
		}
	}

	public class Then<T> {
		If<T> ifGet;
		Func<T,T> getIfTrue;
		internal Func<T,T> getIfFalse;
		internal Then (If<T> ifGet, Func<T,T> getIfTrue, Func<T,T> getIfFalse = null) {
			this.ifGet = ifGet;
			this.getIfTrue = getIfTrue;
			this.getIfFalse = getIfFalse ?? ((T arg) => arg);
		}

		public static implicit operator T (Then<T> p) {
			return p.ifGet.Success ? p.getIfTrue (p.ifGet.who) : p.getIfFalse (p.ifGet.who);
		}

		public T Else (Func<T, T> ifFalse) {
			this.getIfFalse = ifFalse;
			return (T)this;
		}
	}

	public static class FluentExtensions {
		public static If<T> If<T> (this T obj, Func<T, bool> what)
		{
			return new Fluent.If<T> (obj, what);
		}
	}


}

