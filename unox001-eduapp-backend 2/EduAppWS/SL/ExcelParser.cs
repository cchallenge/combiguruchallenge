﻿using ClosedXML.Excel;
using SharedCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using Unox.Models.ImporterModels;
using Unox.Models.RawModels;
//using Excel = ClosedXML;

/// <summary>
/// Recupero e normalizzazione del foglio excel UNOX contenente Sezioni, Lezioni e Domande
/// </summary>
/// 

namespace Unox
{
    public class ExcelParser
    {
        private ImpLevel curlevel;

        private IEntitiesEngine _engine;
        public ExcelParser(IEntitiesEngine engine)
        {
            curlevel = new ImpLevel();
            _engine = engine;
        }

        private string GetValue(IXLWorksheet worksheet, int row, string col)
        {
            var xlrow = worksheet.Row(row);
            var xlcell = xlrow.Cell(col);
            var sreturn = xlcell.Value;

            //var sreturn = (worksheet.Cells[row, col] as Excel.Range).Value;
            return sreturn == null ? "" : sreturn.ToString();
        }

        private string GetValue(IXLWorksheet worksheet, int row, int col)
        {
            var xlrow = worksheet.Row(row);
            var xlcell = xlrow.Cell(col);
            var sreturn = xlcell.Value;
            // var sreturn = (worksheet.Cells[row, col] as Excel.Range).Value;
            return sreturn == null ? "" : sreturn.ToString();
        }

        private struct LanguageSection
        {
            public string Language { get; set; }
            public int Col { get; set; }

        }

        public ImpLevel Import(string filename, string level, ref CheckResult cr)
        {

            curlevel = new ImpLevel(level);
            System.Globalization.CultureInfo Oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");

            object misValue = System.Reflection.Missing.Value;
            var xlWorkBook = new XLWorkbook(filename);
            string sTipo = "";

            int iMax = 5000;
            int iFirstRow = 1;
            int iLesson = 0;
            int iSection = 0;
            int iQuestion = 0;
            ImpLesson curlesson = null;
            ImpQuestion curquestion = null;
            ImpSection cursection = null;
            int isections = xlWorkBook.Worksheets.Count;
            var images = _engine.GetAll<AppIcon>().Result.Select(x => x.Name).ToList();
            //if (isections < 6)
            //{
            //    cr.Success = false;
            //    cr.Messages.Add($"Ogni livello deve contenere 6 sezioni. Rilevate {isections} sezioni.");
            //    return null;
            //}

            for (int sheets = 1; sheets <= isections; sheets++)
            {

                List<LanguageSection> languagescols = new List<LanguageSection>();
                var xlWorkSheet = xlWorkBook.Worksheet(sheets);
                string section_name = xlWorkSheet.Name.ToString();
                cursection = new ImpSection(section_name);
                bool questionactive = false;
                int iLastKeyword = 1;
                for (int i = 1; i <= iMax; i++)
                {

                    try
                    {
                        if (i == iFirstRow)
                        {
                            /// CHECK SECTION
                            string firstcell = GetValue(xlWorkSheet, 1, 1);
                            if (firstcell.ToUpper() != "LANGUAGES")
                            {
                                cr.Success = false;
                                cr.Messages.Add($"Inizio livello NON TROVATO. Aggiungere la riga delle LANGUAGES e riprovare.");
                                return null;
                            }
                            /// ################# END CHECK
                            int cMax = 100;
                            // LANGUAGES
                            for (int c = 2; c <= cMax; c++)
                            {
                                string language = GetValue(xlWorkSheet, i, c);
                                if (!string.IsNullOrEmpty(language))
                                {
                                    LanguageSection ls = new LanguageSection()
                                    {
                                        Col = c,
                                        Language = language
                                    };
                                    languagescols.Add(ls);
                                }
                            }

                            /// CHECK SECTION
                            if (languagescols.Count == 0)
                            {
                                cr.Success = false;
                                cr.Messages.Add($"Ogni livello deve contenere 5 languages. Languages in prima riga non trovati.");
                                return null;
                            }
                            else
                            {
                                if (languagescols.Count < 5)
                                {
                                    cr.Success = false;
                                    cr.Messages.Add($"Sono state rilevate unicamente {languagescols.Count.ToString()} lingue supportate. IT EN DE FR ES obbligatori.");
                                    return null;
                                }
                                else
                                {
                                    List<string> supportedlang = new List<string>() { "IT", "EN", "DE", "FR", "ES" };
                                    List<string> importedlang = languagescols.Select(x => x.Language.ToUpper()).ToList();
                                    foreach (var ln in supportedlang)
                                    {
                                        if (!importedlang.Contains(ln))
                                        {
                                            cr.Success = false;
                                            cr.Messages.Add($"Non è stata rilevata la lingua avente codice {ln}. IT EN DE FR ES obbligatori.");
                                            return null;

                                        }
                                    }

                                }
                            }
                            /// ################# END CHECK
                        }
                        else
                        {
                            // check terminatori
                            string tag = GetValue(xlWorkSheet, i, 1);
                            // se contiene un terminatore o una key faccio cose

                            /// CHECK SECTION
                            if ((iLastKeyword + 20) < i)
                            {
                                cr.Success = false;
                                cr.Messages.Add($"Sono state rilevate più di 20 righe vuote dopo l'ultimo tag riconosciuto. Verificare il file e riprovare. Verificare presenza di END_SECTION");
                                return null;
                            }

                            /// ################# END CHECK
                            if (!string.IsNullOrEmpty(tag))
                            {

                                // parse tag
                                if (tag.ToUpper().Contains("END_LESSON"))
                                {
                                    iLastKeyword = i;
                                    iLesson++;
                                    // close lesson and save
                                    cursection.Lezioni.Add(curlesson);

                                }
                                else if (tag.ToUpper().Contains("LESSON"))
                                {
                                    iLastKeyword = i;
                                    // open new lesson 
                                    curlesson = new ImpLesson(tag);
                                    curlesson.Lezione.Order = iLesson;
                                    foreach (LanguageSection ls in languagescols)
                                    {
                                        curlesson.Lezione.Description.Add(ls.Language, GetValue(xlWorkSheet, i, ls.Col));
                                    }


                                }
                                else if (tag.ToUpper().Contains("END_QUESTION"))
                                {
                                    iLastKeyword = i;
                                    iQuestion++;
                                    // close question and save
                                    curlesson.Domande.Add(curquestion);

                                }
                                else if (tag.ToUpper().Contains("QUESTION"))
                                {
                                    iLastKeyword = i;
                                    curquestion = new ImpQuestion(tag);
                                    curquestion.Domanda.Order = iQuestion;

                                }
                                else if (tag.ToUpper().Contains("TEMPLATE"))
                                {
                                    iLastKeyword = i;
                                    string templatename = GetValue(xlWorkSheet, i, 2); // template posizionato accanto al tag template
                                    /// CHECK SECTION
                                    var templates = _engine.GetAll<Template>().Result;
                                    var check = (from t in templates
                                                 where t.Name.ToUpper() == templatename.ToUpper()
                                                 select t).FirstOrDefault();
                                    if (check == null)
                                    {
                                        cr.Success = false;
                                        cr.Messages.Add($"Template non censito presente nel file in importazione. Censire il template {templatename} o correggere e riprovare.");
                                        return null;
                                    }

                                    /// ################# END CHECK

                                    curquestion.Domanda.Type = templatename;

                                }
                                else if (tag.ToUpper().Contains("TEXT"))
                                {
                                    iLastKeyword = i;
                                    // fill question
                                    foreach (LanguageSection ls in languagescols)
                                    {
                                        string text = GetValue(xlWorkSheet, i, ls.Col);
                                        string icon1 = GetValue(xlWorkSheet, i, ls.Col + 1);
                                        string icon2 = GetValue(xlWorkSheet, i, ls.Col + 2);
                                        if (!string.IsNullOrEmpty(icon1))
                                        {
                                            if (!images.Contains(icon1))
                                            {
                                                cr.Success = false;
                                                cr.Messages.Add($"Immagine {icon1} in domanda {curquestion.Domanda.Name} non inserita per tipo {curquestion.Domanda.Type}. Verificare e riprovare.");
                                                return null;

                                            }
                                        }

                                        if (!string.IsNullOrEmpty(icon2))
                                        {
                                            if (!images.Contains(icon2))
                                            {
                                                cr.Success = false;
                                                cr.Messages.Add($"Immagine {icon2} in domanda {curquestion.Domanda.Name} non inserita per tipo {curquestion.Domanda.Type}. Verificare e riprovare.");
                                                return null;

                                            }
                                        }

                                        /// CHECK SECTION
                                        bool bok = TemplateHeaderCheck(curquestion.Domanda.Type, text, icon1, icon2);
                                        if (!bok)
                                        {
                                            cr.Success = false;
                                            cr.Messages.Add($"Il controllo sul testo della Sezione {cursection.Sezione.Name}, Lezione {curlesson.Lezione.Name}, Domanda {curquestion.Domanda.Name}  avente template {curquestion.Domanda.Type} non ha dato esito positivo. Testo e/o icone obbligatori. Verificare e riprovare.");
                                            return null;
                                        }

                                        /// ################# END CHECK
                                        RawContent rc = new RawContent()
                                        {
                                            Text = text,
                                            Icon1 = icon1,
                                            Icon2 = icon2
                                        };

                                        List<RawContentEntry> list = new List<RawContentEntry>();
                                        // entries
                                        for (int iraw = i + 1; iraw < 500; iraw++)
                                        {
                                            string c1 = GetValue(xlWorkSheet, iraw, 1);
                                            if (c1.ToUpper() == "END_QUESTION")
                                                break;
                                            else
                                            {
                                                RawContentEntry rce = new RawContentEntry();
                                                rce.C1 = c1;
                                                rce.C2 = GetValue(xlWorkSheet, iraw, ls.Col);
                                                rce.C3 = GetValue(xlWorkSheet, iraw, ls.Col + 1);
                                                rce.C4 = GetValue(xlWorkSheet, iraw, ls.Col + 2);
                                                rce.C5 = GetValue(xlWorkSheet, iraw, ls.Col + 3);
                                                list.Add(rce);
                                            }


                                        }

                                        var rowok = TemplateRowsCheck(curquestion.Domanda.Type, list);
                                        if (!rowok.Item1)
                                        {
                                            cr.Success = false;
                                            cr.Messages.Add($"Il controllo sui dati nella Sezione {cursection.Sezione.Name}, Lezione {curlesson.Lezione.Name}, Domanda {curquestion.Domanda.Name} avente template {curquestion.Domanda.Type} non ha dato esito positivo. Dettagli: {rowok.Item2}. Verificare e riprovare.");
                                            return null;
                                        }
                                        rc.Entries = list;

                                        curquestion.Domanda.Body.Add(ls.Language, rc);

                                    }

                                }
                                else if (tag.ToUpper().Contains("END_SECTION"))
                                {
                                    iLastKeyword = i;
                                    iSection++;
                                    iLesson = 0;
                                    iQuestion = 0;
                                    curlevel.Sezioni.Add(cursection);
                                    break; // e va oltre sui workbook

                                }
                                else if (tag.ToUpper().Contains("SECTION"))
                                {
                                    iLastKeyword = i;
                                    foreach (LanguageSection ls in languagescols)
                                    {
                                        cursection.Sezione.Description.Add(ls.Language, GetValue(xlWorkSheet, i, ls.Col));
                                    }
                                    cursection.Sezione.Icon = GetValue(xlWorkSheet, i, 3);
                                    if (cursection.Sezione.Icon == "")
                                    {
                                        cr.Success = false;
                                        cr.Messages.Add($"Ogni sezione deve contenere un'icona. Sezione {cursection.Sezione.Name} priva di icona");
                                        return null;
                                    }
                                    else
                                    {
                                        if (!images.Contains(cursection.Sezione.Icon))
                                        {
                                            cr.Success = false;
                                            cr.Messages.Add($"Icona di sezione non trovata. Sezione {cursection.Sezione.Name} - Icona mancante {cursection.Sezione.Icon}");
                                            return null;
                                        }
                                    }
                                    cursection.Sezione.Order = iSection;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        cr.Success = false;
                        cr.Messages.Add($"Errore in fase di importazione del file: {ex.Message}");
                        return null;
                    }
                }
            }
            System.Threading.Thread.CurrentThread.CurrentCulture = Oldci;

            return curlevel;

        }


        public _ext_ImpLevel _ext_Import(string filename, string level, ref CheckResult cr)
        {

            _ext_ImpLevel curlevel = new _ext_ImpLevel(level);
            System.Globalization.CultureInfo Oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");

            object misValue = System.Reflection.Missing.Value;
            var xlWorkBook = new XLWorkbook(filename);
            string sTipo = "";

            int iMax = 5000;
            int iFirstRow = 1;
            int iLesson = 0;
            int iSection = 0;
            int iQuestion = 0;
            _ext_ImpLesson curlesson = null;
            _ext_ImpQuestion curquestion = null;
            _ext_ImpSection cursection = null;
            int isections = xlWorkBook.Worksheets.Count;
            var images = _engine.GetAll<AppIcon>().Result.Select(x => x.Name).ToList();
            //if (isections < 6)
            //{
            //    cr.Success = false;
            //    cr.Messages.Add($"Ogni livello deve contenere 6 sezioni. Rilevate {isections} sezioni.");
            //    return null;
            //}

            for (int sheets = 1; sheets <= isections; sheets++)
            {

                List<LanguageSection> languagescols = new List<LanguageSection>();
                var xlWorkSheet = xlWorkBook.Worksheet(sheets);
                string section_name = xlWorkSheet.Name.ToString();
                cursection = new _ext_ImpSection(section_name);
                bool questionactive = false;
                int iLastKeyword = 1;
                for (int i = 1; i <= iMax; i++)
                {

                    try
                    {
                        if (i == iFirstRow)
                        {
                            /// CHECK SECTION
                            string firstcell = GetValue(xlWorkSheet, 1, 1);
                            if (firstcell.ToUpper() != "LANGUAGES")
                            {
                                cr.Success = false;
                                cr.Messages.Add($"Inizio livello NON TROVATO. Aggiungere la riga delle LANGUAGES e riprovare.");
                                return null;
                            }
                            /// ################# END CHECK
                            int cMax = 100;
                            // LANGUAGES
                            for (int c = 2; c <= cMax; c++)
                            {
                                string language = GetValue(xlWorkSheet, i, c);
                                if (!string.IsNullOrEmpty(language))
                                {
                                    LanguageSection ls = new LanguageSection()
                                    {
                                        Col = c,
                                        Language = language
                                    };
                                    languagescols.Add(ls);
                                }
                            }

                            /// CHECK SECTION
                            if (languagescols.Count == 0)
                            {
                                cr.Success = false;
                                cr.Messages.Add($"Ogni livello deve contenere 5 languages. Languages in prima riga non trovati.");
                                return null;
                            }
                            else
                            {
                                if (languagescols.Count < 5)
                                {
                                    cr.Success = false;
                                    cr.Messages.Add($"Sono state rilevate unicamente {languagescols.Count.ToString()} lingue supportate. IT EN DE FR ES obbligatori.");
                                    return null;
                                }
                                else
                                {
                                    List<string> supportedlang = new List<string>() { "IT", "EN", "DE", "FR", "ES" };
                                    List<string> importedlang = languagescols.Select(x => x.Language.ToUpper()).ToList();
                                    foreach (var ln in supportedlang)
                                    {
                                        if (!importedlang.Contains(ln))
                                        {
                                            cr.Success = false;
                                            cr.Messages.Add($"Non è stata rilevata la lingua avente codice {ln}. IT EN DE FR ES obbligatori.");
                                            return null;

                                        }
                                    }

                                }
                            }
                            /// ################# END CHECK
                        }
                        else
                        {
                            // check terminatori
                            string tag = GetValue(xlWorkSheet, i, 1);
                            // se contiene un terminatore o una key faccio cose

                            /// CHECK SECTION
                            if ((iLastKeyword + 20) < i)
                            {
                                cr.Success = false;
                                cr.Messages.Add($"Sono state rilevate più di 20 righe vuote dopo l'ultimo tag riconosciuto. Verificare il file e riprovare. Verificare presenza di END_SECTION");
                                return null;
                            }

                            /// ################# END CHECK
                            if (!string.IsNullOrEmpty(tag))
                            {

                                // parse tag
                                if (tag.ToUpper().Contains("END_LESSON"))
                                {
                                    iLastKeyword = i;
                                    iLesson++;
                                    // close lesson and save
                                    cursection.Lezioni.Add(curlesson);

                                }
                                else if (tag.ToUpper().Contains("LESSON"))
                                {
                                    iLastKeyword = i;
                                    // open new lesson 
                                    curlesson = new _ext_ImpLesson(tag);
                                    curlesson.Lezione.Order = iLesson;
                                    foreach (LanguageSection ls in languagescols)
                                    {
                                        curlesson.Lezione.Description.Add(ls.Language, GetValue(xlWorkSheet, i, ls.Col));
                                    }


                                }
                                else if (tag.ToUpper().Contains("END_QUESTION"))
                                {
                                    iLastKeyword = i;
                                    iQuestion++;
                                    // close question and save
                                    curlesson.Domande.Add(curquestion);

                                }
                                else if (tag.ToUpper().Contains("QUESTION"))
                                {
                                    iLastKeyword = i;
                                    curquestion = new _ext_ImpQuestion(tag);
                                    curquestion.Domanda.Order = iQuestion;

                                }
                                else if (tag.ToUpper().Contains("TEMPLATE"))
                                {
                                    iLastKeyword = i;
                                    string templatename = GetValue(xlWorkSheet, i, 2); // template posizionato accanto al tag template
                                    /// CHECK SECTION
                                    var templates = _engine.GetAll<Template>().Result;
                                    var check = (from t in templates
                                                 where t.Name.ToUpper() == templatename.ToUpper()
                                                 select t).FirstOrDefault();
                                    if (check == null)
                                    {
                                        cr.Success = false;
                                        cr.Messages.Add($"Template non censito presente nel file in importazione. Censire il template {templatename} o correggere e riprovare.");
                                        return null;
                                    }

                                    /// ################# END CHECK

                                    curquestion.Domanda.Type = templatename;

                                }
                                else if (tag.ToUpper().Contains("TEXT"))
                                {
                                    iLastKeyword = i;
                                    // fill question
                                    foreach (LanguageSection ls in languagescols)
                                    {
                                        string text = GetValue(xlWorkSheet, i, ls.Col);
                                        string icon1 = GetValue(xlWorkSheet, i, ls.Col + 1);
                                        string icon2 = GetValue(xlWorkSheet, i, ls.Col + 2);
                                        if (!string.IsNullOrEmpty(icon1))
                                        {
                                            if (!images.Contains(icon1))
                                            {
                                                cr.Success = false;
                                                cr.Messages.Add($"Immagine {icon1} in domanda {curquestion.Domanda.Name} non inserita per tipo {curquestion.Domanda.Type}. Verificare e riprovare.");
                                                return null;

                                            }
                                        }

                                        if (!string.IsNullOrEmpty(icon2))
                                        {
                                            if (!images.Contains(icon2))
                                            {
                                                cr.Success = false;
                                                cr.Messages.Add($"Immagine {icon2} in domanda {curquestion.Domanda.Name} non inserita per tipo {curquestion.Domanda.Type}. Verificare e riprovare.");
                                                return null;

                                            }
                                        }

                                        /// CHECK SECTION
                                        bool bok = TemplateHeaderCheck(curquestion.Domanda.Type, text, icon1, icon2);
                                        if (!bok)
                                        {
                                            cr.Success = false;
                                            cr.Messages.Add($"Il controllo sul testo della Sezione {cursection.Sezione.Name}, Lezione {curlesson.Lezione.Name}, Domanda {curquestion.Domanda.Name}  avente template {curquestion.Domanda.Type} non ha dato esito positivo. Testo e/o icone obbligatori. Verificare e riprovare.");
                                            return null;
                                        }

                                        /// ################# END CHECK
                                        RawContent rc = new RawContent()
                                        {
                                            Text = text,
                                            Icon1 = icon1,
                                            Icon2 = icon2
                                        };

                                        List<RawContentEntry> list = new List<RawContentEntry>();
                                        // entries
                                        for (int iraw = i + 1; iraw < 500; iraw++)
                                        {
                                            string c1 = GetValue(xlWorkSheet, iraw, 1);
                                            if (c1.ToUpper() == "END_QUESTION")
                                                break;
                                            else
                                            {
                                                RawContentEntry rce = new RawContentEntry();
                                                rce.C1 = c1;
                                                rce.C2 = GetValue(xlWorkSheet, iraw, ls.Col);
                                                rce.C3 = GetValue(xlWorkSheet, iraw, ls.Col + 1);
                                                rce.C4 = GetValue(xlWorkSheet, iraw, ls.Col + 2);
                                                rce.C5 = GetValue(xlWorkSheet, iraw, ls.Col + 3);
                                                list.Add(rce);
                                            }


                                        }

                                        var rowok = TemplateRowsCheck(curquestion.Domanda.Type, list);
                                        if (!rowok.Item1)
                                        {
                                            cr.Success = false;
                                            cr.Messages.Add($"Il controllo sui dati nella Sezione {cursection.Sezione.Name}, Lezione {curlesson.Lezione.Name}, Domanda {curquestion.Domanda.Name} avente template {curquestion.Domanda.Type} non ha dato esito positivo. Dettagli: {rowok.Item2}. Verificare e riprovare.");
                                            return null;
                                        }
                                        rc.Entries = list;

                                        curquestion.Domanda.Body.Add(ls.Language, rc);

                                    }

                                }
                                else if (tag.ToUpper().Contains("END_SECTION"))
                                {
                                    iLastKeyword = i;
                                    iSection++;
                                    iLesson = 0;
                                    iQuestion = 0;
                                    curlevel.Sezioni.Add(cursection);
                                    break; // e va oltre sui workbook

                                }
                                else if (tag.ToUpper().Contains("SECTION"))
                                {
                                    iLastKeyword = i;
                                    foreach (LanguageSection ls in languagescols)
                                    {
                                        cursection.Sezione.Description.Add(ls.Language, GetValue(xlWorkSheet, i, ls.Col));
                                    }
                                    cursection.Sezione.Icon = GetValue(xlWorkSheet, i, 3);
                                    if (cursection.Sezione.Icon == "")
                                    {
                                        cr.Success = false;
                                        cr.Messages.Add($"Ogni sezione deve contenere un'icona. Sezione {cursection.Sezione.Name} priva di icona");
                                        return null;
                                    }
                                    else
                                    {
                                        if (!images.Contains(cursection.Sezione.Icon))
                                        {
                                            cr.Success = false;
                                            cr.Messages.Add($"Icona di sezione non trovata. Sezione {cursection.Sezione.Name} - Icona mancante {cursection.Sezione.Icon}");
                                            return null;
                                        }
                                    }
                                    cursection.Sezione.Order = iSection;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        cr.Success = false;
                        cr.Messages.Add($"Errore in fase di importazione del file: {ex.Message}");
                        return null;
                    }
                }
            }
            System.Threading.Thread.CurrentThread.CurrentCulture = Oldci;

            return curlevel;

        }


        private Tuple<bool, string> TemplateRowsCheck(string type, List<RawContentEntry> list)
        {
            var bok = new Tuple<bool, string>(true, "");
            var images = _engine.GetAll<AppIcon>().Result.Select(x => x.Name).ToList();
            var colors = _engine.GetAll<AppColor>().Result.Select(x => x.Name).ToList();
            int irow = 0;
            if (type == "S2")
            {
                // tutto non obbligatorio
                foreach (var rc in list)
                {
                    if (irow == 0)
                    {
                        // fieldcheck
                        if (!(rc.C1 == "ID") || !(rc.C2 == "ELEMENT_1") || !(rc.C3 == "ELEMENT_2"))
                        {

                            bok = new Tuple<bool, string>(false, "Intestazione campi non corretta per domanda di tipo S2. Colonne corrette: ID ELEMNT_1 ELEMENT_2");
                            break;
                        }
                    }
                    else
                    {

                    }
                    irow++;

                }
            }
            else if (type == "S3")
            {
                foreach (var rc in list)
                {
                    if (irow == 0)
                    {
                        // fieldcheck
                        if (!(rc.C1 == "ID") || !(rc.C2 == "LABEL") || !(rc.C3 == "ICON") || !(rc.C4 == "COLOR"))
                        {

                            bok = new Tuple<bool, string>(false, "Intestazione campi non corretta per domanda di tipo S3. Colonne corrette: ID LABEL ICON COLOR");
                            break;
                        }
                    }
                    else
                    {
                        if (!images.Contains(rc.C3))
                        {
                            bok = new Tuple<bool, string>(false, $"Icona {rc.C3} non presente sul CMS. Caricare icona o modificare.");
                            break;

                        }

                        if (!colors.Contains(rc.C4))
                        {
                            bok = new Tuple<bool, string>(false, $"Colore {rc.C4} non presente sul CMS. Censire colore o modificare.");
                            break;

                        }

                        if (string.IsNullOrEmpty(rc.C4))
                        {
                            bok = new Tuple<bool, string>(false, $"Colore non indicato per domanda di tipo S3. Compilare colore o modificare.");
                            break;

                        }
                    }
                    irow++;
                }
            }
            else if (type == "S4")
            {
                int truecount = 0;
                foreach (var rc in list)
                {

                    if (irow == 0)
                    {
                        // fieldcheck
                        if (!(rc.C1 == "ID") || !(rc.C2 == "IMAGE") || !(rc.C3 == "CHECK"))
                        {

                            bok = new Tuple<bool, string>(false, "Intestazione campi non corretta per domanda di tipo S4. Colonne corrette: ID IMAGE CHECK");
                            break;
                        }
                    }
                    else
                    {
                        if (!images.Contains(rc.C2))
                        {
                            bok = new Tuple<bool, string>(false, $"Icona {rc.C2} non presente sul CMS. Caricare icona o modificare.");
                            break;
                        }

                        bool btrue = false;
                        var correct = bool.TryParse(rc.C3, out btrue);
                        if (!correct)
                        {
                            bok = new Tuple<bool, string>(false, $"Campo true o false errato per una domanda di tipo S4. {rc.C3} non interpretabile.");
                            break;
                        }
                        else
                        {
                            if (btrue)
                                truecount++;
                        }
                    }
                    irow++;

                }

                if ((truecount > 1) && (bok.Item1))
                {
                    bok = new Tuple<bool, string>(false, $"Per il template S4 è possibile UNA SOLA risposta corretta. Sono state inserite {truecount.ToString()} risposte True.");

                }
                else if ((truecount == 0) && (bok.Item1))
                {
                    bok = new Tuple<bool, string>(false, $"Per il template S4 è obbligatorio inserire UNA risposta corretta. Sono state inserite 0 risposte True.");
                }
            }
            else if (type == "S5_S9")
            {
                int truecount = 0;
                foreach (var rc in list)
                {
                    if (irow == 0)
                    {
                        // fieldcheck
                        if (!(rc.C1 == "ID") || !(rc.C2 == "ELEMENT_1") || !(rc.C3 == "CHECK"))
                        {

                            bok = new Tuple<bool, string>(false, "Intestazione campi non corretta per domanda di tipo S5_S9. Colonne corrette: ID ELEMENT_1 CHECK");
                            break;
                        }
                    }
                    else
                    {
                        bool btrue = false;
                        var correct = bool.TryParse(rc.C3.ToLower(), out btrue);
                        if (!correct)
                        {
                            bok = new Tuple<bool, string>(false, $"Campo true o false errato per una domanda di tipo S5_S9. {rc.C3} non interpretabile.");
                            break;
                        }
                        else
                        {
                            if (btrue)
                                truecount++;
                        }
                    }
                    irow++;

                }

                if ((truecount < 1) && (bok.Item1))
                {
                    bok = new Tuple<bool, string>(false, $"Per il template S5_S9 è obbligatoria ALMENO UNA risposta corretta. Sono state inserite {truecount.ToString()} risposte True.");
                }

            }
            else if (type == "S6_S7")
            {

                foreach (var rc in list)
                {
                    if (irow == 0)
                    {
                        // fieldcheck
						if (!(rc.C1 == "ID") || !(rc.C2 == "ANSWER") || !(rc.C3 == "UNIT") || !(rc.C4 == "ICON") || !(rc.C5 == "RANGE"))
                        {

                            bok = new Tuple<bool, string>(false, "Intestazione campi non corretta per domanda di tipo S6_S7. Colonne corrette: ID ANSWER UNIT ICON RANGE");
                            break;
                        }
                    }
                    else
                    {
                        List<string> acceptedvalues = new List<string>() { "temperature", "weight", "lenght", "percentage", "time" };
                        string value = rc.C3.ToLower();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (!acceptedvalues.Contains(value))
                            {
                                bok = new Tuple<bool, string>(false, $"Valore non accettato come UNIT di domanda S6_S7. Correggere valore {value}");
                                break;
                            }
                        }

                        if (!string.IsNullOrEmpty(rc.C4))
                        {
                            if (!images.Contains(rc.C4))
                            {
                                bok = new Tuple<bool, string>(false, $"Icona {rc.C4} non presente sul CMS. Caricare icona o modificare.");
                                break;
                            }
                        }

						if (!string.IsNullOrEmpty (rc.C5)) 
						{
							System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex (@"^((100)|(\d{0,2}))$");
							System.Text.RegularExpressions.Match match = regex.Match (rc.C5);
							if (!match.Success) 
							{
								bok = new Tuple<bool, string> (false, $"Valore non accettato come RANGE per la domanda S6_S7, sono accettati solo interi compresi tra 1 e 100. Correggere il valore {rc.C5}.");
								break;
							}
						}

                    }
                    irow++;

                }

            }
            else if (type == "S8")
            {
                string currentid = "";
                int countid = 0;
                foreach (var rc in list)
                {
                    if (irow == 0)
                    {
                        // fieldcheck
                        if (!(rc.C1 == "ID") || !(rc.C2 == "ANSWER") || !(rc.C3 == "UNIT") || !(rc.C4 == "ICON") || !(rc.C5 == "RANGE"))
                        {

                            bok = new Tuple<bool, string>(false, "Intestazione campi non corretta per domanda di tipo S8. Colonne corrette: ID ANSWER UNIT ICON RANGE");
                            break;
                        }
                    }
                    else
                    {

                        if (currentid == "")
                            currentid = rc.C1;
                        string id = rc.C1;
                        if (id == currentid)
                            countid++;
                        else
                        {
                            if (countid < 4)
                            {
                                bok = new Tuple<bool, string>(false, $"Elemento mancante tra i 4 obbligatori per ID {currentid} in domanda di tipo S8. Verificare.");
                                break;
                            }
                            else
                            {
                                currentid = rc.C1;
                                countid = 1;
                            }
                        }


                        List<string> acceptedvalues = new List<string>() { "temperature", "weight", "lenght", "percentage", "time" };
                        string value = rc.C3.ToLower();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (!acceptedvalues.Contains(value))
                            {
                                bok = new Tuple<bool, string>(false, $"Valore non accettato come UNIT di domanda S8. Correggere valore {value}");
                                break;
                            }
                        }
                        else
                        {
                            bok = new Tuple<bool, string>(false, $"Il valore in UNIT per domanda di tipo S8 non può essere vuoto. Valori accettati 'temperature', 'weight', 'lenght', 'percentage', 'time'");
                            break;
                        }

                        if (!images.Contains(rc.C4))
                        {
                            bok = new Tuple<bool, string>(false, $"Icona {rc.C4} non presente sul CMS. Caricare icona o modificare.");
                            break;
                        }

						if (!string.IsNullOrEmpty (rc.C5)) 
						{
							System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex (@"^((100)|(\d{0,2}))$");
							System.Text.RegularExpressions.Match match = regex.Match (rc.C5);
							if (!match.Success) {
								bok = new Tuple<bool, string> (false, $"Valore non accettato come RANGE per la domanda S8, sono accettati solo interi compresi tra 1 e 100. Correggere il valore {rc.C5}.");
								break;
							}
						}

                        string answer = rc.C2;
                        if (string.IsNullOrEmpty(rc.C2))
                        {
                            bok = new Tuple<bool, string>(false, $"Risposta VUOTA in template S8. Correggere.");
                            break;
                        }
                        else
                        {
                            double dcheck = 0;

                            var okparse = double.TryParse(answer, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out dcheck);
                            if (!okparse)
                            {
                                bok = new Tuple<bool, string>(false, $"Valore numerico ERRATO per domanda di tipo S8. Valore: {rc.C2} non riconosciuto.");
                                break;
                            }
                        }

                    }
                    irow++;

                }



            }
            else if (type == "S10_S11_S12")
            {
                ///standby
                ///

                if (list.Count < 6)
                {
                    bok = new Tuple<bool, string>(false, "Sezioni non presenti per domanda di tipo S10_S11_S12. Controllare sequenza delle righe: RIGA1 RIGA2 RIGA3 RIGA4 TEMPERATURE TIME.");

                }
                else
                {

                    foreach (var rc in list)
                    {
                        if (irow < 4)
                        {
                            if (!rc.C1.ToUpper().Contains("RIGA"))
                            {
                                bok = new Tuple<bool, string>(false, $"RIGA non presente in linea {(irow + 1).ToString()} per template S10_S11_S12. Correggere.");

                            }

                        }
                        if (rc.C1.ToUpper().Contains("RIGA"))
                        {
                            List<string> acceptedvalues = new List<string>() { "x", "o" };
                            string value = rc.C2.ToLower();
                            if (!string.IsNullOrEmpty(value))
                            {
                                if (!acceptedvalues.Contains(value))
                                {
                                    bok = new Tuple<bool, string>(false, $"Valore non accettato in RIGA1, linea {(irow + 1).ToString()} per template S10_S11_S12. Correggere valore {value}");
                                }
                            }

                            value = rc.C3.ToLower();
                            if (!string.IsNullOrEmpty(value))
                            {
                                if (!acceptedvalues.Contains(value))
                                {
                                    bok = new Tuple<bool, string>(false, $"Valore non accettato in RIGA2 , linea {(irow + 1).ToString()} per template S10_S11_S12. Correggere valore {value}");
                                }
                            }

                            value = rc.C4.ToLower();
                            if (!string.IsNullOrEmpty(value))
                            {
                                if (!acceptedvalues.Contains(value))
                                {
                                    bok = new Tuple<bool, string>(false, $"Valore non accettato in RIGA3 , linea {(irow + 1).ToString()} per template S10_S11_S12. Correggere valore {value}");
                                }
                            }

                            value = rc.C5.ToLower();
                            if (!string.IsNullOrEmpty(value))
                            {
                                if (!acceptedvalues.Contains(value))
                                {
                                    bok = new Tuple<bool, string>(false, $"Valore non accettato in RIGA4 , linea {(irow + 1).ToString()} per template S10_S11_S12. Correggere valore {value}");
                                }
                            }
                        }
                        else if (irow == 4) // temperature
                        {
                            if (!(rc.C1 == "ASSEX"))
                            {

                                bok = new Tuple<bool, string>(false, "Sezione non corretta per domanda di tipo S10_S11_S12. Controllare sequenza delle righe: RIGA1 RIGA2 RIGA3 RIGA4 ASSEX ASSEY.");
                                break;
                            }
                            else
                            {
                                // eventuale controllo per valori

                            }
                        }
                        else if (irow == 5)
                        {
                            if (!(rc.C1 == "ASSEY"))
                            {

                                bok = new Tuple<bool, string>(false, "Sezione non corretta per domanda di tipo S10_S11_S12. Controllare sequenza delle righe: RIGA1 RIGA2 RIGA3 RIGA4 ASSEX ASSEY.");
                                break;
                            }
                            else
                            {
                                // eventuale controllo per valori

                            }
                        }


                        irow++;

                    }
                }
            }
            else if (type == "S13")
            {
                // tutto non obbligatorio
                string currentid = "";

                int countid = 0;
                foreach (var rc in list)
                {
                    if (irow == 0)
                    {
                        // fieldcheck
                        if (!(rc.C1 == "ID") || !(rc.C2 == "ELEMENT_1") || !(rc.C3 == "UNIT") || !(rc.C4 == "ICON"))
                        {

                            bok = new Tuple<bool, string>(false, "Intestazione campi non corretta per domanda di tipo S13. Colonne corrette: ID ELEMENT_1 UNIT ICON");
                            break;
                        }
                    }
                    else
                    {
                        if (currentid == "")
                            currentid = rc.C1;
                        string id = rc.C1;
                        if (id == currentid)
                            countid++;
                        else
                        {
                            if (countid < 4)
                            {
                                bok = new Tuple<bool, string>(false, $"Elemento mancante tra i 4 obbligatori per ID {currentid} in domanda di tipo S13. Verificare.");
                                break;
                            }
                            else
                            {
                                currentid = rc.C1;
                                countid = 1;
                            }
                        }

                        if (!images.Contains(rc.C4))
                        {
                            bok = new Tuple<bool, string>(false, $"Icona {rc.C4} non presente sul CMS. Caricare icona o modificare.");
                            break;
                        }

                    }
                    irow++;

                }

            }
            else if (type == "S14")
            {

                if (list.Count < 19)
                {
                    bok = new Tuple<bool, string>(false, "Sezioni non presenti per domanda di tipo S14. Controllare sequenza delle righe: RIGA1 RIGA2 RIGA3 RIGA4 ASSEX ASSEY.");

                }
                else
                {

                    string currentid = "";

                    int countid = 0;
                    foreach (var rc in list)
                    {
                        if (rc.C1.ToUpper().Contains("RIGA"))
                        {
                            List<string> acceptedvalues = new List<string>() { "x", "o" };
                            string value = rc.C2.ToLower();
                            if (!string.IsNullOrEmpty(value))
                            {
                                if (!acceptedvalues.Contains(value))
                                {
                                    bok = new Tuple<bool, string>(false, $"Valore non accettato in RIGA1, linea {(irow + 1).ToString()} per template S14. Correggere valore {value}");
                                }
                            }

                            value = rc.C3.ToLower();
                            if (!string.IsNullOrEmpty(value))
                            {
                                if (!acceptedvalues.Contains(value))
                                {
                                    bok = new Tuple<bool, string>(false, $"Valore non accettato in RIGA2 , linea {(irow + 1).ToString()} per template S14. Correggere valore {value}");
                                }
                            }

                            value = rc.C4.ToLower();
                            if (!string.IsNullOrEmpty(value))
                            {
                                if (!acceptedvalues.Contains(value))
                                {
                                    bok = new Tuple<bool, string>(false, $"Valore non accettato in RIGA3 , linea {(irow + 1).ToString()} per template S14. Correggere valore {value}");
                                }
                            }

                            value = rc.C5.ToLower();
                            if (!string.IsNullOrEmpty(value))
                            {
                                if (!acceptedvalues.Contains(value))
                                {
                                    bok = new Tuple<bool, string>(false, $"Valore non accettato in RIGA4 , linea {(irow + 1).ToString()} per template S14. Correggere valore {value}");
                                }
                            }
                        }

                        if (irow == 6)
                        {
                            // fieldcheck
                            if (!(rc.C1 == "ID") || !(rc.C2 == "ELEMENT_1") || !(rc.C3 == "UNIT") || !(rc.C4 == "ICON"))
                            {

                                bok = new Tuple<bool, string>(false, "Intestazione campi non corretta per domanda di tipo S14. Colonne corrette: ID ELEMENT_1 UNIT ICON");
                                break;
                            }
                        }
                        else if (irow == 4) // temperature
                        {
                            if (!(rc.C1 == "ASSEX"))
                            {

                                bok = new Tuple<bool, string>(false, "Sezione non corretta per domanda di tipo S14. Controllare sequenza delle righe: RIGA1 RIGA2 RIGA3 RIGA4 ASSEX ASSEY ID.");
                                break;
                            }
                            else
                            {
                                // eventuale controllo per valori

                            }
                        }
                        else if (irow == 5)
                        {
                            if (!(rc.C1 == "ASSEY"))
                            {

                                bok = new Tuple<bool, string>(false, "Sezione non corretta per domanda di tipo S14.  Controllare sequenza delle righe: RIGA1 RIGA2 RIGA3 RIGA4  ASSEX ASSEY ID.");
                                break;
                            }
                            else
                            {
                                // eventuale controllo per valori

                            }
                        }
                        else if (irow > 6)
                        {


                            if (currentid == "")
                                currentid = rc.C1;
                            string id = rc.C1;
                            if (id == currentid)
                                countid++;
                            else
                            {
                                if (countid < 3)
                                {
                                    bok = new Tuple<bool, string>(false, $"Elemento mancante tra i 3 obbligatori per ID {currentid} in domanda di tipo S14. Verificare.");
                                    break;
                                }
                                else
                                {
                                    currentid = rc.C1;
                                    countid = 1;
                                }
                            }
                            if (!string.IsNullOrEmpty(rc.C4))
                            {
                                if (!images.Contains(rc.C4))
                                {
                                    bok = new Tuple<bool, string>(false, $"Icona {rc.C4} non presente sul CMS. Caricare icona o modificare.");
                                    break;
                                }
                            }

                        }
                        irow++;

                    }
                }
            }
            return bok;
        }

        private bool TemplateHeaderCheck(string type, string text, string icon1, string icon2)
        {
            bool bok = true;
            if (type == "S2")
            {
                // tutto non obbligatorio
            }
            else if (type == "S3")
            {
                // tutto non obbligatorio
            }
            else if (type == "S4")
            {
                if (text == "")
                {
                    bok = false;
                }
            }
            else if (type == "S5_S9")
            {
                if (text == "")
                {
                    bok = false;
                }
            }
            else if (type == "S6_S7")
            {
                if (text == "")
                {
                    bok = false;
                }
            }
            else if (type == "S8")
            {
                if (text == "")
                {
                    bok = false;
                }
            }
            else if (type == "S10_S11_S12")
            {
                if ((text == "") && (icon1 == "") && (icon2 == ""))
                {
                    bok = false;
                }
                else
                {


                }
            }
            else if (type == "S13")
            {
                // tutto non obbligatorio
            }
            else if (type == "S14")
            {
                if ((text == "") && (icon1 == ""))
                {
                    bok = false;
                }

            }
            return bok;


        }

        private static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;

            }
            finally
            {
                GC.Collect();
            }
        }

    }
}
