﻿using System;
using System.Linq;
using System.Web;
using EduAppWS.Jobs;
using Hangfire;
using Hangfire.Annotations;
using Hangfire.Dashboard;
using Hangfire.Mongo;
using Microsoft.Owin;
using Microsoft.Owin.Extensions;
using Owin;
using ServerCore.Interfaces;

[assembly: OwinStartup(typeof(EduAppWS.Startup))]
namespace EduAppWS
{
	public class Global : HttpApplication
	{
		protected void Application_Start(Object sender, EventArgs e)
		{
        }

		protected void Session_Start(Object sender, EventArgs e)
		{
        }

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{
            //Previene il redirect dell "Unauthorized"
            this.Context.Response.SuppressFormsAuthenticationRedirect = true;
		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{
        }

		protected void Application_Error(Object sender, EventArgs e)
		{

		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
	}

	public class Startup
	{
		public void Configuration(IAppBuilder app)
		{
            var mongocs = System.Configuration.ConfigurationManager.ConnectionStrings["mongoDBCS"].ConnectionString;
			var mongoDBName = System.Configuration.ConfigurationManager.AppSettings["mongoDBName"];
			var scheduleCkeck = System.Configuration.ConfigurationManager.AppSettings["scheduleCkeck"];

			GlobalConfiguration.Configuration
			                   .UseMongoStorage(mongocs, mongoDBName);

			app.UseHangfireDashboard("/hangfire", new DashboardOptions
			{
				Authorization = new Hangfire.Dashboard.IDashboardAuthorizationFilter[] { new DashAuth() }
			});
			app.UseHangfireServer(new BackgroundJobServerOptions { Activator = new NancyContainerJobActivator() });
//			//app.UseNancy ();
			app.UseStageMarker(PipelineStage.MapHandler);

            RecurringJob.AddOrUpdate<CheckUserLoginTimelapse>(x => x.StartChecking(), scheduleCkeck);
		}
	}

	public class DashAuth : Hangfire.Dashboard.IDashboardAuthorizationFilter
	{
		public bool Authorize([NotNull] DashboardContext context)
		{
            var auth = Bootstrapper.AppContainer.Resolve<IAuthBEAppModule>();
			var ctx = HttpContext.Current;
            if (ctx.Request.Cookies["token"] != null)
                return auth.Auth(null, ctx.Request.Cookies["token"].Value ?? "").Claims.Contains("SuperAdmin");
            return false;
		}
	}

	public class NancyContainerJobActivator : JobActivator
	{

		public NancyContainerJobActivator()
		{

		}

		public override object ActivateJob(Type type)
		{
			return Bootstrapper.AppContainer.Resolve(type);
		}
	}
}
