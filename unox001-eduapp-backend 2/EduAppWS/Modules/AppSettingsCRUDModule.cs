﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduAppWS.CMS;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using ServerCore.NancyModule;
using System.Threading.Tasks;
using SharedCore.Entities;
using System.IO;
using Unox;
using Unox.Models.ImporterModels;
using Unox.Models.RawModels;
using System.Text.RegularExpressions;

namespace EduAppWS.Modules
{
    public class CMSAppSettings : CMSEntity<AppSettings> {

       
    }

    [CRUDModule(EntityName = "appsettings", PrimaryKeyName = "CorrelationId")]
    public class AppSettingsCRUDModule : CRUDModule<CMSAppSettings, AppSettings>
    {
        IAuthBEAppModule _auth;
        IBEAppCoreContext _beAppContext;
        IContentBEAppModule _storage;
        public AppSettingsCRUDModule(IBEEntityModule ee, IAuthBEAppModule auth, IBEAppCoreContext beAppContext, IContentBEAppModule beContents) : base(ee)
        {
            _auth = auth;
            _beAppContext = beAppContext;
            _storage = beContents;
        }


    }

}