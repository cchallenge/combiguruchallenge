﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nancy;
using Newtonsoft.Json.Linq;
using ServerCore.Interfaces.Logging;
using UnoxEduAppModule;
using ServerCore.Interfaces;
using System.Threading.Tasks;
using Unox.Models.RawModels;
using SharedCore.Entities;
using System.Text.RegularExpressions;
using System.Globalization;
using SharedCore.Interfaces;

namespace EduAppWS
{
    public class NancyUnoxEduModule : NancyModule
    {
        ILog logger = LogProvider.GetLogger("NancyUnoxEduModule");
        IBEEntityModule _ee;
        /*
         __          _______                   _            
         \ \        / / ____|                 | |           
          \ \  /\  / / (___    _ __ ___  _   _| |_ ___  ___ 
           \ \/  \/ / \___ \  | '__/ _ \| | | | __/ _ \/ __|
            \  /\  /  ____) | | | | (_) | |_| | ||  __/\__ \
             \/  \/  |_____/  |_|  \___/ \__,_|\__\___||___/

                                                            */



        public NancyUnoxEduModule(ServerCore.Interfaces.IBEEntityModule entityModule) : base("edu")
        {
            _ee = entityModule;


            this.Get["test"] = x => Response.AsJson(new { result = true });

            this.Get["learningPath"] = x => Response.AsJson(
                Task.Run(() => GetLearningPath((string)Request.Query.userId, (string)Request.Query.lang, (string)Request.Query.measureUnit)).Result);

            this.Get["learningPath/level"] = x => Response.AsJson(
                Task.Run(() => GetUserLevel((string)Request.Query.userId)).Result);

            this.Get["lessons/{sectionId}"] = x => Response.AsJson(
                Task.Run(() => GetAllLessons((string)x.sectionId, (string)Request.Query.userId, (string)Request.Query.lang, (string)Request.Query.measureUnit)).Result
            );

            this.Get["lesson/{lessonId}"] = x => Response.AsJson(
                Task.Run(() => GetLesson((string)x.lessonId, (string)Request.Query.userId, (string)Request.Query.lang, (string)Request.Query.measureUnit)).Result
            );

            this.Get["challenge/{levelId}"] = x => Response.AsJson(
                Task.Run(() => GetChallenge((string)x.levelId, (string)Request.Query.userId, (string)Request.Query.lang, (string)Request.Query.measureUnit)).Result
            );

            this.Get["lesson/{lessonId}/finished"] = x => Response.AsJson(
                Task.Run(() => SetLessonFinished((string)x.lessonId, (string)Request.Query.userId)).Result
                );

            this.Get["challenge/{levelId}/finished"] = x => Response.AsJson(
                Task.Run(() => SetChallengeFinished((string)x.levelId, (string)Request.Query.userId)).Result
                );

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///                 Extensions to support Edxiting challenges !!                                     ///
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            this.Get["_ext_learningPath/level"] = x => Response.AsJson(
                Task.Run(() => _ext_GetUserLevel((string)Request.Query.userId)).Result
            );

            this.Get["_ext_lessons/{sectionId}"] = x => Response.AsJson(
                Task.Run(() => _ext_GetAllLessons((string)x.sectionId, (string)Request.Query.userId, (string)Request.Query.lang, (string)Request.Query.measureUnit)).Result
            );

            this.Get["_ext_update_set_nickname/{nickName}"] = x => Response.AsJson(
                Task.Run(() => _ext_UpdateOrSetNickname((string)x.nickName, (string)Request.Query.userId)).Result
            );

            this.Get["_ext_set_user_institution/{institutionId}"] = x => Response.AsJson(
                Task.Run(() => _ext_SetUserInstitution((string)x.institutionId, (string)Request.Query.userId)).Result
            );

            this.Get["_ext_get_user_profile/user"] = x => Response.AsJson(
                Task.Run(() => _ext_GetUserProfile((string)Request.Query.userId)).Result
            );

            this.Get["_ext_lesson/{lessonId}/finished"] = x => Response.AsJson(
                Task.Run(() => _ext_SetLessonFinished((string)x.lessonId, (string)Request.Query.userId)).Result
            );

            //this one differs from the traditional challenge, identifies last step in the learning path - hence default in the name
            this.Get["lpathchallenge/{levelId}"] = x => Response.AsJson(
                Task.Run(() => _ext_GetLearningPathChallenge((string)x.levelId, (string)Request.Query.userId, (string)Request.Query.lang, (string)Request.Query.measureUnit)).Result
            );

            this.Get["lpathchallenge/{levelId}/finished"] = x => Response.AsJson(
                Task.Run(() => _ext_SetLearningPathChallengeFinished((string)x.levelId, (string)Request.Query.userId)).Result
            );

            //call issued when a challenge has been completed by the user
            this.Get["_ext_challenge/{challengeId}/finished"] = x => Response.AsJson(
                Task.Run(() => _ext_SetChallengeFinished((string)x.challengeId, (string)Request.Query.userId,
                                                                    (string)Request.Query.lang,
                                                                    (string)Request.Query.measureUnit,
                                                                    (string)Request.Query.totalCorrect,
                                                                    (string)Request.Query.totalQuestions)).Result
            );

            //call issued to retrieve challenges attributed to the calls' specified institution
            this.Get["_ext_challenges/all"] = x => Response.AsJson(
                Task.Run(() => _ext_GetActiveChallenges((string)Request.Query.userId,
                                                                    (string)Request.Query.lang,
                                                                    (string)Request.Query.measureUnit)).Result
            );

            //call issued to retrieve the challenge content
            this.Get["_ext_challenge/{challengeId}"] = x => Response.AsJson(
                Task.Run(
                                    () => _ext_GetChallenge((string)x.challengeId,
                                                                    (string)Request.Query.userId,
                                                                    (string)Request.Query.lang,
                                                                    (string)Request.Query.measureUnit)).Result
            );

            this.Get["_ext_ranking/{instituteId}/filterInstitute"] = x => Response.AsJson(
                Task.Run(() => _ext_GetInstitutionRanking((string)x.instituteId)).Result
             );

            this.Get["_ext_ranking/nullInstitute"] = x => Response.AsJson(
               Task.Run(() => _ext_GetNullInstitutionRanking()).Result
            );


            this.Get["unoxurl"] = x => Response.AsText(
               Task.Run(() => GetUnoxUrl()).Result
               );

            this.Get["cheat/{correlationid}"] = x => Response.AsJson(
               Task.Run(() => CheatForUser((string)x.correlationid)).Result
               );
        }

        /*
          _                    _       _____                _     _           
         | |                  | |     |  __ \              (_)   | |          
         | |     _____   _____| |___  | |__) | __ _____   ___  __| | ___ _ __ 
         | |    / _ \ \ / / _ \ / __| |  ___/ '__/ _ \ \ / / |/ _` |/ _ \ '__|
         | |___|  __/\ V /  __/ \__ \ | |   | | | (_) \ V /| | (_| |  __/ |   
         |______\___| \_/ \___|_|___/ |_|   |_|  \___/ \_/ |_|\__,_|\___|_|   
                                                                              */



        static LearningPath _learningPath;

        private async Task<string> GetUnoxUrl()
        {
            var engine = _ee.Engine;
            var unoxurl = (await engine.GetAll<Unox.Models.RawModels.AppSettings>()).Where(x => x.Name == "UnoxUrl").FirstOrDefault();

            return unoxurl.Value.ToString();
        }

        async Task<object> GetLearningPath(string userId, string lang, string measureUnit)
        {
            logger.Info(() => $"Richiesta learning path INIZIO per utente: {userId} in lingua: {lang} con unità di misura codice: {measureUnit}");
            var engine = _ee.Engine;
            var key = $"{lang}_{measureUnit}";

            var cachedLearningPath = (await engine.GetAll<LearningPath>()).FirstOrDefault(p => p.Key.ToLower() == key.ToLower());

            if (cachedLearningPath != null)
                return cachedLearningPath;
            else
                return GenerateAndSaveLearningPath(lang, measureUnit);

            return null;
        }

        public async Task<LearningPath> GenerateAndSaveLearningPath(string lang, string measureUnit)
        {
            logger.Info(() => $"GenerateAndSaveLearningPath - Richiesta learning path INIZIO in lingua: {lang} con unità di misura codice: {measureUnit}");


            var engine = _ee.Engine;
            LearningPath appLearningPath = engine.New<LearningPath>();
            appLearningPath.Key = $"{lang}_{measureUnit}";
            var level = (await engine.GetAll<Unox.Models.RawModels.Level>()).OrderBy(x => int.Parse(x.Name)).ToList();
            if (level != null)
            {
                var unoxLevels = new List<UnoxEduAppModule.Level>();
                foreach (var l in level)
                {
                    var unoxSections = new List<UnoxEduAppModule.Section>();
                    //Richiedo le sezioni per quel livello
                    var section = (await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.ChildOf, l)).OrderBy(x => x.Order).ToList();
                    if (section != null)
                    {
                        foreach (var s in section)
                        {
                            var lessons = (await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, s)).OrderBy(x => x.Order).ToList();
                            unoxSections.Add(new UnoxEduAppModule.Section()
                            {
                                CorrelationId = s.CorrelationId,
                                Image = await GetIconUrlByName(s.Icon),
                                Title = ParseForCorrectMetrics(s.Description[lang.ToUpper()], measureUnit),
                                NLessons = lessons.Count()
                            });
                        }
                    }
                    else
                    {
                        logger.Warn(() => $"NON ho trovato sezioni per il livello con id {l.CorrelationId}");
                    }

                    unoxLevels.Add(new UnoxEduAppModule.Level()
                    {
                        Title = ParseForCorrectMetrics(l.Description[lang.ToUpper()], measureUnit),
                        CorrelationId = l.CorrelationId,
                        Image = await GetIconUrlByName(l.Image),
                        ChallengeImage = await GetIconUrlByName(l.ChallengeImage),
                        ChallengeTitle = ParseForCorrectMetrics(l.ChallengeTitle[lang.ToUpper()], measureUnit),
                        ScienceImage = await GetIconUrlByName(l.ScienceImage),
                        ScienceTitle = ParseForCorrectMetrics(l.ScienceTitle[lang.ToUpper()], measureUnit),
                        ScienceHTML = l.Science[lang.ToUpper()],
                        Sections = unoxSections
                    });
                }
                appLearningPath.Levels = unoxLevels;
                appLearningPath.Contents = await GetLearningPathContents(appLearningPath);
                logger.Info(() => $"Richiesta learning path COMPLETATA in lingua: {lang} con unità di misura codice: {measureUnit}");

                await engine.Update<LearningPath>(appLearningPath);

                return appLearningPath;
            }
            else
            {
                logger.Warn(() => $"NON ho trovato livelli");

                return null;
            }

        }

        private async Task<string[]> GetLearningPathContents(LearningPath lp)
        {
            //FIXME: creare una chiamata per i soli contents richiesti
            var engine = _ee.Engine;
            List<string> contents = new List<string>();
            var icons = await engine.GetAll<Unox.Models.RawModels.AppIcon>();
            foreach (var l in lp.Levels)
            {
                if (!contents.Contains(l.Image))
                    contents.Add(l.Image);
                if (!contents.Contains(l.ChallengeImage))
                    contents.Add(l.ChallengeImage);
                if (!contents.Contains(l.ScienceImage))
                    contents.Add(l.ScienceImage);
                foreach (var s in l.Sections)
                {
                    if (!contents.Contains(s.Image))
                        contents.Add(s.Image);
                }
            }
            //.Select(x => x.FileKey).ToArray<string>();
            return contents.ToArray();
        }

        /*
          _                                           _____ _           _ _                       
         | |                                 ___     / ____| |         | | |                      
         | |     ___  ___ ___  ___  _ __    ( _ )   | |    | |__   __ _| | | ___ _ __   __ _  ___ 
         | |    / _ \/ __/ __|/ _ \| '_ \   / _ \/\ | |    | '_ \ / _` | | |/ _ \ '_ \ / _` |/ _ \
         | |___|  __/\__ \__ \ (_) | | | | | (_>  < | |____| | | | (_| | | |  __/ | | | (_| |  __/
         |______\___||___/___/\___/|_| |_|  \___/\/  \_____|_| |_|\__,_|_|_|\___|_| |_|\__, |\___|
                                                                                        __/ |     
                                                                                       |___/      
                                                                                       */
        async Task<object> GetAllLessons(string sectionId, string userId, string lang, string measureUnit)
        {
            logger.Info(() => $"Richiesta di tutte le lezioni per utente: {userId} in lingua: {lang} con unità di misura codice: {measureUnit} per la sezione: {sectionId}");

            var lessonlist = new List<LessonHeader>();
            var engine = _ee.Engine;

            var section = await engine.Get<Unox.Models.RawModels.Section>(sectionId);
            SectionStat foundsection = null;
            if (section != null)
            {
                var userstat = await GetUserLevel(userId) as UserStats;

                foreach (var levelstat in userstat.LevelsStat)
                {

                    foundsection = levelstat.SectionsStat.Where(x => x.CorrelationId == sectionId).FirstOrDefault();
                    if (foundsection != null)
                        break;

                }

                if (foundsection != null)
                {
                    //var sresult = (from ss in userstat.LevelsStat
                    //               where ss.CorrelationId == sectionId
                    //               select ss).FirstOrDefault();

                    int ilesson = 1;
                    var lessons = (await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, section)).OrderBy(x => x.Order).ToList();
                    foreach (var lesson in lessons)
                    {
                        bool isaccessible = (ilesson <= (foundsection.LessonsFinishedNumber + 1));


                        LessonHeader lh = new LessonHeader()
                        {
                            CorrelationId = lesson.CorrelationId,
                            Description = lesson.Description[lang.ToUpper()],
                            AccessibleLesson = isaccessible
                        };
                        lessonlist.Add(lh);
                        ilesson++;
                    }
                }
                else
                {

                    throw new Exception("Section not found !");
                }
            }
            else
            {

                throw new Exception("Section not found !");
            }

            return lessonlist;
        }

        async Task<object> _ext_GetAllLessons(string sectionId, string userId, string lang, string measureUnit)
        {

            logger.Info(() => $"Richiesta di tutte le lezioni per utente: {userId} in lingua: {lang} con unità di misura codice: {measureUnit} per la sezione: {sectionId}");

            var lessonlist = new List<LessonHeader>();
            var engine = _ee.Engine;

            var section = await engine.Get<Unox.Models.RawModels.Section>(sectionId);
            SectionStat foundsection = null;
            if (section != null)
            {
                var userstat = await _ext_GetUserLevel(userId) as _ext_UserStatsDefaultPath;

                foreach (var levelstat in userstat.LevelsStat)
                {

                    foundsection = levelstat.SectionsStat.Where(x => x.CorrelationId == sectionId).FirstOrDefault();
                    if (foundsection != null)
                        break;

                }

                if (foundsection != null)
                {
                    //var sresult = (from ss in userstat.LevelsStat
                    //               where ss.CorrelationId == sectionId
                    //               select ss).FirstOrDefault();

                    int ilesson = 1;
                    var lessons = (await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, section)).OrderBy(x => x.Order).ToList();
                    foreach (var lesson in lessons)
                    {
                        bool isaccessible = (ilesson <= (foundsection.LessonsFinishedNumber + 1));


                        LessonHeader lh = new LessonHeader()
                        {
                            CorrelationId = lesson.CorrelationId,
                            Description = lesson.Description[lang.ToUpper()],
                            AccessibleLesson = isaccessible
                        };
                        lessonlist.Add(lh);
                        ilesson++;
                    }
                }
                else
                {

                    throw new Exception("Section not found !");
                }
            }
            else
            {

                throw new Exception("Section not found !");
            }

            return lessonlist;
        }


        public async Task<string> GetQuestionTitleForLang(string type, string lang)
        {
            string sresult = "";
            var engine = _ee.Engine;
            var templates = await engine.GetAll<Unox.Models.RawModels.Template>();

            sresult = (from t in templates
                       where t.Name == type
                       select t.Titles[lang.ToUpper()]).FirstOrDefault();

            return sresult;
        }


        public async Task<List<_ext_ChallengeHeader>> _ext_GetActiveChallenges(string userId, string lang, string measureUnit)
        {
            logger.Info(() => $"Richiesta challenge in lingua: {lang} per utente: {userId} con unità di misura codice: {measureUnit}");

            var engine = _ee.Engine;

            //1. filter all expired challenges
            var allChallenges = (await engine.GetAll<Unox.Models.RawModels._ext_Challenge>()).OrderBy(x => x.DateStart).ToList();
            logger.Debug("Numero sfide trovate " + allChallenges.Count());
            List<_ext_ChallengeHeader> activeChallenges = new List<_ext_ChallengeHeader>();
            foreach (var c in allChallenges)
            {
                if (c.DateEnd >= DateTime.Now && DateTime.Now >= c.DateStart) {
                    _ext_ChallengeHeader ch = new _ext_ChallengeHeader()
                    {
                        CorrelationId = c.CorrelationId,
                        Title = c.Title[lang.ToUpper()],
                        Image = c.Image,
                        DateFrom = c.DateStart,
                        DateTo = c.DateEnd
                    };
                    activeChallenges.Add(ch);
                }
            }
            logger.Debug("Rispondo con numero sfide " + activeChallenges.Count());
            //2. filter and set if the user has completed this current challenge or not.
            var uCorrId = await _ext_GetOrSetUser(userId);
            if (uCorrId == null) {
                logger.Debug("_ext_GetActiveChallenges - user found to be null.. this should not have happened!");
                return null;
            }
            var user = await engine.Get<Unox.Models.RawModels._ext_User>(uCorrId);
            var userachievements = (await engine.GetAll<Unox.Models.RawModels.UserAchievement>(EntityRelation.Linked, user)).ToList();

            foreach (var active in activeChallenges)
            {
                logger.Debug("Rispondo con numero sfide - punto#2");
                var id = (from u in userachievements
                                 where u.AchievementId == active.CorrelationId
                                 select u).FirstOrDefault();
                active.Completed = (id != null);
            }
            logger.Debug("Rispondo con numero sfide: " + activeChallenges.Count() + " tra le qualli " + activeChallenges.Where(x => x.Completed).ToList().Count() + " sono state completate !");

            return activeChallenges;
        }

        public async Task<object> _ext_GetChallenge(string challengeId, string userId, string lang, string measureUnit)
        {
            logger.Info(() => $"Richiesta sfida: {challengeId} in lingua: {lang} per utente: {userId} con unità di misura codice: {measureUnit}");

            var engine = _ee.Engine;
            UnoxEduAppModule._ext_Challenge appChallenge = new UnoxEduAppModule._ext_Challenge();
            appChallenge.CorrelationId = challengeId;
            var challenge = await engine.Get<Unox.Models.RawModels._ext_Challenge>(challengeId);

            if (challenge == null) {
                logger.Debug("Some error has occurred, the challenge " + challengeId + " was not found!");
                return appChallenge;
            }

            // description in lingua della lezione
            var description = challenge.Description[lang.ToUpper()];
            appChallenge.Description = description;
            // recupero domande
            var questions = (await engine.GetAll<Unox.Models.RawModels.Question>("childOf", challenge)).OrderBy(x => x.Order).ToList();

            if (questions == null)
            {
                logger.Warn(() => $"NON ho trovato domande per la sfida con id {challengeId}");
                return appChallenge;
            }
            else {
                logger.Debug("Found a total of " + questions.Count() + " questions");
            }

            List<UnoxEduAppModule.Question> appQuestions = new List<UnoxEduAppModule.Question>();
            // trovate. costruisco il dettaglio 
            foreach (var question in questions)
            {
                UnoxEduAppModule.Question q = new UnoxEduAppModule.Question();
                q.CorrelationId = question.CorrelationId;
                q.TemplateType = (TemplateType)Enum.Parse(typeof(TemplateType), question.Type.ToUpper());
                q.Title = await GetQuestionTitleForLang(question.Type, lang.ToUpper()); // question.Name;

                var body = question.Body;
                var languagebody = (from c in body
                                        where c.Key == lang.ToUpper()
                                        select c.Value).FirstOrDefault();
                if (languagebody != null)
                {
                    q.Description = ParseForCorrectMetrics(languagebody.Text, measureUnit);
                    q.Payload = GetPayloadForTemplate(question, languagebody, measureUnit);
                }
                appQuestions.Add(q);
            }

            appChallenge.Questions = appQuestions;
            logger.Debug("Fetched user challenge with a total of " + appChallenge.Questions.Count() + " questions");

            appChallenge.Contents = await GetLessonContents(appChallenge);

        return appChallenge;
        }


        public async Task<object> GetLesson(string lessonId, string userId, string lang, string measureUnit)
        {
            logger.Info(() => $"Richiesta lezione: {lessonId} in lingua: {lang} per utente: {userId} con unità di misura codice: {measureUnit}");

            var engine = _ee.Engine;
            UnoxEduAppModule.Lesson appLesson = new UnoxEduAppModule.Lesson();
            appLesson.CorrelationId = lessonId;
            var lesson = await engine.Get<Unox.Models.RawModels.Lesson>(lessonId);
            if (lesson != null)
            {
                // description in lingua della lezione
                var description = lesson.Description[lang.ToUpper()];
                appLesson.Description = description;
                // recupero domande
                var questions = (await engine.GetAll<Unox.Models.RawModels.Question>("childOf", lesson)).OrderBy(x => x.Order).ToList();
                if (questions != null)
                {
                    List<UnoxEduAppModule.Question> appQuestions = new List<UnoxEduAppModule.Question>();
                    // trovate. costruisco il dettaglio 
                    foreach (var question in questions)
                    {
                        UnoxEduAppModule.Question q = new UnoxEduAppModule.Question();
                        q.CorrelationId = question.CorrelationId;
                        q.TemplateType = (TemplateType)Enum.Parse(typeof(TemplateType), question.Type.ToUpper());
                        q.Title = await GetQuestionTitleForLang(question.Type, lang.ToUpper()); // question.Name;

                        var body = question.Body;
                        var languagebody = (from c in body
                                            where c.Key == lang.ToUpper()
                                            select c.Value).FirstOrDefault();
                        if (languagebody != null)
                        {
                            q.Description = ParseForCorrectMetrics( languagebody.Text, measureUnit);
                            q.Payload = GetPayloadForTemplate(question, languagebody, measureUnit);
                        }
                        appQuestions.Add(q);

                    }

                    appLesson.Questions = appQuestions;
                    appLesson.Contents = await GetLessonContents(appLesson);
                }
                else
                {
                    logger.Warn(() => $"NON ho trovato domande per la lezione con id {lessonId}");
                }
            }
            else
            {
                logger.Warn(() => $"NON ho trovato la lezione con id {lessonId}");
            }
            return appLesson;
        }


        class SectionQuestions
        {
            public Unox.Models.RawModels.Section Section { get; set; }
            public List<Unox.Models.RawModels.Question> Questions { get; set; }
        }

        public async Task<object> GetChallenge(string levelId, string userId, string lang, string measureUnit)
        {
            logger.Info(() => $"Richiesta challenge: {levelId} in lingua: {lang} per utente: {userId} con unità di misura codice: {measureUnit}");

            var engine = _ee.Engine;
            UnoxEduAppModule.Lesson challengeLesson = new UnoxEduAppModule.Lesson();

            challengeLesson.CorrelationId = levelId;
            List<Unox.Models.RawModels.Question> challenge_questions = new List<Unox.Models.RawModels.Question>();

            var level = await engine.Get<Unox.Models.RawModels.Level>(levelId);


            List<SectionQuestions> sqs = new List<SectionQuestions>();
            var sections = (await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.ChildOf, level)).ToList();
            int iTotalQuestions = 0;
            foreach (var section in sections)
            {
                SectionQuestions sq = new SectionQuestions();
                sq.Section = section;
                sq.Questions = new List<Unox.Models.RawModels.Question>();

                var lessons = (await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, section)).ToList();
                foreach (var les in lessons)
                {
                    var lesson_questions = (await engine.GetAll<Unox.Models.RawModels.Question>(EntityRelation.ChildOf, les)).ToList();
                    sq.Questions.AddRange(lesson_questions);
                    iTotalQuestions += lesson_questions.Count;
                }
                sqs.Add(sq);
            }

            int inquestions = Math.Min(18, iTotalQuestions);
            Random rnd = new Random(DateTime.Now.Millisecond);
            while (challenge_questions.Count < inquestions)
            {
                for (int s = 0; s < sqs.Count; s++)
                {
                    var qsts = sqs[s].Questions;
                    int tot = qsts.Count - 1;
                    bool bproceed = false;
                    int iTry = 5;
                    while ((!bproceed) & (iTry > 0))
                    {
                        int i = rnd.Next(tot);
                        var qtoadd = qsts[i];
                        var check = (from cq in challenge_questions
                                     where cq.CorrelationId == qtoadd.CorrelationId
                                     select cq).FirstOrDefault();
                        if (check == null)
                        {
                            challenge_questions.Add(qtoadd);
                            bproceed = true;
                        }
                        else
                        {
                            for (int g = 0; g < qsts.Count; g++)
                            {
                                 qtoadd = qsts[g];
                                 check = (from cq in challenge_questions
                                             where cq.CorrelationId == qtoadd.CorrelationId
                                             select cq).FirstOrDefault();
                                if (check == null)
                                {
                                    challenge_questions.Add(qtoadd);
                                    bproceed = true;
                                    break;
                                }                                
                            }
                            iTry--;
                        }
                    }
                }
            }
           

            challengeLesson.Description = "CHALLENGE";
           

            List<UnoxEduAppModule.Question> appQuestions = new List<UnoxEduAppModule.Question>();
            // trovate. costruisco il dettaglio 
            foreach (var question in challenge_questions)
            {
                UnoxEduAppModule.Question q = new UnoxEduAppModule.Question();
                q.CorrelationId = question.CorrelationId;
                q.TemplateType = (TemplateType)Enum.Parse(typeof(TemplateType), question.Type.ToUpper());
                q.Title = await GetQuestionTitleForLang(question.Type, lang.ToUpper()); // question.Name;

                var body = question.Body;
                var languagebody = (from c in body
                                    where c.Key == lang.ToUpper()
                                    select c.Value).FirstOrDefault();
                if (languagebody != null)
                {
					q.Description = ParseForCorrectMetrics( languagebody.Text, measureUnit);
                    q.Payload = GetPayloadForTemplate(question, languagebody, measureUnit);
                }
                appQuestions.Add(q);

            }

            challengeLesson.Questions = appQuestions;
            challengeLesson.Contents = await GetLessonContents(challengeLesson);


            return challengeLesson;
        }

        public async Task<object> _ext_GetLearningPathChallenge(string levelId, string userId, string lang, string measureUnit)
        {
            logger.Info(() => $"Richiesta defaultchallenge: {levelId} in lingua: {lang} per utente: {userId} con unità di misura codice: {measureUnit}");

            var engine = _ee.Engine;
            UnoxEduAppModule.Lesson challengeLesson = new UnoxEduAppModule.Lesson();

            challengeLesson.CorrelationId = levelId;
            List<Unox.Models.RawModels.Question> challenge_questions = new List<Unox.Models.RawModels.Question>();

            var level = await engine.Get<Unox.Models.RawModels.Level>(levelId);

            List<SectionQuestions> sqs = new List<SectionQuestions>();
            var sections = (await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.ChildOf, level)).ToList();
            int iTotalQuestions = 0;
            foreach (var section in sections)
            {
                SectionQuestions sq = new SectionQuestions();
                sq.Section = section;
                sq.Questions = new List<Unox.Models.RawModels.Question>();

                var lessons = (await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, section)).ToList();
                foreach (var les in lessons)
                {
                    var lesson_questions = (await engine.GetAll<Unox.Models.RawModels.Question>(EntityRelation.ChildOf, les)).ToList();
                    sq.Questions.AddRange(lesson_questions);
                    iTotalQuestions += lesson_questions.Count;
                }
                sqs.Add(sq);
            }

            int inquestions = Math.Min(18, iTotalQuestions);
            Random rnd = new Random(DateTime.Now.Millisecond);
            while (challenge_questions.Count < inquestions)
            {
                for (int s = 0; s < sqs.Count; s++)
                {
                    var qsts = sqs[s].Questions;
                    int tot = qsts.Count - 1;
                    bool bproceed = false;
                    int iTry = 5;
                    while ((!bproceed) & (iTry > 0))
                    {
                        int i = rnd.Next(tot);
                        var qtoadd = qsts[i];
                        var check = (from cq in challenge_questions
                                     where cq.CorrelationId == qtoadd.CorrelationId
                                     select cq).FirstOrDefault();
                        if (check == null)
                        {
                            challenge_questions.Add(qtoadd);
                            bproceed = true;
                        }
                        else
                        {
                            for (int g = 0; g < qsts.Count; g++)
                            {
                                qtoadd = qsts[g];
                                check = (from cq in challenge_questions
                                         where cq.CorrelationId == qtoadd.CorrelationId
                                         select cq).FirstOrDefault();
                                if (check == null)
                                {
                                    challenge_questions.Add(qtoadd);
                                    bproceed = true;
                                    break;
                                }
                            }
                            iTry--;
                        }
                    }
                }
            }


            challengeLesson.Description = "CHALLENGE";


            List<UnoxEduAppModule.Question> appQuestions = new List<UnoxEduAppModule.Question>();
            // trovate. costruisco il dettaglio 
            foreach (var question in challenge_questions)
            {
                UnoxEduAppModule.Question q = new UnoxEduAppModule.Question();
                q.CorrelationId = question.CorrelationId;
                q.TemplateType = (TemplateType)Enum.Parse(typeof(TemplateType), question.Type.ToUpper());
                q.Title = await GetQuestionTitleForLang(question.Type, lang.ToUpper()); // question.Name;

                var body = question.Body;
                var languagebody = (from c in body
                                    where c.Key == lang.ToUpper()
                                    select c.Value).FirstOrDefault();
                if (languagebody != null)
                {
                    q.Description = ParseForCorrectMetrics(languagebody.Text, measureUnit);
                    q.Payload = GetPayloadForTemplate(question, languagebody, measureUnit);
                }
                appQuestions.Add(q);

            }

            challengeLesson.Questions = appQuestions;
            challengeLesson.Contents = await GetLessonContents(challengeLesson);


            return challengeLesson;
        }

        /*
         * 
          _______                   _       _         ______         _                   
         |__   __|                 | |     | |       |  ____|       | |                  
            | | ___ _ __ ___  _ __ | | __ _| |_ ___  | |__ __ _  ___| |_ ___  _ __ _   _ 
            | |/ _ \ '_ ` _ \| '_ \| |/ _` | __/ _ \ |  __/ _` |/ __| __/ _ \| '__| | | |
            | |  __/ | | | | | |_) | | (_| | ||  __/ | | | (_| | (__| || (_) | |  | |_| |
            |_|\___|_| |_| |_| .__/|_|\__,_|\__\___| |_|  \__,_|\___|\__\___/|_|   \__, |
                             | |                                                    __/ |
                             |_|                                                   |___/ 
                                                                                                 */
        private JObject GetPayloadForTemplate(Unox.Models.RawModels.Question question, RawContent body, string measureUnit)
        {
            string type = question.Type.ToUpper();
            IEnumerable<RawContentEntry> entries = body.Entries;
            JObject jresult = new JObject();
            string modelname = $"{type}Model";

            if (type == "S2")
            {
                jresult = new JObject() {
                                { modelname, Task.Run(() => GetPayloadForS2 (entries, measureUnit)).Result }
                            };
            }
            else if (type == "S3")
            {
                jresult = new JObject() {
                                { modelname, Task.Run(() => GetPayloadForS3 (body, measureUnit)).Result }
                            };
            }
            else if (type == "S4")
            {
                jresult = new JObject() {
                                { modelname, Task.Run(() => GetPayloadForS4 (entries)).Result }
                            };
            }
            else if (type == "S5_S9")
            {
                jresult = new JObject() {
                                { modelname, Task.Run(() => GetPayloadForS5_S9 (entries, measureUnit)).Result }
                            };

            }
            else if (type == "S6_S7")
            {
                jresult = new JObject() {
                                { modelname, Task.Run(() => GetPayloadForS6_S7 (entries, measureUnit)).Result }
                            };

            }
            else if (type == "S8")
            {
                jresult = new JObject() {
                                { modelname, Task.Run(() => GetPayloadForS8 (body, measureUnit)).Result }
                            };
            }
            else if (type == "S10_S11_S12")
            {
                jresult = new JObject() {
                                { modelname, Task.Run(() => GetPayloadForS10_S11_S12 (body, measureUnit)).Result }
                            };
            }
            else if (type == "S13")
            {
                jresult = new JObject() {
                                { modelname, Task.Run(() => GetPayloadForS13 (body, measureUnit)).Result }
                            };
            }
            else if (type == "S14")
            {
                jresult = new JObject() {
                                { modelname, Task.Run(() => GetPayloadForS14 (body, measureUnit)).Result }
                            };
            }
            return jresult;
        }

        private async Task<JObject> GetPayloadForS13(RawContent body, string metrics = "1")
        {
            JObject jr = new JObject();
            JArray jresult = new JArray();
            Random rnd = new Random(DateTime.Now.Millisecond);
            var entries = body.Entries.OrderBy(x => x.C1);
            string iCurID = "1";
            JArray jsingleitem = new JArray();
            JArray jxaxis = new JArray();
            int iOrder = 0;
            int inumber = 0;
         
            foreach (var row in entries)
            {
                if (row.C1 != "ID")
                {
                    if (iCurID != row.C1)
                    {
                        JObject sa = new JObject() {
                            { "SubAnswers", jsingleitem },
                            { "Order", iOrder },
                            { "RndOrder", rnd.Next() }

                        };

                        jresult.Add(sa); // last                       
                        jsingleitem = new JArray();
                        iCurID = row.C1;
                        iOrder++;

                    }

                    if (inumber == 3)
                    {
                        var conv = ParseForCorrectMetricSymbol(row.C2, row.C3, metrics);
                        JObject jo = new JObject () {
												{"ImagePath",  await GetIconUrlByName(row.C4)},
												{"UnitMeasure",  conv.Symbol}, // get temperature code
                                                {"Answer", conv.Answer }
											};
                        jxaxis.Add(jo);
                        inumber = 0;

                    }
                    else
                    {
                      
                        var conv = ParseForCorrectMetricSymbol(row.C2, row.C3, metrics);
                        JObject jo = new JObject () {
												{"ImagePath",  await GetIconUrlByName(row.C4)},
												{"UnitMeasure",  conv.Symbol}, // get temperature code
                                                {"Answer", conv.Answer }
											};
                        jsingleitem.Add(jo);
                        inumber++;
                    }
                    
                }
            }

            JObject subanswer = new JObject() {
                { "SubAnswers", jsingleitem },
                { "Order", iOrder },
                { "RndOrder", rnd.Next() }
            };
            jresult.Add(subanswer); // last

            jresult = new JArray(jresult.OrderBy(x => x["RndOrder"]).ToArray());
            jr = new JObject() {
                { "Answers", jresult },
                { "XAxis", jxaxis }
            };
           

            return jr;
        }

        private async Task<JObject> GetPayloadForS14(RawContent body, string metrics = "1")
        {
            JObject jresult = new JObject();
            JArray jsubitems = new JArray();
            var entries = body.Entries;//.OrderBy(x => x.C1);
            string bottomimage = await GetIconUrlByName(body.Icon1);
           
            List<List<bool>> points = new List<List<bool>>();
            List<List<bool>> answers = new List<List<bool>>();
            List<string> XAxis = new List<string>();
            List<string> YAxis = new List<string>();
            string iCurID = "1";
            JArray jsingleitem = new JArray();
            foreach (var row in entries)
            {
                List<string> lineavalori = new List<string>()
                                {  row.C2, row.C3, row.C4, row.C5 };
                if (row.C1.ToUpper().Contains("RIGA"))
                {
                    List<bool> lineaA = new List<bool>();
                    List<bool> lineaP = new List<bool>();

                    foreach (string lv in lineavalori)
                    {
                        bool ispoint = false;
                        bool isanswer = false;


                        if (!string.IsNullOrEmpty(lv))
                        {
                            if (lv.ToUpper() == "O")
                            {
                                isanswer = true;
                                ispoint = true;
                            }
                            if (lv.ToUpper() == "X")
                            {
                                isanswer = false;
                                ispoint = true;
                            }
                        }
                        else
                        {
                            isanswer = false;
                            ispoint = false;
                            // no answer no point                       
                        }

                        lineaA.Add(isanswer);
                        lineaP.Add(ispoint);
                    }
                    points.Add(lineaP);
                    answers.Add(lineaA);
                }
                else if (row.C1.ToUpper().Contains("ASSEX"))
                {
                    foreach (string lv in lineavalori)
                    {
                        if (!string.IsNullOrEmpty(lv))
                        {
                            XAxis.Add(ParseForCorrectMetrics(lv, metrics));
                        }
                        else
                        {
                            XAxis.Add("");
                        }
                    }
                }
                else if (row.C1.ToUpper().Contains("ASSEY"))
                {
                    foreach (string lv in lineavalori)
                    {
                        if (!string.IsNullOrEmpty(lv))
                        {
                            YAxis.Add(ParseForCorrectMetrics(lv, metrics));
                        }
                        else
                        {
                            YAxis.Add("");
                        }
                    }

                }
                else if ((row.C1 != "ID") && (!row.C1.ToUpper().Contains("ASSEY")) && (!row.C1.ToUpper().Contains("ASSEX")) && (!row.C1.ToUpper().Contains("RIGA")))
                {
                    if (iCurID != row.C1)
                    {
                        JObject sa = null;
                        if (jsingleitem.Count > 0)
                           sa = new JObject() { { "SubAnswers", jsingleitem } };
                        else
                           sa = new JObject() { { "SubAnswers", null } };

                        jsubitems.Add(sa); // last
                        jsingleitem = new JArray();
                        iCurID = row.C1;

                    }

                    if (string.IsNullOrEmpty(row.C2) && string.IsNullOrEmpty(row.C3))
                    {
                        // do nothing
                        JObject jo = new JObject() {
                                                {"ImagePath",  "" },
                                                {"UnitMeasure",  ""}, // get temperature code
                                                {"Answer", "" }
                                            };
                        jsingleitem.Add(jo);
                    }
                    else
                    {

                        var conv = ParseForCorrectMetricSymbol (row.C2, row.C3, metrics);
						JObject jo = new JObject () {
												{"ImagePath",  await GetIconUrlByName(row.C4)},
												{"UnitMeasure",  conv.Symbol}, // get temperature code
                                                {"Answer", conv.Answer }
											};
						jsingleitem.Add (jo);
                    }

                }
            }

            JObject subanswer = null;
            if (jsingleitem.Count > 0)
                subanswer = new JObject() { { "SubAnswers", jsingleitem } };
            else
                subanswer = new JObject() { { "SubAnswers", null } };
           
            jsubitems.Add(subanswer); // last


            bool[][] pointarray = new bool[4][];
            int y = 0;
            int x = 0;
            pointarray[0] = new bool[4];
            pointarray[1] = new bool[4];
            pointarray[2] = new bool[4];
            pointarray[3] = new bool[4];
            foreach (List<bool> lr in points)
            {

                foreach (bool val in lr)
                {
                    pointarray[x][y] = val;
                    x++;
                }
                x = 0;
                y++;
            }

            bool[,] answersarray = new bool[4, 4];
            y = 0;
            x = 0;
            foreach (List<bool> lr in answers)
            {
                foreach (bool val in lr)
                {
                    answersarray[x, y] = val;
                    x++;
                }
                x = 0;
                y++;
            }

            var yaxis = YAxis.ToArray().Reverse();

            jresult = new JObject()
                        {
                           
                            {"BottomImagePath", bottomimage },
                            { "CorrectAnswers", JArray.FromObject(answersarray)},
                            { "Points", JArray.FromObject( pointarray)},
                            { "XAxisTitles", JArray.FromObject(XAxis)},
                            { "YAxisTitles", JArray.FromObject(yaxis)},
                            { "Answers", jsubitems }
                        };

            return jresult;
        }


        private async Task<JObject> GetPayloadForS10_S11_S12(RawContent body, string metrics = "1")
        {
            JObject jresult = new JObject();
            var entries = body.Entries;//.OrderBy(x => x.C1);
            string topimage = await GetIconUrlByName(body.Icon1);
            string bottomimage = await GetIconUrlByName(body.Icon2);
            List<List<bool>> points = new List<List<bool>>();
            List<List<bool>> answers = new List<List<bool>>();
            List<string> XAxis = new List<string>();
            List<string> YAxis = new List<string>();
            foreach (var row in entries)
            {
                List<string> lineavalori = new List<string>()
                                {  row.C2, row.C3, row.C4, row.C5 };
                if (row.C1.ToUpper().Contains("RIGA"))
                {
                    List<bool> lineaA = new List<bool>();
                    List<bool> lineaP = new List<bool>();

                    foreach (string lv in lineavalori)
                    {
                        bool ispoint = false;
                        bool isanswer = false;


                        if (!string.IsNullOrEmpty(lv))
                        {
                            if (lv.ToUpper() == "O")
                            {
                                isanswer = true;
                                ispoint = true;
                            }
                            if (lv.ToUpper() == "X")
                            {
                                isanswer = false;
                                ispoint = true;
                            }
                        }
                        else
                        {
                            isanswer = false;
                            ispoint = false;
                            // no answer no point                       
                        }

                        lineaA.Add(isanswer);
                        lineaP.Add(ispoint);
                    }
                    points.Add(lineaP);
                    answers.Add(lineaA);
                }
                else if (row.C1.ToUpper().Contains("ASSEX"))
                {
                    foreach (string lv in lineavalori)
                    {
                        if (!string.IsNullOrEmpty(lv))
                        {
                            XAxis.Add(ParseForCorrectMetrics(lv, metrics));
                        }
                        else
                        {
                            XAxis.Add("");
                        }
                    }
                }
                else if (row.C1.ToUpper().Contains("ASSEY"))
                {
                    foreach (string lv in lineavalori)
                    {
                        if (!string.IsNullOrEmpty(lv))
                        {
                            YAxis.Add(ParseForCorrectMetrics(lv, metrics));
                        }
                        else
                        {
                            YAxis.Add("");
                        }
                    }

                }
            }


            bool[][] pointarray = new bool[4][];
            int y = 0;
            int x = 0;
            pointarray[0] = new bool[4];
            pointarray[1] = new bool[4];
            pointarray[2] = new bool[4];
            pointarray[3] = new bool[4];
            foreach (List<bool> lr in points)
            {
               
                foreach (bool val in lr)
                {
                    pointarray[x][y] = val;
                    x++;
                }
                x = 0;
                y++;
            }

            bool[,] answersarray = new bool[4, 4];
            y = 0;
            x = 0;
            foreach (List<bool> lr in answers)
            {
                foreach (bool val in lr)
                {
                    answersarray[x, y] = val;
                    x++;
                }
                x = 0;
                y++;
            }

            var yaxis = YAxis.ToArray().Reverse();

            jresult = new JObject()
                        {
                            {"TopImagePath", topimage },
                            {"BottomImagePath", bottomimage },
                            { "CorrectAnswers", JArray.FromObject(answersarray)},
                            { "Points", JArray.FromObject( pointarray)},
                            { "XAxisTitles", JArray.FromObject(XAxis)},
                            { "YAxisTitles", JArray.FromObject(yaxis)}
                        };

            return jresult;
        }

        private async Task<JObject> GetPayloadForS3(RawContent body, string metrics = "1")
        {

            JArray jresult = new JArray();
            Random rnd = new Random(DateTime.Now.Millisecond);
            var entries = body.Entries.OrderBy(x => x.C1);
            int iorder = 0;
            foreach (var row in entries)
            {
                if (row.C1 != "ID")
                {
                    JObject jentry = new JObject() {
                                                {"ImagePath",  await GetIconUrlByName(row.C3)},
                                                {"BkgColor", await GetColorByName(row.C4)},
                                                { "Text",  ParseForCorrectMetrics (row.C2,metrics) },
                                                { "CorrectOrder", iorder},
                                                { "Order", rnd.Next()}
                                            };
                    jresult.Add(jentry);
                }
                iorder++;
            }

            jresult = new JArray(jresult.OrderBy(x => x["Order"]).ToArray());
            JObject jo = new JObject() {
                                                { "ImagePath", await GetIconUrlByName(body.Icon1) },
                                                { "Answers", jresult}
                        };

            return jo;
        }

        private async Task<JArray> GetPayloadForS4(IEnumerable<RawContentEntry> entries)
        {
            JArray jresult = new JArray();
            Random rnd = new Random(DateTime.Now.Millisecond);
           
            foreach (var row in entries)
            {
                if (row.C1 != "ID")
                {
                    JObject jo = new JObject() {
                                                {"ImagePath",  await GetIconUrlByName(row.C2)},
                                                {"Correct", bool.Parse(row.C3)},
                                                { "Order", rnd.Next()}
                                            };
                    jresult.Add(jo);
                }

            }
            jresult = new JArray(jresult.OrderBy(x => x["Order"]).ToArray());
            return jresult;
        }

        private async Task<JArray> GetPayloadForS5_S9(IEnumerable<RawContentEntry> entries, string metrics = "1")
        {
            JArray jresult = new JArray();
            Random rnd = new Random(DateTime.Now.Millisecond);
            foreach (var row in entries)
            {
                if (row.C1 != "ID")
                {
                    JObject jo = new JObject() {
                                                {"Answer",  ParseForCorrectMetrics(row.C2,metrics)},
                                                {"Correct", bool.Parse(row.C3)},
                                                 { "Order", rnd.Next()}
                                            };
                    jresult.Add(jo);
                }

            }
            jresult = new JArray(jresult.OrderBy(x => x["Order"]).ToArray());
            return jresult;
        }

        private async Task<JArray> GetPayloadForS6_S7(IEnumerable<RawContentEntry> entries, string metrics = "1")
        {
            JArray jresult = new JArray();
            foreach (var row in entries)
            {
                if (row.C1 != "ID")
                {
                    var conv = ParseForCorrectMetricSymbol(row.C2, row.C3, metrics);
					string [] percAnswers = new string[] {"", ""};
					if (!String.IsNullOrEmpty (row.C5)) 
					{
						percAnswers = AnswerPercentage (conv.Answer, row.C5);
					}
                    JObject jo = new JObject() {
                                                {"ImagePath",  await GetIconUrlByName(row.C4)},
                                                {"UnitMeasure",  conv.Symbol}, // get temperature code
                                                {"Answer", conv.Answer },
												{"MinValueAnswer", percAnswers[0]},
												{"MaxValueAnswer", percAnswers[1]}
                                            };
                    jresult.Add(jo);
                }

            }
            return jresult;
        }

        private async Task<JArray> GetPayloadForS8(RawContent body, string metrics = "1")
        {
            JObject jr = new JObject();
            JArray jresult = new JArray();
            var entries = body.Entries.OrderBy(x => x.C1);
            string iCurID = "1";
            JArray jsingleitem = new JArray();
            foreach (var row in entries)
            {
                if (row.C1 != "ID")
                {
                    if (iCurID != row.C1)
                    {
                        JObject sa = new JObject() { { "SubAnswers", jsingleitem } };
                        jresult.Add(sa); // last
                        jsingleitem = new JArray();
                        iCurID = row.C1;

                    }

                    var conv = ParseForCorrectMetricSymbol(row.C2, row.C3, metrics);
					string [] percAnswers = new string [] { "", "" };
					if (!String.IsNullOrEmpty (row.C5)) {
						percAnswers = AnswerPercentage (conv.Answer, row.C5);
					}
                    JObject jo = new JObject() {
                                                {"ImagePath",  await GetIconUrlByName(row.C4)},
                                                {"UnitMeasure",  conv.Symbol}, // get temperature code
                                                {"Answer", conv.Answer },
												{"MinValueAnswer", percAnswers[0]},
												{"MaxValueAnswer", percAnswers[1]}
                                            };
                    jsingleitem.Add(jo);

                }
            }

            JObject subanswer = new JObject() { { "SubAnswers", jsingleitem } };
            jresult.Add(subanswer); // last

            return jresult;
        }

        private async Task<JObject> GetPayloadForS2(IEnumerable<RawContentEntry> entries, string metrics = "1")
        {
            JObject jresult = new JObject();
            JArray jleft = new JArray();
            JArray jright = new JArray();
            Random rnd = new Random(DateTime.Now.Millisecond);
            foreach (var row in entries)
            {
                if (row.C1 != "ID")
                {
                    int opposite = int.Parse(row.C1) + entries.Count();
                    // nella C1 abbiamo l'id
                    JObject left = new JObject() {
                                    { "CorrelationId", row.C1 },
                                    { "Text", ParseForCorrectMetrics (row.C2,metrics)  },
                                    { "LinkedCorrelationId", opposite.ToString()},
                                    { "Order", rnd.Next()}
                                };
                    JObject right = new JObject() {
                                    { "CorrelationId", opposite.ToString() },
                                    { "Text", ParseForCorrectMetrics (row.C3,metrics) },
                                    { "LinkedCorrelationId", row.C1},
                                    { "Order", rnd.Next()}
                                };
                    jleft.Add(left);
                    jright.Add(right);
                }

            }

            jleft = new JArray(jleft.OrderBy(x => x["Order"]).ToArray());
            jright = new JArray(jright.OrderBy(x => x["Order"]).ToArray());
            jresult = new JObject() {
                            {"LeftAnswers", jleft },
                            {"RightAnswers", jright }
                        };

            return jresult;
        }

        private class ConvertedAnswer
        {
            public string Answer { get; set; }
            public string Symbol { get; set; }
        }

		private string[] AnswerPercentage (string answer, string percentage)
		{
			double dperc = 0;
			double.TryParse (percentage, out dperc);
			dperc = dperc / 100;

			double dpercmin = 1 - dperc;
			double dpercmax = 1 + dperc;

			double dval = 0;
			double.TryParse (answer, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out dval);

			string [] percVals = new string [] {"", ""};
			percVals[0] = (dval * dpercmin).ToString();
			percVals [1] = (dval * dpercmax).ToString ();

			return percVals;
		}

        private ConvertedAnswer ParseForCorrectMetricSymbol(string answer, string symbol, string metrics)
        {
            ConvertedAnswer result = new ConvertedAnswer();
            string converted = answer;
            string unit = symbol;
            if (symbol == "temperature")
            {
                if (metrics == "1") // METRIC
                {
                    unit = " °C";
                }
                else
                {
                    double dval = 0;
                    double.TryParse(answer, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out dval);
                    double dfahr = Math.Round((dval * 1.8) + 32, 0);
                    converted = dfahr.ToString();
                    unit = " °F";
                }
            }
            else if (symbol == "percentage")
            {
                if (metrics == "1") // METRIC
                {
                    unit = " %";
                }
                else
                {
                    unit = " %";
                }

            }
            else if (symbol == "time")
            {
                if (metrics == "1") // METRIC
                {
                    unit = " min";
                }
                else
                {
                    unit = " min";
                }

            }
            else if (symbol == "lenght")
            {
                if (metrics == "1") // METRIC
                {
                    unit = " cm";
                }
                else
                {
                    double pconversion = 0.393701d;
                    double dval = 0;
                    double.TryParse(answer, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out dval);
                    double dinch = Math.Round((dval * pconversion), 2);
                    double dmil = dinch * 1000;
                    converted = dmil.ToString();
                    unit = " mil";
                }
            }
            else if (symbol == "weight")
            {

                if (metrics == "1") // METRIC
                {
                    unit = " gr";
                }
                else
                {
                    double pconversion = 0.00220462d;
					pconversion = pconversion * 16; //tramuto in udm oncia!
                    double dval = 0;
                    double.TryParse(answer, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out dval);
                    double dlb = Math.Round((dval * pconversion), 0);
                    converted = dlb.ToString();
                    unit = " oz";
                }
            }

            return new ConvertedAnswer() { Answer = converted, Symbol = unit };

        }

        private string ParseForCorrectMetrics(string text, string metrics)
        {
            if (!String.IsNullOrEmpty(text))
            {
                var re = new Regex("{(?<value>[0-9\\.]+)(?<unit>C|MM|CM|GR|KG)}");

                return re.Replace(text, (m) =>
                {
                    var converted = "";
                    var val = m.Groups["value"].Value;
                    var unit = m.Groups["unit"].Value;

                    if (metrics == "1") // METRIC
                    {
                        if (unit.ToUpper() == "C")
                        {
                            converted = $"{val} °C";
                        }
                        else if (unit.ToUpper() == "GR")
                        {
                            converted = $"{val} g";
                        }
                        else if (unit.ToUpper() == "KG")
                        {
                            converted = $"{val} kg";
                        }
                        else if (unit.ToUpper() == "CM")
                        {
                            converted = $"{val} cm";
                        }
                        else if (unit.ToUpper() == "MM")
                        {
                            converted = $"{val} mm";
                        }

                    }
                    else
                    {
                        if (unit == "C")
                        {
                            double dval = 0;
                            double.TryParse(val, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out dval);
                            double dfahr = Math.Round((dval * 1.8) + 32, 0);
                            converted = $"{dfahr.ToString()} °F";
                        }
                        else if (unit.ToUpper() == "GR")
                        {
                            double pconversion = 0.00220462d;
							pconversion = pconversion * 16; //tramuto in udm oncia!
                            double dval = 0;
                            double.TryParse(val, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out dval);
                            double dlb = Math.Round((dval * pconversion), 0);
                            converted = $"{dlb.ToString()} oz";
                        }
                        else if (unit.ToUpper() == "KG")
                        {
							double pconversion = 2.20462d; //0.45359237d; errore tasso di conversione KG to lbs
                            double dval = 0;
                            double.TryParse(val, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out dval);
                            double dlb = Math.Round((dval * pconversion), 0);
                            converted = $"{dlb} lbs";
                        }
                        else if (unit.ToUpper() == "CM")
                        {
                            double pconversion = 0.393701d;
                            double dval = 0;
                            double.TryParse(val, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out dval);
                            double dinch = Math.Round((dval * pconversion), 2);
                            //converted = $"{dinch} in";
                            //rp: da inch a milli-inch
                            double mil = dinch * 1000;
                            converted = $"{mil} mil";

                        }
                        else if (unit.ToUpper() == "MM")
                        {
                            double pconversion = 0.0393701d;
                            double dval = 0;
                            double.TryParse(val, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out dval);
                            double dinch = Math.Round((dval * pconversion), 2);
                            //converted = $"{dinch} in";
                            //rp: da inch a milli-inch
                            double mil = dinch * 1000;
                            converted = $"{mil} mil";

                        }
                    }

                    return converted;
                });
            }
            return text;
        }

        private async Task<string> GetIconUrlByName(string icon)
        {
            if (!String.IsNullOrEmpty(icon))
            {
                string sresult = "";
                var engine = _ee.Engine;
                var icons = await engine.GetAll<Unox.Models.RawModels.AppIcon>();
                sresult = (from c in icons
                           where c.Name.ToUpper() == icon.ToUpper()
                           select c.FileKey).FirstOrDefault();

                return sresult;
            }
            return icon;
        }

        private async Task<string> GetColorByName(string colorname)
        {
            if (!String.IsNullOrEmpty(colorname))
            {
                string sresult = "";
                var engine = _ee.Engine;
                var colors = await engine.GetAll<Unox.Models.RawModels.AppColor>();
                sresult = (from c in colors
                           where c.Name.ToUpper() == colorname.ToUpper()
                           select c.Code).FirstOrDefault();

                return sresult;
            }
            return colorname;
        }


        private async Task<string[]> GetLessonContents(UnoxEduAppModule.Lesson lesson)
        {
            //FIXME: creare una chiamata per i soli contents richiesti
            var engine = _ee.Engine;
            List<string> iconsused = new List<string>();
            foreach (var q in lesson.Questions)
            {
                if (q.TemplateType == TemplateType.S4)
                {
                    JObject jpayload = JObject.FromObject(q.Payload);
                    var jarr = jpayload[$"{q.TemplateType}Model"];
                    JArray jarray = JArray.FromObject(jarr);
                    foreach (JObject jo in jarray)
                    {
                        string img = jo["ImagePath"].ToString();
                        if (!iconsused.Contains(img))
                            iconsused.Add(img);
                    }
                }
                else if (q.TemplateType == TemplateType.S3)
                {
                    JObject jpayload = JObject.FromObject(q.Payload);
                    var jobj = jpayload[$"{q.TemplateType}Model"] as JObject;

                    string topimage = jobj["ImagePath"].ToString();
                    if (!iconsused.Contains(topimage))
                        iconsused.Add(topimage);

                    var jinnerobj = jobj["Answers"];
                    JArray jarray = JArray.FromObject(jinnerobj);
                    foreach (JObject jo in jarray)
                    {
                        string img = jo["ImagePath"].ToString();
                        if (!string.IsNullOrEmpty(img))
                            if (!iconsused.Contains(img))
                                iconsused.Add(img);
                    }
                }
                else if (q.TemplateType == TemplateType.S8)
                {
                    JObject jpayload = JObject.FromObject(q.Payload);
                    var jobj = jpayload[$"{q.TemplateType}Model"] as JArray;

                    foreach (JObject jelement in jobj)
                    {
                        JArray jarray = jelement["SubAnswers"] as JArray;
                        foreach (JObject jo in jarray)
                        {
                            string img = jo["ImagePath"].ToString();
                            if (!string.IsNullOrEmpty(img))
                                if (!iconsused.Contains(img))
                                    iconsused.Add(img);
                        }
                    }
                }
                else if (q.TemplateType == TemplateType.S6_S7)
                {
                    JObject jpayload = JObject.FromObject(q.Payload);
                    var jarr = jpayload[$"{q.TemplateType}Model"];
                    JArray jarray = JArray.FromObject(jarr);
                    foreach (JObject jo in jarray)
                    {
                        string img = jo["ImagePath"].ToString();
                        if (!iconsused.Contains(img))
                            iconsused.Add(img);
                    }
                }
                else if (q.TemplateType == TemplateType.S10_S11_S12)
                {
                    JObject jpayload = JObject.FromObject(q.Payload);
                    var jobj = jpayload[$"{q.TemplateType}Model"] as JObject;

                    string topimage = jobj["TopImagePath"].ToString();
                    if (!iconsused.Contains(topimage))
                        iconsused.Add(topimage);

                    string bottomimage = jobj["BottomImagePath"].ToString();
                    if (!iconsused.Contains(bottomimage))
                        iconsused.Add(bottomimage);
                }
                else if (q.TemplateType == TemplateType.S13)
                {
                    JObject jpayload = JObject.FromObject(q.Payload);
                    var jobj = jpayload[$"{q.TemplateType}Model"] as JObject;
                    var janswers = jobj["Answers"] as JArray;
                    foreach (JObject jelement in janswers)
                    {
                        JArray jarray = jelement["SubAnswers"] as JArray;
                        foreach (JObject jo in jarray)
                        {
                            string img = jo["ImagePath"].ToString();
                            if (!string.IsNullOrEmpty(img))
                                if (!iconsused.Contains(img))
                                    iconsused.Add(img);
                        }
                    }

                    var jaxis = jobj["XAxis"] as JArray;
                    {
                        foreach (JObject jelement in jaxis)
                        {                            
                                string img = jelement["ImagePath"].ToString();
                                if (!string.IsNullOrEmpty(img))
                                    if (!iconsused.Contains(img))
                                        iconsused.Add(img);
                            
                        }

                    }
                }
                else if (q.TemplateType == TemplateType.S14)
                {
                    JObject jpayload = JObject.FromObject(q.Payload);
                    var jobj = jpayload[$"{q.TemplateType}Model"] as JObject;

                   
                    string bottomimage = jobj["BottomImagePath"].ToString();
                    if (!iconsused.Contains(bottomimage))
                        iconsused.Add(bottomimage);

                    var jsub = jobj[$"Answers"] as JArray;

                    foreach (JObject jelement in jsub)
                    {
                        JArray jarray = jelement["SubAnswers"] as JArray;
                        foreach (JObject jo in jarray)
                        {
                            string img = jo["ImagePath"].ToString();
                            if (!string.IsNullOrEmpty(img))
                                if (!iconsused.Contains(img))
                                    iconsused.Add(img);
                        }
                    }

                }


            }

            //var icons = (await engine.GetAll<Unox.Models.RawModels.AppIcon>()).Select(x => x.FileKey).ToArray<string>();
            //return icons;
            return iconsused.ToArray();
        }


        /*
         *                      _                       
                               | |                      
          _   _ ___  ___ _ __  | | __ _ _   _  ___ _ __ 
         | | | / __|/ _ \ '__| | |/ _` | | | |/ _ \ '__|
         | |_| \__ \  __/ |    | | (_| | |_| |  __/ |   
          \__,_|___/\___|_|    |_|\__,_|\__, |\___|_|   
                                         __/ |          
                                        |___/           */

        private async Task<bool> CheatForUser(string cheaterid)
        {
            var engine = _ee.Engine;
            
            var levels = await engine.GetAll<Unox.Models.RawModels.Level>();
            foreach (var l in levels)
            {
                var sections = await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.ChildOf, l);
                foreach (var s in sections)
                {
                    var lessons = await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, s);
                    foreach (var ls in lessons)
                    {
                        await SetLessonFinished(ls.CorrelationId, cheaterid);
                    }
                }

            }


            return true;

        }

        async Task<_ext_Ranking> _ext_GetInstitutionRanking(string institutionId)
        {
            var engine = _ee.Engine;
            var allUsers = await engine.GetAll<Unox.Models.RawModels._ext_User>();
            //1. filter
            List<_ext_User> institutionUsers = allUsers.Where(x => x.Institution == institutionId).ToList();

            //2. order and build new data structure
            var rankedInstitutionUsers = institutionUsers.ToList().OrderByDescending(x => x.Points);
            List<_ext_QualifiedUser> r = new List<_ext_QualifiedUser>();
            _ext_Ranking ranking = new _ext_Ranking();
            int rank = 1;
            foreach (var user in rankedInstitutionUsers)
            {
                _ext_QualifiedUser _ext_u = new _ext_QualifiedUser();
                _ext_u.UniqueIdentifier = user.UserId;
                _ext_u.Points = user.Points;
                _ext_u.RankPosition = rank++;
                _ext_u.Nickname = user.Nickname;
                r.Add(_ext_u);
                logger.Debug("Added a new qualified user with position: " + _ext_u.RankPosition + " nickname " + _ext_u.Nickname + " points " + _ext_u.Points);
            }

            ranking.Ranking = r;

            return ranking;
        }

        async Task<_ext_Ranking> _ext_GetNullInstitutionRanking() {

            var engine = _ee.Engine;
            var allUsers = await engine.GetAll<Unox.Models.RawModels._ext_User>();

            //1. filter those with null institute
            var nullInstitute = allUsers.Where(x => String.IsNullOrEmpty(x.Institution)).ToList();
            //2. rank them
            var allNullRanked = nullInstitute.ToList().OrderByDescending(x => x.Points);
            
            //3. build new data structure
            List<UnoxEduAppModule._ext_QualifiedUser> r = new List<UnoxEduAppModule._ext_QualifiedUser>();
            _ext_Ranking ranking = new _ext_Ranking();
            int rank = 1;
            foreach (var user in allNullRanked)
            {
                UnoxEduAppModule._ext_QualifiedUser _ext_u = new UnoxEduAppModule._ext_QualifiedUser();
                _ext_u.UniqueIdentifier = user.UserId;
                _ext_u.Points = user.Points;
                _ext_u.RankPosition = rank++;
                _ext_u.Nickname = user.Nickname;
                r.Add(_ext_u);
                logger.Debug("Added a new qualified user with position: " + _ext_u.RankPosition + " nickname " + _ext_u.Nickname + " points " + _ext_u.Points);
            }

            ranking.Ranking = r;

            return ranking;
        }

/*
        private int[] ComputeIntervals(List<_ext_User> ordUsers, string userId) {

            //first find userIds position in the users list
            int p = 0;
            for (; p < ordUsers.Count(); p++)
            {
                if (ordUsers.ElementAt(p).UserId.Equals(userId))
                {
                    break;
                }
            }

            if (p == ordUsers.Count())
            {
                logger.Debug("User was not found !! This should not have happened!");
                return null;
            }
            if (ordUsers.Count() <= maxUsersDisplayRanking)
                return new int[] { 0, ordUsers.Count() };                                   //Case 1
            if(p >= 0 && p < maxStart)
                return new int[] {0, 2*prev, ordUsers.Count()-maxEnd, ordUsers.Count() };   //Case 2
            if ( (p >= (ordUsers.Count() - maxEnd)) && (p < ordUsers.Count()))
                return new int[] { 0, maxStart, ordUsers.Count() - 10, ordUsers.Count() };  //Case 3
            //Case 4, p is outside start and end interval boundaries.
            return new int[] {0, maxStart,
                                ((p-5 < maxStart)? maxStart:p-5), p,
                                p, (p+5 > (ordUsers.Count() - maxEnd))? (ordUsers.Count() - maxEnd): p+5,
                                maxEnd, ordUsers.Count()
            };
        }
*/
        async Task<object> SetLessonFinished(string lessonId, string userId)
        {
            var engine = _ee.Engine;
            var usercorrid = await GetOrSetUser(userId);
            var user = await engine.Get<Unox.Models.RawModels.User>(usercorrid);

            var lesson = await engine.Get<Unox.Models.RawModels.Lesson>(lessonId);

            logger.Debug(() => $"USERSTAT: {"Lesson " + lessonId + " was finnished by " + userId}");

            await engine.Set(lesson, EntityRelation.Linked, user);
           
            var section = await engine.Get<Unox.Models.RawModels.Section>(EntityRelation.ParentOf, lesson);
            var sectionlessons = await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, section);
            var alluserlessons = (await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.Linked, user)).Select(x => x.CorrelationId);

            var userachievements = (await engine.GetAll<Unox.Models.RawModels.UserAchievement>(EntityRelation.Linked, user)).ToList();
            await UpdateAchievement(engine, userachievements, lesson, user);
            //  var achievements = userachievements.Achievements.ToList();

            int iTot = sectionlessons.Count();
            int iDone = 0;
            foreach (var sl in sectionlessons.Select(x => x.CorrelationId))
            {
                if (alluserlessons.Contains(sl))
                    iDone++;
            }

            if (iTot == iDone)
            {
                logger.Debug("all lessons inside the section have been completed!");
                // section conclusa
                await engine.Set(section, EntityRelation.Linked, user);
                await UpdateAchievement(engine, userachievements, section, user);
                var level = await engine.Get<Unox.Models.RawModels.Level>(EntityRelation.ParentOf, section);
                var levelsections = await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.ChildOf, level);
                var allusersections = (await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.Linked, user)).Select(x => x.CorrelationId);
                iTot = levelsections.Count();
                iDone = 0;
                foreach (var ls in levelsections.Select(x => x.CorrelationId))
                {
                    if (allusersections.Contains(ls))
                        iDone++;
                }

                if (iTot == iDone)
                {
                    logger.Debug("all sections inside a level have been completed!");
                    // section conclusa
                    var eeresult = await engine.Set(level, EntityRelation.Linked, user);
                    await UpdateAchievement(engine, userachievements, level, user);

                }
            }
            return await GetUserLevel(userId);

        }

        class PortingDetails
        {
            public bool PortingApplied { get; set; }
            public string CorrelationId { get; set; }
        }

        private async Task<PortingDetails> _ext_ApplyPortingApplicable(string userId)
        {
            var engine = _ee.Engine;
            var v2_users = await engine.GetAll<Unox.Models.RawModels._ext_User>();
            string correlationId = (from u in v2_users
                                        where u.UserId == userId
                                        select u.CorrelationId).FirstOrDefault();
            
            //user already exists in v2 -> no need to port anything, already took care of this.
            if (correlationId != null)
            {
                logger.Debug("No porting required, v2 already exists!! " + userId);
                return new PortingDetails
                {
                    PortingApplied = false,
                    CorrelationId = correlationId
                };

            }
            //else. check if user exists in v1.
            var v1_users = await engine.GetAll<Unox.Models.RawModels.User>();
            correlationId = (from u in v1_users
                                where u.UserId == userId
                                select u.CorrelationId).FirstOrDefault();

            //this is a new user, create new one.
            if (correlationId == null) {
                logger.Debug("No porting required, this is a new user!! " + userId);
                correlationId = await _ext_GetOrSetUser(userId);
                return new PortingDetails
                {
                    PortingApplied = false,
                    CorrelationId = correlationId
                };
            }

            logger.Debug("Porting required for userId!! " + userId);

            //else. user is present in v1 but has no trace in v2 -> porting applies.
            //create new user                
            var newuser = engine.New<Unox.Models.RawModels._ext_User>();
            newuser.UserId = userId;
            newuser.Points = 0;
            var eeres = await engine.Update<Unox.Models.RawModels._ext_User>(newuser);
            if (!eeres.Success)
            {
                logger.Debug("failed to create new user ..");
                return new PortingDetails
                {
                    PortingApplied = true,
                    CorrelationId = ""
                };
            }

            v2_users = await engine.GetAll<Unox.Models.RawModels._ext_User>();
            string newCorrelationId = (from u in v2_users
                             where u.UserId == userId
                             select u.CorrelationId).FirstOrDefault();

            var v2_user = await engine.Get<Unox.Models.RawModels._ext_User>(newCorrelationId);
            var v1_user = await engine.Get<Unox.Models.RawModels.User>(correlationId);

            var userLevels = await engine.GetCorrelationIdsByRelation(EntityRelation.Linked, "User", v1_user.CorrelationId, "Level");
            var userSections = await engine.GetCorrelationIdsByRelation(EntityRelation.Linked, "User", v1_user.CorrelationId, "Section");
            var userLessons = await engine.GetCorrelationIdsByRelation(EntityRelation.Linked, "User", v1_user.CorrelationId, "Lesson");

            int userScore = userLevels.Count() * Int32.Parse(this.levelCompletedPoints) + 
                            userSections.Count() * Int32.Parse(this.sectionCompletedPoints) + 
                            userLessons.Count() * Int32.Parse(this.lessonCompletedPoints);

            v2_user.Points = userScore;
            await engine.Update<Unox.Models.RawModels._ext_User>(v2_user);

            foreach (var userLevel in userLevels)
            {
                logger.Debug("Porting a level to the newly v2_user!");
                var level = await engine.Get<Unox.Models.RawModels.Level>(userLevel);
                _ext_LinkAchievement(level, EntityRelation.Linked, v2_user);
            }

            foreach (var userSection in userSections)
            {
                logger.Debug("Porting a section to the newly v2_user!");
                var section = await engine.Get<Unox.Models.RawModels.Section>(userSection);
                _ext_LinkAchievement(section, EntityRelation.Linked, v2_user);
            }

            foreach (var userLesson in userLessons)
            {
                logger.Debug("Porting a lesson to the newly v2_user!");
                var lesson = await engine.Get<Unox.Models.RawModels.Lesson>(userLesson);
                _ext_LinkAchievement(lesson, EntityRelation.Linked, v2_user);
            }

            return new PortingDetails
            {
                PortingApplied = true,
                CorrelationId = newCorrelationId
            };
        }

        private async void _ext_LinkAchievement(Entity toLink, string relation, Unox.Models.RawModels._ext_User linkTo) {

            var engine = _ee.Engine;

            await engine.SetEntityRelation(toLink.GetType().Name, toLink.CorrelationId, relation, "_ext_User", linkTo.CorrelationId);
            //await engine.Set(toLink, relation, linkTo);

            UserAchievement ua = engine.New<UserAchievement>();
            ua.AchievementDate = DateTime.Now;//Fix.
            ua.AchievementId = toLink.CorrelationId;

            await engine.Update<UserAchievement>(ua);
            await engine.SetEntityRelation("UserAchievement", ua.CorrelationId, relation, "_ext_User", linkTo.CorrelationId);
            //await engine.Set(ua, relation, linkTo);
        }

        //This is the first function called from the FE once when user loggs in, account validation and update.
        //In here we enforce user profile porting from v1 -> v2.
        async Task<_ext_UserProfile> _ext_GetUserProfile(string userId) {

            var engine = _ee.Engine;

            var portingDetails = await _ext_ApplyPortingApplicable(userId);

            var user = await engine.Get<Unox.Models.RawModels._ext_User>(portingDetails.CorrelationId);

            if (user == null)
            {
                logger.Debug("GetUserProfile: this should not have happened, user was null while trying to update nickname");
                return new _ext_UserProfile();
            }

            return new _ext_UserProfile
            {
                UserId = user.UserId,
                Nickname = user.Nickname,
                Institution = user.Institution,
                Score = user.Points
            };
        }

        //In here we set (only once) the institution the user belongs to.
        async Task<bool> _ext_SetUserInstitution(string institutionId, string userId)
        {
            logger.Debug("User: " + userId + " is trying to set its institution to: " + institutionId);
            var engine = _ee.Engine;

            var usercorrid = await _ext_GetOrSetUser(userId);
            var user = await engine.Get<Unox.Models.RawModels._ext_User>(usercorrid);

            if (user == null)
            {
                logger.Debug("This should not have happened, user was null while trying to update nickname");
                return false;
            }

            if (user.Institution != null) {
                logger.Debug("The user institution should be immutable, error!");
                return false;
            }

            //get institutions list and verify if the currennt one exists
            var institutions = await engine.GetAll<Unox.Models.RawModels._ext_Institution>();
            var uIID = (from inst in institutions
                             where inst.Identifier == institutionId
                             select inst.Identifier).FirstOrDefault();

            logger.Debug("User is trying to set its institution to: " + institutionId);
            //institution code does not exist
            if (uIID == null) 
                return false;
           
            user.Institution = uIID;
            var response = await engine.Update<Unox.Models.RawModels._ext_User>(user);
            logger.Debug("///User institution was set to: " + uIID);
            return response.Success;
        }
            
        async Task<bool> _ext_UpdateOrSetNickname(string nickname, string userId) {


            logger.Debug("Trying to update user nickname to " + nickname + " for userId: " + userId);
            var engine = _ee.Engine;

            var usercorrid = await _ext_GetOrSetUser(userId);
            var user = await engine.Get<Unox.Models.RawModels._ext_User>(usercorrid);

            if (user == null) {
                logger.Debug("This should not have happened, user was null while trying to update nickname");
                return false;
            }

            user.Nickname = nickname;
            var response = await engine.Update<Unox.Models.RawModels._ext_User>(user);

            return response.Success;
        }

        string lessonCompletedPoints = System.Configuration.ConfigurationManager.AppSettings["lessonCompletedBonus"];
        string sectionCompletedPoints = System.Configuration.ConfigurationManager.AppSettings["sectionCompletedBonus"];
        string levelCompletedPoints = System.Configuration.ConfigurationManager.AppSettings["levelCompletedBonus"];

        async Task<object> _ext_SetLessonFinished(string lessonId, string userId)
        {

            var engine = _ee.Engine;
            var usercorrid = await _ext_GetOrSetUser(userId);
            var user = await engine.Get<Unox.Models.RawModels._ext_User>(usercorrid);

            var lesson = await engine.Get<Unox.Models.RawModels.Lesson>(lessonId);
            var userachievements = (await engine.GetAll<Unox.Models.RawModels.UserAchievement>(EntityRelation.Linked, user)).ToList();

            //fix: check if user has already completed this lesson: if yes do not act on the score, otherwise yes.
            var item = (from a in userachievements
                        where a.AchievementId == lesson.CorrelationId
                        select a).FirstOrDefault();

            if(item == null)
                user.Points += Int32.Parse(lessonCompletedPoints);


            logger.Debug(() => $"USERSTAT: {"Lesson " + lessonId + " was finnished by " + userId}");

            await engine.Set(lesson, EntityRelation.Linked, user);

            var section = await engine.Get<Unox.Models.RawModels.Section>(EntityRelation.ParentOf, lesson);
            var sectionlessons = await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, section);
            var alluserlessons = (await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.Linked, user)).Select(x => x.CorrelationId);

            await UpdateAchievement(engine, userachievements, lesson, user);

            int iTot = sectionlessons.Count();
            int iDone = 0;
            foreach (var sl in sectionlessons.Select(x => x.CorrelationId))
            {
                if (alluserlessons.Contains(sl))
                    iDone++;
            }

            if (iTot == iDone)
            {
                if (item == null)
                    user.Points += Int32.Parse(sectionCompletedPoints);
                logger.Debug("all lessons inside the section have been completed!");
                // section conclusa
                await engine.Set(section, EntityRelation.Linked, user);
                await UpdateAchievement(engine, userachievements, section, user);
                var level = await engine.Get<Unox.Models.RawModels.Level>(EntityRelation.ParentOf, section);
                var levelsections = await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.ChildOf, level);
                var allusersections = (await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.Linked, user)).Select(x => x.CorrelationId);
                iTot = levelsections.Count();
                iDone = 0;
                foreach (var ls in levelsections.Select(x => x.CorrelationId))
                {
                    if (allusersections.Contains(ls))
                        iDone++;
                }

                if (iTot == iDone)
                {
                    if (item == null)
                        user.Points += Int32.Parse(levelCompletedPoints);
                    logger.Debug("all sections inside a level have been completed!");
                    // section conclusa
                    var eeresult = await engine.Set(level, EntityRelation.Linked, user);
                    await UpdateAchievement(engine, userachievements, level, user);

                }
            }

            await engine.Update<Unox.Models.RawModels._ext_User>(user);
            return await _ext_GetUserLevel(userId);

        }

        private async Task<bool> UpdateAchievement(SharedCore.Interfaces.IEntitiesEngine engine, List<UserAchievement> userachievements, Entity entity, Unox.Models.RawModels.User user)
        {
            var item = (from a in userachievements
                        where a.AchievementId == entity.CorrelationId
                        select a).FirstOrDefault();

            if (item == null)
            {
                UserAchievement ua = engine.New<UserAchievement>();
                ua.AchievementDate = DateTime.Now;
                ua.AchievementId = entity.CorrelationId;

                var resp = await engine.Update<UserAchievement>(ua);
                if(user is _ext_User)//Fix. otherwise it is linked to user.
                    resp = await engine.SetEntityRelation("UserAchievement", ua.CorrelationId, EntityRelation.Linked, "_ext_User", user.CorrelationId);
                else
                    resp = await engine.Set(ua, EntityRelation.Linked, user);

                return resp.Success;
            }
            else
            {
                item.AchievementDate = DateTime.Now;
                return (await engine.Update<UserAchievement>(item)).Success;
            }

        }


        string challengeBonus = System.Configuration.ConfigurationManager.AppSettings["challengeCompletedBonus"];

        async Task<object> _ext_SetChallengeFinished(string challengeId, string userId, string lang, string measureUnit, string totalCorrect, string totalQuestions)
        {
            logger.Debug(() => $"USERSTAT: {"Challenge " + challengeId + " was finnished by " + userId}");

            var engine = _ee.Engine;
            var usercorrid = await _ext_GetOrSetUser(userId);
            var user = await engine.Get<Unox.Models.RawModels._ext_User>(usercorrid);

            var userachievements = (await engine.GetAll<Unox.Models.RawModels.UserAchievement>(EntityRelation.Linked, user)).ToList();
            var challenge = await engine.Get<Unox.Models.RawModels._ext_Challenge>(challengeId);

            await engine.Set(challenge, EntityRelation.Linked, user);

            var resp = await UpdateAchievement(engine, userachievements, challenge, user);

            Int32 extraPoints = (Int32)((Double.Parse(challengeBonus) * (Double.Parse(totalCorrect) / Double.Parse(totalQuestions))));
            user.Points += extraPoints;
            logger.Debug("///Attributed a total of partials: " + extraPoints + " totalCorrect: " + totalCorrect + " totalQuestions: " + totalQuestions);
            await engine.Update<Unox.Models.RawModels._ext_User>(user);

            logger.Debug("///Get challenge status list !");
            var challenges = await _ext_GetActiveChallenges(userId, lang, measureUnit);

            return new _ext_ChallengeStat {
                _ext_UserPoints = user.Points,
                ChallengeList = challenges
            };
        }

        async Task<object> _ext_SetLearningPathChallengeFinished(string levelId, string userId)
        {
            logger.Debug(() => $"_ext_USERSTAT: {"Challenge " + levelId + " was finnished by " + userId}");
            logger.Info(() => $"DefaultChallengeStat: {"Challenge " + levelId + " was successfully completed by " + userId}");

            var engine = _ee.Engine;
            var usercorrid = await _ext_GetOrSetUser(userId);
            var user = await engine.Get<Unox.Models.RawModels._ext_User>(usercorrid);
            var userachievements = (await engine.GetAll<Unox.Models.RawModels.UserAchievement>(EntityRelation.Linked, user)).ToList();

            var bonus = System.Configuration.ConfigurationManager.AppSettings["lessonCompletedBonus"];

            user.Points += Int32.Parse(bonus);
            //update user stats
            await engine.Update<Unox.Models.RawModels._ext_User>(user);
            logger.Debug(() => $"_ext_USERSTAT: {"Completed a total of " + userachievements.Count + " challenges, amounting to a total of " + user.Points}");

            var level = await engine.Get<Unox.Models.RawModels.Level>(levelId);
            var sections = await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.ChildOf, level);
            foreach (var s in sections)
            {

                var lessons = await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, s);
                foreach (var l in lessons)
                {
                    await engine.Set(l, EntityRelation.Linked, user);
                    await UpdateAchievement(engine, userachievements, l, user);
                }

                await engine.Set(s, EntityRelation.Linked, user);
                await UpdateAchievement(engine, userachievements, s, user);
            }
            await engine.Set(level, EntityRelation.Linked, user);
            await UpdateAchievement(engine, userachievements, level, user);

            return await _ext_GetUserLevel(userId);
        }

        async Task<object> SetChallengeFinished(string levelId, string userId)
        {
            logger.Debug(() => $"USERSTAT: {"Challenge " + levelId + " was finnished by " + userId}");
            logger.Info(() => $"ChallengeStat: {"Challenge " + levelId + " was successfully completed by " + userId}");

            var engine = _ee.Engine;
            var usercorrid = await GetOrSetUser(userId);
            var user = await engine.Get<Unox.Models.RawModels.User>(usercorrid);
            var userachievements = (await engine.GetAll<Unox.Models.RawModels.UserAchievement>(EntityRelation.Linked, user)).ToList();

            var bonus = System.Configuration.ConfigurationManager.AppSettings["levelCompletedBonus"];
            int numValue;
            bool parsed = Int32.TryParse(bonus, out numValue);

            var points = numValue * userachievements.Count;
            logger.Debug(() => $"USERSTAT: {"Completed a total of " + userachievements.Count + " challenges, amounting to a total of " + points}");

            var level = await engine.Get<Unox.Models.RawModels.Level>(levelId);
            var sections = await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.ChildOf, level);
            foreach (var s in sections)
            {

                var lessons = await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, s);
                foreach (var l in lessons)
                {
                    await engine.Set(l, EntityRelation.Linked, user);
                    await UpdateAchievement(engine, userachievements, l, user);
                }

                await engine.Set(s, EntityRelation.Linked, user);
                await UpdateAchievement(engine, userachievements, s, user);
            }
            await engine.Set(level, EntityRelation.Linked, user);
            await UpdateAchievement(engine, userachievements, level, user);

            return await GetUserLevel(userId);
        }

        /// <summary>
        /// Verifica l'esistenza di un utente e, se nuovo, lo inserisce
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public async Task<string> GetOrSetUser(string userid)
        {
            string correlationid = "";
            var engine = _ee.Engine;
            var users = await engine.GetAll<Unox.Models.RawModels.User>();
            correlationid = (from u in users
                             where u.UserId == userid
                             select u.CorrelationId).FirstOrDefault();
            if (correlationid == null)
            {
                var newuser = engine.New<Unox.Models.RawModels.User>();
                newuser.UserId = userid;
                var eeres = await engine.Update<Unox.Models.RawModels.User>(newuser);
                users = await engine.GetAll<Unox.Models.RawModels.User>();
                correlationid = (from u in users
                                 where u.UserId == userid
                                 select u.CorrelationId).FirstOrDefault();
            }

            return correlationid;
        }

        /// <summary>
        /// Verifica l'esistenza di un utente e, se nuovo, lo inserisce
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public async Task<string> _ext_GetOrSetUser(string userid)
        {
            string correlationid = "";
            var engine = _ee.Engine;
            var users = await engine.GetAll<Unox.Models.RawModels._ext_User>();
            correlationid = (from u in users
                             where u.UserId == userid
                             select u.CorrelationId).FirstOrDefault();
            if (correlationid == null)
            {
                var newuser = engine.New<Unox.Models.RawModels._ext_User>();
                newuser.UserId = userid;
                newuser.Points = 0;
                var eeres = await engine.Update<Unox.Models.RawModels._ext_User>(newuser);
                users = await engine.GetAll<Unox.Models.RawModels._ext_User>();
                correlationid = (from u in users
                                 where u.UserId == userid
                                 select u.CorrelationId).FirstOrDefault();
            }

            return correlationid;
        }


        async Task<object> GetUserLevel(string userId)
        {
            logger.Info(() => $"Richiesta GetUserLevel per utente: {userId}");
            var userstats = new UserStats();
            userstats.UserId = userId;
            userstats.LevelsStat = new List<LevelStat>();

            var engine = _ee.Engine;
            var usercorrid = await GetOrSetUser(userId);
            var user = await engine.Get<Unox.Models.RawModels.User>(usercorrid);
            
            var appsettings = await engine.GetAll<Unox.Models.RawModels.AppSettings>();      

            // fade start
            var fadestart = appsettings.Where(x => x.Name == "FadeStart").FirstOrDefault();
            // fade interval 
            var fadeinterval = appsettings.Where(x => x.Name == "FadeInterval").FirstOrDefault();
            // fade decrement 
            var fadedecrement = appsettings.Where(x => x.Name == "FadeDecrement").FirstOrDefault();

            var achievements = await engine.GetAll<Unox.Models.RawModels.UserAchievement>(EntityRelation.Linked, user);

            // new performance boost
            var tree = (await engine.GetAll<Tree>()).FirstOrDefault();
            if (tree == null) // THE OLD WAY
            {

                // tutti i livelli
                var levels = (await engine.GetAll<Unox.Models.RawModels.Level>()).OrderBy(x => int.Parse(x.Name)).ToList();
                // i livelli completati dall'utente
                var userlevels = (await engine.GetAll<Unox.Models.RawModels.Level>(EntityRelation.Linked, user)).ToList();
                var userlevelscorrids = new List<string>();
                // le sezioni completate dall'utente
                var usersections = (await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.Linked, user)).Select(x => x.CorrelationId).ToList();
                // le lezioni completate dall'utente
                var userlessons = new HashSet<string>((await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.Linked, user)).Select(x => x.CorrelationId).ToList());
                // l'ultimo livello dell'utente. se non ha completato nulla è il primo livello 
                string lastCorrelationId = "";
                Unox.Models.RawModels.Level lastuserlevel = null;
                if (userlevels.Count > 0)
                {
                    lastuserlevel = userlevels.OrderBy(x => int.Parse(x.Name)).Last();
                    userlevelscorrids = userlevels.Select(x => x.CorrelationId).ToList(); 
                }


                if (lastuserlevel == null)
                    lastCorrelationId = levels.FirstOrDefault().CorrelationId;
                else
                {

                    int iLast = int.Parse(lastuserlevel.Name) + 1;
                    if (levels.Count() >= iLast)
                        lastCorrelationId = levels.FirstOrDefault(l => l.Name == iLast.ToString()).CorrelationId;
                    else
                        lastCorrelationId = levels.FirstOrDefault(l => l.Name == (iLast - 1).ToString()).CorrelationId;

                }

                // per ogni livello, completato o meno, aggiungo un LevelStat dentro a UserStats
                foreach (var level in levels)
                {
                    // ####################################### LIVELLO
                    LevelStat ls = new LevelStat();
                    ls.CorrelationId = level.CorrelationId;
                    ls.IsCurrentLevel = (ls.CorrelationId == lastCorrelationId);
                    ls.SectionsStat = new List<SectionStat>();
                    ls.Percentuale = 0;
                    ls.Sbiadimento = 1;
                    ls.IsEnabled = (userlevelscorrids.Contains(level.CorrelationId));
                    int finishedlessons = 0;
                    int totallessons = 0;
                    double sbiadimentisezioni = 0;
                    double totsezioni = 0;
                    

                    var levelachievement = (from l in achievements
                                            where l.AchievementId == level.CorrelationId
                                            select l).FirstOrDefault();

                    if (levelachievement != null)
                    {
                        // calcolo lo sbiadimento
                        var date = levelachievement.AchievementDate;
                        var days = Math.Round((DateTime.Now - date).TotalDays, 0);
                        var start = int.Parse(fadestart.Value);
                        var interval = int.Parse(fadeinterval.Value);
                        var decrement = int.Parse(fadedecrement.Value) / 10d;
                      

                    }
                    // sezioni di livello
                    var levelsections = await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.ChildOf, level);
                    foreach (var levelsection in levelsections)
                    {
                        // ############################################################# SEZIONE

                        SectionStat ss = new SectionStat();
                        ss.LessonsFinishedNumber = 0;
                        ss.LessonsTotalNumber = 0;
                        ss.Percentuale = 0;
                        ss.Sbiadimento = 1;

                        ss.CorrelationId = levelsection.CorrelationId;

                        var sectionachievement = (from l in achievements
                                                  where l.AchievementId == levelsection.CorrelationId
                                                  select l).FirstOrDefault();
                        if (sectionachievement != null)
                        {
                            // calcolo lo sbiadimento
                            var date = sectionachievement.AchievementDate;
                            var days = Math.Round((DateTime.Now - date).TotalDays, 0);
                            var start = int.Parse(fadestart.Value);
                            var interval = int.Parse(fadeinterval.Value);
                            var decrement = int.Parse(fadedecrement.Value) / 10d;
                            if (days > start)
                            {
                                int remainingdays = (int)days - start;
                                var totaldecrement = Math.Round(decrement * (remainingdays / interval), 0) / 10;
                                ss.Sbiadimento = Math.Max(0.5f, (float)ss.Sbiadimento - (float)totaldecrement);
                            }

                        }

                        var sectionlessons = await engine.GetCorrelationIdsByRelation(EntityRelation.ChildOf, "Section", levelsection.CorrelationId, "Lesson");
                        //<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, levelsection);
                        ss.LessonsTotalNumber = sectionlessons.Count();
                        totallessons += ss.LessonsTotalNumber;
                        foreach (var sectionlesson in sectionlessons)
                        {
                            // ############################################################################# LEZIONE
                            if (userlessons.Contains(sectionlesson))
                            {
                                ss.LessonsFinishedNumber++;
                                finishedlessons++;
                            }

                        }

                        if (ss.LessonsTotalNumber > 0)
                            ss.Percentuale = (float)ss.LessonsFinishedNumber / (float)ss.LessonsTotalNumber;
                        totsezioni = totsezioni + 1;
                        sbiadimentisezioni += ss.Sbiadimento;
                        // sbiadimento ci penso io
                        /// TODO: sbiadimento - Data in SetLessonFinished                            
                        ((List<SectionStat>)ls.SectionsStat).Add(ss);
                    }
                    if (totsezioni == 0)
                        totsezioni = 1;
                    double mediasbiad = sbiadimentisezioni / totsezioni;
                    ls.Sbiadimento = Math.Max(0.5f, (float)mediasbiad);
                    if (totallessons > 0)
                        ls.Percentuale = (float)finishedlessons / (float)totallessons;
                    ((List<LevelStat>)userstats.LevelsStat).Add(ls);
                }
            }
            else // THE NEW FAST AND FURIOUS WAY
            {

                var treenodes = tree.TreeNodes;

                // tutti i livelli
                var levels = (from tn in treenodes
                              where tn.Type == "Level"
                              select tn
                              ).ToList();

                // i livelli completati dall'utente
                var userlevels = (await engine.GetAll<Unox.Models.RawModels.Level>(EntityRelation.Linked, user)).ToList();
                var userlevelscorrids = new List<string>();
                // le sezioni completate dall'utente
                var usersections = (await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.Linked, user)).Select(x => x.CorrelationId).ToList();
                // le lezioni completate dall'utente
                var userlessons = new HashSet<string>((await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.Linked, user)).Select(x => x.CorrelationId).ToList());
                // l'ultimo livello dell'utente. se non ha completato nulla è il primo livello 
                string lastCorrelationId = "";
                Unox.Models.RawModels.Level lastuserlevel = null;
               
                if (userlevels.Count > 0)
                {
                    lastuserlevel = userlevels.OrderBy(x => int.Parse(x.Name)).Last();
                    userlevelscorrids = userlevels.Select(x => x.CorrelationId).ToList();
                }

                if (lastuserlevel == null)
                    lastCorrelationId = levels.FirstOrDefault().CorrelationId;
                else
                {
                    int i = levels.FindIndex(x => x.CorrelationId == lastuserlevel.CorrelationId) + 1;
                    int inext = Math.Min(levels.Count -1, i );
                    lastCorrelationId = levels[inext].CorrelationId;                   

                }


                // per ogni livello, completato o meno, aggiungo un LevelStat dentro a UserStats
                foreach (var level in levels)
                {
                    // ####################################### LIVELLO
                    LevelStat ls = new LevelStat();
                    ls.CorrelationId = level.CorrelationId;
                    ls.IsCurrentLevel = (ls.CorrelationId == lastCorrelationId);
                    ls.SectionsStat = new List<SectionStat>();
                    ls.Percentuale = 0;
                    ls.Sbiadimento = 1;
                    ls.IsEnabled = (userlevelscorrids.Contains(level.CorrelationId));
                    int finishedlessons = 0;
                    int totallessons = 0;
                    double sbiadimentisezioni = 0;
                    double totsezioni = 0;


                    var levelachievement = (from l in achievements
                                            where l.AchievementId == level.CorrelationId
                                            select l).FirstOrDefault();

                    if (levelachievement != null)
                    {
                        // calcolo lo sbiadimento
                        var date = levelachievement.AchievementDate;
                        var days = Math.Round((DateTime.Now - date).TotalDays, 0);
                        var start = int.Parse(fadestart.Value);
                        var interval = int.Parse(fadeinterval.Value);
                        var decrement = int.Parse(fadedecrement.Value) / 10d;
                        if (days > start)
                        {
                            int remainingdays = (int)days - start;
                            var totaldecrement = Math.Round(decrement * (remainingdays / interval), 0) / 10;
                            ls.Sbiadimento = Math.Max(0.5f, (float)ls.Sbiadimento - (float)totaldecrement);
                        }

                    }
                    // sezioni di livello
                    var levelsections = (from tn in treenodes
                                         where tn.Type == "Section" && tn.Parent == level.CorrelationId
                                         select tn).ToList();


                                         
                    foreach (var levelsection in levelsections)
                    {
                        // ############################################################# SEZIONE

                        SectionStat ss = new SectionStat();
                        ss.LessonsFinishedNumber = 0;
                        ss.LessonsTotalNumber = 0;
                        ss.Percentuale = 0;
                        ss.Sbiadimento = 1;

                        ss.CorrelationId = levelsection.CorrelationId;

                        var sectionachievement = (from l in achievements
                                                  where l.AchievementId == levelsection.CorrelationId
                                                  select l).FirstOrDefault();
                        if (sectionachievement != null)
                        {
                            // calcolo lo sbiadimento
                            var date = sectionachievement.AchievementDate;
                            var days = Math.Round((DateTime.Now - date).TotalDays, 0);
                            var start = int.Parse(fadestart.Value);
                            var interval = int.Parse(fadeinterval.Value);
                            var decrement = int.Parse(fadedecrement.Value) / 10d;
                            if (days > start)
                            {
                                int remainingdays = (int)days - start;
                                var totaldecrement = Math.Round(decrement * (remainingdays / interval), 0) / 10;
                                ss.Sbiadimento = Math.Max(0.5f, (float)ss.Sbiadimento - (float)totaldecrement);
                            }

                        }

                        var sectionlessons = (from tn in treenodes
                                             where tn.Type == "Lesson" && tn.Parent == levelsection.CorrelationId
                                              select tn).ToList();

                        //<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, levelsection);
                        ss.LessonsTotalNumber = sectionlessons.Count();
                        totallessons += ss.LessonsTotalNumber;
                        foreach (var sectionlesson in sectionlessons)
                        {
                            // ############################################################################# LEZIONE
                            if (userlessons.Contains(sectionlesson.CorrelationId))
                            {
                                ss.LessonsFinishedNumber++;
                                finishedlessons++;
                            }

                        }

                        if (ss.LessonsTotalNumber > 0)
                            ss.Percentuale = (float)ss.LessonsFinishedNumber / (float)ss.LessonsTotalNumber;

                        // sbiadimento ci penso io
                        sbiadimentisezioni += ss.Sbiadimento;
                        totsezioni = totsezioni + 1;
                        ((List<SectionStat>)ls.SectionsStat).Add(ss);
                    }
                    if (totsezioni == 0)
                        totsezioni = 1;
                    double mediasbiad = sbiadimentisezioni / totsezioni;
                    ls.Sbiadimento = Math.Max(0.5f, (float)mediasbiad);
                    if (totallessons > 0)
                        ls.Percentuale = (float)finishedlessons / (float)totallessons;
                    ((List<LevelStat>)userstats.LevelsStat).Add(ls);
                }
            }
            logger.Info(() => $"FINE Richiesta GetUserLevel per utente: {userId}");
            logger.Debug(() => $"USERSTAT: {Newtonsoft.Json.JsonConvert.SerializeObject(userstats)}");
            return userstats;
        }

        async Task<object> _ext_GetUserLevel(string userId)
        {
            
            logger.Info(() => $"Richiesta _ext_GetUserLevel per utente: {userId}");
            var userstats = new _ext_UserStatsDefaultPath();
            userstats.UserId = userId;
            userstats.LevelsStat = new List<LevelStat>();

            var engine = _ee.Engine;
            var usercorrid = await _ext_GetOrSetUser(userId);
            var user = await engine.Get<Unox.Models.RawModels._ext_User>(usercorrid);
            //set the points
            userstats._ext_UserPoints = user.Points;
            //
            var appsettings = await engine.GetAll<Unox.Models.RawModels.AppSettings>();

            // fade start
            var fadestart = appsettings.Where(x => x.Name == "FadeStart").FirstOrDefault();
            // fade interval 
            var fadeinterval = appsettings.Where(x => x.Name == "FadeInterval").FirstOrDefault();
            // fade decrement 
            var fadedecrement = appsettings.Where(x => x.Name == "FadeDecrement").FirstOrDefault();

            var achievements = await engine.GetAll<Unox.Models.RawModels.UserAchievement>(EntityRelation.Linked, user);

            // new performance boost
            var tree = (await engine.GetAll<Tree>()).FirstOrDefault();
            if (tree == null) // THE OLD WAY
            {

                // tutti i livelli
                var levels = (await engine.GetAll<Unox.Models.RawModels.Level>()).OrderBy(x => int.Parse(x.Name)).ToList();
                // i livelli completati dall'utente
                var userlevels = (await engine.GetAll<Unox.Models.RawModels.Level>(EntityRelation.Linked, user)).ToList();
                var userlevelscorrids = new List<string>();
                // le sezioni completate dall'utente
                var usersections = (await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.Linked, user)).Select(x => x.CorrelationId).ToList();
                // le lezioni completate dall'utente
                var userlessons = new HashSet<string>((await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.Linked, user)).Select(x => x.CorrelationId).ToList());
                // l'ultimo livello dell'utente. se non ha completato nulla è il primo livello 
                string lastCorrelationId = "";
                Unox.Models.RawModels.Level lastuserlevel = null;
                if (userlevels.Count > 0)
                {
                    lastuserlevel = userlevels.OrderBy(x => int.Parse(x.Name)).Last();
                    userlevelscorrids = userlevels.Select(x => x.CorrelationId).ToList();
                }


                if (lastuserlevel == null)
                    lastCorrelationId = levels.FirstOrDefault().CorrelationId;
                else
                {

                    int iLast = int.Parse(lastuserlevel.Name) + 1;
                    if (levels.Count() >= iLast)
                        lastCorrelationId = levels.FirstOrDefault(l => l.Name == iLast.ToString()).CorrelationId;
                    else
                        lastCorrelationId = levels.FirstOrDefault(l => l.Name == (iLast - 1).ToString()).CorrelationId;

                }

                // per ogni livello, completato o meno, aggiungo un LevelStat dentro a UserStats
                foreach (var level in levels)
                {
                    // ####################################### LIVELLO
                    LevelStat ls = new LevelStat();
                    ls.CorrelationId = level.CorrelationId;
                    ls.IsCurrentLevel = (ls.CorrelationId == lastCorrelationId);
                    ls.SectionsStat = new List<SectionStat>();
                    ls.Percentuale = 0;
                    ls.Sbiadimento = 1;
                    ls.IsEnabled = (userlevelscorrids.Contains(level.CorrelationId));
                    int finishedlessons = 0;
                    int totallessons = 0;
                    double sbiadimentisezioni = 0;
                    double totsezioni = 0;

                    var levelachievement = (from l in achievements
                                            where l.AchievementId == level.CorrelationId
                                            select l).FirstOrDefault();

                    if (levelachievement != null)
                    {
                        // calcolo lo sbiadimento
                        var date = levelachievement.AchievementDate;
                        var days = Math.Round((DateTime.Now - date).TotalDays, 0);
                        var start = int.Parse(fadestart.Value);
                        var interval = int.Parse(fadeinterval.Value);
                        var decrement = int.Parse(fadedecrement.Value) / 10d;


                    }
                    // sezioni di livello
                    var levelsections = await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.ChildOf, level);
                    foreach (var levelsection in levelsections)
                    {
                        // ############################################################# SEZIONE

                        SectionStat ss = new SectionStat();
                        ss.LessonsFinishedNumber = 0;
                        ss.LessonsTotalNumber = 0;
                        ss.Percentuale = 0;
                        ss.Sbiadimento = 1;

                        ss.CorrelationId = levelsection.CorrelationId;

                        var sectionachievement = (from l in achievements
                                                  where l.AchievementId == levelsection.CorrelationId
                                                  select l).FirstOrDefault();
                        if (sectionachievement != null)
                        {
                            // calcolo lo sbiadimento
                            var date = sectionachievement.AchievementDate;
                            var days = Math.Round((DateTime.Now - date).TotalDays, 0);
                            var start = int.Parse(fadestart.Value);
                            var interval = int.Parse(fadeinterval.Value);
                            var decrement = int.Parse(fadedecrement.Value) / 10d;
                            if (days > start)
                            {
                                int remainingdays = (int)days - start;
                                var totaldecrement = Math.Round(decrement * (remainingdays / interval), 0) / 10;
                                ss.Sbiadimento = Math.Max(0.5f, (float)ss.Sbiadimento - (float)totaldecrement);
                            }

                        }

                        var sectionlessons = await engine.GetCorrelationIdsByRelation(EntityRelation.ChildOf, "Section", levelsection.CorrelationId, "Lesson");
                        //<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, levelsection);
                        ss.LessonsTotalNumber = sectionlessons.Count();
                        totallessons += ss.LessonsTotalNumber;
                        foreach (var sectionlesson in sectionlessons)
                        {
                            // ############################################################################# LEZIONE
                            if (userlessons.Contains(sectionlesson))
                            {
                                ss.LessonsFinishedNumber++;
                                finishedlessons++;
                            }
                        }

                        if (ss.LessonsTotalNumber > 0)
                            ss.Percentuale = (float)ss.LessonsFinishedNumber / (float)ss.LessonsTotalNumber;
                        totsezioni = totsezioni + 1;
                        sbiadimentisezioni += ss.Sbiadimento;
                        // sbiadimento ci penso io
                        /// TODO: sbiadimento - Data in SetLessonFinished                            
                        ((List<SectionStat>)ls.SectionsStat).Add(ss);
                    }
                    if (totsezioni == 0)
                        totsezioni = 1;
                    double mediasbiad = sbiadimentisezioni / totsezioni;
                    ls.Sbiadimento = Math.Max(0.5f, (float)mediasbiad);
                    if (totallessons > 0)
                        ls.Percentuale = (float)finishedlessons / (float)totallessons;
                    ((List<LevelStat>)userstats.LevelsStat).Add(ls);
                }
            }
            else // THE NEW FAST AND FURIOUS WAY
            {

                var treenodes = tree.TreeNodes;

                // tutti i livelli
                var levels = (from tn in treenodes
                              where tn.Type == "Level"
                              select tn
                              ).ToList();

                // i livelli completati dall'utente
                var userlevels = (await engine.GetAll<Unox.Models.RawModels.Level>(EntityRelation.Linked, user)).ToList();
                var userlevelscorrids = new List<string>();
                // le sezioni completate dall'utente
                var usersections = (await engine.GetAll<Unox.Models.RawModels.Section>(EntityRelation.Linked, user)).Select(x => x.CorrelationId).ToList();
                // le lezioni completate dall'utente
                var userlessons = new HashSet<string>((await engine.GetAll<Unox.Models.RawModels.Lesson>(EntityRelation.Linked, user)).Select(x => x.CorrelationId).ToList());
                // l'ultimo livello dell'utente. se non ha completato nulla è il primo livello 
                string lastCorrelationId = "";
                Unox.Models.RawModels.Level lastuserlevel = null;

                if (userlevels.Count > 0)
                {
                    lastuserlevel = userlevels.OrderBy(x => int.Parse(x.Name)).Last();
                    userlevelscorrids = userlevels.Select(x => x.CorrelationId).ToList();
                }

                if (lastuserlevel == null)
                    lastCorrelationId = levels.FirstOrDefault().CorrelationId;
                else
                {
                    int i = levels.FindIndex(x => x.CorrelationId == lastuserlevel.CorrelationId) + 1;
                    int inext = Math.Min(levels.Count - 1, i);
                    lastCorrelationId = levels[inext].CorrelationId;

                }


                // per ogni livello, completato o meno, aggiungo un LevelStat dentro a UserStats
                foreach (var level in levels)
                {
                    // ####################################### LIVELLO
                    LevelStat ls = new LevelStat();
                    ls.CorrelationId = level.CorrelationId;
                    ls.IsCurrentLevel = (ls.CorrelationId == lastCorrelationId);
                    ls.SectionsStat = new List<SectionStat>();
                    ls.Percentuale = 0;
                    ls.Sbiadimento = 1;
                    ls.IsEnabled = (userlevelscorrids.Contains(level.CorrelationId));
                    int finishedlessons = 0;
                    int totallessons = 0;
                    double sbiadimentisezioni = 0;
                    double totsezioni = 0;


                    var levelachievement = (from l in achievements
                                            where l.AchievementId == level.CorrelationId
                                            select l).FirstOrDefault();

                    if (levelachievement != null)
                    {
                        // calcolo lo sbiadimento
                        var date = levelachievement.AchievementDate;
                        var days = Math.Round((DateTime.Now - date).TotalDays, 0);
                        var start = int.Parse(fadestart.Value);
                        var interval = int.Parse(fadeinterval.Value);
                        var decrement = int.Parse(fadedecrement.Value) / 10d;
                        if (days > start)
                        {
                            int remainingdays = (int)days - start;
                            var totaldecrement = Math.Round(decrement * (remainingdays / interval), 0) / 10;
                            ls.Sbiadimento = Math.Max(0.5f, (float)ls.Sbiadimento - (float)totaldecrement);
                        }

                    }
                    // sezioni di livello
                    var levelsections = (from tn in treenodes
                                         where tn.Type == "Section" && tn.Parent == level.CorrelationId
                                         select tn).ToList();



                    foreach (var levelsection in levelsections)
                    {
                        // ############################################################# SEZIONE

                        SectionStat ss = new SectionStat();
                        ss.LessonsFinishedNumber = 0;
                        ss.LessonsTotalNumber = 0;
                        ss.Percentuale = 0;
                        ss.Sbiadimento = 1;

                        ss.CorrelationId = levelsection.CorrelationId;

                        var sectionachievement = (from l in achievements
                                                  where l.AchievementId == levelsection.CorrelationId
                                                  select l).FirstOrDefault();
                        if (sectionachievement != null)
                        {
                            // calcolo lo sbiadimento
                            var date = sectionachievement.AchievementDate;
                            var days = Math.Round((DateTime.Now - date).TotalDays, 0);
                            var start = int.Parse(fadestart.Value);
                            var interval = int.Parse(fadeinterval.Value);
                            var decrement = int.Parse(fadedecrement.Value) / 10d;
                            if (days > start)
                            {
                                int remainingdays = (int)days - start;
                                var totaldecrement = Math.Round(decrement * (remainingdays / interval), 0) / 10;
                                ss.Sbiadimento = Math.Max(0.5f, (float)ss.Sbiadimento - (float)totaldecrement);
                            }

                        }

                        var sectionlessons = (from tn in treenodes
                                              where tn.Type == "Lesson" && tn.Parent == levelsection.CorrelationId
                                              select tn).ToList();

                        //<Unox.Models.RawModels.Lesson>(EntityRelation.ChildOf, levelsection);
                        ss.LessonsTotalNumber = sectionlessons.Count();
                        totallessons += ss.LessonsTotalNumber;
                        foreach (var sectionlesson in sectionlessons)
                        {
                            // ############################################################################# LEZIONE
                            if (userlessons.Contains(sectionlesson.CorrelationId))
                            {
                                ss.LessonsFinishedNumber++;
                                finishedlessons++;
                            }

                        }

                        if (ss.LessonsTotalNumber > 0)
                            ss.Percentuale = (float)ss.LessonsFinishedNumber / (float)ss.LessonsTotalNumber;

                        // sbiadimento ci penso io
                        sbiadimentisezioni += ss.Sbiadimento;
                        totsezioni = totsezioni + 1;
                        ((List<SectionStat>)ls.SectionsStat).Add(ss);
                    }
                    if (totsezioni == 0)
                        totsezioni = 1;
                    double mediasbiad = sbiadimentisezioni / totsezioni;
                    ls.Sbiadimento = Math.Max(0.5f, (float)mediasbiad);
                    if (totallessons > 0)
                        ls.Percentuale = (float)finishedlessons / (float)totallessons;
                    ((List<LevelStat>)userstats.LevelsStat).Add(ls);
                }
            }
            logger.Info(() => $"FINE Richiesta _ext_GetUserLevel per utente: {userId}");
            logger.Debug(() => $"_ext_USERSTAT: {Newtonsoft.Json.JsonConvert.SerializeObject(userstats)}");
            return userstats;
        }

    }

}