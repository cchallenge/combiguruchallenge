﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduAppWS.CMS;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using ServerCore.NancyModule;
using System.Threading.Tasks;
using SharedCore.Entities;
using System.IO;
using Unox;
using Unox.Models.ImporterModels;
using Unox.Models.RawModels;
using System.Text.RegularExpressions;

namespace EduAppWS.Modules
{   

    public class CMSIAppIcon : CMSEntity<AppIcon> { }

    [CRUDModule(EntityName = "appicon", PrimaryKeyName = "CorrelationId")]
    public class AppIconCRUDModule : CRUDModule<CMSIAppIcon, AppIcon>
    {
        IAuthBEAppModule _auth;
        IBEAppCoreContext _beAppContext;
        IContentBEAppModule _storage;
        public AppIconCRUDModule(IBEEntityModule ee, IAuthBEAppModule auth, IBEAppCoreContext beAppContext, IContentBEAppModule beContents) : base(ee)
        {
            _auth = auth;
            _beAppContext = beAppContext;
            _storage = beContents;
        }

        protected override void OnBeforeCreate(CMSIAppIcon entity)
        {
            entity.Entity.CorrelationId = entity.Entity.Name;

            entity.Entity.DateUpdate = DateTime.Now;

            base.OnBeforeCreate(entity);
        }

        protected override CMSIAppIcon DoCreate(CMSIAppIcon entity)
        {
            if (!Regex.IsMatch(entity.Entity.Name, "^[a-zA-Z0-9_]+$"))
            {
                throw new ArgumentException("Il nome dell'icona può contenere solo caratteri alfanumerici"); 
            }
           string tempfile = entity.Entity.FileKey;
            string finalName = entity.Entity.Name +  new FileInfo(tempfile).Extension;
            //DateTime? lastmodified = new DateTime();
            // var stream = _storage.GetContent(_beAppContext, tempfile, out lastmodified);
            string finalFile = $"/images/{finalName}";
            tempfile = tempfile.Replace(@"\", @"/");
            var contentresult = _storage.FinalizeTempPath(_beAppContext, tempfile, finalFile );
            if (contentresult.Success)
            {
                entity.Entity.FileKey = finalFile;
                entity.Entity.ImageUrl = $"contents{finalFile}";
                return base.DoCreate(entity);
            }
            else
            {
                return null;
            }
           

            
        }

        protected override int DoUpdate(CMSIAppIcon entity, string id)
        {
            string tempfile = entity.Entity.FileKey;
            string finalName = entity.Entity.Name + new FileInfo(tempfile).Extension;
            //DateTime? lastmodified = new DateTime();
            // var stream = _storage.GetContent(_beAppContext, tempfile, out lastmodified);
            string finalFile = $"/images/{finalName}";
            tempfile = tempfile.Replace(@"\", @"/");
            var contentresult = _storage.FinalizeTempPath(_beAppContext, tempfile, finalFile);
            if (contentresult.Success)
            {
                entity.Entity.FileKey = finalFile;
                entity.Entity.ImageUrl = $"contents{finalFile}";
                return base.DoUpdate(entity, id);
            }
            else
            {
                return 0;
            }
        }
    }
}