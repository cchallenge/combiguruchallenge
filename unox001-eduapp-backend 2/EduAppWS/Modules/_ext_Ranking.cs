﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduAppWS.CMS;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using ServerCore.NancyModule;
using System.Threading.Tasks;
using SharedCore.Entities;
using System.IO;
using Unox;
using Unox.Models.ImporterModels;
using Unox.Models.RawModels;
using System.Text.RegularExpressions;
using ServerCore.Interfaces.Logging;

namespace EduAppWS.Modules
{

    public class Rank : Entity
    {
        public string Nickname { get; set; }
        public string InstitutionName { get; set; }
        public int InstitutionIdentifier { get; set; }
        public string Email { get; set; }
        public int Score { get; set; }
    }

    public class CMSRank : CMSEntity<Rank> { }

    [CRUDModule(EntityName = "ranking", PrimaryKeyName = "CorrelationId")]
    public class RankingCRUDModule : CRUDModule<CMSRank, Rank>
    {
        ILog _logger = LogProvider.GetLogger("NancyUnoxEduModule");
        IAuthBEAppModule _auth;
        IBEAppCoreContext _beAppContext;
        IContentBEAppModule _storage;

        public RankingCRUDModule(IBEEntityModule ee, IAuthBEAppModule auth, IBEAppCoreContext beAppContext, IContentBEAppModule beContents) : base(ee)
        {
            _auth = auth;
            _beAppContext = beAppContext;
            _storage = beContents;
        }

        protected override IEnumerable<CMSRank> DoGetAll(out long count)
        {
            Dictionary<string, object> filter = this.getDictionaryForFilterQuery();

            List<CMSRank> users = null;
            if (filter != null && filter.Count() > 0)
                users = Task.Run(() => GetAllQualifiedUsers(filter)).Result;
            else
                users = Task.Run(() => GetAllQualifiedUsers(null)).Result;

            count = users.Count();
            return users.AsEnumerable();
        }

        private async Task<List<CMSRank>> GetAllQualifiedUsers(Dictionary<string, object> filter) {
            if(filter!=null) { 
                string s = "";
                foreach(KeyValuePair<string, object> kvp in filter)
                {
                    //textBox3.Text += ("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                    s += string.Format("Key = {0}, Value = {1}", kvp.Key, kvp.Value.ToString());
                }
                _logger.Debug("///Filter is: " + s);
            }
            var users = await EE.Engine.GetAll<_ext_User>();
            var lUsers = await EE.Engine.GetAll<LoginStatUser>();
            var institutions = await EE.Engine.GetAll<_ext_Institution>();
            List<CMSRank> qusers = new List<CMSRank>();
            
            foreach (var user in users) {
                var logU = (from lu in lUsers
                                        where lu.CorrelationId == user.UserId
                                        select lu).FirstOrDefault();            

                var uInstitution = (from uInst in institutions
                            where uInst.Identifier == user.Institution
                            select uInst).FirstOrDefault();
                
                if (filter == null)//filter does not apply
                {
                    var r = new CMSRank
                    {
                        Entity = new Rank
                        {
                            CorrelationId = user.CorrelationId,
                            Nickname = !String.IsNullOrEmpty(user.Nickname)? user.Nickname:"Annomymous",
                            InstitutionName = (uInstitution != null) ? uInstitution.Name : "N/A",
                            InstitutionIdentifier = (uInstitution != null) ? Int32.Parse(uInstitution.Identifier) : 0,
                            Email = (logU != null) ? logU.Email : "N/A",
                            Score = user.Points
                        }
                    };
                    qusers.Add(r);

                }
                else 
                {//institutionIdentifier filter only
                    if (uInstitution != null && uInstitution.Identifier == filter["institutionIdentifier"].ToString())
                    {
                        var r = new CMSRank
                        {
                            Entity = new Rank
                            {
                                CorrelationId = user.CorrelationId,
                                Nickname = user.Nickname,
                                InstitutionName = uInstitution.Name,
                                InstitutionIdentifier = Int32.Parse(uInstitution.Identifier),
                                Email = (logU != null) ? logU.Email : "N/A",
                                Score = user.Points
                            }
                        };
                        qusers.Add(r);
                    }
                }
            }

            return qusers;
        }

    }
}