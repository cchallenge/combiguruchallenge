﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduAppWS.CMS;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using ServerCore.NancyModule;
using System.Threading.Tasks;
using SharedCore.Entities;
using System.IO;
using Unox;
using Unox.Models.ImporterModels;
using Unox.Models.RawModels;
using System.Text.RegularExpressions;
using ServerCore.Interfaces.Logging;

namespace EduAppWS.Modules
{

    public class CMSChallenge : CMSEntity<_ext_Challenge>
    {
        public string ITTitle { get; set; }
        public string ENTitle { get; set; }
        public string DETitle { get; set; }
        public string FRTitle { get; set; }
        public string SPTitle { get; set; }

        public string ITDesc { get; set; }
        public string ENDesc { get; set; }
        public string DEDesc { get; set; }
        public string FRDesc { get; set; }
        public string SPDesc { get; set; }

        public string ImageUrl { get; set; }

    }

    [CRUDModule(EntityName = "challenges", PrimaryKeyName = "CorrelationId")]
    public class ChallengeCRUDModule : CRUDModule<CMSChallenge, _ext_Challenge>
    {
        ILog _logger = LogProvider.GetLogger("NancyUnoxEduModule");
        IAuthBEAppModule _auth;
        IBEAppCoreContext _beAppContext;
        IContentBEAppModule _storage;

        public ChallengeCRUDModule(IBEEntityModule ee, IAuthBEAppModule auth, IBEAppCoreContext beAppContext, IContentBEAppModule beContents) : base(ee)
        {
            _auth = auth;
            _beAppContext = beAppContext;
            _storage = beContents;
        }


        protected override CMSChallenge DoCreate(CMSChallenge entity)
        {
            var engine = EE.Engine;

            entity.Entity.Title = new Dictionary<string, string>(){
                    {  "IT", entity.ITTitle },
                    {  "EN", entity.ENTitle },
                    {  "DE", entity.DETitle },
                    {  "FR", entity.FRTitle },
                    {  "SP", entity.SPTitle }
            };

            entity.Entity.Description = new Dictionary<string, string>(){
                    {  "IT", entity.ITDesc },
                    {  "EN", entity.ENDesc },
                    {  "DE", entity.DEDesc },
                    {  "FR", entity.FRDesc },
                    {  "SP", entity.SPDesc },
            };
            _logger.Debug("Adding a new challenge which starts: " + entity.Entity.DateStart + " and expires: " + entity.Entity.DateEnd);

            entity.Entity.UserName = Context.CurrentUser.UserName;
            entity.Entity.ExcelFile = entity.Entity.ExcelFile;
            _logger.Debug("creating a new challenge issued by " + entity.Entity.UserName);
            CMSChallenge newEntity = (CMSChallenge)base.DoCreate(entity);
            _logger.Debug("created new challenge with correlationId " + newEntity.Entity.CorrelationId);

            var cr = Task.Run(() => _ext_TryImport(entity, newEntity.Entity.CorrelationId)).Result;
            if (!cr.Success)
            {
                string message = "";
                foreach (var m in cr.Messages)
                    message += $"\r\n{m}";
                throw new Exception($"IMPORTAZIONE FALLITA - {message}");
            }
//            else
//            {
                //bool btree = Task.Run(() => RebuildTree()).Result;
//            }

            return newEntity;
        }

        public async Task<CheckResult> _ext_TryImport(CMSChallenge entity, string challengeCorrelationId)
        {
            Guid gname = Guid.NewGuid();
            var dir = new DirectoryInfo(Path.Combine(_beAppContext.RootPath, "tmp"));
            if (!dir.Exists)
                dir.Create();
            string filename = Path.Combine(dir.FullName, $"{gname.ToString("N")}.xlsx");
            try
            {
                string tempfile = entity.Entity.ExcelFile;
                DateTime? lastmodified = new DateTime();
                using (var stream = _storage.GetContent(_beAppContext, tempfile, out lastmodified))
                {
                    if (File.Exists(filename))
                        File.Delete(filename);

                    using (var fileStream = File.Create(filename))
                    {
                        stream.Seek(0, SeekOrigin.Begin);
                        stream.CopyTo(fileStream);
                    }

                    if (File.Exists(filename))
                        Console.Write("SUCCESS");

                    var engine = EE.Engine;

                    ExcelParser xl = new ExcelParser(engine);
                    CheckResult cr = new CheckResult();
                    cr.Success = true;
                    /**
                     * Challenges differ in structure from the defaul learningPath level.
                     * Single hierarchy: challenge - questions, however, we maintain currrent 
                     * structure and the good part of the structure is level1/section1/lession1/...
                     */
                    var importedlevel = xl._ext_Import(filename, "-1", ref cr);//XXX: (-1) a fix to reuse existing code 
                    if (cr.Success)
                    {
                        cr = await NormalizeLevel(importedlevel, cr, challengeCorrelationId);
                    }//we do not have any validation 
                    return cr;
                }
            }
            catch (Exception ex)
            {
                CheckResult cr = new CheckResult();
                _logger.Fatal($"IMPORTAZIONE FALLITA {ex.Message}");
                cr.Success = false;
                cr.Messages.Add($"IMPORTAZIONE FALLITA {ex.Message}");
                return cr;
            }
            finally
            {
                if (File.Exists(filename))
                    File.Delete(filename);
            }
        }

        protected override CMSChallenge BeforeWrite(CMSChallenge entity)
        {
            entity.Entity.Description = new Dictionary<string, string>(){
                {  "IT", entity.ITDesc },
                {  "EN", entity.ENDesc },
                {  "DE", entity.DEDesc },
                {  "FR", entity.FRDesc },
                {  "SP", entity.SPDesc }
            };

            entity.Entity.Title = new Dictionary<string, string>(){
                {  "IT", entity.ITTitle },
                {  "EN", entity.ENTitle },
                {  "DE", entity.DETitle },
                {  "FR", entity.FRTitle },
                {  "SP", entity.SPTitle },
            };

            _logger.Info("BeforeWrite - prepared the entity accordingly");
            return base.BeforeWrite(entity);
        }

        private async Task<string> GetIconUrlByName(string icon)
        {
            if (!String.IsNullOrEmpty(icon))
            {
                string sresult = "";
                var engine = EE.Engine;
                var icons = await engine.GetAll<Unox.Models.RawModels.AppIcon>();
                sresult = (from c in icons
                           where c.Name == icon
                           select c.FileKey).FirstOrDefault();

                return sresult;
            }
            return icon;
        }


        protected override CMSChallenge AfterRead(CMSChallenge entity)
        {
            if (entity.Entity.Description != null)
            {
                entity.ITDesc = entity.Entity.Description["IT"];
                entity.ENDesc = entity.Entity.Description["EN"];
                entity.DEDesc = entity.Entity.Description["DE"];
                entity.FRDesc = entity.Entity.Description["FR"];
                entity.SPDesc = entity.Entity.Description["SP"];
            }

            if (entity.Entity.Title != null)
            {
                entity.ITTitle = entity.Entity.Title["IT"];
                entity.ENTitle = entity.Entity.Title["EN"];
                entity.DETitle = entity.Entity.Title["DE"];
                entity.FRTitle = entity.Entity.Title["FR"];
                entity.SPTitle = entity.Entity.Title["SP"];
            }

            entity.ImageUrl = Task.Run(() => GetIconUrlByName(entity.Entity.Image.Replace("contents", ""))).Result;
            entity.ImageUrl = $"contents{entity.ImageUrl}";

            _logger.Info("Read#1 an entity with the following contents: " + entity.ToString());
            _logger.Info("Read#2 an entity with the following contents: " + entity.Entity.ToString());

            return base.AfterRead(entity);
        }

        protected override int DoUpdate(CMSChallenge entity, string id)
        {
            return base.DoUpdate(entity, id);
        }



        /// </summary>
        /// <param name="livello"></param>
        /// <returns></returns>
        public async Task<CheckResult> NormalizeLevel(_ext_ImpLevel livello, CheckResult cr, string challengeCorrelationId)
        {
            var engine = EE.Engine;

            //
            var challenge = await engine.Get<_ext_Challenge>(challengeCorrelationId);

            if (challenge == null)
            {
                _logger.Debug("Some error has occurred, challenge was not found in the db!");
                cr.Success = false;
                return cr;
            }

            //la domande sono sempre contenute nella prima sezione, prima lezione
            List<_ext_ImpQuestion> domande = livello.Sezioni.ElementAt(0).Lezioni.ElementAt(0).Domande;
            foreach (var domanda in domande)
            {
                var curdomanda = domanda.Domanda;
                curdomanda.CorrelationId = engine.NewCorrelationId("Question");
                await engine.Update<Question>(curdomanda);
                await engine.Set(curdomanda, "childOf", challenge);

            }
            // debug
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(livello);
            _logger.Info("JSON DI IMPORTAZIONE");
            _logger.Info(json);
            return cr;
        }
    }
}