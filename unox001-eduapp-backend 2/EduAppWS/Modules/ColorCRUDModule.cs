﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduAppWS.CMS;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using ServerCore.NancyModule;
using System.Threading.Tasks;
using SharedCore.Entities;
using System.IO;
using Unox;
using Unox.Models.ImporterModels;
using Unox.Models.RawModels;
using System.Text.RegularExpressions;

namespace EduAppWS.Modules
{
    public class CMSColor : CMSEntity<AppColor> {

       
    }

    [CRUDModule(EntityName = "appcolor", PrimaryKeyName = "CorrelationId")]
    public class ColorCRUDModule : CRUDModule<CMSColor, AppColor>
    {
        IAuthBEAppModule _auth;
        IBEAppCoreContext _beAppContext;
        IContentBEAppModule _storage;
        public ColorCRUDModule(IBEEntityModule ee, IAuthBEAppModule auth, IBEAppCoreContext beAppContext, IContentBEAppModule beContents) : base(ee)
        {
            _auth = auth;
            _beAppContext = beAppContext;
            _storage = beContents;
        }

        protected override CMSColor DoCreate(CMSColor entity)
        {
            return base.DoCreate(entity);
        }

    }

}