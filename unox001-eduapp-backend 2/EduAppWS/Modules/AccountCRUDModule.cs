﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduAppWS.CMS;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using ServerCore.NancyModule;
using System.Threading.Tasks;
using SharedCore.Entities;
using EduAppWS;
using Nancy.ModelBinding;
using Unox.Models.RawModels;
using ServerCore.Interfaces.Logging;

namespace ws.Modules

{

	public class Account : Entity
	{
		public string UserToken { get; set; }
		public string UserName { get; set; }
		public string Claims { get; set; }
		public string Password { get; set; }
		public bool Enabled { get; set; }
	}

    public class CMSAccount : CMSEntity<Account> { }

    [CRUDModule(EntityName = "account", PrimaryKeyName = "CorrelationId")]
    public class AccountCRUDModule : CRUDModule<CMSAccount, Account>
    {
        IAuthBEAppModule _auth;
        IBEAppCoreContext _beAppContext;
        public AccountCRUDModule(IBEEntityModule ee, IAuthBEAppModule auth, IBEAppCoreContext beAppContext) : base(ee)
        {
            _auth = auth;
            _beAppContext = beAppContext;

			this.Post["/account_login/update"] = x => ResponseJson(UpdateUserLogin(this.Bind<UpdateLoginRequest>()));
        }

		protected override IEnumerable<CMSAccount> DoGetAll(out long count)
		{
            var res = _auth.List(_beAppContext, out count);
            Console.WriteLine("DoGetAllAccounts");
            return res.Select(auth => new CMSAccount
			{
				Entity = new Account
				{
					UserName = auth.UserName,
					UserToken = auth.UserToken,
					Claims = string.Join(", ", auth.Claims),
					Password = passwordTemplate,
					CorrelationId = auth.CorrelationId,
					Enabled = auth.Success
				}
			});
		}

		string passwordTemplate = "passwordmd5";
		protected override CMSAccount DoGet(string id)
		{
            Console.WriteLine("DoGet accounts");
            CMSAccount acc = null;

			var res = _auth.Get(_beAppContext, id);

			if (res.Success)
			{
				acc = new CMSAccount
				{
					Entity = new Account
					{
						UserName = res.UserName,
						UserToken = res.UserToken,
						Claims = string.Join(", ", res.Claims),
						Password = passwordTemplate,
						Enabled = res.Success,
						CorrelationId = id
					}
				};
			}

			return acc;
		}

        protected override CMSAccount DoCreate(CMSAccount entity)
        {
            Console.WriteLine("DoCreate");
            entity.Entity.Password = SecurityHelper.MD5String(SecurityHelper.GenerateNewPassword());

			var claims = entity.Entity.Claims.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

			var res =_auth.Create(_beAppContext, entity.Entity.UserName, entity.Entity.Password, claims, entity.Entity.Enabled);

			return res.Success ? entity : null;
        }

        protected override int DoUpdate(CMSAccount entity, string id)
        {
			var claims = entity.Entity.Claims.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

			if (entity.Entity.Password.Equals(passwordTemplate))
			{
				entity.Entity.Password = null;
			}

			var res = _auth.Update(_beAppContext, id, entity.Entity.UserName, entity.Entity.Password, claims, entity.Entity.Enabled);

			return res.Success ? 0 : 1;
        }

        protected override int DoDelete(string id)
        {
            Console.WriteLine("DoDelete");
            var res = _auth.Delete(_beAppContext, id);

            return res.Success ? 0 : 1;
        }


		object UpdateUserLogin(UpdateLoginRequest updateLogin)
		{
            Console.WriteLine("UpdateUserLogin");
			var loginStat = new LoginStatUser
			{
				CorrelationId = updateLogin.UserId,
				Email = updateLogin.Email,
				LastLogin = DateTime.UtcNow,
				Lang = updateLogin.Lang
			};

			loginStat.SetEngine(EE.Engine);

			Task.Run(async() => await EE.Engine.Update(loginStat)).Wait();

			return loginStat;
		}
    }
}