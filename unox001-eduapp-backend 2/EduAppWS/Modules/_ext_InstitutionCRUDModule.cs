﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduAppWS.CMS;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using ServerCore.NancyModule;
using System.Threading.Tasks;
using SharedCore.Entities;
using EduAppWS;
using Nancy.ModelBinding;
using Unox.Models.RawModels;
using ServerCore.Interfaces.Logging;

namespace ws.Modules

{

    public class CMSInstitution : CMSEntity<_ext_Institution> { }

    [CRUDModule(EntityName = "institution", PrimaryKeyName = "CorrelationId")]
    public class InstitutionCRUDModule : CRUDModule<CMSInstitution, _ext_Institution>
    {
        ILog logger = LogProvider.GetLogger("CMSInstitution");
        IAuthBEAppModule _auth;
        IBEAppCoreContext _beAppContext;
        public InstitutionCRUDModule(IBEEntityModule ee, IAuthBEAppModule auth, IBEAppCoreContext beAppContext) : base(ee)
        {
            _auth = auth;
            _beAppContext = beAppContext;

        }


        protected override CMSInstitution DoCreate(CMSInstitution entity)
        {
            entity.Entity.Identifier = GenerateUniqueIdentifier();// this one we generate ourselves, imposing uniqueness criteria
            Console.WriteLine("DoCreate institution " + entity.Entity.Name + " with contact " + entity.Entity.Contact + " identifier: " + entity.Entity.Identifier);
            logger.Info(() => $"DoCreate institution " + entity.Entity.Name + " with contact " + entity.Entity.Contact + " identifier: " + entity.Entity.Identifier);

            return base.DoCreate(entity);

        }

        //XXX: brute force solution, does not scale well with # of entries - sparse-search data structures are suited.
        private string GenerateUniqueIdentifier() {

            var engine = EE.Engine;
            var existingIdentifiers = Task.Run(() => engine.GetAll<_ext_Institution>()).Result;
            Boolean idChosen = true;
            Random rng = new Random();
            int rndIdentifier = 0, skipped = 0;
            while (idChosen)
            {
                rndIdentifier = rng.Next((int)Math.Pow(10, 5), (int)Math.Pow(10, 6));
                foreach (var elm in existingIdentifiers)
                {
                    if (elm.Identifier.Equals(rndIdentifier.ToString()))
                        break;
                    skipped++;
                }
                if (skipped == existingIdentifiers.Count())
                    idChosen = false;
            }

            return rndIdentifier.ToString();
        }

        protected override int DoUpdate(CMSInstitution entity, string id)
        {
            return base.DoUpdate(entity, id);
        }

        protected override int DoDelete(string id)
        {
            return base.DoDelete(id);
        }
    }
}