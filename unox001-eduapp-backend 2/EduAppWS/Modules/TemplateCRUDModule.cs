﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduAppWS.CMS;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using ServerCore.NancyModule;
using System.Threading.Tasks;
using SharedCore.Entities;
using System.IO;
using Unox;
using Unox.Models.ImporterModels;
using Unox.Models.RawModels;
using System.Text.RegularExpressions;

namespace EduAppWS.Modules
{
    public class CMSTemplate : CMSEntity<Template> {

        public string ITLabel { get; set; }
        public string ENLabel { get; set; }
        public string ESLabel { get; set; }
        public string DELabel { get; set; }
        public string FRLabel { get; set; }
    }

    [CRUDModule(EntityName = "template", PrimaryKeyName = "CorrelationId")]
    public class TemplateCRUDModule : CRUDModule<CMSTemplate, Template>
    {
        IAuthBEAppModule _auth;
        IBEAppCoreContext _beAppContext;
        IContentBEAppModule _storage;
        public TemplateCRUDModule(IBEEntityModule ee, IAuthBEAppModule auth, IBEAppCoreContext beAppContext, IContentBEAppModule beContents) : base(ee)
        {
            _auth = auth;
            _beAppContext = beAppContext;
            _storage = beContents;
        }

        protected override CMSTemplate AfterRead(CMSTemplate entity)
        {
            try
            {
                entity.ITLabel = entity.Entity.Titles["IT"];
                entity.ENLabel = entity.Entity.Titles["EN"];
                entity.DELabel = entity.Entity.Titles["DE"];
                entity.FRLabel = entity.Entity.Titles["FR"];
                entity.ESLabel = entity.Entity.Titles["ES"];
            }
            catch {

                entity.ITLabel = entity.Entity.Titles["it"];
                entity.ENLabel = entity.Entity.Titles["en"];
                entity.DELabel = entity.Entity.Titles["de"];
                entity.FRLabel = entity.Entity.Titles["fr"];
                entity.ESLabel = entity.Entity.Titles["es"];
            }

            return base.AfterRead(entity);
        }

        protected override CMSTemplate DoCreate(CMSTemplate entity)
        {
            entity.Entity.Name = entity.Entity.Name.ToUpper();
            var engine = EE.Engine;
            var existingtemplates = Task.Run(() => engine.GetAll<Template>()).Result;

            var check = (from t in existingtemplates
                         where t.Name == entity.Entity.Name
                          select t.CorrelationId).FirstOrDefault();

            if (string.IsNullOrEmpty(check))
            {

                entity.Entity.Titles = new Dictionary<string, string>(){
                {  "IT", entity.ITLabel },
                {  "EN", entity.ENLabel },
                {  "DE", entity.DELabel },
                {  "FR", entity.FRLabel },
                {  "ES", entity.ESLabel }
            };


                return base.DoCreate(entity);
            }
            else
            {
                throw new Exception("Template già esistente. Modificare o eliminare il precedente e riprovare.");


            }
        }
        
        protected override int DoUpdate(CMSTemplate entity, string id)
        {
            entity.Entity.Name = entity.Entity.Name.ToUpper();
            entity.Entity.Titles = new Dictionary<string, string>(){
                {  "IT", entity.ITLabel },
                {  "EN", entity.ENLabel },
                {  "DE", entity.DELabel },
                {  "FR", entity.FRLabel },
                {  "ES", entity.ESLabel }
            };
            return base.DoUpdate(entity, id);
        }


    }

}