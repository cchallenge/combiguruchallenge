using System;
using System.Collections.Generic;

namespace EduAppWS
{
	public class Response
	{
		public bool Success { get; set; }
		public object Payload { get; set; }
		public string[] Errors { get; set; }
	}
}

