﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduAppWS.CMS;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using ServerCore.NancyModule;
using System.Threading.Tasks;
using SharedCore.Entities;
using System.IO;
using Unox;
using Unox.Models.ImporterModels;
using Unox.Models.RawModels;
using System.Text.RegularExpressions;

namespace EduAppWS.Modules
{
	public class CMSNotificationMessage : CMSEntity<NotificationMessage> {

       
    }

    [CRUDModule(EntityName = "notificationMessage", PrimaryKeyName = "CorrelationId")]
    public class NotificationMessageCRUDModule : CRUDModule<CMSNotificationMessage, NotificationMessage>
    {
        public NotificationMessageCRUDModule(IBEEntityModule ee) : base(ee)
        {
            
        }
    }

}