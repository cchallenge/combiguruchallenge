using System;
using Nancy;
using System.Data;
using System.Configuration;
using Nancy.Responses;
using System.Web.Security;
using Nancy.Security;
using System.Collections.Generic;
using System.Linq;
using EduAppWS.CMS;
using ServerCore.Interfaces;

namespace EduAppWS.Modules
{
	public class DbModule: BackendModule
	{
		readonly IBEEntityModule _ee;
		public DbModule (string root, IBEEntityModule ee) : base (root)
		{
			_ee = ee;
		}

		public DbModule (IBEEntityModule ee) : base ()
		{
			_ee = ee;
		}

		public IBEEntityModule EE { get; set; }
	}


	public class SecureModule: DbModule {

		public SecureModule (string root, IBEEntityModule ee) : base (root, ee)
		{
			init ();
		}

		public SecureModule (IBEEntityModule ee) : base (ee)
		{
			init ();
		}

		void init ()
		{
			Before.AddItemToEndOfPipeline ( c =>{
				if (c.CurrentUser == null) {
					return new JsonResponse(
						new Response {
							Success = false,
							Errors = new [] { "EUNAUTHORIZED" }
						},
						new DefaultJsonSerializer()
					).WithStatusCode(HttpStatusCode.Unauthorized);
				}

				return null;
			});
		}
	}
}
