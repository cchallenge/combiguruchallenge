﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduAppWS.CMS;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using ServerCore.NancyModule;
using System.Threading.Tasks;
using SharedCore.Entities;
using System.IO;
using Unox;
using Unox.Models.ImporterModels;
using Unox.Models.RawModels;
using System.Text.RegularExpressions;

namespace EduAppWS.Modules
{

    public class CMSLevel : CMSEntity<Level>
    {
        public string ITDesc { get; set; }
        public string ENDesc { get; set; }
        public string ESDesc { get; set; }
        public string DEDesc { get; set; }
        public string FRDesc { get; set; }

        public string ITChallenge { get; set; }
        public string ENChallenge { get; set; }
        public string ESChallenge { get; set; }
        public string DEChallenge { get; set; }
        public string FRChallenge { get; set; }

        public string ImageUrl { get; set; }
        public string ChallengeImageUrl { get; set; }
        public string ScienceImageUrl { get; set; }

        public string ITScience { get; set; }
        public string ENScience { get; set; }
        public string ESScience { get; set; }
        public string DEScience { get; set; }
        public string FRScience { get; set; }

        public string ITScienceTitle { get; set; }
        public string ENScienceTitle { get; set; }
        public string ESScienceTitle { get; set; }
        public string DEScienceTitle { get; set; }
        public string FRScienceTitle { get; set; }


    }

    [CRUDModule(EntityName = "level", PrimaryKeyName = "CorrelationId")]
    public class LevelCRUDModule : CRUDModule<CMSLevel, Level>
    {
        IAuthBEAppModule _auth;
        IBEAppCoreContext _beAppContext;
        IContentBEAppModule _storage;
        public LevelCRUDModule(IBEEntityModule ee, IAuthBEAppModule auth, IBEAppCoreContext beAppContext, IContentBEAppModule beContents) : base(ee)
        {
            _auth = auth;
            _beAppContext = beAppContext;
            _storage = beContents;
        }


        protected override CMSLevel DoCreate(CMSLevel entity)
        {

            var engine = EE.Engine;
            var existinglevels = Task.Run(() => engine.GetAll<Level>()).Result;

            var check = (from t in existinglevels
                         where t.Name == entity.Entity.Name
                         select t.CorrelationId).FirstOrDefault();

            if (string.IsNullOrEmpty(check))
            {


                entity.Entity.Description = new Dictionary<string, string>(){
                    {  "IT", entity.ITDesc },
                    {  "EN", entity.ENDesc },
                    {  "DE", entity.DEDesc },
                    {  "FR", entity.FRDesc },
                    {  "ES", entity.ESDesc }
                    };

                entity.Entity.ChallengeTitle = new Dictionary<string, string>(){
                    {  "IT", entity.ITChallenge },
                    {  "EN", entity.ENChallenge },
                    {  "DE", entity.DEChallenge },
                    {  "FR", entity.FRChallenge },
                    {  "ES", entity.ESChallenge }
                    };

                entity.Entity.Science = new Dictionary<string, string>(){
                    {  "IT", entity.ITScience },
                    {  "EN", entity.ENScience },
                    {  "DE", entity.DEScience },
                    {  "FR", entity.FRScience },
                    {  "ES", entity.ESScience }
                    };

                entity.Entity.ScienceTitle = new Dictionary<string, string>(){
                    {  "IT", entity.ITScienceTitle },
                    {  "EN", entity.ENScienceTitle },
                    {  "DE", entity.DEScienceTitle },
                    {  "FR", entity.FRScienceTitle },
                    {  "ES", entity.ESScienceTitle }
                    };

                return base.DoCreate(entity);
            }
            else
            {
                throw new Exception("Livello già esistente. Modificare o eliminare il precedente e riprovare.");
            }
        }

        private async Task<string> GetIconUrlByName(string icon)
        {
            if (!String.IsNullOrEmpty(icon))
            {
                string sresult = "";
                var engine = EE.Engine;
                var icons = await engine.GetAll<Unox.Models.RawModels.AppIcon>();
                sresult = (from c in icons
                           where c.Name == icon
                           select c.FileKey).FirstOrDefault();

                return sresult;
            }
            return icon;
        }


        protected override CMSLevel BeforeWrite(CMSLevel entity)
        {
            entity.Entity.Description = new Dictionary<string, string>(){
                {  "IT", entity.ITDesc },
                {  "EN", entity.ENDesc },
                {  "DE", entity.DEDesc },
                {  "FR", entity.FRDesc },
                {  "ES", entity.ESDesc }
            };

            entity.Entity.ChallengeTitle = new Dictionary<string, string>(){
                {  "IT", entity.ITChallenge },
                {  "EN", entity.ENChallenge },
                {  "DE", entity.DEChallenge },
                {  "FR", entity.FRChallenge },
                {  "ES", entity.ESChallenge }
            };

            entity.Entity.Science = new Dictionary<string, string>(){
                {  "IT", entity.ITScience },
                {  "EN", entity.ENScience},
                {  "DE", entity.DEScience },
                {  "FR", entity.FRScience },
                {  "ES", entity.ESScience }
            };
            entity.Entity.ScienceTitle = new Dictionary<string, string>(){
                    {  "IT", entity.ITScienceTitle },
                    {  "EN", entity.ENScienceTitle },
                    {  "DE", entity.DEScienceTitle },
                    {  "FR", entity.FRScienceTitle },
                    {  "ES", entity.ESScienceTitle }
                    };


            return base.BeforeWrite(entity);
        }

        protected override int DoUpdate(CMSLevel entity, string id)
        {

            //entity.Entity.Description = new Dictionary<string, string>(){
            //    {  "IT", entity.ITDesc },
            //    {  "EN", entity.ENDesc },
            //    {  "DE", entity.DEDesc },
            //    {  "FR", entity.FRDesc },
            //    {  "ES", entity.ESDesc }
            //};

            //entity.Entity.ChallengeTitle = new Dictionary<string, string>(){
            //    {  "IT", entity.ITChallenge },
            //    {  "EN", entity.ENChallenge },
            //    {  "DE", entity.DEChallenge },
            //    {  "FR", entity.FRChallenge },
            //    {  "ES", entity.ESChallenge }
            //};
            return base.DoUpdate(entity, id);
        }



        protected override CMSLevel AfterRead(CMSLevel entity)
        {
            if (entity.Entity.Description != null)
            {
                entity.ITDesc = entity.Entity.Description["IT"];
                entity.ENDesc = entity.Entity.Description["EN"];
                entity.DEDesc = entity.Entity.Description["DE"];
                entity.FRDesc = entity.Entity.Description["FR"];
                entity.ESDesc = entity.Entity.Description["ES"];
            }
            if (entity.Entity.ChallengeTitle != null)
            {
                entity.ITChallenge = entity.Entity.ChallengeTitle["IT"];
                entity.ENChallenge = entity.Entity.ChallengeTitle["EN"];
                entity.DEChallenge = entity.Entity.ChallengeTitle["DE"];
                entity.FRChallenge = entity.Entity.ChallengeTitle["FR"];
                entity.ESChallenge = entity.Entity.ChallengeTitle["ES"];
            }
            if (entity.Entity.Science != null)
            {
                entity.ITScience = entity.Entity.Science["IT"];
                entity.ENScience = entity.Entity.Science["EN"];
                entity.DEScience = entity.Entity.Science["DE"];
                entity.FRScience = entity.Entity.Science["FR"];
                entity.ESScience = entity.Entity.Science["ES"];
            }
            if (entity.Entity.ScienceTitle != null)
            {
                entity.ITScienceTitle = entity.Entity.ScienceTitle["IT"];
                entity.ENScienceTitle = entity.Entity.ScienceTitle["EN"];
                entity.DEScienceTitle = entity.Entity.ScienceTitle["DE"];
                entity.FRScienceTitle = entity.Entity.ScienceTitle["FR"];
                entity.ESScienceTitle = entity.Entity.ScienceTitle["ES"];
            }


            entity.ImageUrl = Task.Run(() => GetIconUrlByName(entity.Entity.Image.Replace("contents", ""))).Result;
            entity.ImageUrl = $"contents{entity.ImageUrl}";
            entity.ChallengeImageUrl = Task.Run(() => GetIconUrlByName(entity.Entity.ChallengeImage.Replace("contents", ""))).Result;
            entity.ChallengeImageUrl = $"contents{entity.ChallengeImageUrl}";

            if (entity.Entity.ScienceImage != null)
            {
                entity.ScienceImageUrl = Task.Run(() => GetIconUrlByName(entity.Entity.ScienceImage.Replace("contents", ""))).Result;
                entity.ScienceImageUrl = $"contents{entity.ScienceImageUrl}";
            }

            return base.AfterRead(entity);
        }

    }
}