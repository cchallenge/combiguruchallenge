﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduAppWS.CMS;
using ServerCore.Interfaces;
using SharedCore.Interfaces;
using ServerCore.NancyModule;
using System.Threading.Tasks;
using SharedCore.Entities;
using System.IO;
using Unox;
using Unox.Models.ImporterModels;
using Unox.Models.RawModels;
using NLog;
using ServerCore.Interfaces.Logging;
using EduAppWS;


namespace ws.Modules
{
    public class Import : Entity
    {
        public DateTime DataImport { get; set; }
        public string UserName { get; set; }
        public string Status { get; set; }

        public string Message { get; set; }
        public string ExcelFile { get; set; }
        public string Level { get; set; }

    }

    public class CMSImport : CMSEntity<Import>
    {

        public bool AutoImport { get; set; }
    }

    [CRUDModule(EntityName = "import", PrimaryKeyName = "CorrelationId")]
    public class ImportCRUDModule : CRUDModule<CMSImport, Import>
    {
        ILog _logger = LogProvider.GetLogger("NancyUnoxEduModule");
        IAuthBEAppModule _auth;
        IBEAppCoreContext _beAppContext;
        IContentBEAppModule _storage;

        public ImportCRUDModule(IBEEntityModule ee, IAuthBEAppModule auth, IBEAppCoreContext beAppContext, IContentBEAppModule beContents) : base(ee)
        {
            _auth = auth;
            _beAppContext = beAppContext;
            _storage = beContents;

            this.Get["refreshtree/all", true] = async (x, ct) => await RefreshTree();

        }

        private async Task<string> RefreshTree()
        {
            await RebuildTree();
            return "Cache Log: Learning Path successfully rebuilt ";
        }

        /// <summary>
        /// Prima di salvare lancio l'importazione, verifico come è andata e salvo il risultato
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected override CMSImport DoCreate(CMSImport entity)
        {
            _logger.Info("ImportCRUDModule::DoCreate " + entity.ToString());
            entity.Entity.DataImport = DateTime.Now;
            entity.Entity.ExcelFile = entity.Entity.ExcelFile;
            entity.Entity.UserName = Context.CurrentUser.UserName;
            var cr = Task.Run(() => TryImport(entity, entity.AutoImport)).Result;
            if (cr.Success)
            {
                bool btree = Task.Run(() => RebuildTree()).Result;

                if (entity.AutoImport)
                    entity.Entity.Status = $"Imported";
                else
                    entity.Entity.Status = "Verified";

            }
            else
            {
                bool btree = Task.Run(() => RebuildTree()).Result;
                entity.Entity.Status = "Failed";
                string message = "";
                foreach (var m in cr.Messages)
                    message += $"\r\n{m}";
                entity.Entity.Message = message;
                base.DoCreate(entity);
                throw new Exception($"IMPORTAZIONE FALLITA - {message}");
            }

            return base.DoCreate(entity);
        }

        /// <summary>
        /// Esegue il tentativo di importazione del file
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<CheckResult> TryImport(CMSImport entity, bool import)
        {
            Guid gname = Guid.NewGuid();
            //string rootPath = _beAppContext.RootPath == null ? "/Users/utente/Projects/UNOX/EduAppBackend" : _beAppContext.RootPath;
            //var dir = new DirectoryInfo (Path.Combine (rootPath, "tmp"));
            var dir = new DirectoryInfo(Path.Combine(_beAppContext.RootPath, "tmp"));
            if (!dir.Exists)
                dir.Create();
            string filename = Path.Combine(dir.FullName, $"{gname.ToString("N")}.xlsx");
            try
            {
                string tempfile = entity.Entity.ExcelFile;
                DateTime? lastmodified = new DateTime();
                using (var stream = _storage.GetContent(_beAppContext, tempfile, out lastmodified))
                {


                    if (File.Exists(filename))
                        File.Delete(filename);

                    using (var fileStream = File.Create(filename))
                    {
                        stream.Seek(0, SeekOrigin.Begin);
                        stream.CopyTo(fileStream);
                    }

                    if (File.Exists(filename))
                        Console.Write("SUCCESS");

                    var engine = EE.Engine;

                    ExcelParser xl = new ExcelParser(engine);
                    CheckResult cr = new CheckResult();
                    cr.Success = true;
                    var importedlevel = xl.Import(filename, entity.Entity.Level, ref cr);
                    if (cr.Success)
                    {
                        if (import)
                        {
                            cr = await NormalizeLevel(importedlevel, cr);
                        }
                        else
                        {
                            cr.Messages.Add($"VERIFICA RIUSCITA CON SUCCESSO");
                        }
                    }

                    return cr;
                }
            }
            catch (Exception ex)
            {
                CheckResult cr = new CheckResult();
                _logger.Fatal($"IMPORTAZIONE FALLITA {ex.Message}");
                cr.Success = false;
                cr.Messages.Add($"IMPORTAZIONE FALLITA {ex.Message}");
                return cr;
            }
            finally
            {

                if (File.Exists(filename))
                    File.Delete(filename);
            }


        }

        /// <summary>
        /// Controlli di integrità e correttezza del livello
        /// </summary>
        /// <param name="livello"></param>
        /// <returns></returns>
        public async Task<bool> CheckLevel(ImpLevel livello)
        {

            return true;
        }

        public async Task<ImpLevel> GetLevelFromDB(string correlationId)
        {

            return null;

        }

        /// <summary>
        /// Normalizza il livello in una classe 
        /// </summary>
        /// <param name="livello"></param>
        /// <returns></returns>
        public async Task<CheckResult> NormalizeLevel(ImpLevel livello, CheckResult cr)
        {


            //return await Task.Run(async () =>
            //{
            //_logger.Info("########### THIS IS A USELESS TEST ########");
            var level = livello.Livello;
            var engine = EE.Engine;


            var levels = await engine.GetAll<Level>();
            var foundlevel = (from c in levels
                              where c.Name == level.Name
                              select c).FirstOrDefault();

            if (foundlevel != null)
            {
                // do nothing
                level = foundlevel;
            }
            else
            {
                level.CorrelationId = engine.NewCorrelationId("Level");
                await engine.Update<Level>(level);
            }

            // trovo le sezioni collgate al livello
            var existingsections = await engine.GetAll<Section>("childOf", level);
            foreach (var sezione in livello.Sezioni)
            {
                var cursezione = sezione.Sezione;

                var foundsection = (from c in existingsections
                                    where c.Name == cursezione.Name
                                    select c).FirstOrDefault();
                if (foundsection != null)
                {
                    // sezione collegata al livello trovata. update
                    cursezione.CorrelationId = foundsection.CorrelationId;
                    await engine.Update<Section>(cursezione);

                }
                else
                {
                    cursezione.CorrelationId = engine.NewCorrelationId("Section");
                    await engine.Update<Section>(cursezione);
                    await engine.Set(cursezione, "childOf", level);
                }

                // lessons
                var existinglessons = await engine.GetAll<Lesson>("childOf", cursezione);
                foreach (var lezione in sezione.Lezioni)
                {
                    var curlezione = lezione.Lezione;

                    var foundlesson = (from c in existinglessons
                                       where c.Name == curlezione.Name
                                       select c).FirstOrDefault();
                    if (foundlesson != null)
                    {
                        // sezione collegata al livello trovata. update
                        curlezione.CorrelationId = foundlesson.CorrelationId;
                        await engine.Update<Lesson>(curlezione);
                    }
                    else
                    {
                        curlezione.CorrelationId = engine.NewCorrelationId("Lesson");
                        await engine.Update<Lesson>(curlezione);
                        await engine.Set(curlezione, "childOf", cursezione);
                    }

                    // questions
                    var existingquestions = (await engine.GetAll<Question>("childOf", curlezione));//.Select(x => x.Name).ToList();
                    foreach (var domanda in lezione.Domande)
                    {
                        var curdomanda = domanda.Domanda;

                        var foundquestion = (from c in existingquestions
                                             where c.Name == curdomanda.Name
                                             select c).FirstOrDefault();
                        if (foundquestion != null)
                        {
                            // sezione collegata al livello trovata. update
                            curdomanda.CorrelationId = foundquestion.CorrelationId;
                            await engine.Update<Question>(curdomanda);
                        }
                        else
                        {
                            curdomanda.CorrelationId = engine.NewCorrelationId("Question");
                            await engine.Update<Question>(curdomanda);
                            await engine.Set(curdomanda, "childOf", curlezione);
                        }


                    }

                    // DELETE delle domande che non esistono piu'
                    if (existingquestions != null)
                    {
                        var importedquestions = lezione.Domande.Select(x => x.Domanda.Name).ToList();
                        foreach (var eq in existingquestions)
                        {
                            if (!importedquestions.Contains(eq.Name))
                            {
                                await engine.Delete<Question>(eq.CorrelationId);

                            }
                        }
                    }

                }

                // DELETE delle lezioni che non esistono piu'
                if (existinglessons != null)
                {
                    var importedlessons = sezione.Lezioni.Select(x => x.Lezione.Name).ToList();
                    foreach (var el in existinglessons)
                    {
                        if (!importedlessons.Contains(el.Name))
                        {
                            await engine.Delete<Lesson>(el.CorrelationId);

                        }
                    }
                }


            }

            // DELETE delle lezioni che non esistono piu'
            if (existingsections != null)
            {
                var importedsections = livello.Sezioni.Select(x => x.Sezione.Name).ToList();
                foreach (var es in existingsections)
                {
                    if (!importedsections.Contains(es.Name))
                    {
                        await engine.Delete<Section>(es.CorrelationId);

                    }
                }
            }

            // save json
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(livello);
            _logger.Info("JSON DI IMPORTAZIONE");
            _logger.Info(json);
            return cr;
            //});
        }

        /// <summary>
        /// Costruisce una struttura logica da interrogare velocemente per recuperare i figli delle
        /// Section
        /// e 
        /// Level
        /// </summary>
        /// <returns></returns>
        public async Task<bool> RebuildTree()
        {
            bool ret = true;
            try
            {
                // raccolgo i livelli
                var engine = EE.Engine;
                string backuptree = "";
                // estraggo Tree
                var tree = (await engine.GetAll<Tree>()).FirstOrDefault();
                if (tree == null)
                {
                    tree = engine.New<Tree>();
                    tree.TreeNodes = new List<TreeNode>();
                    tree.Created = DateTime.Now;
                }
                else
                {
                    backuptree = Newtonsoft.Json.JsonConvert.SerializeObject(tree);
                    tree.TreeNodes = new List<TreeNode>();
                    tree.Created = DateTime.Now;
                }


                // Levels
                var levels = (await engine.GetAll<Level>()).OrderBy(x => int.Parse(x.Name)).ToList();

                foreach (var level in levels)
                {
                    var tn = new TreeNode()
                    {
                        CorrelationId = level.CorrelationId,
                        Type = "Level",
                        Parent = null,
                        Children = new List<string>()

                    };



                    var sections = await engine.GetAll<Section>(EntityRelation.ChildOf, level);
                    if (sections != null)
                        sections = sections.OrderBy(x => x.Order).ToList();
                    foreach (var section in sections)
                    {
                        tn.Children.Add(section.CorrelationId);
                        var tn_section = new TreeNode()
                        {
                            CorrelationId = section.CorrelationId,
                            Type = "Section",
                            Parent = level.CorrelationId,
                            Children = new List<string>()
                        };

                        var lessons = (await engine.GetAll<Lesson>(EntityRelation.ChildOf, section)).OrderBy(x => x.Order).ToList();
                        foreach (var lesson in lessons)
                        {
                            tn_section.Children.Add(lesson.CorrelationId);
                            var tn_lesson = new TreeNode()
                            {
                                CorrelationId = lesson.CorrelationId,
                                Type = "Lesson",
                                Parent = section.CorrelationId,
                                Children = new List<string>()

                            };
                            tree.TreeNodes.Add(tn_lesson);

                        }
                        tree.TreeNodes.Add(tn_section);
                    }
                    tree.TreeNodes.Add(tn);


                } // end levels
                tree.Created = DateTime.Now;

                if (tree.TreeNodes.Count > 0)
                {
                    // tree è ricostruito. update
                    var res = await engine.Update<Tree>(tree);
                    if (res.Success)
                    {
                        //clear learning path
                        await engine.DeleteAll<UnoxEduAppModule.LearningPath>();
                        // resolve ext module
                        var nuem = Bootstrapper.AppContainer.Resolve<NancyUnoxEduModule>();
                        // language and measure units lists
                        List<string> unitList = new List<string>() { "1", "2" };
                        List<string> languageList = new List<string>() { "it", "en", "de", "fr", "es" };
                        // per ogni chiave creo il livello
                        foreach (var measureUnit in unitList)
                        {
                            foreach (var lang in languageList)
                            {
                                await nuem.GenerateAndSaveLearningPath(lang, measureUnit);
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                ret = false;
                _logger.Error($"Error during TREE BUILDING {ex.Message}");

            }
            return ret;
        }

    }
}