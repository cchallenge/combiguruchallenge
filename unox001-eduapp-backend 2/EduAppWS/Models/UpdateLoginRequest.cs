﻿using System;
namespace EduAppWS
{
	public class UpdateLoginRequest
	{
		public string UserId { get; set; }

		public string Email { get; set; }

		public string Lang { get; set; }
	}
}
