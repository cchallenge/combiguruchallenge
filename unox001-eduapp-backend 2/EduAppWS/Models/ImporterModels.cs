﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unox.Models.RawModels;

namespace Unox.Models.ImporterModels
{
    public class _ext_ImpLevel
    {
        public _ext_ImpLevel(string name = "")
        {
            if (string.IsNullOrEmpty(name))
                name = "Level0";
            Livello = new _ext_Level()
            { Name = name };
            Sezioni = new List<_ext_ImpSection>();
        }
        public _ext_Level Livello { get; set; }
        public string Filename { get; set; }
        public DateTime Data { get; set; }
        public List<_ext_ImpSection> Sezioni { get; set; }
    }

    public class ImpLevel
    {
        public ImpLevel(string name = "")
        {
            if (string.IsNullOrEmpty(name))
                name = "Level0";
            Livello = new Level()
            { Name = name };
            Sezioni = new List<ImpSection>();
        }
        public Level Livello { get; set; }
        public string Filename { get; set; }
        public DateTime Data { get; set; }
        public List<ImpSection> Sezioni { get; set; }
    }

    public class _ext_ImpSection
    {
        public _ext_ImpSection(string name = "")
        {
            if (string.IsNullOrEmpty(name))
                name = "0_0";
            Sezione = new _ext_Section()
            {
                Name = name,
                Description = new
            Dictionary<string, string>()
            };
            Lezioni = new List<_ext_ImpLesson>();
        }
        public _ext_Section Sezione { get; set; }
        public List<_ext_ImpLesson> Lezioni { get; set; }

    }

    public class ImpSection
    {
        public ImpSection(string name = "")
        {
            if (string.IsNullOrEmpty(name))
                name = "0_0";
            Sezione = new Section()
            {
                Name = name,
                Description = new
            Dictionary<string, string>()
            };
            Lezioni = new List<ImpLesson>();
        }
        public Section Sezione { get; set; }
        public List<ImpLesson> Lezioni { get; set; }

    }

    public class _ext_ImpLesson
    {
        public _ext_ImpLesson(string name = "")
        {
            if (string.IsNullOrEmpty(name))
                name = "LESSON_0";
            Lezione = new _ext_Lesson()
            {
                Name = name,
                Description = new
            Dictionary<string, string>()
            };
            Domande = new List<_ext_ImpQuestion>();

        }
        public _ext_Lesson Lezione { get; set; }
        public List<_ext_ImpQuestion> Domande { get; set; }

    }

    public class ImpLesson
    {
        public ImpLesson(string name = "")
        {
            if (string.IsNullOrEmpty(name))
                name = "LESSON_0";
            Lezione = new Lesson()
            {
                Name = name,
                Description = new
            Dictionary<string, string>()
            };
            Domande = new List<ImpQuestion>();

        }
        public Lesson Lezione { get; set; }
        public List<ImpQuestion> Domande { get; set; }

    }

    public class _ext_ImpQuestion
    {
        public _ext_ImpQuestion(string name = "")
        {
            if (string.IsNullOrEmpty(name))
                name = "QUESTION_0";
            Domanda = new Question()
            { Name = name };
            Domanda.Body = new Dictionary<string, RawContent>();

        }
        public Question Domanda { get; set; }
    }

    public class ImpQuestion
    {
        public ImpQuestion(string name = "")
        {
            if (string.IsNullOrEmpty(name))
                name = "QUESTION_0";
            Domanda = new Question()
            { Name = name };
            Domanda.Body = new Dictionary<string, RawContent>();

        }
        public Question Domanda { get; set; }
    }
}