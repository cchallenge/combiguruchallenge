﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedCore.Entities;

namespace Unox.Models.RawModels
{
    public class User : Entity
    {
        public string UserId { get; set; }
    }

    public class _ext_User: User {

        public int Points { get; set; }
        public string Nickname { get; set; }
        public string Institution { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            _ext_User objAsQualifiedUser = obj as _ext_User;
            if (objAsQualifiedUser == null) return false;
            else return objAsQualifiedUser.UserId == this.UserId;
        }

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///                 Extensions to support Edxiting challenges !!                                     ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class _ext_Institution: Entity {
        /** A human identifier of the institution */
        public string Name { get; set; }
        public string Description { get; set; }
        /** Contact expressed as email, phone number, name etc.*/
        public string Contact { get; set; }
        /** 5 digit identifier of the institution: unique, guaranteed by application logic */
        public string Identifier { get; set; }

    }

    public class _ext_Challenge: Entity {
        public IDictionary<string, string> Description { get; set; }
        public IDictionary<string, string> Title { get; set; }
        public string Image { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public string UserName { get; set; }
        public string ExcelFile { get; set; }

    }

    public class _ext_Level : Entity
    {
        public string Name { get; set; }
        public IDictionary<string, string> Description { get; set; }
        public string Image { get; set; }
        public IDictionary<string, string> ChallengeTitle { get; set; }
        public string ChallengeImage { get; set; }

        public IDictionary<string, string> ScienceTitle { get; set; }
        public IDictionary<string, string> Science { get; set; }
        public string ScienceImage { get; set; }
    }

    public class Level: Entity
    {
        public string Name { get; set; }
		public IDictionary<string, string> Description { get; set; }
		public string Image { get; set; }
		public IDictionary<string, string> ChallengeTitle { get; set; }
        public string ChallengeImage { get; set; }

        public IDictionary<string, string> ScienceTitle { get; set; }
        public IDictionary<string, string> Science { get; set; }
        public string ScienceImage { get; set; }
    }

    public class _ext_Section : Entity
    {
        public string Name { get; set; }
        public IDictionary<string, string> Description { get; set; }
        public string Icon { get; set; }
        public int Order { get; set; }
    }

    public class Section : Entity
    {
        public string Name { get; set; }
        public IDictionary<string, string> Description { get; set; }
        public string Icon { get; set; }
        public int Order { get; set; }
    }

    public class _ext_Lesson : Entity
    {
        public string Name { get; set; }
        public IDictionary<string, string> Description { get; set; }
        public int Order { get; set; }
    }

    public class Lesson: Entity
    {
        public string Name { get; set; }
        public IDictionary<string,string> Description { get; set; }
        public int Order { get; set; }
    }

    public class _ext_Question : Question {}

    public class Question: Entity
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public IDictionary<string, RawContent> Body { get; set; }
        public int Order { get; set; }
    }

    public class UserAchievement : Entity
    {
        public string AchievementId { get; set; }
        
        public DateTime AchievementDate { get; set; }     

    }


    public class AppSettings : Entity
    {
        public string Name { get; set; }
        public string Value { get; set; }

    }

    public class AppIcon : Entity
    {
        public string Name { get; set; }
        public string FileKey { get; set; }
        public string ImageUrl { get; set; }
        public DateTime DateUpdate { get; set; }

    }

    public class LevelCompleted : Entity
    {
        public string Name { get; set; }
        public string FileKey { get; set; }
        public string ImageUrl { get; set; }
        public DateTime DateUpdate { get; set; }

    }

    public class Tree : Entity
    {
        public List<TreeNode> TreeNodes { get; set; }
        public DateTime Created { get; set; }


    }

    public class TreeNode
    {
        public string CorrelationId { get; set; }
        public string Type { get; set; }
        public List<string> Children { get; set; }
        public string Parent { get; set; }


    }

    public class AppColor : Entity
    {
        public string Name { get; set; }
        public string Code { get; set; }
      

    }

    public class Template : Entity
    {
        public string Name { get; set; }
        public IDictionary<string,string> Titles { get; set; }

    }

	public class LoginStatUser : Entity
	{
		public string Email { get; set; }

		public DateTime LastLogin { get; set; }

		public string Lang { get; set; }
	}

	public class NotificationMessage : Entity
	{
		public string Lang { get; set; }

		public string TextMessage { get; set; }
	}

    public class RawContent
    {
        public string Text { get; set; }
        public string Icon1 { get; set; }
        public string Icon2 { get; set; }

        public IEnumerable<RawContentEntry> Entries { get; set; }

    }

    public class RawContentEntry {
        public string C1 { get; set; }
        public string C2 { get; set; }
        public string C3 { get; set; }
        public string C4 { get; set; }
        public string C5 { get; set; }

    }

    public class CheckResult
    {
        public bool Success { get; set; }

        public List<string> Messages { get; set; }

        public CheckResult()
        {

            Messages = new List<string>();
        }
    }

}
