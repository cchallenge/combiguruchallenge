﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EduAppWS
{
    public class PushMessage
    {
        public string Message { get; set; }
		public int Id { get; set; }
		public int AppleBadge { get; set; }
        public List<string> Tags { get; set; }
    }

	public class ApplePushNotification
	{
		public string alert { get; set; }

		public int badge { get; set; }
	}

	public class ApplePushModel
	{
		public ApplePushModel()
		{
			aps = new ApplePushNotification()
			{
				alert = "",
				badge = 0
			};
		}

		public ApplePushModel(string message, int badge = 0)
		{
			aps = new ApplePushNotification()
			{
				alert = message,
				badge = badge
			};
		}

		public ApplePushNotification aps { get; set; }
	}

	public class GCMPushModel
	{
		public Dictionary<string, object> data { get; set; }
		string message { get; set; }
		int id { get; set; }
		string[] tags { get; set; }
		public GCMPushModel(PushMessage message)
		{
			data = new Dictionary<string, object> {
				{"message" , message.Message },
				{"id", message.Id },
				{"corrId", message.Tags.FirstOrDefault() }
			};

			this.message = message.Message;
			this.id = message.Id;
			this.tags = message.Tags.ToArray();
		}
	}
}

