using System;
using Nancy;
using System.IO;

namespace EduAppWS
{
	public static class NancyExtensions
	{
		public static Nancy.Response AsError(this IResponseFormatter formatter, HttpStatusCode statusCode, string message)
		{
			return new Nancy.Response
			{
				StatusCode = statusCode,
				ContentType = "text/plain",
				Contents = stream => (new StreamWriter(stream) { AutoFlush = true }).Write(message)
			};
		}
	}
}

