﻿
function init(appModel) {
var cmsApp = appModel;
var layout = @require('app/layouts/main_layout.html');
@include('app/init.js');
@include('app/forms/*');
@include('app/end.js');

}