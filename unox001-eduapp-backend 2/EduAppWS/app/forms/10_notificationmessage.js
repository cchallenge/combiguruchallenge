﻿//CONTENT
var notificationMessages = nga.entity('notificationMessage').identifier(nga.field('correlationId'));

var fields = [
    nga.field('lang'),
    nga.field('textMessage', 'text').label('Message')
];

notificationMessages.listView()
	.fields(fields)
    .title("Notification message list");

notificationMessages.creationView()
    .fields(fields)
    .title("Create message for language: {entry.values.lang}");

notificationMessages.editionView()
    .fields(fields)
    .title("Edit message for language: {entry.values.lang}");

notificationMessages.deletionView().title('Delete message for language: {entry.values.lang}');

admin.addEntity(notificationMessages);

if(isSuperAdmin) {
	menu.addChild( 
        nga.menu(notificationMessages)
		.title('Notification message template')
            .icon('<span class="glyphicon glyphicon-asterisk"></span>')
	);
}
//FINE - CONTENT