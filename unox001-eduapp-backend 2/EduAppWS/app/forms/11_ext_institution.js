﻿//INSTITUTION

var institution = nga.entity('institution').identifier(nga.field('correlationId'));

institution.listView()
    .title('Institutions list')
    .fields(
        nga.field('name').label('Name'),
        nga.field('description').label("Description"),
        nga.field('contact').label("Contact"),
        nga.field('identifier').label("Unique Identifier")
    );

institution.listView().listActions(['show', 'edit']);
institution.listView().actions(['batch', 'create']);

institution.showView()
    .title('Institution details')
    .fields([
        nga.field('name').label('Name'),
        nga.field('description').label("Description"),
        nga.field('contact').label("Contact"),
        nga.field('identifier').label("Unique Identifier")
    ]);


institution.creationView().fields([
    nga.field('name').label('Name'),
    nga.field('description').label("Description"),
    nga.field('contact').label("Contact")
]);



institution.editionView()
    .title('Edit institution')
    .fields([
        nga.field('name').label('Name'),
        nga.field('description').label("Description"),
        nga.field('contact').label("Contact")
    ]);

//setGenericFormErrorHandling(accounts.showView());
//setGenericFormErrorHandling(accounts.editionView());
//setGenericFormErrorHandling(accounts.creationView());


admin.addEntity(institution);

if (isSuperAdmin) {
    var isCGuruChallengeVoicePresent = (menu.getChildByTitle("CombiGuru Challenge") != null);
    if (!isCGuruChallengeVoicePresent)
        menu.addChild(nga.menu().title('CombiGuru Challenge')
            .addChild(
            nga.menu(institution)
                .title('Institutions')
                .icon('<span class="glyphicon glyphicon-user"></span>')
            )
        );
    else
        menu.getChildByTitle("CombiGuru Challenge")
            .addChild(
            nga.menu(institution)
                .title('Institutions')
                .icon('<span class="glyphicon glyphicon-user"></span>')
            );

}

// END - INSTITUTION