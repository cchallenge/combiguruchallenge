﻿//CONTENT
var imports = nga.entity('import').identifier(nga.field('correlationId'));

imports.listView()
	.fields([		
        nga.field('dataImport', 'datetime').format('dd/MM/yyyy HH:mm').label('Date of Import'),
        nga.field('status').label('Status'),
        nga.field('message').label('Message'),
        nga.field('level').label('Level'),
        nga.field('userName').label('User'),
         nga.field('excelFile').label('File Uploaded')
      
    ])
    .title("Importazioni");

imports.creationView()
    .fields([
        nga.field('level', 'choice')
            .choices([
                { value: '1', label: 'Level 1' },
                { value: '2', label: 'Level 2' },
                { value: '3', label: 'Level 3' },
                { value: '4', label: 'Level 4' },
                { value: '5', label: 'Level 5' },
                { value: '6', label: 'Level 6' },
                { value: '7', label: 'Level 7' },
                { value: '8', label: 'Level 8' },
                { value: '9', label: 'Level 9' },
                { value: '10', label: 'Level 10' },
                { value: '11', label: 'Level 11' },
                { value: '12', label: 'Level 12' }              
            ]),
        nga.field('excelFile', 'file').uploadInformation({ 'url': 'ng-contents/upload', 'apifilename': 'fileName' }).label('Seleziona il file Excel del livello'),
        nga.field('autoImport', 'choice').choices([{ value: true, label: 'Importazione' }, { value: false, label: 'Solo Verifica' }]).defaultValue(true).label('Importazione o Verifica'),
    ])
    .title("Importazioni");


admin.addEntity(imports);


setGenericFormErrorHandling(imports.creationView());


if(isSuperAdmin) {
	menu.addChild( 
        nga.menu(imports)
		.title('Importazioni')
            .icon('<span class="glyphicon glyphicon-upload"></span>')
	);
}
//FINE - CONTENT