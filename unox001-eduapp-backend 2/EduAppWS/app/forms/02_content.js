﻿//CONTENT
var contents = nga.entity('ng-contents').identifier(nga.field('ngId'));

contents.listView()
	.fields([
		nga.field('', 'template').label('Filename')
    		.template('<contents-link filekey="{{ entry.values.fileKey }}" filename="{{ entry.values.fileKey }}"></contents-link>'),
    	nga.field('lastModified', 'datetime'),
    	nga.field('size').label('Size (Byte)')
    ])
    .title("Contenuti");

contents.listView()
	.title('Contenuti')
	.exportFields([
	nga.field('fileKey')
]);

contents.creationView().fields([
	nga.field('fileKey'),
    nga.field('file','file').uploadInformation({'url': 'ng-contents/upload', 'apifilename': 'fileName'} )
]);


admin.addEntity(contents);

if(isSuperAdmin) {
	menu.addChild( 
		nga.menu(contents)
		.title('Contenuti')
		.icon('<span class="glyphicon glyphicon-picture"></span>')
	);
}
//FINE - CONTENT