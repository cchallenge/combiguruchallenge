﻿//CONTENT
var levels = nga.entity('level').identifier(nga.field('correlationId'));
var buttonrefresh = '<a class="btn btn-default" href="refreshtree/all"  target="_blank">Refresh Cache</a>';
levels.listView()
    .fields([
        nga.field('name').label('Name'),
        nga.field('iTDesc').label('Italian'),
        nga.field('eNDesc').label('English'),
        nga.field('imageUrl').template('<img src="{{ entry.values.imageUrl }}" height="42" width="42" />')   
         // Select the field to be displayed
    ]).listActions(['delete', 'edit' ])
    .title("Levels")
    .actions([buttonrefresh, 'create', 'export']);

var lkicons = nga.entity('appicon').identifier(nga.field('correlationId'));
lkicons.listView().fields([
    
    nga.field('name').label('Name').template('<img src="{{ entry.values.imageUrl }}" height="42" width="42" />')   

]);


levels.creationView()
    .fields([
        nga.field('name', 'choice')
            .choices([
                { value: '1', label: 'Level 1' },
                { value: '2', label: 'Level 2' },
                { value: '3', label: 'Level 3' },
                { value: '4', label: 'Level 4' },
                { value: '5', label: 'Level 5' },
                { value: '6', label: 'Level 6' },
                { value: '7', label: 'Level 7' },
                { value: '8', label: 'Level 8' },
                { value: '9', label: 'Level 9' },
                { value: '10', label: 'Level 10' },
                { value: '11', label: 'Level 11' },
                { value: '12', label: 'Level 12' }
            ]).label('Level Name').validation({ required: true }),
        nga.field('image', 'reference')
            .targetEntity(lkicons).targetField(nga.field('name').map((v, e) => e.name))
            .sortField('name')
            .sortDir('ASC').remoteComplete(true).label('Level Icon').validation({ required: true }),     
        nga.field('iTDesc').label('Italian Description').validation({ required: true }),
        nga.field('eNDesc').label('English Description').validation({ required: true }),
        nga.field('fRDesc').label('French Description').validation({ required: true }),
        nga.field('dEDescl').label('German Description').validation({ required: true }),
        nga.field('eSDesc').label('Spanish Description').validation({ required: true }),
        nga.field('challengeImage', 'reference')
            .targetEntity(lkicons).targetField(nga.field('name').map((v, e) => e.name))
            .sortField('name')
            .sortDir('ASC').remoteComplete(true).label('Challenge Icon').validation({ required: true }),              
        nga.field('iTChallenge').label('Italian Challenge Title').validation({ required: true }),
        nga.field('eNChallenge').label('English Challenge Title').validation({ required: true }),
        nga.field('fRChallenge').label('French Challenge Title').validation({ required: true }),
        nga.field('dEChallenge').label('German Challenge Title').validation({ required: true }),
        nga.field('eSChallenge').label('Spanish Challenge Title').validation({ required: true }),
        nga.field('scienceImage', 'reference')
            .targetEntity(lkicons).targetField(nga.field('name').map((v, e) => e.name))
            .sortField('name')
            .sortDir('ASC').remoteComplete(true).label('The Science Behind Icon').validation({ required: false }),
        nga.field('iTScienceTitle').label('Italian Title - The Science Behind').validation({ required: false }),
        nga.field('iTScience', 'wysiwyg').label('Italian The Science Behind').validation({ required: false }),
        nga.field('eNScienceTitle').label('English Title - The Science Behind').validation({ required: false }),
        nga.field('eNScience', 'wysiwyg').label('English The Science Behind').validation({ required: false }),
        nga.field('fRScienceTitle').label('French Title - The Science Behind').validation({ required: false }),
        nga.field('fRScience', 'wysiwyg').label('French The Science Behind').validation({ required: false }),
        nga.field('dEScienceTitle').label('German Title - The Science Behind').validation({ required: false }),
        nga.field('dEScience', 'wysiwyg').label('German The Science Behind').validation({ required: false }),
        nga.field('eSScienceTitle').label('Spanish Title - The Science Behind').validation({ required: false }), 
        nga.field('eSScience', 'wysiwyg').label('Spanish The Science Behind').validation({ required: false })             

    ])
    .title("Create a New Level")
    .onSubmitSuccess(['progression', 'notification', '$state', 'entry', 'entity', '$window', function (progression, notification, $state, entry, entity, $window) {
    // stop the progress bar
    progression.done();
    // add a notification

    notification.log('File caricato correttamente: ' + entry.values.status, { addnCls: 'humane-flatty-success' });
    // redirect to the list view
    //$state.go($state.get('list'), { entity: entity.name() });
    $window.history.back();
    // cancel the default action (redirect to the edition view)
    return false;
}]);

levels.editionView()
    .fields([
        nga.field('name', 'choice')
            .choices([
                { value: '1', label: 'Level 1' },
                { value: '2', label: 'Level 2' },
                { value: '3', label: 'Level 3' },
                { value: '4', label: 'Level 4' },
                { value: '5', label: 'Level 5' },
                { value: '6', label: 'Level 6' },
                { value: '7', label: 'Level 7' },
                { value: '8', label: 'Level 8' },
                { value: '9', label: 'Level 9' },
                { value: '10', label: 'Level 10' },
                { value: '11', label: 'Level 11' },
                { value: '12', label: 'Level 12' }
            ]).label('Level Name').editable(false),
        nga.field('image', 'reference')
    .targetEntity(lkicons).targetField(nga.field('name'))
    .sortField('name')
    .sortDir('ASC').remoteComplete(true).label('Level Icon').validation({ required: true }), 
        nga.field('iTDesc').label('Italian Description').validation({ required: true }),
        nga.field('eNDesc').label('English Description').validation({ required: true }),
        nga.field('fRDesc').label('French Description').validation({ required: true }),
        nga.field('dEDesc').label('German Description').validation({ required: true }),
        nga.field('eSDesc').label('Spanish Description').validation({ required: true }),
        nga.field('challengeImage', 'reference')
            .targetEntity(lkicons).targetField(nga.field('name'))
            .sortField('name')
            .sortDir('ASC').remoteComplete(true).label('Challenge Icon').validation({ required: true }),       
        nga.field('iTChallenge').label('Italian Challenge Title').validation({ required: true }),
        nga.field('eNChallenge').label('English Challenge Title').validation({ required: true }),
        nga.field('fRChallenge').label('French Challenge Title').validation({ required: true }),
        nga.field('dEChallenge').label('German Challenge Title').validation({ required: true }),
        nga.field('eSChallenge').label('Spanish Challenge Title').validation({ required: true }),
        nga.field('scienceImage', 'reference')
            .targetEntity(lkicons).targetField(nga.field('name').map((v, e) => e.name))
            .sortField('name')
            .sortDir('ASC').remoteComplete(true).label('The Science Behind Icon').validation({ required: false }),
        nga.field('iTScienceTitle').label('Italian Title - The Science Behind').validation({ required: false }),
        nga.field('iTScience', 'wysiwyg').label('Italian The Science Behind').validation({ required: false }),
        nga.field('eNScienceTitle').label('English Title - The Science Behind').validation({ required: false }),
        nga.field('eNScience', 'wysiwyg').label('English The Science Behind').validation({ required: false }),
        nga.field('fRScienceTitle').label('French Title - The Science Behind').validation({ required: false }),
        nga.field('fRScience', 'wysiwyg').label('French The Science Behind').validation({ required: false }),
        nga.field('dEScienceTitle').label('German Title - The Science Behind').validation({ required: false }),
        nga.field('dEScience', 'wysiwyg').label('German The Science Behind').validation({ required: false }),
        nga.field('eSScienceTitle').label('Spanish Title - The Science Behind').validation({ required: false }),
        nga.field('eSScience', 'wysiwyg').label('Spanish The Science Behind').validation({ required: false }) 
    ])
    .title("Edit the Level");


admin.addEntity(levels);

setGenericFormErrorHandling(levels.creationView());
setGenericFormErrorHandling(levels.editionView());

if (isSuperAdmin) {
    menu.addChild(
        nga.menu(levels)
            .title('Level Manager')
            .icon('<span class="glyphicon glyphicon-star"></span>')
    );
}
//FINE - CONTENT