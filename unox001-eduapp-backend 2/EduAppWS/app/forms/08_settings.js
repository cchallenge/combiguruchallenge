﻿//CONTENT
var appsettings = nga.entity('appsettings').identifier(nga.field('correlationId'));

appsettings.listView()
    .fields([
        nga.field('name').label('Name'),
        nga.field('value').label('Value')      
    ]).listActions(['edit'])
    .title("Settings");


appsettings.creationView()
    .fields([
        nga.field('name').label('Name'),
        nga.field('value').label('Value')
    ]).title("Settings");

appsettings.editionView()
    .fields([
        nga.field('name').label('Name').editable(false),
        nga.field('value').label('Value')     
    ]).title("Settings");


admin.addEntity(appsettings);

setGenericFormErrorHandling(appsettings.creationView());
setGenericFormErrorHandling(appsettings.editionView());

if (isSuperAdmin) {
    menu.addChild(
        nga.menu(appsettings)
            .title('App Settings')
            .icon('<span class="glyphicon glyphicon-wrench"></span>')
    );
}
//FINE - CONTENT