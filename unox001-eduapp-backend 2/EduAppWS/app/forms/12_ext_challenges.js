﻿//CHALLENGE
var challenge = nga.entity('challenges').identifier(nga.field('correlationId'));

challenge.listView()
	.fields([
        nga.field('dateStart', 'datetime').format('dd/MM/yyyy HH:mm').label('Start Date'),
        nga.field('dateEnd', 'datetime').format('dd/MM/yyyy HH:mm').label('End Date'),
        nga.field('iTTitle').label('Italian title'),
        nga.field('eNTitle').label('English title'),
        nga.field('userName').label('User'),
        nga.field('imageUrl').template('<img src="{{ entry.values.imageUrl }}" height="42" width="42" />'), 
        nga.field('excelFile').label('Challenge File')      
    ]).listActions(['delete', 'edit'])
    .title("Challenge List")
    .actions(['create', 'export']);

var lkicons = nga.entity('appicon').identifier(nga.field('correlationId'));
lkicons.listView().fields([

    nga.field('name').label('Name').template('<img src="{{ entry.values.imageUrl }}" height="42" width="42" />')

]);


challenge.creationView()
    .fields([
        nga.field('iTTitle').label('Italian title').validation({ required: true }),
        nga.field('eNTitle').label('English title').validation({ required: true }),
        nga.field('dETitle').label('German title').validation({ required: true }),
        nga.field('fRTitle').label('French title').validation({ required: true }),
        nga.field('sPTitle').label('Spanish title').validation({ required: true }),
        nga.field('image', 'reference')
            .targetEntity(lkicons).targetField(nga.field('name').map((v, e) => e.name))
            .sortField('name')
            .sortDir('ASC').remoteComplete(true).label('Challenge Icon').validation({ required: true }),
        nga.field('iTDesc', 'wysiwyg').label('Italian description').validation({ required: true }),
        nga.field('eNDesc', 'wysiwyg').label('English description').validation({ required: true }),
        nga.field('dEDesc', 'wysiwyg').label('German description').validation({ required: true }),
        nga.field('fRDesc', 'wysiwyg').label('French description').validation({ required: true }),
        nga.field('sPDesc', 'wysiwyg').label('Spanish description').validation({ required: true }),
        nga.field('dateStart', 'datetime').format('dd/MM/yyyy HH:mm').label('Start Date'),
        nga.field('dateEnd', 'datetime').format('dd/MM/yyyy HH:mm').label('End Date'),
        nga.field('excelFile', 'file').uploadInformation({ 'url': 'ng-contents/upload', 'apifilename': 'fileName' }).label('Seleziona il file Excel della sfida').validation({ required: true })
    ])
    .title("Create Challenge");

challenge.editionView()
    .fields([
        nga.field('iTTitle').label('Italian title').validation({ required: true }),
        nga.field('eNTitle').label('English title').validation({ required: true }),
        nga.field('dETitle').label('German title').validation({ required: true }),
        nga.field('fRTitle').label('French title').validation({ required: true }),
        nga.field('sPTitle').label('Spanish title').validation({ required: true }),
        nga.field('image', 'reference')
            .targetEntity(lkicons).targetField(nga.field('name').map((v, e) => e.name))
            .sortField('name')
            .sortDir('ASC').remoteComplete(true).label('Challenge Icon').validation({ required: true }),
        nga.field('iTDesc', 'wysiwyg').label('Italian description').validation({ required: true }),
        nga.field('eNDesc', 'wysiwyg').label('English description').validation({ required: true }),
        nga.field('dEDesc', 'wysiwyg').label('German description').validation({ required: true }),
        nga.field('fRDesc', 'wysiwyg').label('French description').validation({ required: true }),
        nga.field('sPDesc', 'wysiwyg').label('Spanish description').validation({ required: true }),
        nga.field('dateStart', 'datetime').format('dd/MM/yyyy HH:mm').label('Start Date').validation({ required: true }),
        nga.field('dateEnd', 'datetime').format('dd/MM/yyyy HH:mm').label('End Date').validation({ required: true }),
        nga.field('excelFile', 'file').uploadInformation({ 'url': 'ng-contents/upload', 'apifilename': 'fileName' }).label('Seleziona il file Excel della sfida').validation({ required: true })
    ])
    .title("Edit Challenge");

admin.addEntity(challenge);

setGenericFormErrorHandling(challenge.creationView());


if (isSuperAdmin) {
    var isCGuruChallengeVoicePresent = (menu.getChildByTitle("CombiGuru Challenge") != null);
    if (!isCGuruChallengeVoicePresent)
        menu.addChild(nga.menu().title('CombiGuru Challenge')
            .addChild(
            nga.menu(challenge)
                .title('Challenges')
                .icon('<span class="glyphicon glyphicon-upload"></span>')
            )
        );
    else
        menu.getChildByTitle("CombiGuru Challenge")
            .addChild(
                nga.menu(challenge)
                .title('Challenges')
                .icon('<span class="glyphicon glyphicon-upload"></span>')
            );


}
//FINE - CHALLENGE