﻿//CONTENT
var levelcompleted = nga.entity('levelcompleted').identifier(nga.field('correlationId'));

levelcompleted.listView()
    .fields([
        nga.field('dateUpdate', 'datetime').format('dd/MM/yyyy HH:mm').label('Last modified'),
        nga.field('name').label('Name'),
        nga.field('', 'template').label('')
            .template('<img src="{{ entry.values.imageUrl }}" style="background-color:#FF7F27;"  width="50" />')

    ])
    .title("Level Completed Image Archive");

levelcompleted.creationView()
    .fields([
        nga.field('name').label('Name'),
        nga.field('fileKey', 'file').uploadInformation({ 'url': 'ng-contents/upload', 'apifilename': 'fileName' }).label('Carica immagine')
    ])
    .title("Add Image");


admin.addEntity(levelcompleted);

if (isSuperAdmin) {
    menu.addChild(
        nga.menu(levelcompleted)
            .title('Level Completed Images')
            .icon('<span class="glyphicon glyphicon-picture"></span>')
    );
}
//FINE - CONTENT