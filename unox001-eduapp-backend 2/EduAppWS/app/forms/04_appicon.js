﻿//CONTENT
var appicon = nga.entity('appicon').identifier(nga.field('correlationId'));
var d = new Date();
var n = d.getTime();

appicon.listView()
	.fields([		
        nga.field('dateUpdate', 'datetime').format('dd/MM/yyyy HH:mm').label('Last modified'),
        nga.field('name').label('Name'),
        nga.field('', 'template').label('')
            .template('<img src="{{ entry.values.imageUrl }}?' + n + '" style="background-color:#FF7F27;" height="42" width="42" />')     
      
    ]).listActions(['delete', 'edit'])
    .title("Icon Archive");

appicon.creationView()
    .fields([
        nga.field('name').label('Name'),
        nga.field('fileKey', 'file').uploadInformation({ 'url': 'ng-contents/upload', 'apifilename': 'fileName' }).label('Carica il file icona')
    ])
    .title("Add Icon Image");

appicon.editionView()
    .fields([
        nga.field('name').label('Name').editable(false),
        nga.field('fileKey', 'file').uploadInformation({ 'url': 'ng-contents/upload', 'apifilename': 'fileName' }).label('Sostituisci il file icona')
    ])
    .title("Replace Icon Image");
admin.addEntity(appicon);

if(isSuperAdmin) {
	menu.addChild( 
        nga.menu(appicon)
		.title('Icon Archive')
            .icon('<span class="glyphicon glyphicon-picture"></span>')
	);
}
//FINE - CONTENT