﻿//CONTENT
var templates = nga.entity('template').identifier(nga.field('correlationId'));

templates.listView()
    .fields([
        nga.field('name').label('Name'),
        nga.field('iTLabel').label('Italian'),
        nga.field('eNLabel').label('English')
    ]).listActions(['delete', 'edit'])
    .title("Template titles");

templates.creationView()
    .fields([
        nga.field('name', 'choice')
            .choices([
                { value: 'S2', label: 'S2' },
                { value: 'S3', label: 'S3' },
                { value: 'S4', label: 'S4' },
                { value: 'S5_S9', label: 'S5_S9' },
                { value: 'S6_S7', label: 'S6_S7' },                
                { value: 'S8', label: 'S8' },
                { value: 'S10_S11_S12', label: 'S10_S11_S12' },
                { value: 'S13', label: 'S13' },
                { value: 'S14', label: 'S14' }               
            ]).label('Template Code').validation({ required: true }),
        nga.field('iTLabel').label('Italian').validation({ required: true }),
        nga.field('eNLabel').label('English').validation({ required: true }),
        nga.field('fRLabel').label('French').validation({ required: true }),
        nga.field('dELabel').label('German').validation({ required: true }),
        nga.field('eSLabel').label('Spanish').validation({ required: true })
    ])
    .title("Edit the title of the template in different languages");

templates.editionView()
    .fields([
        nga.field('name', 'choice')
            .choices([
                { value: 'S2', label: 'S2' },
                { value: 'S3', label: 'S3' },
                { value: 'S4', label: 'S4' },
                { value: 'S5_S9', label: 'S5_S9' },
                { value: 'S6_S7', label: 'S6_S7' },
                { value: 'S8', label: 'S8' },
                { value: 'S10_S11_S12', label: 'S10_S11_S12' },
                { value: 'S13', label: 'S13' },
                { value: 'S14', label: 'S14' }
            ]).label('Template Code').editable(false),
        nga.field('iTLabel').label('Italian').validation({ required: true }),
        nga.field('eNLabel').label('English').validation({ required: true }),
        nga.field('fRLabel').label('French').validation({ required: true }),
        nga.field('dELabel').label('German').validation({ required: true }),
        nga.field('eSLabel').label('Spanish').validation({ required: true })
    ])
    .title("Edit the title of the template in different languages");


admin.addEntity(templates);


setGenericFormErrorHandling(templates.creationView());
setGenericFormErrorHandling(templates.editionView());

if (isSuperAdmin) {
    menu.addChild(
        nga.menu(templates)
            .title('Template Titles')
            .icon('<span class="glyphicon glyphicon-list-alt"></span>')
    );
}
//FINE - CONTENT