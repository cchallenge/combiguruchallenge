﻿//CONTENT
var ranking = nga.entity('ranking').identifier(nga.field('correlationId'));

var institution = nga.entity('institution').identifier(nga.field('identifier'));
    institution.listView().fields([
        nga.field('name').label('Name')
]);

ranking.listView()
    .title("Ranking")
    .fields([
        nga.field('nickname').label("Nickname"),
        nga.field('email'),
        nga.field('score'),
        nga.field('institutionName'),
        nga.field('institutionIdentifier')
        // Select the field to be displayed
    ])
    .filters([
        nga.field('institutionIdentifier')
]);
    

admin.addEntity(ranking);

if (isSuperAdmin) {
    var isCGuruChallengeVoicePresent = (menu.getChildByTitle("CombiGuru Challenge") != null);
    if (!isCGuruChallengeVoicePresent)
        menu.addChild(nga.menu().title('CombiGuru Challenge')
            .addChild(
                nga.menu(ranking)
                .title('Ranking')
                .icon('<span class="glyphicon glyphicon-star"></span>'))
        );
    else
        menu.getChildByTitle("CombiGuru Challenge")
            .addChild(
            nga.menu(ranking)
                .title('Ranking')
                .icon('<span class="glyphicon glyphicon-star"></span>')
        );

}