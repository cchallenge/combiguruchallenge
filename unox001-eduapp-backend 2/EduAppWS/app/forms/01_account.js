﻿//ACCOUNT

var accounts = nga.entity('account').identifier(nga.field('correlationId'));

accounts.listView()
    .title('Lista Accounts')
    .fields(
    	nga.field('userName'),
        nga.field('claims'),
        nga.field('enabled', 'choice')
	    .choices([
	    	{ value: true, label: 'Si' },
		    { value: false, label: 'No' }
	    ])
    );

accounts.listView().listActions(['show', 'edit']);
accounts.listView().actions(['batch', 'create']);


accounts.showView()
    .title('{{entry.values.userName}}')
    .fields([
        nga.field('userName'),
        nga.field('password', 'password'),
        nga.field('userToken'),
        nga.field('claims'),
        nga.field('enabled', 'choice')
	    .choices([
	    	{ value: true, label: 'Si' },
		    { value: false, label: 'No' }
	    ])
    ]);


accounts.creationView().fields([
    nga.field('userName'),
    nga.field('claims'),
    nga.field('enabled', 'choice')
	    .choices([
	    	{ value: true, label: 'Si' },
		    { value: false, label: 'No' }
	    ])
]);



accounts.editionView()
    .title('Edit {{entry.values.userName}}')
    .fields([
        nga.field('userName'),
        nga.field('password', 'password'),
        nga.field('claims'),
    	nga.field('enabled', 'choice')
	    .choices([
	    	{ value: true, label: 'Si' },
		    { value: false, label: 'No' }
	    ])
    ]);

//setGenericFormErrorHandling(accounts.showView());
//setGenericFormErrorHandling(accounts.editionView());
//setGenericFormErrorHandling(accounts.creationView());


admin.addEntity(accounts);

if(isSuperAdmin) {
	menu.addChild(
			nga.menu(accounts)
			.icon('<span class="glyphicon glyphicon-user"></span>')
		);
}

// END - ACCOUNT