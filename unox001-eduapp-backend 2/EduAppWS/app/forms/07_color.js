﻿//CONTENT
var appcolors = nga.entity('appcolor').identifier(nga.field('correlationId'));

appcolors.listView()
    .fields([
        nga.field('name').label('Name'),
        nga.field('code').label('HEX Code'),
        nga.field('code').label('Sample').template('<div style="color:#FFFFFF; text-align: center; font-weight: bold; font-size: 10pt; background-color:{{ entry.values.code }};" height="42" width="42" >TEXT</div>')   
         // Select the field to be displayed
    ]).listActions(['delete', 'edit'])
    .title("Colors");


appcolors.creationView()
    .fields([
        nga.field('name').label('Name'),
        nga.field('code').label('HEX Code').template('<input colorpicker="hex" type="text" ng-model="entry.values.code" />')        

    ])
    .title("Create a New Color");

appcolors.editionView()
    .fields([
        nga.field('name').label('Name'),
        nga.field('code').label('HEX Code').template('<input colorpicker="hex" type="text" ng-model="entry.values.code" />')   
    ])
    .title("Edit the Level");


admin.addEntity(appcolors);

setGenericFormErrorHandling(appcolors.creationView());
setGenericFormErrorHandling(appcolors.editionView());

if (isSuperAdmin) {
    menu.addChild(
        nga.menu(appcolors)
            .title('Color Manager')
            .icon('<span class="glyphicon glyphicon-eye-open"></span>')
    );
}
//FINE - CONTENT