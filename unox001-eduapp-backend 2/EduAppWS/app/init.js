﻿(function () {
    "use strict";

   

    @include('app/modules/*');

    // declare a new module called 'myApp', and make it require the `ng-admin` module as a dependency
    var myApp = angular.module('cmsApp', ['ng-admin', 'ui.bootstrap', 'colorpicker.module']);

    //var cmsApp = @Model.AsJson;

    // custom controllers
    myApp.controller('App', ['$scope', '$window', function ($scope, $window) {
        // used in header.html
        $scope.model = cmsApp;
    }]);

    @include('app/directives/*');


    var setGenericFormErrorHandling = function (aForm) {
        aForm.onSubmitError(['error', 'form', 'progression', 'notification', function (error, form, progression, notification) {
            // mark fields based on errors from the response
            error.data.forEach(function (violation) {
                if (form[violation.propertyPath]) {
                    form[violation.propertyPath].$valid = false;
                }
            });
            // stop the progress bar
            progression.done();
            // add a notification
            if (error.data.length != 1) {
                notification.log(`Some values are invalid, see details in the form`, { addnCls: 'humane-flatty-error' });
            }
            else {
                notification.log(error.data[0].message.replace('Exception -', ''), { addnCls: 'humane-flatty-error' });
            }
            // cancel the default action (default error messages)
            return false;
        }]);

        return aForm;
    }

    var isSuperAdmin = (cmsApp.userLevel == 'superadmin');

    var lang = cmsApp.lang;

    //FIELD DEFINITIONS: https://github.com/marmelab/ng-admin/blob/master/doc/reference/Field.md
    //GLIPHYCON REF: http://www.w3schools.com/bootstrap/bootstrap_ref_comp_glyphs.asp

    myApp.config(['RestangularProvider', function (RestangularProvider) {

        RestangularProvider.setDefaultHeaders({ 'Accept': 'application/json' });

        RestangularProvider.addFullRequestInterceptor(function(element, operation, what, url, headers, params, httpConfig) {

        	if(lang != '') {

	    		params.lang = lang;
	    	}
	        return { params: params };
	    });
    }]);


    // declare a function to run when the module bootstraps (during the 'config' phase)
    myApp.config(['NgAdminConfigurationProvider', function (nga) {

        var admin = nga.application(cmsApp.name)
            .baseApiUrl(cmsApp.restPath + '/') // main API endpoint
            .layout(layout)
        ;
        var menu = nga.menu();

      

        if(isSuperAdmin) {

	        menu.addChild(nga.menu().template('<a href="hangfire"><span class="glyphicon glyphicon-list"></span> Jobs</a>'));
        }

        function entity(name) {

			return nga.entity(name).identifier(nga.field('correlationId'));
		}
