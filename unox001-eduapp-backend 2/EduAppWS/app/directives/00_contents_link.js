﻿function contentsLink() {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            filekey: "@",
            filename: "@"
        },
        template: `<a href="contents/{{ filekey }}"> {{ filename }}</a>`
    };
}

contentsLink.$inject = [];

myApp.directive('contentsLink', contentsLink);